SOURCE = gaysir_epistlene

PANDOC_OPTS = \
  -t docbook4 --top-level-division=part

DBLATEX_OPTS = \
  -p pdf.xsl

all: $(SOURCE).pdf $(SOURCE).epub

$(SOURCE).xml: $(SOURCE)-body.xml Makefile book.xml bookinfo.xml
	xmllint --nonet  --xinclude --postvalid book.xml > $@.new && \
		mv $@.new $@

$(SOURCE).pdf: $(SOURCE).xml Makefile pdf.xsl
	dblatex $(DBLATEX_OPTS) $(SOURCE).xml -o $@

$(SOURCE)-body.xml: $(SOURCE).rst Makefile
	pandoc -s -o $@ $(PANDOC_OPTS) $(SOURCE).rst
	sed -i \
		-e 's%article%book%g' \
		-e 's%<title>[0-9]*</title>%<title></title>%g' \
		-e 's%^    <blockquote>%    <partintro><blockquote>%g' \
		-e 's%^    </blockquote>%    </blockquote></partintro>%g' \
		-e 's%“%<quote>%g' \
		-e 's%”%</quote>%g' $@

.xml.epub:
	dbtoepub -s epub.xsl $^ -o $@

.xml.html:
	xmlto html-nochunks $^

clean:
	$(RM) *~

distclean: clean
	$(RM) gaysir_epistlene.epub gaysir_epistlene.pdf
	$(RM) gaysir_epistlene-body.xml

.SUFFIXES: .xml .html .pdf .epub
