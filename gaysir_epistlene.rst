
AbbiCabo © 2014

   Copyright © norsk utgave

   Omslagsbilde er tatt av Ingebjørg Hammersland.

   ISBN …

   Første utgave, første opplag 2014 – kun elektronisk kladd til
   lesning.

   Materialet i denne boken er omfattet av åndsverklovens bestemmelser.
   Uten særskilt avtale med forfatter er enhver eksemplarfremstilling og
   tilgjengeliggjøring tillatt i den utstrekning det er hjemlet i lov
   eller avtale.

   Kontakt forfatter på epost: sylvia.johnsen@gmail.com

   Utnyttelse i strid med lov eller avtale kan medføre erstatningsansvar
   og inndragelse, og kan straffes med bøter eller fengsel.

Fri diktning fra liv og røre.

Forum
=====

  How can we know the dancer from the dance?

  — William Butler Yeats

1
-

Noen uker inn i en tilværelse som singel sov hun gjennom natten,
impulsen til å fortvile gikk over og hun inviterte til middagsselskapet.
Sju av de nærmeste vennene skulle få servert et måltid, enkel mat,
drikke og vennskap på en tirsdag. Hun samlet en bouquet av viner for
gjester med gane for den slags. Etegilde for de nærmeste sto høyt på
listen over ettertraktede seire i livets jakt på den slags. Kjæresten
pakket kofferten og dro. Slutten på forholdet gjorde det viktigere å by
opp til middag. Hun ville bevise noe, skryte på seg kunnskap i
kategorien mat og selskap. Hun var fri til å følge enhver lyst og seier
sikret hun best ved å bryte mønster. Avgjørelsen tok hun raskt, en
flyktig tanke og umiddelbart fulgte hun opp med å sende epost til
inviterte gjester. Bestevenninnen Else meldte forfall. Hun inviterte
Rachel fra jobb, en kollega hun aldri hadde hatt hjemme. Tonen dem
imellom var god. Begge lo av de samme vitsene. Rachel måtte være blant
gjestene.

Allerede dagen etter invitasjonene visste hun leiligheten manglet fire
stoler. Platene under bordet utvidet bordlengden fra seks til åtte
plasser. Fleksibel størrelse på middagsbordet hjelper ikke hvis fire
stoler i riktig høyde står igjen. Samboeren tok med to stoler i
flyttelasset, og de var vant til å låne to klappstoler fra naboen. Manko
var fire sitteplasser. Hun gruet seg til å gå over gangen for å låne
klappstolene. Samboeren var venner med naboen, de koplet seg på Facebook
og holdt kontakt. Hun kviet seg for å ta skrittene over gangen og
blottstille livet alene.

To dager før middagen kunne hun ikke vente lenger. Hun ringte på døren
til naboen og fortalte om dilemmaet. Klappstolene reddet dagen, var det
mulig, og hvor mange klappstoler fantes? Hun lurte. Naboen var
overdrevet blid og imøtekommende.

“I gode naboskap er ikke det her no’ problem.”

Oppkvikkende kvalme spredde seg i ganen, intensjonene til tross, hun
skulle gjerne vært takknemligheten foruten. Gjestene måtte sitte.

“Du kaster deg vel inn i selskapslivet?”

Hun vendte det døve øret til.

“Ut på sjekkemarkedet?”

“Nei, nei.”

Hun ristet på hodet.  Naboen ville ikke gi seg. Nysgjerrig til det siste.
Tolv år bodde hun i gården. De begynte med et vennlig nikk, en
forståelse. Samboeren bragte samtalene lenger, og før hun visste ordet
av det spiste alle middager sammen og traff hverandre på byen som gamle
kjente. De var merkelige venner. Hun ville ha slutt på perversitetene,
gå tilbake til behagelig kjølig avstand mellom fremmede.

“Nettsjekking er tingen.”

“Ja.”

Hun ville ikke fortelle, si hun allerede før de flyttet fra hverandre
registrerte en profil på Gaysir. Der stirret hun på bildene og følte seg
fortapt. Hun snakket ikke med noen på nettstedet, leste bare i debattene
og lurte på hvordan verden utviklet seg mens hun var bortgjemt. Naboen
behøvde ikke vite om Gaysir.

Middagsplanene innbefattet gratinerte poteter og kjøttmat tilberedt på
ypperste vis, etter oppskrift fra en kokebok fra landets beste kokk,
kniv og gaffel var kriteriet for glupske appetitter. Hun trengte
sittestolene. Alternativet var å servere salater, hånd mat, eller
varianter der gjestene kunne gå fritt i leiligheten. De mikser og nyter
uformell samtale? Hun planla å gjøre ting annerledes, stivere, formelt.
Atypisk for henne. Beviset på overlevelsesinstinkt lå i de gratinerte
potetene, pesto vektet i gram og timer med steketid på kjøttet.

De fire klappstolene bar hun med adskillig trøbbel til leiligheten etter
å ha insistert at det gikk greit å bære dem selv.

Hun rørte i grytene, bøyd over forundret av den gode lukten, avbrutt
hvert tiende minutt når hun sjekket ovnsvarmen for steika. Hun støtte
borti en spesiell utfordring, en forglemmelse like viktig som antall
stoler rundt middagsbordet. Tallerkener manglet. Klappstolene sto lent
mot veggen. Hun stilte de til bordet og dekket på duk da hun kom å tenke
på standardsettet i skapet, kjøpt på Ikea, seks store tallerkener i
enkel hvit keramikk. Hjemover hadde hun stoppet på ICA Maxi og kjøpte en
prima steik til tusenlappen, innsauset kjøttet i pesto og kunne skryte
på seg førsteklasses mat til åtte personer. Kuriosa på oppskriften,
selleristand, persille og kryddersennep kjøpte hun dagene i forveien.
Kjøkkenet var fylt med oppskriftens ulike smaker. Hun ville vise seg
fullt kapabel til å invitere venner til middagsselskap. Hun var stolt og
singel. Tilværelse hadde betydning. Den praktiske forskjellen mellom
leiligheten før og etter samlivsbruddet besto i hovedsak det manglet
tallerkener til det antallet gjester hun hadde invitert. Hun forsto for
sent de essensielle endringene ikke stoppet fravær av potteplanter i
vinduskarmen og et par stoler. Seks middagstallerkener i skapet var ikke
nok. Hun trengte åtte.

Naboen lo. De sladret, delte nyheter og ekskjæresten ville vite hvordan
hun minutter før gjestene kom bar over ekstra bestikk og ti tallerkener.
De ville le av henne. Naboens servise fra Glassmagasinet erstattet de
hvite fabrikatplatene fra IKEA. Ekskjæresten ville bli orientert.
Stresset pustet hun lettere når hun fikk se det bekledde middagsbordet
klar til å glede sju venner. Singellivet strippet bort fellesvenner,
gjester kom tilbake, gamle bekjentskaper fra skoledagene og påbegynte
vennskap med løfte om å bli dypere. Skjønne-Rachel takket ja til
invitasjonen og ville komme til middagen.

Dørklokken ringte og timingen rev henne ut av en stille tilfredshet.
Attityden alt ordner seg snudde til panikk. Hun stresset, Rachel steg
inn. Stemningen oppjaget til det siste, hun strevde med å skjule
tidsnød. Rachel ankom samtidig med Benjamin, vennen med lengst
ansiennitet i venneflokken. De fikk et glass vin. Kollegaen spankulerte
rundt i stuen og stilte kontrollspørsmål, grov dypt i minner fra
barndommen. Rachel var spesielt nysgjerrig på ungdomsårene etter at
Benjamin fortalte om kjæresten på videregående, en ubehjelpelig gutt med
fregner og *Adam & The Ants* inspirert frisyre.

De fleste i selskapet traff hverandre jevnlig, bortsett fra Rachel
kjente de godt rammene for kvelden. Gjestene ankom tett. Hun ville gjøre
mer for å holde kontakt når selskapet var over.

“Har du bodd her lenge.”

Sa Rachel.

Blikket vandret. De besøkte aldri hverandre, hun mistenkte det var
adskillig enklere kår her enn hos kollegaen.

“Ja, føl deg selv som hjemme.”

Kutyme endret seg aldri. Velkomstdrinkene dyttet hun i hendene på hver
gjest straks de fikk av skoene. Det er nok drikke. Overraskelsen lot hun
ligge i fryseren, lollipopis til dessert. De gratinerte potetene i ovnen
og steiken med en ytre pestokant skapte forventninger. Godlukt i hver
krok vekket appetitten. Hun skålte og takket gjestene for at de kom.
Stemningen var god, hvilket skyldes like mye en spontan glede over at
nettopp hun inviterte, og mindre et uttrykk for kvaliteten på
matlagingen.

“Det var lenge siden.”

Sa Benjamin.

Forberedelser, nøye planlagte kruseduller, bare de mest nysgjerrige på
mat og dets kultur disket opp perfekte middagsselskap. Hun gjorde sitt
beste.

“Jeg mangler koselighetsgenet.”

Hun ga en forklaring på hvorfor det kun en sjelden gang initiert små
gester med hygge. Heller slo hun ut i store fakter, overrasket, framfor
å putre jevnt og stødig. Gjestene protesterte—høflige, lo når hun
antydet biologiske forklaringer lå bak og forklarte hvorfor hun aldri
stilte med bakte kaker, ledet allsang på bursdager og inviterte til
impulsive kafébesøk. Fraværet av tradisjoner stakk dypere enn biologi.
Hun og moren ble enig om å slutte feire bursdager året hun fylte tolv.
Middagsselskaper var sjeldne. Samboeren tok alltid for gitt de skulle
invitere. Hun forsto det var praktisk å samle til middag hjemme. Som i
mange ting satt konvensjoner premissene. Tradisjoner gjorde alt lettere,
og hun aksepterte skjebnen.

Vennene tilla middagsinvitasjoner større betydning. Leilighet, bestikk
og asjetter, teveapparat, stereolyd og møbler i riktig design var like
viktig, hvis ikke viktigere enn maten. Tingene forlot samboeren, de
delte hennes skjebne. Hun satt igjen, strippet for rekvisitter på digre
sponinfiserte møbelflater fra Ikea. Småknipset pakket hun i håndterbare
esker. Støvsamlere fnyste hun og stuet eskene opp langs veggen på
soverommet. Eskene ville stå der til broren var villig til å kjøre dem
bort. Regelen var at ting mulig å løfte med en hånd bragte ekskjæresten
inn. Rælet skulle vekk. Tid for fest. Essensielle møbler, inkludert fire
klappstoler sto på plass. Flere måneder bar hun full kvote gjennom
tollen på Oslo Lufthavn, ymse vinsorter, billige viner og en eksklusiv
whisky kjøpt i julegave til svigerinnens eldre far. Brennevinet eide hun
fullt og helt etter at broren surret det til og ble separert i november
fra det flotteste kvinnfolket han kunne håpe og få. Hun drakk ikke
whisky. Flasken ville stå der til hun fikk gjester som likte brunt
brennevin. Kvelden for å feire singeltilværelsen virket like fornuftig
anledning til å knekke korken som noe annet.

Vennene reagerte på Rachels undring. De observerte en nysgjerrighet på
grensen til taktløs. Iver etter å bli bedre kjent presset fram direkte
tale? Hun elsket det med Rachel, den nerdete og barnlige tilnærmingen
til alt. Kollegaen grov og lurte. Hun ville som Rachel vite mer, fort,
glede seg over en venn å ringe på tomme ettermiddager.

Hun sto alene på kjøkkenet med Benjamin og han benyttet sjansen og
spurte:

“Er du interessert, hvem er hun?”

“Nei, Rachel er bare en kollega.”

“Ja da så.”

Forferdelse over spørsmålet følte hun først senere. Mest sannsynlig
svarte hun overbevisende. Hvordan kunne han tenke tanken? Benjamin synes
å akseptere svaret.

“Det er altfor tidlig å se etter noe nytt. Hun er bare en på jobben jeg
liker. Fullstendig hetero.”

Han slo seg til ro med forklaringen fordi den hadde det i seg å være
sant for alle bortsett fra siste taler.

“Jeg er ikke klar for noe nytt.”

Sa hun brydd.

“Greit.”

To av gjestene kom sent. Homsegutten hun diltet etter på universitetet
kom med en eksklusiv flaske olivenolje til huset innbundet i rød sløyfe
og medbragt deit, en kjekk ung mann. Som ungdommelig friskus syndet
vennen villig på alle tenkelige måter og sto ikke tilbake for å avtale
sextreff. Han brukte lokkenavnet PikkiHagen. Han vokste fra seg
barnsligheter og endret profil. Når hun ertet hentet hun fram
kallenavnet. PikkiHagen hadde profilen til en voksen profil, og ga
gjerne en forklaring på hvorfor han dumpet den gamle.

“Enklere å få sexen du vil i dempet toneleie.”

Sa han og smilte skrullet.

Forsinket ga det henne tid til å fikse de siste gjøremålene på
kjøkkenet. Småting. Hun tørket benken for matsøl og knytte den overfylte
søppelposen. Ting som tar tid og det er vanskelig å planlegge. Gjestene
var mange nok til at de fant variasjoner i ordveksling når hun gikk fra
stuen.

Energien blusset mellom Rachel og Benjamin. De var nære å heve stemmen
til et politisk dilemma egnet til å ødelegge stemningen. Hun foretrakk
mange rundt middagsbordet for da sørget gjestene for variasjoner i
ordveksling. De sto fritt til å skifte samtalepartnere hvis samtalen
gikk tom for ufarlige tema. Hun stirret stygt på Benjamin. Vennen
skjerpet fokus og sørget for å lette stemning. Politikk fant ikke
fotfeste under middagen. Ganske snart hentet Rachel fram en mer
humørfylt tone. Pulsene normaliserte seg for alle, lå på en flat linje
og gjestene skålte trygge på suksess. Alle satt rundt bordet når hun
sendte inn en tredje vinflaske. PikkiHagen drakk medbragt. Han kunne
sine viner, hun drakk et glass før hun satt fram husets pappvin og
sikret de ikke skulle gå tom. Stemmeleiet hevet seg en anelse for hvert
glass, og kvelden lovet godt.

“Velkommen.”

Sju gjester, inkludert Rachel hun knapt pratet med uten å smile,
mennesker som gjorde henne glad. Alle samlet i hjemmet. Halvparten på
harde klappstoler, resten på klassiske kjøkkenstoler med nytrukne
tekstilputer som matchet duken. Vårens eksperiment i møbelsnekkeri
frisket til med to meter restlagerstoff. Hun hadde lånt en stiftemaskin
på jobben og gikk til verks. To dagers planlegging var nok, og hun trakk
stolene i nytt stoff. De ble riktig fine.

“Takk fordi dere kom til sommerens første middagsselskap.”

“Har vi nok vin.”

Alle fulgte hennes eksempel og hevet glasset. Hun grep den tredje tomme
vinflasken med venstre hånd og var i tvil om hva hun skulle gjøre.

“Vent, jeg henter en ny flaske.”

Hun løp til kjøkkenet for å fylle karavellen. Gjestene sippet i glasset
og ventet tålmodig på vertinnens skål.

“Skål.”

Alle runget ut alkoholens hilsen. Hun fylte på glassene og fant ingen
grunn til å utsette lenger en hyggelig kveld.

Gjestene kastet seg over de gratinerte potetene i løsere form enn de var
vant til. De forsynte seg grådig av steiken i perfekt og rødlig farge.
Hun stirret på den hvite melken i potetene og ergret seg over at sausen
ikke stivnet. Gulostflakene valket på toppen. Potetskiver gratinert i
melkestivelse kunne stått i ovnen lenger. Hun stolte på at konsistensen
stivnet hvis de sto et par minutter lenger, men gjestene ville ikke
vente. De spiste glupsk.

“Kjøttet er i det minste perfekt.”

Det var ikke bare skryt.

“Og brokkolien.”

Gjestene forsynte seg to ganger. Gamle venner hun gladelig slapp løs
visste hvordan forsere sulten. Rachel varmet, et nytt og spennende
bekjentskap. Benjamin fikk alltid fram latter. Deiten til PikkiHagen
lente seg over til Rachel og visket små observasjoner. PikkiHagen
tilførte frekk snert og konverserte høyt. Han var langt mer
underholdende enn de fleste hun kjente.

“Si meg, jeg har skaffet profil på Gaysir.”

“Nei, det må du ikke gjøre!”

Sa Pikkihagen.

“Hvorfor?”

Vennen slang fram hånden og viftet med fingeren for å understreke
betydningen av uttalelsene. Rådslaget måtte vente. Han forlangte å bli
fulgt.

“Du har selv sagt Gaysir er E6 til sex.”

Sa hun.

Ikke for det hun ønsket sex. For ti år siden vurderte hun i tre dager
sextreff og fant at det var vanskelig å få avtaler med dem hun ville.
Hun var for kresen, redd, ubesluttsom og tok til vettet før ting ble
alvorlig. Glimt i øyet og potensiale for noe varig var det eneste
alternativet for henne.

“I ditt tilfelle mer Riksvei 1. Lite trafikkert, men hendig.”

Sa Pikkihagen til allmenn fornøyelse. Gjestene lo.

“Takk, tror jeg.”

“Du er ikke horribel vennen.”

Bedyret Pikkihagen, og de lot samtalen stoppet der. Hun visste det ikke
var utseende han kommenterte, snarere evnen. Han likte å si hun hadde
for vane å irritere folk. De kjente hverandre godt.

Paret på bordenden traff hun ikke mer det året. Navnene forblir anonyme
i denne sammenhengen siden de ikke er relevant for fortellingen videre.
Hun kjente de godt, møtte dem sjeldnere. Sant skal sies rakk paret på
bordenden aldri å treffe samboeren før kjærlighetsforholdet tok slutt.
Venner og nok vin i huset var tilstrekkelig for å krysse av middagen som
vellykket. Før kvelden var over drakk de sju gjestene fjorten flasker og
åpnet whiskyen fra taxfreebutikken, drakk den brune spriten til kaffen
og sverget hun inviterte til de beste middagsselskapene. Koketteri.
Hvert bitt i det rødlige og saftige kjøttet utgjorde et eget måltid i
pestoflavør slik mannen i kjøttdisken lovet. Desserten lå på nivået
naturlig for henne å diske sammen. Gjestene fikk en lollipopis hver. Hun
overrasket og bekreftet. Ingen følte seg støtt, eller forventet mer fra
vertskapet. Middagen svarte til forventningene.

“Is er håndterbart. Jeg begynte forsiktig med en oppskrift til hovedrett
og disker opp med en enkel dessert.”

Sa hun.

Påtatt sjarme og riktig letthet når vekten av blodrødt kjøtt fordøyde
sakte.

Hun fulgte gamle krigsråd og forsøkte å gjøre svakhetene til styrke.
Saften fra lollipopis rant ned spiserøret og kjølte kroppen egget med
vin. Hun grep etter mer vin. Gjestene løftet automatisk glassene til
skulderhøyde, rettet ryggen og fulgte i en hyllest til kvelden. De gikk
ikke tom for drikke, det fantes flere flasker på kjøkkenet og en
pappeske halvfull. Dette var ingen tid til å stoppe.

Hun flerret smilet og gledet seg over tilværelsens letthet omgitt av
sanne venner.

“La oss skåle oss ut av denne misere. SKÅL.”

Glassene klirret. Alt var vel til hodepinen skurret neste dag.

2
-

Hun skriver lister, de er ofte til hjelp for å sortere tankene. Listene
ligger i lommen før oppgavene sakte hukes av; en etter en, og en gang
iblant haker hun aldri av gjøremål. Listene blir fillete og revner i
bukselommen før hun hiver hva som er igjen av papiret i nærmeste
søppelkasse.

Handlelister skrev hun aldri. Mellom radene i supermarkedet likte hun å
trekke varene ut etter innfallsmetoden og slenge dem i handlevognen.
Lister kom i veien. Favorittmat var hermetikk og suppeposer som lå i
skapet til en dag de ikke lenger lå der. Fersk mat råtnet, fikk en smak
og lukt før konsistensen myknet og ble slim. Lister hjalp ingenting mot
forråtnelsesprosessen i kjøleskapet. Kjøleskap levert med egen
datamaskin og skjerm i døren var framtidsvisjoner hun leste om i
magasiner. De kunne liste mat, siste forbruksdato og varsle om
forråtning før det skjedde. Hun tenkte det ville være smart, men hun
hadde ikke et sånt kjøleskap. Svinnet i fryseboksen ville forsvinne helt
hvis hun planla hverdagen bedre. Svakheten var egoisme, et overordnet
ønske om å prioritere velvære når hun hadde penger til mat i overflod.
Hun hadde ingen å stå ansvarlig for hvis det bognet over og enkelte
grønnsaker myknet i prosessen. Alenehusholdning kompromisset ikke for
handlelister. Bedre kontroll med hva hun spiste til middag ville
ødelegge gleden med å slenge varer rundt. Hun skrev ikke handlelister,
men andre typer lister grep hun gjerne til.

Hun var opptatt av temalister om hvordan skape fred og ti gode
egenskaper hos en statsminister. Mye skrev hun hver gang det vekket et
engasjement. Hun hadde lister for ymse tema. De siste årene skrev hun
mest praktiske lister til hjelp for å organisere livet. Gjøremål fylte
listene til glede og besvær. Hun kategoriserte cd-musikken i en
database. Jobben tok fire måneder fordi hun valgte å føre inn de enkelte
lydsporene etter lengde og gi hver og en personlig stjernekarakter. Hun
rangerte de enkelte låtene og ga egen vurdering for hver låt. Hver ledig
kveld foran teven knatret hun inn på tastaturet titler og sporlengde.
Året etter førte hun de samme listene inn i Spotify. Hun skrev bare de
mest spilte artistene som fikk tre stjerner eller flere. Hun gikk lei.
Musikktjenestene listet klassikerne for henne og de strømmet gjennom
stereoanlegget endeløse oppdagelser. Hun slapp surret med den
gammelmodige mp3-spilleren. Et sted mistet hun piffen og sluttet å knote
egne musikklister. Programmet på mobiltelefonen tolket låtene og laget
listene for henne. Oppramset musikk antok personligheten til kjedelige
handlelister hun frigjorde seg fra når stjernene føltes like
omskiftelige som Toro supper.

Lenge før hun fant mening i livet ga strukturerte lister en visshet og
fokus. Lister sto sentralt i alle eksamensforberedelser hun gjorde i
studietiden. Hun delte pensum på førstesemesteret i temaene filosofi og
historie, logikk og naturvitenskap. Hun skrev lister for hver retning.
Fornuften tilsa en utdannelse innenfor næringsliv. Markedsføringsfaget
var ingenting annet enn flere lange lister med selvfølgeligheter. Hun
valgte fag å supplere på universitetet. Instituttet for litteraturfaget
utga lister over bøker hun måtte lese før eksamen, lange lister over
enda lengre bøker. Hvilket fag hun skulle ta når eksamen var overstått
utgjorde en egen liste over planer for framtiden. Hvilket fag hun skulle
velge var en orientering i studieretninger og fagkretser, pensumlister
og annen hyllevare.

Engasjert kastet hun seg inn i ungdomspolitikk, ble fadder for ferske
studenter og tok vakter i studentbaren. Hun ville mer; og de satte henne
på listen til Studentparlamentet.

Kunstfag var et fag for framtiden. Hun ville ta eksamen i kunsthistorie
en gang før pensjonisttilværelsen og etter en karriere. Det fantes en
antakelse om at visse studiepoeng måtte vente til hun ble gammel, og de
hang på listen lik gledestråler og var ikke livsviktige. Hun skrev
lister om hvilke fag til hvilke år, en nedtegnelse over målsetninger de
neste tjuefemårene. Målbærende lister som festet en eim av dårlig
samvittighet rundt halsen det skulle vise seg vanskelig å bli kvitt.
Sommertid besøkte hun Europa. Hun og venninnen kjøpte interrailbilletter
og haket av kjente monumenter i rasende fart: Sett, besøkt og glemt. Hun
skrev listen før de dro og venninnen stilte lojalt bak og sikret jevn
fordeling mellom strender og turistmagneter hvis disse ikke samsvarte.
De dro på interrail med en klar og utvetydig avtale. Listen brant i
lommen, og hun jaget turistattraksjoner nedskrevet i sirlig håndskrift,
strøk og la til daglig på reisefot. Professoren på Blindern talte i
timer om søylene i Kölnerdomen, hundreogfemtimeter høye raget de midt i
Köln, ikke langt fra togstasjonen. Kirken tok de forfjamset for seg på
en halv dag. Skyggen av domen skjulte et museum til minne om romertiden.
Germanske stammer led under okkupasjon fra det barbariske rike i sør.
Hauger potter og kar, miniatyrhus like de hun så i legoland fortalte
historien skjult under bakken. Kölnerdomen reist i hele sin prakt og
overraskelsen, museet inntil dets rake motsetning. Kirken gjorde byen,
domen sto på listen og steinsøylene var eventyret turistene kom for å
se. Museet forvirret hun seg inn i for å bruke resten av dagen før toget
tok dem sørover.

Neste stopp var Italias hovedstad, deretter skulle de til feststrender
der romere faktisk levde og elsket. Hun lente seg mot veggen og beundret
kulden i grå stein, styrken i tidens tann hevet over arbeidere som
dyttet og dro byggesteinene til det massive bygget. Viten om hvilket
arbeid som lå bak burde få enhver til å tenke. Hun satte etter besøket i
Kölnerdomen ord på spørsmålene en ung sjel trenger å kvele før de griper
deg fullt og helt. Togturen til Roma var lang og krevde de siste
kreftene; trengselen på toget og mangel på soveplass skapte rom for
ettertanke. Hvordan kunne tyskere bygge noe vakkert? Besøket på museet i
domens skygge manet fram spørsmålet. Listen hun skrev på togturen til
Roma vekket senere skamfølelse, for listen avslørte for første gang en
mangel i henne.

“Hva er de ti største forbrytelsene mot menneskerettighetene?”

Sa hun.

Rachel forsto ikke spørsmålet. Hun fortalte om listen, øvelsen trigget i
møte med tysk høykultur. Hun strevde med å finne balansepunktet, si hva
hun fant vesentlig. Livsprosjektet listet hva som var viktig. Hun tenkte
lenge over svaret. Likevel ble svaret feil, en akademisk perversitet
fjernet fra smerte i rygger, armer og bein.

Kölnerdomen reiste seg på lytene til arbeidere, etterkommere i
generasjoner jobbet med kirken. Barn, barnebarn, barn til barnebarn,
hvert eneste familieledd og dets historie glemt. Døde skall ofret for
religiøs forgjengelighet. Alt bygd, komponert, diktet og levd. Ingenting
var forgjeves og likevel, småtterier mot de døde kroppene knust under
sorte steiner. De ti største forbrytelsene mot menneskeheten skrev hun
etter uklare kriterier. Hun satte et mål om at drapene, krig, vold,
utbytting og utnytting ikke skulle komme i veien for en objektiv
vurdering. Hun gikk grundig til verks, listet tjue kandidater der verste
historiske anomali i maktutøvelse skulle trone. Ødeleggelse av varig
verdi, det destruktive som ikke bragte folk videre.

“Hva tror du jeg skrev øverst på listen.”

Sa hun.

Rachel spurte hva betydde mest, hva hun ville dø for? Hun oppdaget at
spørsmålet var vanskelig for hun ville ikke dø. Hun fortalte i stedet
denne teite innsiktreisen fra interrail i ungdommen. Hvorfor tok hun
ikke heller for seg listen over hvilke seksuelle eskapader hun hadde som
ambisjon å mestre, listen hun skrev omtrent på samme tidspunkt?
Barnslige nedtegnelser fra en frigjort, verdensvant kvinne der
ambisjonen om gruppesex listet på tredje, etter sex med kvinne. Hun kom
aldri lenger, forsto sent hun aldri ville ha behovet for intimlek i
flokk. Forelsket i kvinner var hennes legning, hvilket hun ikke delte
med Rachel. Venninnen stoppet på første punkt. Samtalen sklei inn i det
politiske og Rachel ville ha svar på hva hun mente om kronikken
møysommelig revet fra avisen. Samtalen ville skape en kile mellom dem,
og hun gjenkjente behovet. Det var lenge siden hun ga verdenshistorien
en dom. Oppsummert skrev hun listen over de verste forbrytelser mot
menneskeheten i ti punkter. Det historiske spørsmålet hun utfordret
Rachel til å gi svaret på ville avsløre henne. Rachel fikk en ledetråd.

“Først var *ikke* holocaust. Det var nummer to.”

Rachels foreldre var jøder. Historier fra Nazitysklands holocaust bar
familien i kroppen. Minnene lå dypt forankret, genetisk nesten.

“Religion var ikke på listen i det hele tatt.”

Hun tilhørte ikke dem som foraktet de religiøse og anklaget kirken for
alle sosiale urettferdigheter når kristne organisasjoner synes å være de
eneste som gjorde en innsats i bybildet. Kristenfolket var ikke
uskyldige, de var der og historiens dom ville gjøre dem ansvarlige også
for kriminalitet Rachel endevendte hjernen for å sette i ord. Tidenes
største forbrytelse mot menneskeheten slik hun så ting for tjue år siden
var en vanskelig nøtt å knekke. Nettopp derfor måtte Rachel vite hva
slags person hun var. Politisk engasjement i ungdommen skapte et gap
mellom dem. Privatlivet veltet inn i lunsjsamtalene. Hun etterlyste
sosial samvittighet, og Rachel kom bort med utklippet fra gårsdagens
avis. Hun hadde lest kronikken og visste ikke hva hun skulle si om
staten Israel. Trusselen eksploderte i en debatt om verdier. Rachel
lyste vakker. Hun likte at venninnene gikk i kirken, enda mer sikker enn
hun på hvordan verden fungerte. Slik trygghet var blott en lengsel
skjult bak spørsmål.

“Du er født konservativ, sa mor om meg.”

“Ja, hva så?”

“Nei, ingenting.”

Anklagen til mor gikk de fleste hus forbi, men de som kjente moren
visste betegnelsen konservativ var alt annet enn kompliment. Rachel
kjente ikke moren. Hun fortalte trivielle historier fra oppveksten for å
skape en forståelse og bygge bro, men de var fremmed for hverandre.
Nasjonaldagen inviterte Rachel til frokostbrunsj, alle kom i
nasjonaldrakt og hadde medbrakt vin. Forutsetningene for å forstå en
oppvekst i tunika og sandaler lå skjult i lag med bunadssølv og en god
Sauvignon.

“Jeg forteller deg for at du skal forstå hvor jeg kommer fra.”

Hun brukte kostymer hver dag. Livet var fylt med staffasje. De gråt for
intuitivt forsto begge farene. Rachels øyne blåste opp og stakk
spørrende ut fra hovne puter over kinnbeinet.

“Jeg mener, en ung og naiv student lister de ti første forbrytelsene mot
menneskeheten og hva tror du jeg satte på topp?”

Hun terpet. Rachel skulle svare og hun stirret intens på et forvirret og
blankt blikk. Etter eget hode listet hun som ung voksen kvinne ti
historiske maktgrep og rangerte disse fra en til ti. Hun var enig i mye
av det som sto skrevet i kronikken, og visste Rachel følte sterkt for
det ene perspektivet. Mer enn før var det viktig å poengtere ting som de
var da hun ivret til kamp i ungdomstiden.

“Jeg er enig, i dag. Men det var ikke enkelt.”

Hun stokket tankene.

Febrilsk kjempet hun for at politisk fortid ikke skulle være i veien for
dem. Meninger skilte dem ikke, det var livet. Rachel kokte. Hun visste
hva venninnen sitret mest over, og likevel satt hun sjokkert pal stille.
Politiske diskusjoner var utypisk for Rachel, og bare det gjorde henne
utilpasset og utløste et skjelv i overleppen. Diskusjonen var altfor
høflig. Tematikken framsto som det minste problemet. Målløs traff de en
diger uenighet i en programfestet konflikt. Før levde hun for
kranglinger om det uviktige som framsto som det aller viktigste.
Samtalen venninnen dro i gang bragte ingenting godt. Rachel oppsøkte
henne. Hun trakk et større perspektiv for å komplisere bildet med en
sannhet. Hun forsto Rachel. Frustrasjon, argumenter alene ravet sterkt
og her lå følelsene. Rachel var ikke fienden. Sannheten var like gjerne
at hun, eller sånne som henne ledet an og ville drukne verden i blod i
ren og skjær uvitenhet. Politisk korrekthet balanserte fint på
knivsegget. Tenårene gikk til brennende kamp mot all urett. Gi opp
idealene for Rachel ville være uærlig. Hun vridde seg i kontorstolen og
tenkte på piken hun var fjorten år gammel på sommerleir. De arrangerte
loppemarked og pengene rullet inn. Dansegulvet vibrerte til kampsanger
for å frigjøre Nelson Mandela. Hun hadde en fortid i agitasjon og
revolusjon. Overfallet fra Rachel krevde finesse der en overflatisk
lesning skapte friksjon de kunne være foruten. Hvis hun ikke var en
bedreviter, hva ville hun sagt annerledes?

Blikket til Rachel sklei over til et glassaktig uttrykk.

Hun forsøkte å formulere seg forsiktigere. Sånn talte hun, klart og
sterkt, direkte og idealistisk. Rachel måtte kjenne fortiden,
motivasjonen for alt de første årene i politikken. Hun skrek slagord,
utforsket og befestet standpunkt i ung alder. Rachel fortvilte i møte
med fortiden.

Stemmene skar luft, kolleger sto sjokkert og stirret.

Hun lot Rachel geleide dem til et ledig møterom. De marsjerte taktfast,
lukket døren og satte seg på hver side av møtebordet. En stund forsøkte
begge å fokusere på det politiske, beskrive omforent situasjon på marken
de kunne bli enig om, i et lovet land de aldri hadde besøkt. Inntrykk
fra film, repriser på teve, historiske trivia. Mest snakket de om seg
selv og hvordan alt var før i tiden når de satt uskyldig klistret foran
teven. Rachel gråt.

Hun fortalte om listen. Spørsmålet ble stilt, rent akademisk fordi hun
nektet å tro blind og døv. Hun tvilte på om Rachel så problemet? Stemmen
snufset gjennom snørr og tårer. Nå ulte de høyt. Daglig spiste begge på
denne tiden lunsj.

Hver dag satt de og smilte til de samme vitsene, utvekslet tanker om
hvordan gjøre en forskjell, to kolleger i felles front. Humor lik. Samme
smak i kantinemat. Hvordan visste de vennskapet ville overleve
krangelen? De gråt, sårbare og redd for å miste den andre. Sannheten var
at framtiden var usikker, de visste ingenting.

“Vi kan aldri bli venner.”

Stemmebåndet sprakk. Rachel gråt. De hylte. “Jo da.”

Utenfor det skjoldete glassvinduet stoppet sjefen, tvilte og gikk
videre. Tiden på møterommet ryddet unna det verste, roet følelsene og
neste dag ville alt være glemt.

“Dette er den jeg er. Norsk.”

Sa hun. “Hva satte jeg på topp?”

Rachel lyttet.

“Holocaust var nummer to, Stalin og mange år med utrenskninger og
dødslister en sterk nummer tre.”

Tidspunktet hun forsto perspektivet i livet var vridd til en akademisk
perversitet kokte ned til øyeblikket hun skrev liste over ti
forbrytelser mot menneskeheten og i fullt alvor satte fabrikker som
drepte millioner av mennesker på andreplass, bak den verste forbrytelsen
i hennes øyne.

“Først, foran alle andre satte jeg brenningen av biblioteket i
Alexandria”

Hun stoppet, trakk pusten, fortalte sannheten. Halvåret som ildfull
kommunist trettenår gammel var ikke den mest skamfulle ungdomssynd.

“Du elsker bøker.”

Rachel forsto hva hun forsøkte å si. Hva hun måtte mene hadde ingen
verdi. Vennskapet hvilte ikke på livsfjerne tankeslott. Hun hadde tenkt
mye og funnet luksusnisjen bare forbeholdt de bortskjemte. Levde de ikke
i et fredelig hjørne i verden, fylt med kunst og frie tanker. Dødskamp
var noe de andre brøt med. Rachel trengte ikke å bringe den hjem.
Avisdebattene gjorde skade, skapte friksjon der alle med hell kunne roe
nervene.

“De brant verker vi vet eksisterte fra omtale i andre tekster, dramaer
fra de største mestere, gamle greske manuskripter tapt for alltid i
flammehav.”

Det var de kristne, og dermed sa hun noe om religion likevel. Ettertiden
ble robbet for tanker og ideer, ruiner sto der religionene reiste
katedraler i stein folk beundrer. Artefakter slår den ene ihjel og er
det eneste som overlever tidens tann. Mennesker i millioner faller fra
og tapt lærdom kjennetegner hver generasjon. Vår skriftkultur, teknologi
og arkivsystemer hindrer ikke at menneskets erfaringer blir skuslet
bort. Daglig går verdier tapt. Biblioteket i Alexandria var der unike
objekter, bygning og ideer i ett.

Hun tenkte på Kölnerdomen.

“Ja.”

Rachel aksepterte unnskyldningen, forklaringen hun måtte ty til for å få
venninnen til å forstå. De tørket tårer. Ting kunne vært bedre, hun
følte takknemlighet fordi Rachel var der. Hvis hun mistet Rachel ville
alt være tapt? Livet var å foretrekke framfor hvite vegger i hjemmet,
alene, der hun flyktet gjennom det trådløse nettverket inn i Gaysir.

Viljen til å forandre verden var der. Nysgjerrig og rede til kamp mot
urettferdighet, en politisk bevissthet hun bar fra ungdomstiden.
Politisk radikalisme tok et jafs fra tenårene da hun ervervet en unse
klassebevissthet som for alltid ville holde tilbake de mest rabiate
ropene på ansvarstaking. Hun valgte venstresiden halvveis i høyrebølgen,
og kanskje derfor overrasket valget alle. Snille piker følger trender i
tiden. Foran postkassen med innmeldingsskjema til Unge Høyre stoppet
kroppen i frossen positur. Papiret hang på fingertuppene og øyeblikket
hun nektet å slippe tak representerte en tidlig politisk helomvending.
Hun ville ha mange i årenes løp. Hun gjorde opp status etter året i
politisk yr. Tenårene vekslet mellom ekstreme motpoler på henholdsvis
høyre- og venstresiden. Hun var ung når russiske tropper fløy inn i de
hardbarka asiatiske fjellområdene, rettferdighet framsto som det
viktigste.

“Hvis USA får lov til å være imperialistiske bør Sovjetunionen få gjøre
det samme.”

Sa hun.

Minnene fra de første politiske utopier var flaue og pinlige
barneskritt. Hun var selverklært kommunist i et halvår før hun valgte
motsatt pol og idealiserte USA og dets forsvar av den frie verden. Hun
visste verken hvor det var, eller hva det ville si å være fri. Konseptet
frihet ble bevissthet langsomt, og det gikk mange år før hun sluttet å
kjempe og heller spankulerte løs og ledig i homoparaden, smilende til
folk på fortauet, stråle i takt til Gloria Gaynor *I am who I am* og
faktisk kjente følelsen på kroppen.

Hun fant trøst i gamle floskler som sa ungdomstid gis til sosialismen,
og voksenliv erkjenner livets realiteter, spesifikt dets begrensninger.

Hun følte ingen trygghet slik Rachel aldri stilte spørsmål og eksisterte
uanfektet. Venninnen tok aldri standpunkt i det politiske landskap fordi
væren isolert var å leve. Hun fylte fjorten år og vinden snudde, ungdom
valgte partiet Høyre og de fosset fram på meningsmålinger i en nasjon
stemplet av etterkrigstidens sosialdemokratiske seierherrer. Avisene
skrev om de konservative partienes framganger og kalte fenomenet som
preget landet for høyrebølgen. Innmeldingsskjema slapp aldri tak. Det
ville være enkleste sak i verden å glippe fingrene. Hun tenkte på moren,
tre uker innlagt på sykehus med diagnose svulst og kirurgens skalpell
som kuttet presise snitt i underlivet. En fingerglipp ville være
ødestigende. Tevedebatter der statsministerkandidatene herjet svevde i
bakhode. Hun innså der og da på baksiden av shoppingsenteret, alene
foran postkassen; den politiske reisen begynte det året. Partiet i
vinden, forkjemper for festbrus og enhver som skriker høyest ville ikke
gjøre moren lykkeligere, barndommen tryggere. Fra intet møtte hun seg
selv, følte en voldsom identifikasjon med klasse, og ubemerket trakk hun
hånden med innmeldingsskjemaet.

“Ikke i dag, ikke denne uken. Kanskje aldri?”

Moren var aleneforsørger. De tre ukene hun og broren bodde hos tanten
var de verste ukene i livet. Redselen under dynen, frykten for å reise
seg og gå inn i den fremmede stuen satt friskt i minne. Tanten var ikke
slem. Barnet var i den alderen bare tanken på å miste moren bygget mørke
det var vanskelig å grave seg utfra. Hun vokste raskt den gangen,
hormonene spratt vilt i kroppen og for hver centimeter i høyde bekymret
de små detaljer og bragte uro hun aldri ville gjenskape. Voksesmerter er
vanskelig nok for de unge uten maske, og enda verre for de hormonelle.
Høyreist tok personligheten form, og omtrent samtidig som moren kom hjem
fra sykehuset landet hun i det politisk midt der sosialdemokratiet
definerte de ideologiske mulighetene. Hun tok voksne og seriøse valg.
Ettersom årene gikk ble hun mer usikker på hvilken ideologi som best
fanget de personlige behovene, og hvorvidt det var av betydning. Et
vekkelsesløp gjorde henne til en av rikets lydige gunstlinger.

Endringsviljen ropte og rev i kroppen, hun søkte partier som satt på
agendaen å endevende, revolusjonere og redefinere virkeligheten. Mye ble
oppnådd politisk i årene fra barndom til moden kvinne. Partnerskapsloven
ga homofile rett til å danne familier, og deretter kom ekteskapsloven
som gikk enda lenger. Hun tilhørte en generasjon som stilte krav og
aldri sa unnskyld. Hun stilte spørsmål og tillot sjelden å føle på
ubehaget. Forandring kommer innenfra, individet vokste utover og den
klassebevisste fjortenåringen med hjerte til en fanatiker måtte gi tapt
for en fetert trettiåring—god og mett. Viljen til å snu opp ned på alt
var mer skadelig enn skapende, bestemte hun. Slik gikk den ideologiske
reisen hun først følte truet i voksen alder etter å ha gjort hva hun
kunne for å løfte seg fra klasseåket. Det kritiske øye heftet ved
maktutøvelse uavhengig politisk arv.

Lett ble livet aldri, men hun tenkte på slektninger. Tante, alltid en
kausjon fra å miste hjemmet, kriser på løpende bånd, strandet i Spania
uten flybillett hjem og penger til å kjøpe. Tomt kjøleskap og trusselen
om sult. Pengebekymringer var bagateller for svigerinnen der hverdag
truet selve livet. Broren var ikke snill. Hun mistenkte moren for å vite
hva broren skjulte innabords. Først etter mange år forsto hun redslene
han spredde i fylla, sjukelig sjalu bar broren på en voldsom eiersjuke.
Han oste usikker, tante surret med penger og moren unnskyldte dem begge.
Psykoanalyse hjalp ikke hjelpeløse mennesker. Hun fant det fungerte best
å unngå familien.

Hun levde lenge i villfarelsen profilen på Gaysir ble opprettet for de
gode krangler, retoriske øvelser med den hensikt å bli kjent på
nettstedet og lære andre å kjenne. Betraktninger om tidspunkt han valgte
for å gjøre seg synlig i det perifere homolandskapet var flyktige.
Gaysir monopoliserte sjekkemarkedet for homser og lesber. Spørsmålet om
hvorfor hun valgte dette øyeblikket i tid skjøv hun unna. Bruddet med
kjæresten var ferskt. Erkjennelser lå dypt og hun følte kun på
overflaten i spekteret følelsene flomme fritt. Hun visste det var noe
galt med mantra.

“Nei takk. Jeg er her for forumet.”

Sa hun til lyden ble hul.

Inntil videre var fornektelsene sanne, for hun løy ugjerne og aldri med
vilje. Svaret gjenga hun til det kjedsommeligste hver gang venner
spurte, eller en fremmed sendte tekstmelding til profilens postkasse.

Forum underholdt i en kjedelig hverdag. Hun håpet å luske ut hvem de
andre på forumet var, hvordan de tenkte og hva de gjorde? Enavnene hang
klistret i bakgrunnen, de mange profilene på hovedtavlen i periferien
for debatt som lyttet. Noen profiler gjorde seg synlig når de trykket på
tommelen. Hun søkte begrenset på hovedtavlen, geografisk knyttet hun
søket til hovedstaden og aldersgruppen la hun tett på egen alder.
Søkeresultatet vokste aldri til to skillefaner. De var ikke Rachel, og
kom aldri til å bli det. Kollegaen var heterofil og hun trodde ikke på
konvertering verken den ene eller andre veien. De første spede skritt i
lesbisk fortegn var kyss på pikerommet, og den nakne sannhet var hun
tapte gode venninner i ungdommen, bifil og søkende. Benjamin foreslo
bedre seksuelle erfaringer med menn og hun ville ikke surret bort
venninnene som sluttet å ringe da hun gjorde kur. Så feil han tok. Hun
hadde flotte forhold til menn. Sannheten var hvis erfaringene med
kvinner de første voksenårene hadde vært gode ville hun blitt lesbisk
tidligere.

Hovedtavlen på Gaysir listet kvinner i alle fasonger. Søket ville miste
mening hvis hun slapp til kreti og pleti, mannfolk skjøv hun unna og
transer. Kvinnene hang ubesværlige i dvale. Deres eksistens skurret i
bakhode, og de var del av livet, frisk pust. Hun følte det var tidlig å
spane etter ny kjæreste fordi bruddet var ferskt. Grunnen til å bli
værende på Gaysir fant hun på forum. Slik lød løgnen, og det virket ikke
presserende å bryte fri. Livsløgner er sjelden en forseelse når den
skadelidende begrenser seg til personen i speilet. Nettstedet løftet de
skeive, og forum var kanalen for diskusjon. Debattene trakk
oppmerksomheten med søte løfter om rasjonalitet slik fluepapir lokket
fluene. Gaysir var med å sikre nisjemarkedets ensartethet, servert lå de
homofile slaktet, monopolisert samlet Gaysir alle i et rom for
likesinnede. Sarte sjeler søkte deg. Diskusjonen ga en
start—introduksjon til homoliv—skapte en reklamekanal der alt startet
hvis du ikke var tøff nok til å surfe profilene direkte.

Gaysir forum trekker de anonyme standpunkt fram, slapp løs
frustrasjoner, retoriske kriger og hun følte bremsehjulene gikk ut døren
med samboeren. Fasader var et valg der hun viste hensyn. Hun var for
gammel til å heftes ved en familie som lot alt fare. Kjæresten hadde
ingen hold, og jobben hadde blitt rutine. Debattene aktiviserte de siste
skrupler mot synlighet. Gaysir kunne tilby anonyme profiler der ethvert
standpunkt lå skjult bak epostadresser og et elektronisk kallenavn hun
sto fritt til å dikte fra eget hode. Enavnet var personlig, og skjult,
en annen personlighet og likevel det samme menneske hun traff i speilet.
Hun hadde rasjonelle grunner for å opprette profilen på Gaysir. Grønne
prikker varslet om tilstedeværelse på hovedtavlen, lik flagg over
slottsbalkongen når kongen er hjemme. Hun søkte det ubestemmelige,
avsporinger og det enkleste valget var å følge debattene. Frykten måtte
overvinnes. Snikende angst hver gang hun sto alene, livet på hold.
Redselen overskred aldri følelsene for skrittene ut av skapet.
Forumsdebatt var enda en flukt. Veien fra stuen til folkeliv gikk
gjennom nettstedet. Hun var ikke emosjonelt klar for annet enn debatt.
Deits ventet. Før hun søkte kjæreste ville hun ta en pause. Livsløgnen
hun dyrket var at tilværelsen på Gaysir fant brennstoff på forum.

“De ser meg. Jeg er. Jeg er ikke død.”

Enavnet skjulte det private trodde hun. Åpen lå ordet. Det eneste
sannferdige i tilværelsen lå i det brennende engasjementet. Debatt
avsluttet ørkenvandringen i tosomhet og skapte et monster.

Løftet om nye opplevelser lå i det mangehodete landskapet og deits
dyttet hun bort til en uviss framtid. Gode krangler åpner dører, tvang
tålmodighet inn i et spor. Hun søkte debattene for å ha en trygg base
til det ikke lenger var mulig å gjemme seg bak meninger. Målet å
konfrontere demoner måtte vente. Debattene var tryggere, og de ga pauser
mellom slagene, utsatte søket etter neste forhold. Hun følte seg langt
fra klar til nye runder forelskelse og altslukende fråtseri i kropp og
sjel.

Hun skilte seg ikke fra de andre på forum. Hun bråket med konfronterende
uredd tone, kastet inn flammende innlegg og trodde på hvert ord. Bulder
til en stemme som brukte skrift i kalkstein skrevet i store bokstaver,
alfabetsuppe i ledbelysning over et nettverk som strekte seg i kabel fra
tak til bakken. Opptreden i forum tok fart og hun ytret seg i
primalskrik. Hodet vridde rundt til lyden av parringshyl godt pakket inn
i fordekt uenighet. Lek lik tiurens dans for den utskårne.

Mening varierte, men standpunktene forsvarte hun kompromissløst og med
integritet. Noe annet var utenkelig.

Hun trykte på profilene, pekte på enavn og sjekket spesielt nøye de som
mente som hun på forum. Delte de tilhørighet, vekket det en interesse
raskt. Gjenkjennelse kom fra fornemmelse, noe uttalt, uttrykk satt i
skrift og måten ting ble sagt. Kvinner på forum vekket større interesse
enn mennene, slik var det bare. Deres peker fulgte hun til døren på jakt
etter en antakelse, røske i hypotenusen og presse videre i den
personlige sfære. Vennelisten fristet ofte mer enn profilene hun stirret
i senk. Klart og tydelig for enhver titter søkte hun noe annet enn
debatt.

De skulle se henne. Tilstedeværelse var obligatorisk. Hun søkte
planløst, innenfor definerte rammer. Forumsdebatt lokket de nysgjerrige
nærmere. Kom å se!

Singel, på sidelinjen gjorde hun det eneste fornuftige, tok sats og bega
seg inn i de harmløse flørters øvelse. Sjekking gjennom
meningsutfoldelse, ikke ulikt marsjen hun gikk hvert år sammen med de
andre i homoparaden på til slutt under Skeive dager. Dagen kledde hun
seg alltid i noe fint. Den politiske kraften i å leve godt kunne ikke
overdrives. Derfor valgte hun sterke farger, de fineste Conversene og
hvis været tillot et skjørt eller skorts. Hun blottet solbrun hud og
flekket tennene i ett bredt smil. Vågale år eglet hun seg innpå en diger
transe kledd opp for dagen i tights, parykk bærende på en forvokst
squash. Hun ville vrikke kroppen til discorytmer når det vakre monsteret
lot som og svelget grønnsaken til allmenn forlystelse. *Party!* Skeive
dager var årets fest. Dansemusikken dunket over de mange stemmene. Jakt
en dag om året. Gaysir åpnet alle dager og utfordringen var å finne
selskap langs veien.

Forum fylte de outrerte stemmer, kvinnene trakk blikket. Reklames verdi,
hun studerte mysteriet markedsføring lenge før faget ble programvare på
mobiltelefonen. Instinktivt følte hun nærværet dekket behovet i
øyeblikket. Hun valgte nøye et flatterende profilbilde. Hun trengte tid
til å vise verden hun fantes. Gaysir plasserte henne i det lesbiske
landskapet, og det var alt, bortsett fra en brokk samling lesbefilmer i
bokhyllen. Kjæresten dro og livet fikk mening foran den bærbare
datamaskinen. Hovedtavlen fylte mengder enavn villige og anonyme.

Seksuell preferanse lesbisk, høyde, ikkerøyker og engasjert. Bilde sa
resten. Kjerneidentiteten hun klamret seg til var et todimensjonalt
uttrykk hun skiftet bilde på nesten daglig.

Samboerens exit ribbet leiligheten for lesbiske totempeler. Tastetrykk i
forum var siste rest av kjønnsidentitet. Her fant hun andre lesbisk, det
sosiale nettverk for likesinnede. Oppslag til fester. Nisjesamfunnet
Gaysir var den sosiale motorveien på internettet. Forum ga en plattform.
Artikulert, trent og spisset for flørt. Hun trengte treningen. Debatt
var det valgte formatet. Debatt ga nok. Tilfreds sendte hun japp mot
øvrighet og tok gjerne en kamp mot de allmenngyldige sannheter. Hun
hatet selvfølgeligheter og politisk korrekthet. Mobbere. Idioter.
Liberalister som trodde livet var harmløs moro.

Mye kunne hun tatt tak i på forum. Hun sto på scenen, valgte arena
bevisst og bestemte hvor linjen gikk. Hun forestilte tilværelsen på
nettforum handlet om å henge i baren, eller biljardbordet og gurgle øl i
takt med bakgrunnsmusikken; slenge seg på organiserte utflukter og smile
skjelmsk til dørvakten på So, utestedet for de lesbiske i Oslo. Hun
samlet luft til å puste, trengte tid til å forberede angrep. Motet fant
hun på forum. Debattene underholdt, samtidig skrev hun krasse
kommentarer der familiens skjebne lurte i bakgrunn. Giftige og krasse
innlegg startet en ordflom. Hvert femte svar tok hun ordet. Ingen fikk
fred, tjente poeng på fravær.

Et typisk moralsk standpunkt glitret sjelden i et sosialt nettverk for
homofile og lesbiske og alt annet skeivt. Rent intuitivt forsto
seksuelle minoriteter at moral var fienden. Liberale tradisjoner
beskyttet friheten til å gjøre gale ting. Spill, hor og dop rir
gratisrunder på forum. Hun var en stemme mot alt. Hun var ingen liberal,
tvert imot.

Tommelminuser på innlegg sladret om leserens preferanser. Hun vekket
følelser, og det var ikke standpunktet, men tonen—krass og knapp. Hver
dag startet et dusin debattråder, fire til fem av dem åpen for gode
argumenter. Moralistene var de minst populære. Hun omfavnet stempelet.
Argumenterte rett fram med overbevisning. Kjent ståsted og tydelig
vinklet. Ingen misforståelser var mulig og alle kjente igjen tradisjonen
fra den sosialdemokratiske middelvei, bortsett fra at hun rammet bredt.
Forum var det eneste alternativet i et mørkt toneleie der livet besto av
regninger og atter mer regninger. Hun sparte penger hver måned til tørre
kvadratmeter, varmt og trygt, sentralt lokalisert i sentrum fordi hun
eide ingen bil og kunne aldri tenke seg å pendle til jobb. Kravene til
komfort skeiet ikke utover det normale. Hun klarte livet ganske bra.
Komfort er hva livet ville gi hvis hun holdt seg frisk og heldig. Liv i
komfort ville være en seier. Hun var et løvetannbarn med en klassereise
i livsanskuelsen av de sjeldne. Utdanning, sparte penger og fast jobb.

Alt stemte. Bortsett fra en blemme. Hun var et seksuelt avvik—seksuelt
orientert til besvær og i direkte konflikt med den rette linjen fra
ungdomsskolen til markedsdiplom. Hun var lesbisk, tilhørte periferien i
brytning mot det konvensjonelle.

Solid jobb, arbeidsgiver med eksklusivitet og en trygg havn. Søkte hun
lån i banken ville de analysere livet, snu opp ned og sette en karakter
for å fastsette kredittverdighet. Hun bodde på samme adresse på tiende
året, ingen krøll, dommer eller betalingsvansker. Irrasjonell opptreden
var ikke tillatt, og i tjue år var et par private fyllehistorier som
endte i spy og migrene alt. Fyll dukker ikke opp i kredittanalyser.

Et indre smil, stolt tilbakeskuende og synkronisert, hun møtte verden
med fandenivoldsk obstanasighet. Arret, hvis hun skulle kalle det et
arr, mer var det rift på ego, den indre bevissthet som fortalte hun var
god nok—altså ikke best. God nok.

Homobevegelsen ville kalle uroen selvhat. Lett avviste hun tanken, for
de folkene var fanatiske. Diskresjoner plasserte de på hyller i et
lukket skap. Kravet homopolitikeren stilte til den enkelte var å lufte
skittentøyet, men de kjente ikke henne. Hun ville benekte ethvert
problem, stå rak og trygg på seksualiteten. Problemer fantes ikke, hun
sang. “Hvilken løgn?” Fortrengt var årene der sannheten lå uttalt i
stillhet, knapt hørbart. Først når hun flyttet sammen med kjæresten
måtte hun si fra til moren. Hun hadde ingenting å frykte, født som hun
var i en liberal ånd av en mor med haiketommel og lesbiske venninner,
den tyranniske broren og tanten satte standarden. Ytre friksjon var
fraværende for hun gjorde aldri noe galt. Før hun fylte fem år hang hun
med under armen da moren tok familien på haiketur til Tyskland. De
voksne søkte frihet, reiste på måfå, livet var moro og de levde en
frigjort revolusjon. Lesbisk var ingen skamplett. Barnet lekte, studerte
og gledet seg over alle muligheter. Fritt leide til voksenliv. Hun følte
seg en smule uoppdragen. Hun var ramm til å slippe impulsene løs. Dagen
hun bestemte seg for å bli samboer med en kvinne ringte hun moren.

Moren advarte.

“Det blir ikke barn av sånt.”

“Nei, mor.”

Det var aldri et valg. Gode karakterer på skolen, ett plettfritt liv,
det eneste tapet var fraværet av barnebarnet. Hun ville født de vakreste
barn. Dårlig samvittighet ovenfor bestemor lurte i bakhode. Broren
plantet en sønn til verden, tungt press forduftet, slekten var sikret.
Skeiv og stolt, og ingenting skjult eller fordekt. Det skapte aldri
problemer i familien å komme ut av skapet. Friksjon oppsto i
spenningsfeltet mellom hva som var mulig og smart. Hun lærte seg å
verdsette konservatisme der det lønte seg å leve i et skalkeskjul. Løgn
satt dypt, var uaktuelt. Intervju til en jobb tok for seg fakta:
Arbeidssøker, åpen lesbisk, samboer og høyst sannsynlig barnløs resten
av livet. Hun løp for å rekke resepsjonen og kom heseblesende til
intervjuet. Jobben eide hun ved å svare sannferdig. Frihet, seksualitet,
alt lå åpent og hun var den mest forutsigbare hun visste. Barrierene
viste seg aldri.

“Ingen er perfekt.”

Hun hørte seg selv tenke tanken. Selvhat, eller realisme? Erkjennelsen
om at det finnes andre der ute forskjellig fra henne, de fleste faktisk.

3
-

Revolusjoner forandrer alt. Makten mister tålmodighet og folket mister
hode. Hvordan det hele skulle foregå brukte hun mye tid på å tenke over.
Ingen behøvde å dø, folket lot seg rive med i en felles avsky mot
makten. Storhetstanker om hvordan den politiske agendaen snudde i en
profilert og velskrevet debatt ridde marerittet om natten. Livet var
tomt. Bloggen og stemmen på forumsdebattene veltet edder og galle,
budskapet vokste; hun ville la seg invitere til direkteoverført debatt
på teve og der var hun god, overbevisende og forførte alle. Store tanker
om å omvende og overbevise måtte være den ultimate drivkraft for å gå
dypt ned i materie og krangle til busten føyk. Hvor ville hun få en
invitasjon til samfunnsdebatt? Hun hadde en agenda å beskytte de svake
fra forsøplet propaganda og forfeilet humor på nettet. Omgitt av de
lekreste damer på hovedtavlen satt begivenhetene på forum i gang en
frenetisk plan å egle seg innpå via omveier. Hun kjempet valgets kvaler;
lurte bak hver sving hvem skulle bli neste kjæreste? Hun skulle videre,
treffe mennesker og døyvet smerten en kort stund ved å tenke store
tanker. Voldsomme turbulenser på forum fylte tomrom til hun var klar.

Hvordan livet på forumet skulle forandre ting framsto vagt. Drømmen
dreide seg mer om å gjøre inntrykk. De vekket følelser mer enn konkrete
handlingsløp. Hun drømte om noe ubestemt. Rachel var forelsket og
snakket i lav stemme på telefonen.

“Hvem er denne mysterie mannen?”

“Åh, ingen.”

Sa Rachel.

Når krangelen deres var et minne ville hun naturlig slippe til, detaljer
delt mellom venninner ville forsure tilværelsen. Hun takket seg til og
fokuserte på jobben. Samboeren vaket i en grå sky, for hver dag et
vagere minne, og blikket vandret oftere til Gaysir. Søket hun fant på
forhåndsdefinerte profiler hang høyt på hovedtavlen. Hva hun søkte var
ubestemmelig, uten at hun ville innrømme noe som helst villig.

Tomme ettermiddager i et singelliv fylte tidsrommet før sengetid. Timene
sklei. Teven gikk på tomgang—hun likeså. Forum var det livligste, kilde
til glede og ergrelse. Hun gjorde sine meninger kjent. Tråder i linjen
med hva-gjør-du-nå appellerte delvis.

“Lytter til KlemFM.”

Sa hun.

Radiokanalen for lett og glatt popmusikk skjemte ingen og fikk tiden til
å gå. Oppildnet, debatter vekket følelser. Få debatter smalt mer enn den
om monarkiet. Rachel fniste over hva mannen i andre enden sa og det
fantes bare en retning for det nyforelskede paret. Hun måtte lenger ned
i de mange irrganger på Gaysir. Flukten fikk til den bærbare
datamaskinen, skjermblink, debattråder og hun kopte mot hovedtavlen i
timevis. Hun finleste innleggene og ristet på hode i nittiniprosent av
alt hun leste. Homobevegelsen smykket seg med utallige monarkister.
Glitter og stas, selvsagt magneter for de homofiles nesegrus beundring.
Hun fant ingenting sjarmerende med de kongelige.

Hyllest til de kongelige, tre og fire innlegg smisket til systemet i et
åpenbart sjekkefrieri. Kongefamilien ferierte, fanskarer fulgte og hun
provoserte med viten og vilje.

“Av med hodet hans!”

Skrev hun.

Ladede meningsutfoldelser og finjustert retorikk egnet til å forskjønne
budskapet kalte forumdeltaker trolling. Hun likte å tale for den brutale
sannheten. Nivået på forumsdebattene var lavere enn forventet. Hun la
seg lavt, satte fokus på høflig, saklig og retorisk grep det var lett og
gripe—sjikane. Skapelsen av moderne paralleller var grep hun valgte i
retorikkens tjeneste. Vår kultur ser skrått på bedriftsledere med
gigantiske sluttpakker som får millionbeløp for å slutte i jobben. Hvis
direktørene unnskylder vanstyret med å peke på styret som bevilget
sluttpakken blir de likevel uglesett. Konseptet å få noe for ingenting,
en gave urett, unnskylder ikke mottaker. Hun trakk parallellene i
debatten og var godt fornøyd. Direktørene blir pepet ut, de kongelige
basket i fordeler, synlige og skjulte, uten plagsomme protester. Hun
resonnerte fritt og anonymt rundt monarkiets eksistens. Forum var stedet
for kreativ meningsutfoldelse.

Hun koste seg.

Hun kunne fortalt mer om hvordan debattene på forum vekslet fra et
absurde til pulsdrevet verkebyller. Retorikk er en øvelse, og hun valgte
den rasjonelle ovenfraogned stemmen.

Dagene sklei fra fjolleriet om hva folk spiste til middag, hørte på i
øyeblikket. Samfunnsengasjerte debatter om kongehuset, innvandring og
rasisme. Mest sannsynlig og et trist faktum er at ingen av debattene om
rikets tilstand vil være interessante for deg som leser. Jeg kan nøye
meg med å beskrive galskapen i kyberlivet og hvordan det omfavner,
pleier og aler fram fremmed angst i de mest rasjonelle mennesker.
Anonymisert og umiddelbart lokker nettets praterom fram primitive
følelser, stormkuler svirret og grep tak. Tunger løper løpsk. Primitive,
dype drifter bobler på overflaten. Alle kjenner igjen egenskaper i gode
nettdebatter. Samtaler i nærhet finnes overalt og kan like gjerne være i
den alminnelige dagspresse, personlige blogger, eller på et forum for de
homofile og lesbiske. Hun var ikke på Facebook, og tok ordet kun
sporadisk i avisdebatter. Aldri før hadde hun valgt et ståsted som på
forum. Innlogget manet hun fram intensitet i timelig besøk, stadig
påfyll fra en stemmeskingring på kanten av det dannede. Vekslinger.
Jabber. Kommentarer med tommeltotter. Ikke som på Facebook med
likerknapp. Tommelen gikk opp og ned, og alle kunne se stemmene, lik
keiseren på Colosseum lyste fra podiet. Hun påpekte forskjellen for den
som leste måtte forstå det var avgjørende hvor debatten fant sted, tone,
stil og følelsene de vekket. Forum var et hjem, et lukket rom med
mennesker du like gjerne kan treffe på byen i helgen. Selv anonyme
gjenkjente hun i fraser og uttrykk.

Oslo er en landsby.

Hun var sint, i opposisjon til det politisk korrekte i et landskap fylt
av homofile og andre lesbiske. Gaysir var stedet alt skjedde, hvor folk
traff hverandre på likefot. Hun var seg selv. Emosjonelt i ubalanse,
sint og singel.

Poenget var ikke å skjule seg bak anonyme enavn. Hun ville vise sinne,
indignasjon, følelser og ståsted. Stedet var i skyggen for jobbsøknader
og fanget aldri opp dialogen i forum. Profilteksten på Gaysir var skjult
for arbeidsgiver, familien og utenforstående. Sirkelen utenfor periferi,
heteroer og andre kunne umulig forstå hvor lukket og klamt alt virket.
For den norske homoen finnes det ikke et mer gjennomsiktig landskap enn
Gaysir. Bare en ting visste hun, å skrive enigheter gjorde ingen mening,
og debattere friksjon og vise individet bak enavnet var verdifullt. Hun
følte ingen skam, sto rak, kjente etter og mente hvert ord hun skrev.

Retorisk, dempet, rasjonalitet til underholdning. Forum slapp løs alt,
og mer. Sterkere følelser og ærlighet. Hvem var hun? En sint lesbe,
akademiker og emosjonell.

Alt var lov, dragningen mot å si hva som gjelder og debatterer i god
tro. Åpen for å omvende, tankene lå i bakevja og skurret. Nettdebatt
krevde sterk lut. Gaysir var familien hun ønsket på laget, søkte aksept
fra, visdom hvis mulig. Likesinnede visste hvor galt det sto til i
nasjonen Norge. Forum slapp løs dyret. Hun omfavnet nasjonen i systemtro
besinnelse. Monarkidebatten friskt i minne samlet hun krefter, holdt
tilbake og ventet på angrep fra alle kanter. Standpunkt sto tydelig,
klare for å bli angrepet og forsvart. Urettferdigheter gjorde henne
kvalm.

Smil i munnviken. Distanse til argumentene. Hun tas ikke alvorlig. Hvor
rasjonelt er en knapp som tillater bruker å riste opprørt over livets
realiteter.

Mennesket lukket øyene der det var komfortabelt å snu ryggen. De nektet
å kjenne premissene for urettferdighet. Blodtrykket steg, pulsen banket,
ubehaget hun følte var følelsen av fortapelse. Mekanismene for vår tid,
skriftlig og muntlig protester manglet sanksjoner mot vedvarende
urettferdighet. Skjelven grep hun tastaturet for å svare skrullene. Hun
møtte unison kritikk. Omkranset av homofile menn lå demokratiet brakk i
en stakk stund, og hun innså en tid der paljetter og moro er mindre
viktig var eneste redning for fornuft. En annen tid. De kongelige mistet
appell hos barnløse rumpefolk når det ikke lenger var gøy.

Skansene for å omvende på forum var høye, sjansene og lykkes marginale.

Sinne var en reaksjon på likegyldighet; en falsk tilstand,
livsløgntilfredshet uten vilje til endring. Hvordan det er mulig å
elske; drive forover og samtidig holde fast ved alt som er galt, nekte å
kjempe mot? Groggy oste politisk korrekt. Han stilte saklig spørsmål, og
svaret var like rasjonelt. De brukte samme maske. Tross fornuft gjorde
Groggy hva alle skruller gjør og konkluderte annerledes. Han var bastant
tilhenger av både konge og fedreland.

Hun begynte å ane forum handlet om noe annet enn debatt og
meningsutveksling.

Forbehold manglet og Groggy sto med de andre kranglefantene i
debattråden på stedet hvil. Steile holdninger var nærmest et valg av
livsstil på tvers av hjernen. Debatten var fåfengt. Her rykket ingenting
fra startblokken. Hun sto alene.

Republikken lå død i homseland, og debatten var brakkvann i
kjedsommelighet, dødt og stillestående, egnet kun til å trekke fluer.

Hun viste verktøyene i verktøykassen, ga alt i ektefølt engasjement. Hun
forsto hvorfor, erkjente statusen singel forklarte mye av ståket hun
genererte. Hun var der for å bråke. Fraværet av rasjonalitet i debatten
skar sjelen og skapte uro—rent av ubehag. Resonnementet holdt ikke,
ingen lyttet. Heller stormet aggressive forumstropper mot henne og
galopperte over alt hun ville si. De veltet seg i selvfølgeligheter og
forventet å få passet påskrevet. Hun endte opp og stå skolerett. Ronda
heiet på Groggy. Groggy heiet på Ronda. Ingen heiet på henne. Runddans
der fire profiler på forum kastet møkk, og hun sto alene, omgitt av
sinte og påståelige skruller. Trengt i et hjørne var det bare en ting å
gjøre.

“Jeg gir opp.”

Sa hun. “Jeg forlater gospelkoret og bruker heller tiden på andre
debatter der rasjonelle argumenter har gjennomslagskraft.”

Hun tok seieren. Valget var låst.

Anerkjennelse og paralyse. Ronda sendte et smileikon, indikerte
nederlaget. Han forsto utspillet endret debattens karakter. De
diskuterte ikke lenger monarkiet eller republikkens framtid. Groggy
frettet og forsto definitivt ingenting. Tullingen proklamerte seier og
hevdet hun hadde tapt når faktum var hun vant. Groggy kalte henne feig
og ville krangle mer. Fire skruller sto samlet, de enset ikke rasjonelle
argumenter og var forskrudde. Dvele var poengløst. Hun hadde ingen valg.

Begripelige kulturer belønner begrunnede standpunkt.

Lyspunktet i rask exit var Ronda. Han sendte en personlig melding med
honnør for god krigsånd. Hun hadde sett han før, fulgt debattene og
gjenkjente en voksen og artikulert person. Han tilhørte de mest aktive
forumshorene. Han følte nederlaget. De led, skruller med en
forkjærlighet for paljetter forsto viktighetene av de ensomme stemmer.
Hun trakk minoritetskortet og i forum var det alt som betydde noe.

“Hatten av.”

“Takk.”

“Du kjempet godt.”

Følelsene fra frontkonfrontasjonen la seg med smileikonet. Hun rakk ikke
å reflektere over hvor opprøret kom fra. Året hadde ikke vært uten
lyspunkter. Tristesse kom i fraværet av håp. Rachel gikk på hemmelig
deit, og hun fløt i tomrommet minst like skamfullt. Hun søkte følelser,
meningsutfoldelse og temaene på forum eksploderte i selvet. Krasse ord,
uvøren stil og ordspill.

“Jeg ble sjokkert av Groggy.”

Erkjente hun for Ronda.

“Hvorfor?”

Ronda var en meningsfrende og hun stolte på ham. Han var ikke langt fra
Groggy i alder og mening. De kjente hverandre. Viste hvem den andre var?
Hun følte slektskapet og gjenkjente forskjellene. Politisk korrekthet
var en lammende merkelapp på en homofil. Ronda var en bekjent av Groggy,
viseversa, og nettopp derfor aksepterte han dommen.

“Groggy har mye politisk korrekthet i seg.”

Sa hun.

Erkjennelse og ære for seieren lå dypt. Hun sugde til seg berømmelse.
Ronda bøyde aldri av i eget standpunkt, følelsene satte han til siden,
og heller lot han rasjonalitet vinne analysen. Hun respekterte skillet.
Macho, kroppsatlet og kranglefant leste hun fra profilen til Ronda. Hun
dannet seg et bilde av mannen i singlet og trent brystkasse.
Kroppskultur var viktig for ham, som mange menn på Gaysir, og likevel
var han mer presis enn Groggy. Intelligent, artikulert og voldsom. Hun
leste ham og fant et menneske som levde.

Han var en gladgutt fra Stavanger der Groggy virket grå og forstenet.
Hun møtte kjempen i vennskap. Debatten fylte en funksjon, kretset i det
homofile og lesbiske rom for likesinnede, slik sosiale medier skal
gjøre. Han smisket. Begge sto fast på eget standpunkt, stolt og
rakrygget, ingen ville gi opp. Gjensidig respekt i uenighet var lykke på
forum. Kontrollspørsmålet friskmeldte ham, en sjekkliste hun brukte til
å plassere relasjonen og sikre de forsto meningsutvekslingen riktig.

“Jo da, du vant.”

Ronda forsto. Groggy furtet og sa det var hun som flyktet, mente
antallet stemmer for monarkiet avgjorde.

Hun og Ronda ville debattere igjen, og Groggy kom til å seile i egen
sjø. Helst fektet hun med kjempen i singlet, heller enn å fordampe de
skarpe observasjonene i smisk og hengis til årevis med skyggeboksing.
Debatt måtte være transparent og logisk. Fornuft var idealet. Øvelsen
ville hun påstå. Uenighet besto. Ekte kranglefanter fant glede i andres
nærvær. Han var ingen amatør, hadde ikke falt i gulvet som barn,
intellektet til den aktive kjempen på forum skilte sak og person,
reflektere på tvers av standpunkt og ga retrett når slaget var tapt. Han
gjenkjente nederlaget og det irrasjonelle i eget standpunkt. Hun fikk
honnør for å ha eksponert dem. Tydelig frontet Groggy samme standpunkt,
og samtidig var Ronda alene i å vise respekt.

Hun fant en sann venn i Ronda.

Nettverk klang fint på nittitallet. Slutten av tiåret ga verktøy til å
pleie vennskap perifert. Rasjonelle grunner for hvorfor nettverk var
smart ville alltid være der. Internettet var vanskelig å beherske i
starten. De med kunnskap fikk jobb straks de kodet nettsider og flere
bygget karriere på siden av utdannelser nedskrevet på vitnemålet.
Tidsalderen ga uante muligheter for nerdene. En gutt hun kjente la
pekere til hjemmesider og kalte listen *Jentemagneten*, og det ville
normalt være en ære å henge høyt. Gutten plasserte henne på femteplass,
bildene i fotoalbumet gjorde susen og i løpet av dager besøkte over
firetusen hjemmesiden. De beskuet en oppstyltet curriculum vitae og
fotoalbumet hun laget fra et par hytteturer og en tur til London.
Guttene likte nettsiden, og ukene på listen plasserte henne blant
landets raskest voksende nettsider.

Relativt fort følte hun verden var et surrealistisk annerledes sted.

Moren forsto ingenting av den nye teknologien. Hun mobiliserte via
epostlister studentvennene til middagstreff i kantinebygget på Blindern,
og samtidig manglet moren epost. Moren jobbet for landets ledende
telekommunikasjonsselskap hvor de skrev brev med elektriske
skrivemaskiner på gjennomslagspapir i fire eksemplarer. Teknologien kom
altfor raskt, alle forsto såpass. Hun grep mulighetene som en blant
mange.

Innovasjon var stikkordet. Syklubben tok raskt grep. Nettverket av
homsegutter møtte hver onsdag. De drakk øl og snakket. Syklubben var
stedet for sjekking for de unge mennene, akademikere i jobb eller
studier. Kapable menn med gjennomslagskraft og framtid, framdrift og
pågangsmot. Hvis hun var homofil gutt ville Syklubben vært stedet.
Tidlig opprettet klubben en nettside for medlemmer, først for å samle
påmeldinger til onsdagstreffene. Syklubben investerte i en datatjener og
den grå uanselige boksen åpnet festen. Det var øyeblikket i tid da
teknologi tok over og kanaliserte samtalene i Oslos homoverden.

Realiteten var datatjeneren låste seg bare timer etter Syklubben
igangsatte eksperimentet. Gaysir spebarnet knelte for etterspørselen.
Homsene tørstet etter dialog. Aksjonærene investerte i nye datatjenere;
flere bokser og nettsidene trakk brukere. Gaysir ble pyntet og hang
prangende til skue. Generalforsamlingen løftet vennegjengen til et
fungerende dåttkommselskap, en bedrift der eierne var ansatte.
Aksjonærene var en vennegjeng og de med penger spyttet inn noen kroner
til investeringen. Mennene satt ved langbordet og drakk pils da
framtiden ble vedtatt. Nettverket deres trengte ikke å møte fast
onsdager. Brukere logget på internett, bygget profiler og laget egne
avtaler, hele uken, der alle tidspunkt var lov. Begynnelsen av
årtusenskiftet var tid for nettverksbygging. AIDS drepte ingen i Norge.
Ingen folk hørte om. Medisiner fungerte. Promiskuøs sex utfoldet seg på
nytt i Oslo. Helseutvalget delte fritt gratis kondomer, fem hundre i
uken, saunaer lå åpne og partnerskapsloven føltes gammeldags. To sammen
i et parforhold var unntaket. Programmerere og systemarkitekter fylte en
like viktig funksjon som dommeren i tinghuset. Alle bidro og gjorde hva
de kunne for å bygge tilbudet på Gaysir.

Trym hadde hovedansvaret for utvikling. Nettstedet gikk på luften fra
stuen hans. Sjel og stahet bygde forum. Trym elsket jobben, stillingen i
navet. Gaysir var sentrum og oppfylte drømmen til en ung mann viss evner
lå hakket under supernerdens posisjon.

Trym tok rollen.

Andre solgte Gaysir til et marked. Syklubben tok form på dugnad. Motoren
var datatjenere selskapet kjøpte for aksjonærenes penger, samt kodene
Trym skrev sene nattetimer. Gaysir ble født i stuen til Trym og vokste
raskt til å bli ryggraden i Syklubben.

4
-

Hun var en forumshore.

Feministen i henne likte ikke ordet hore, og under tvil bar hun
merkelappen. De andre synes ikke å bry seg.

Ronda tronet i så godt som hver tråd, mens hun hadde begynt å gjøre seg
gjeldende. Debattene på forum var egnet til å overraske og underholde.
Hun skydde de useriøse debattene og lette etter steder å briljere. Han
debatterte mindre eksklusivt. Sjelden fant han det å henge tiltaksløs på
hovedtavlen bedre enn å diskutere. Hun derimot var i ferd med å oppdage
den andre siden av Gaysir, søket der profiler stirret på hverandre og
undret hvem som lå skjult bak profilbilde. Titalls tomme blikk som aldri
skrev innlegg på forum. Flesteparten av dem fulgte ikke en gang
diskusjonene.

Hun logget på forum oftere. Nysingel var det viktig for henne å delta,
flagge meninger og strutte fjær. Blikket ville vandre når hun var klar
for å møte andre, og debatten var bare første hinder på vei tilbake til
liv.

Mennene på forum var i flertall.

Hun fryktet ikke konfliktene, testosteron overdose. Deltakelse på forum
var nok i første omgang, helt til en stemme fra dypet dukket opp med et
spørsmål som vekket familiaritet. Kvinne ville vite om hun bodde på
Sagene? Kvinnen må ha gjenkjent noe—henne.

“Kjenner jeg deg?”

“Nei, jeg husker deg fra Roterommet.”

Sa kvinnen som kalte seg Fab.

Hun hadde helt glemt kvinnenes variant av Gaysir. Roterommet lå øde,
tidligere var datatjenesten stedet for lesbisk kvinner. Hun foretrakk
Roterommet. Hun unngikk Gaysir, ville ikke ta i tjenesten deres med
ildtang. En håndfull travere tjattet på samme tid hver uke, de holdt
stand, og nettadressen til Roterommet lå aktiv på internettet. Hun
husket romlingene debatterte i nye tråder hver dag, men livet der inne
stivnet. Henvendelsen vekket minnene fra fortiden, uten at hun husket
enavnet Fab, eller gjenkjente avatartegningen kvinnen brukte til
profilbilde. Det uskyldige spørsmålet utløste skredet av gjenkjennelse..
Hun ble nysgjerrig på det fremmede menneske skjult bak en strektegning.

Hun forsto ikke hva slags galskap spørsmålene utløste før mye senere.

Inntil videre lot hun være å slette meldingene, ignorerte dem.
Spørsmålet ble stilt og skulle hun tro dem tilhørte kvinnen en tid der
hun faktisk deltok på nettet med det for øye å forføre. Tiden var kjent,
tilbakelagt og glemt. Fab brakte alt tilbake.

Roterommets trygge havn tok over livet. Hun husket såpass. Gaysir ble
bygget på guttenes premisser, invitasjoner til åpen sjekking sto i
kontrast til jentevarianten som handlet om å samle folk til fest,
utflukter og søke fellesskap blant lesbiske. Søkte hun deits var det
ofte mer å hente på Gaysir der avtaler kunne spikres på dagen. Hva
skulle skje på deiten kunne de avtale på forhånd. Sextreff føltes
unaturlig på Roterommet, bare ideen om at det var mulig skremte.
Roterommet var trygt. Hun holdt seg der, og trakk seg ikke ut før det
ble kleint, eller hun fikk kjæreste. Hun husket ikke hva kom sist.
Kjæresten hadde ingenting med Roterommet å gjøre, men var som alltid en
kropp hun traff blant folk.

Fab minnet om fortiden, tilhørte en ukjent rest fra Roterommet. Bak
enavnet satt en person og studerte forumshorene og visste hvem hun var,
gjenkjente og husket. Hun ble sett. Forumet til romlingene skilte seg
fra Gaysir. Kvinnen må ha tilhørt de unge, gruppen hun sjelden ga tid
når fokuset gikk til kvinner i egen alder. Kvinner hun kunne være
sammen, ha som kjæreste.

Hun visste ikke hvem kvinnen fra fortiden var, men Fab visste om henne.

Spørsmålene haglet, og for hvert spørrende pronomen lærte hun ting om
spørsmålsstiller. Undringen vokste. Sirklene inn til kjernen i en sjel
går rett og spørsmålene vitnet om intelligens. Stemmen irriterte. Kun
sporadisk slapp kvinnen løs små trivia om seg selv, svært lite og nok
til at hun lurte på hva svaret ville vært hvis hun sendte spørsmålene
tilbake. Fab åpnet for å gi mer, ertet og ga obskure detaljer mellom
linjene. Invitasjonen til å følge opp lå åpen. Ubehaget tirret, og for
førstegang på veldig lenge googlet hun eget navn og forsøkte finne hva
var kjent der ute på nettet. Nysgjerrighet ville tippe situasjonen.
Seieren forutsatte kjølig distanse, og det hadde hun i mengder.

Hun var der for å debattere på forum, men bestemte det var på tide å
varme opp til henvendelser. Kvinnen stilte spørsmål og gikk rett på
henne.

“Hva heter du, så kan jeg google deg tilbake?”

Sa hun og ventet.

Kvinnen ble stille.

Hun bestemte seg og ville ha svaret, satt musestille og ventet. Nå var
det hun som jaget, alle spørsmålene måtte få et navn og et bilde. Hun
ville vite hvem skjulte seg bak enavnet.

Tålmodighet er en dyd, i det minste var det en beundringsverdig egenskap
da hun vokste opp, og disiplinen mestret alle i hennes familie. De var
gale alle sammen.

“Jeg er googlet.”

“Selvsagt.”

Søk på bosted og navn førte direkte til hjemmesiden. Sånn var det å ha
et fornavn med klangbunn—spesielt og sporbart. Fab visste hva hun het.
Spørsmålene fortalte nok. Hvor kvalmen dannet seg i kroppen visste ingen
sikkert. Hvilket nysgjerrig pek veltet og gjorde Fabs snoking til følt
overtramp. Begge måtte gi seg tilkjenne skulle de gå videre derfra.

Hun ventet tålmodig. Forum gjorde ventetiden tålig.

De lesbiske gikk forsiktigere fram og ingen på forumet utmerket seg som
spesielt frampå. En ung jente studerte i utlandet og brukte debattene
til å holde kontakt med Norge. En annen kvinne, bare et par år eldre
skrev blogg og brukte tiden til overs på forum. Ordspill, sitater og
morsomheter var sjeldne—spesielt fra den kvinnelige befolkningen.
Babytoy gjorde et framstøt, vrei antydninger, vågale tolkninger og
stemmen hoppet fram på skjermen. Babytoy flørtet på høyt gir.
Faresignalene lyste.

“Gamle lesbiske hespetre.”

Mumlet hun for seg selv.

Vulgære tolkninger var aldri langt fra vokabularet i debattene når
Babytoy dro til og brukte fraser for voksne. Språket var direkte og
brutalt, tiltalte ikke henne.

De retoriske knepene som gjorde det lett å sjalte ut standpunkt fungerte
i debatten på alle bortsett fra sånne som Babytoy. Traktorlesben
tilhørte de useriøse debattantene, og hva verre var, hun skrøt gjerne av
faktum. Svaret på en tråd kunne hun like gjerne være skrevet i søvnen,
og ramset opp kveldens teveprogram. Hun mente aldri å sende skjelmske
blunk, eller noe som kunne bli oppfattet slik.

“Jeg vil pule.”

Sa Babytoy.

Her forsto hun hvor viktig det var å trå forsiktig. Førstegang hun
befant seg på samme side som Babytoy sklei kommentaren over til å bli
denne ublu invitasjonen, en strøm alfabetsøppel det var umulig å
kommentere. Hun ville ikke gi gale signaler.

Deltakelse på forum økte spenningen i livet. Sjekkeleken ville hun
spille senere, når bruddet med samboeren lå et stykke bak i tid.
Målbildet måtte friste, og Babytoy vekket vemmelse. Stemmene på forum,
spesielt de kvinnelige sjokkerte med sitt blotte nærvær. De var ofte
mørkeblå, liberalistiske offerlam. Hun spilte en rolle på forum, og der
ble tilværelsen endret hver dag. Meldingene fra Fab gjorde dagene
lettere og forum mindre viktig. Babytoy sto i grell kontrast, røykte og
skremte livet av henne. Forsiktig tilnærming måtte harmonisere og finne
et likevektspunkt. Hun tenkte mye på når hun igjen ville se seg rundt og
møte livet åpen for kjærlighet. Alderen tynget. Mulig hun var klar til å
møte sjekkemarkedet før forventning. Klokken løp løpsk og kroppen
forfalt. Komfortsonen lå i debatt. Personlige meldinger ga hun et
vennlig og kald avvisning.

“Jeg er her for forum.”

Babytoy var på forum, og hadde ikke fått med seg det var et skille
mellom de som hang på tavla for gjennomskuing og kranglefanter, slike
som henne. Babytoy brydde seg ikke.

“Jeg vil bare debattere.”

Hun hørte munnen si ordene hun mente og skrev ublygslet.

Hun bestemte Fab var kvinnen det var verdt å kjenne. Fab var fra Oslo og
hadde ballen i eget hjørne. Venteleken trillet skillefaner, hun bladde
på nettet og vekslet mellom debattråder. Hun lot Gaysir underholde.
Beslutningen var tatt, og hun skulle gi Fab full oppmerksomhet straks de
fikk kontakt.

“Hvem er du?”

Hun ville stille spørsmålet på nytt, men smurte seg med tålmodighet. En
dyd hun behersket. Døgnet gikk og neste dag fikk hun svaret.
Virkeligheten strekte seg inn i forumet og hun jaget et bilde, en profil
hu kunne rette all overskudd, energi og fokus, de siste dråper sjarme.

Kvinnen stilte betingelser for å gi fra seg eget navn. Hun aksepterte
villig, og mente å respektere ønske, ett løfte hun holdt lenge. Hun
startet med et kort søk, et grafs nærmest for å bekrefte kvinnen
eksisterte. Fab tilhørte virkeligheten. Hun visste det lå et løfte om
mer. Bildet hun likte fant hun i en lokalavis og alt kvinnen hadde sagt
stemte. Avisen førte et intervju i helfigur og hun festet seg til et
beklemt smil. Hun trengte ikke å vite mer. Ubehaget forsvant, hun visste
nok om Fab og kunne si med sikkerhet at bak enavnet fantes en person.
Alle de andre svarene ville komme, for hun valgte tidlig i livet å møte
folk med åpent sinn.

Forum spilte en rolle.

Er det ikke typisk at ny iver for alt som er å debattere på forum vokste
da hun ble singel? Debattene hadde fylt en funksjon, og hun flyttet
fokus.

Spørsmålene snudde, hun ville vite mer, men var fornøyd med et kort søk.
Fab var en identifiserbar kvinne, en sjel og kropp i feltet.
Musebegjæret hadde våknet. Erkjennelsen om at hun var på jakt satt dypt,
og kvinnen utløste et skred enkle spørsmål og åpnet skrinet, undertrykt
bevissthet kanaliserte energi bort fra kranglefantene på forumet.

Hun sendte nummeret til mobiltelefonen og ga kvinnen beskjed om å ringe.

Nøling var aldri et plagsomt trekk i personligheten. Hun ville snakke
med Fab, nysgjerrigpetra, tjueniår ung—for ung, utenfor
tiårsbackupregelen hun satte for en partner.

Hun ventet et døgn før Fab ringte. Tiden brukte hun på å gjøre et
grundigere søk. De samme nettsidene skilte seg ut. Hun leste dem, nøyde
seg ikke med å studere bildet. Hun satt og ventet på en telefon fra et
fremmed vesen, en rar forvirring vaket ì bakgrunnen, og i siste liten
søkte hun med alt hun hadde. Hun søkte på telefonkatalogen og fant Fab i
Lillestrøm. Geografisk bodde kvinnen langt utenfor komfortsonen. Det var
første avviket fra profilen og resultatet trigget angst for flere
løgner. Hun søkte mer intenst, snokesider samlet opp informasjon og hun
tok skrittet for å være sikker. Like før Fab ringte fant hun en
sjokkartet detaljer, og det første hun konfronterte kvinnen med var
løgnene.

“Hei.”

Hun likte stemmen.

“Jeg googlet deg. Nå! Du er tjueåtte år?”

“Jeg fyller år i juni, men lot datoen stå på 1. januar.”

Alder var et følsomt tema. Hun merket på stemmen til Fab svaret trådde
varsomt for å gli framover, og hun likte å bli møtt med forsiktig
skepsis. Aldersforskjellen bekymret, og hun fant ro i sjelen når hun
tenkte over hvor flott det var at personen i andre enden tenkte på samme
måte. Fab var normal.

“Stor aldersforskjell har jeg oversett før.”

Sa Fab.

Hun var gammel. Fab visste hva det ville si, hadde vært der før og
fortalte om en ekskjæreste på samme alder. En kort stund var alt greit.

“Hva med deg?”

Hun hadde vært nesten ni år eldre enn samboeren. Hun sleit hele første
året, deretter bestemte hun seg og tenkte aldri mer på
aldersforskjellen. Hun tenkte Fab ville være på samme måte.

“Alder behøver ikke bety noe.”

Løy hun.

Hun levde syv lange år med samboeren. Hemmeligheten var å slutte å tenke
på forskjellene og bestemme seg for en ting. Hun bestemte seg igjen.
Stemmen trakk henne inn. Hun måtte slippe løs følelsene. Fab var beviset
på liv hun ville utforske. Stemmen var myk og forsiktig. Hun svarte på
spørsmålene og helt til slutt var det ikke flere spørsmål å stille, bare
anklager.

“Du i Lillestrøm, ikke Oslo.”

“Ingen ville snakke med meg når jeg skrev Lillestrøm.”

“Nei, nettopp!”

Fleip i toneleie og hun håpte nyansene var tydelige. God humor bygger
alltid på en dæsj sannhet, og de ville aldri sett hverandre hvis ikke
bosted Oslo sto listet på profilen. Hun forhåndsinnstilte søket på
hovedtavlen og sjaltet bort alle profiler fra distriktet. Hun gjenkjente
dilemmaet. Alder var den store løgnen, men ville ikke komme i veien.
Stemmen til Fab var flott, målrettet, struttet av selvtillit og den
jaktet på henne. Hun likte å bli jaktet, svare på spørsmål og gi etter.

Flyte og nyte—forelskelsen.

Frontangrep i en debatt om surrogasi var kontroversen hun ville unngå på
forumet. Debatten hardnet og det gamle tema ble aktualisert i det
offentlige rom, og alt hun ønsket var å bli bedre kjent med Fab.

Kalenderen var fylt hver kveld. Hun gjorde altfor mye i jaktsesongen for
ingenting fikk endorfinene til å boble mer enn aktiviteter utenfor
hjemmet. Stemmen i telefonen roet, hun behøvde ikke si ja, si noe som
helst. Hun bestemte seg for å jage Fab når sannheten var fangsten lå
skadeskudd, og det var henne. Alt jentebarnet fra Lillestrøm hadde gjort
var å stille en endeløs flom spørsmål i dempet og mild toneleie mot
kadaveret som sprellet forgjeves. Lørdag kunne hun ta imot et stykk
gjest, sent fordi hun hadde avtaler hele uken. Tiden fram til lørdagen
var fylt med gjøremål, og hun trengte tid til å vaske, gjøre overflatisk
flytting av rot og elementær kroppspleie. Hva var mulig, eskene på
soverommet ville hun ikke rekke å fjerne før helgen.

Travelt, hun hadde det altfor travelt.

Forum inviterte til ekstreme standpunkt, ingenting imellom. Skalaen for
meningsytring tok ting lenger og tematikken var spredd. Hun struttet i
all prakt for dem som ville ha fornuftige mennesker i et skisselandskap
fylt med metaforer og hårreisende vyer. Deltakelse på forum var lettere
enn å danse rundt grøten i et vagt og uklart scenario. Ta stilling mot
surrogasi var kontroversielt og hun tillot seg å fråtse i meninger når
debatten våknet. Det var ingen liten sak. Homo-Norge var på krigstid mot
en provokatør for moralfilosofien, homofobien fikk et ansikt de kunne
hate i en kvinne som stokket ordene i usammenhengende stotring og ivret
mot ekteskapslovgivningen. Argumentet mot den radikale ide den lesbiske
partner var sidestilt juridisk mor fant hun i den biologisk funderte
moralfilosofien. Homo-Norge latet som om det lå teologi i bunnen, tro og
iboende hat fra religionens sfære blandet med stygge personangrep tok
form av mobbing. Aggresjonen i debatten skremte, hun låste begrepene mor
og far til biologien, og aldri før var det viktigere å si imot
kjønnsteoretikernes redefinering av en virkelighet for politisk gevinst.
Mobbere ville hun aldri støtte, og når tonen hardnet valgte hun å skrive
innlegg til støtte for moralfilosofen. Markørene i forumsdebatten
forsvarte status quo. Hun struttet med fjærene, mer selvsikker enn de
fleste når hun gikk til krig mot selvgodhet i den politisk korrekte
periferien.

Var hun arrogant når hun kriget mot makten? Litt, utvilsomt.

Hun hadde ingen illusjoner om å endre noe, og tok ordet for å markere
standpunktet for det biologiske perspektiv utfra et ideologisk ståsted
hun fant samsvarte med den lesbiske identiteten. Homobevegelsens
radikaliserte ledelse undertrykte legitime innsigelser og tok henne til
gissel.

“Se på meg. Se på meg.”

Standpunktet hun og moralfilosofen delte ble representert på Gaysir. Hun
var en femtekolonnist i opprør.

Minustomlene raste. Hun ble en av fire profiler på forum med flere
minustomler enn plusstomler. Hun leste konsekvensene fra de to debattene
der temaet surrogasi beseglet holdningen til topplisten i form av
tommelstatistikk. Trym testet topplisten før lansering. Hun skrev
innlegg, ikke et eneste partsinnlegg mot surrogasi skrev hun uten å
tenke over livet som singel.

“Jeg kommer aldri til å ha sex igjen.”

Tenkte hun bekymret.

Innlegget måtte skrives. Hun gråt på sofaen hjemme når hatet mot
moralfilosofen vokste. Meningene hun i essens delte ble slaktet. Hun
ville aldri sanksjonere mobbere, aldri før var det viktigere å skrive,
stå imot, vise det fantes lesbiske stemmer mot surrogasi.

“Jeg kommer aldri til å ha sex igjen.”

Tanken rystet, kom tilbake gjentatte ganger og slapp ikke tak. Hun skrev
og det flommet fra tastaturet. Hun ville ikke stoppe, kunne ikke la
arenaen være fri for dissidenter. Homofile og lesbiske mot surrogasi
trengte representasjon. Homopolitikere skulle ikke ukritisk få
reformulere det biologiske begrepsapparat. Og for hva? Et egoistisk
ønske om barn, alt på de voksnes premisser. Spørsmålene var for viktig.
Hun kunne ikke tillate seg selv å planlegge med tanke på hva gjorde
henne attråverdig i en lesbisk populasjon politisk korrekte hysterikere.
Den personlige integriteten krevde tydelig standpunkt i en debatt som
tok plass i avisene og der talspersoner kidnappet de homofile stemmene.
Moralfilosofisk hamret de løs på med sitater fra luften og måten det
ikke ble sagt.

Mobbing ville hun alltid bekjempe. Siden bermen i skolegården rettet mot
henne i fandenivoldsk ondskap hatet hun mobbere og sverget til de frie
stemmer. Det var ingen vei utenom.

“Hun ble sett. Dessverre.”

Debatten på forum, også den hissige om surrogasi, stoppet i korte
perioder og deltakerne gikk sammen for å diskutere tommeltottregler og
den nye topplisten Trym testet. Han ville ha innspill på den ferske
funksjonaliteten. Statistikk avslørte det triste bildet. Enkelte trakk
til seg minustomler og det blotte nærvær, abstrakte begrep, hun trakk
til seg blikk uavhengig det skrevne. Kranglingen definerte profilen.

Fab stirret, fulgte våken debatten, stilte spørsmål hver dag, men fulgte
den andre henne. Hun ba om å bli sett og det samme gjorde de andre.

“Du har vært veldig aktiv i det siste?”

Balletten rundt grøten var spenning i hele kroppen hun slapp løs på
forum. De nyeste profilene møtte vennlige gester og hun fikk ekstrapoeng
for nærværet, men bare hos Fab. Debatten mot surrogasi fjernet siste
rest godvilje. Pasjonert bølle, ivrig og engasjert, blottet for det
empatiske gen tok hun forumet for hva det utga seg for å være, arnested
for homofile og lesbiske kranglefanter, likesinnede uten hauger
følelseskald bagasje. Hun debatterte for å misjonere når sannheten var
alle pålogget hadde et ønske om å treffe en sjelevenn. Hun skilte seg
ikke fra de andre.

Hun traff Fab i forumet. De snakket i telefonen og hun skulle møte
kvinnen bak enavnet. Ikke lik en deit, annerledes. Hun var forelsket.

Det var en feilkobling, hvilket hun innså altfor sent.

“Debatt.”

Tenkte hun. Der gikk hun i fellen.

Hun trodde altfor lenge forum først og fremst handlet om å debattere i
betydningen å hente opp argumenter og prøve ulike ståsted mot hverandre.
Sammenlignbart med avisenes debattsentraler.

Ingen ville vite hva hun mente.

Hun ønsket knapt nok å vite hva hun selv mente. De andre kvinnene på
forum tok en posisjon, og de ville heller ikke vite hva hun mente.
Hvilken kvinne var viktigere enn hva slags kvinne, og alle de som
dristet seg til meningsytringer besøkte hun. Profilene hun studerte ga
ingenting. Nysgjerrigheten vokste. Tiden logget på Gaysir forandret
henne.

Krangler gjorde mindre vesen, for langt viktigere ble det å vise seg
fram. Skrive noe for å skrive noe. Hun leste og ble lest. Minustomlene
vitnet om engasjement. Hun kunne ikke unngå dem. Fab kastet seg inn et
par ganger, men sa aldri noe kontroversielt. Tommeltotter var også
deltakelse.

“Vi bor her inne.”

Sa Fab.

“Ja.”

Utover kveldene satt hun og telte minutter, timer og dager.

Det var hyggelig at Fab plasserte eget standpunkt nære, opptil henne.
Hun drev spørsmålene, provoserte og satt på spisse etter forbildet.
Innimellom var det bare dem, og hun fortalte det meste, om jobben og
hobbyer, livet på overflaten tilgjengelig for enhver som søkte på
nettet. Livet foregikk foran tastaturet. Veien til Gaysir var for kort,
farlig kort. Følelsene tok over og arenaen åpnet for nye opplevelser.
Hun var ikke der for å debattere. Fab avslørte henne. Hun innså tiden
for gjemsel var over. Prisen for å delta i debatter var minustomler, og
det byttet virket helt greit. Tomlene var kvalitetsstempel for menige
medlemmer. De overflatiske ville ikke se henne, de måtte tvinges til å
forstå hun var en premie.

Fab forsto.

Aldersforskjellen stemplet. Var hun blitt en *cougar?* Hun mistenkte
ansvaret for å avslutte dialogen lå tyngre hos henne. Fab truet å møte
der hun var og bli med hjem. Redselen for å bli tatt av en ung kvinne i
slutten av tjueårene slapp ikke tak.

Hver kveld snakket de i telefonen. Hvis Fab ble med hjem kom de til
leiligheten fylt med minner, skrot og møkk. Samboeren hang igjen i
bokser og innbilte hun i lukt. Tanken på andre i rommene deres var
motbydelig. Såpass udelikat hadde hun det i øyeblikket, et punkt i livet
der alt gikk på tverke. Hvis flørten trakk ut i tid ville hun rekke å
vaske og samle tankene. Eskene med saker flyttet hun til stuegulvet. Hun
rakk knapt å dytte tingene rundt, eget skrot skjøv hun ned i ymse
skuffer før lørdagen. Galskap trigget, hun følte kreftene som dro var
likt fordelt, og emosjonelle rundkast fungerte i samspill med digre
illusjoner. Når som helst kunne alt skli ut i sanden.

De pratet seg i søvn, og hun sov godt.

Hver morgen styrtet hun til Gaysir for å sjekke om Fab hadde lagt igjen
en beskjed i postkassen. Følelsen var alt kunne skje, og snart ville det
være sant.

5
-

Morgenen hun våknet i sengen inntil Fab kom som en fullstendig
overraskelse. Kvinnen bak enavnet løy på nettet, og falt utenfor
regelsettet sirlig nedskrevet på lister sene nachspieltimer da hun var
yngre og dum. Trivielle fakta feil fra Fab om bosted og alder unnskyldte
ikke den største løgneren var henne som falt i fellen. Villig følte hun
dekadentens sødme. Kvinnen bak enavnet sto foran henne på gaten etter en
tilstelning. Hun hadde fortalt om nettverket og pekt til den åpne
nettsiden der det var fullt mulig å følge programmet for hver time. Hvem
som helst kunne lese hva som sto der.

Hun sa ikke det var greit å komme, men visste det kunne skje. Visste hun
hva det ville bety? Hvor mye forsto hun var spørsmålet altfor lett å
besvare.

Overraskelsen hun følte når de sto ovenfor hverandre på gaten blandet
seg i fryd. Lørdag kveld lå åpen. De snakket i telefonen tidligere på
dagen og aldri nevnt situasjonen foran dem. Uken hadde vært usannsynlig
hektisk. Formiddagen likeså. Hun belaget seg på å forlate vennene
tidlig, utslitt og ute av stand som hun var til å ta innover seg de
store endringene de siste månedene. Før hun forlot restauranten oppdaget
hun tekstmeldingene på mobiltelefonen.

“Var det rart hvis de møtte hverandre uanmeldt på gaten utenfor?”

Meldingen var tjueminutter gammel og ble fulgt av en unnskyldning der
Fab kviet seg for å entre lokalet, og sist en tekstmelding fire minutter
gammel som kort fortalte Fab hadde snudd og gikk tilbake hvor hun kom
fra. Hun grep mobiltelefonen resolutt, svarte lynraskt, et rop i håp
sjansen ikke var forspilt. Hun nølte aldri. Klokken var kvart over ti om
kvelden, og hun kunne trekke seg tilbake. Venninnene ble sittende inne i
restauranten. Fab gikk mot sentrum, mens hun skulle andre veien. Vilt
spanet hun etter kvinnen som passet bildet stivnet på hjernehinnen. Hun
ble stående på fortauet. Gaten var folketom. Nølende tittet hun, ante
det beste var å gå nedover, eller skulle hun gjøre som planlagt, gå
oppover. Veien hjem.

“Jeg snur.”

Beskjeden bekreftet situasjonen var reddet. Hun ventet. Fab var på vei.

Hun så Fab.

En silhuett i natten på den mørke gaten. Hun følte uro i tilfelle
venninnene som satt igjen i restauranten skulle stige ut og se dem.

Fab kledde seg feminint, i skjørt, og hun tenkte det var feil å velge
skjørt og støvletter den morgenen. De var feminine begge to, matchet.
Ville Fab finne henne attraktiv? De høye hælene skjulte ikke
høydeforskjellen.

Stresset forskjøv problemene, leiligheten i uorden, tankene gikk til
gulvet hun ikke rakk å vaske, bøsset, eskene oppetter veggen. Fettlaget
på badet var verst. Smartest ville det være å dirigere Fab til et sted å
prate, ta det rolig, rett og slett ikke å ta Fab med hjem. Hun kunne ha
sex i kveld hvis hun sjanset. Fornuft og forsiktige skritt ville hjelpe
hvis målet var å bygge en grunnmur. Hun kjøpte tid til å fikse
leiligheten. Fab var tøff, og hun erkjente de var to som ville til
Sagene, leve løftet i situasjonen. Det fikk briste eller bære. Fab kom
hele veien fra Lillestrøm på et sterkt spor og reiste lenger enn hun
ville reist. Øyeblikket var her, hun skalv nervøs, trygg på utfallet.
Straks Fab sto der klemte de, holdt fast og pustet. Nervøsitet, glede og
spenning. Hun geleidet dem til bussholdeplassen på Majorstuen. En kort
stund tenkte hun det ville være bedre å praie en taxi. Hun gikk bort fra
tanken for det ville være atypisk.

“Hva du ser er hva du får.”

“Hæ?”

“Nei, ingenting.”

Begge var nervøse.

“Jeg må kysse deg, vi får ta en kyssetest på et tidspunkt. Det kan smake
forferdelig.”

Sa hun åpenhjertig.

“Jeg vet, men ikke ennå.”

Hun hadde fortalt teorien om alle gode og dårlige seksuelle erfaringer
ble forhåndsvarslet gjennom kyss.

De dro hjem til henne. Fab beordret lakenskifte straks de kom inn døren
og så den uoppredde dynen. Hun slo i dyne og puter, og Fab vandret rundt
i leiligheten og skapte en mening, registrerte rotet. Kikket i
kjøleskapet. Lent inn i fryseren fant hun gjesten på jakt etter noe å
spise. Hun dro fram knekkebrød og salami. Serverte et glass brus. De
trengte å kysse, til det var ferdiggjort var de fremmed for hverandre.

Knekkebrødet spiste Fab raskt. Begge trippet rundt i stuen, stirret over
gulvet og småpratet om bøkene i hyllene, livet og situasjonen. Til slutt
kom kysset—kyssene. Fab var nydusjet, parfymelukten satt markant, hun
sleit med å smake den virkelige Fab. Kroppen var perfekt, slank med
yppige bryster. Høy nok til at hodene møttes. Leppene skalv, uten å
henge fast. De gikk i gang med å ha sex med iver og pågangsmot. Hun var
fornøyd og brydde seg ikke om å bli tilfredsstilt. Smaken var
tilstrekkelig for vellyst å flomme. Ingenting distraherte henne fra
oppgaven å lukte hver bit i den slanke kroppen. Fab kom for å få ønsker
oppfylt og dirigerte hvor hun ville ha fingrene, hvor mange, og vekslet
med å spre beina for økt effekt. Fab var sensasjonell og krevende. Hun
lot seg ikke affisere, lot mer enn gjerne den unge kvinnen lede.
Engasjementet i sexakten var nytt. Iver undertrykt i år, følelsene
vokste, det kjentes bra og beina spredd foran med tilhørende nakkelukt
var viktigst i øyeblikket. Hun oppnådde alt hun ville.

Sent om lenge sovnet de før hun våknet rundt klokken åtte neste morgen
av gammel vane. Hun kom i skade for å vekke Fab. De kan umulig ha sovet
mer enn et par timer da hun igjen kysset brystene og la merke til små
detaljer tapt i iveren om natten. Arret på siden, et ganske grotesk
sådan skjemte den perfekte solariumbrune huden. Fab dirigerte hendene
bort og hun strøk resten av kroppen. Myk og gylden hud. Kvinnen fra
Lillestrøm var yngre og barberte skrittet. Hun hadde aldri vært med en
kvinne som fjernet håret nedentil. Første flørt hun hadde en roteromling
på tjattet var et enavn som skrøt på seg piercing på hårløse
kjønnslepper. Mange ganger tenkte hun hvor naiv hun var som trodde på
røverhistoriene. Hun tjattet seg inn i en skikkelig forelskelse før det
gikk opp for henne enavnet var falskt og bak fasaden satt en mann og
runket. En slibrig ekkel mann med lesbefetisj. Hun følte seg snytt for
en drømmekvinne og snudde ryggen til lesbeflørting på nettet. Ti år
senere lå hun inntil Fab, en roteromling i levende livet hun ikke husket
derfra, men som husket henne. Virtuell flørting egnet seg ikke for sånne
som henne, åpen og naiv, altfor lett revet med i emosjonelle
kasteballer. Det var årevis siden sist. Hun var eldre og visste mer, og
likevel virket alt kjent. Fab lå der og gjorde levende en drøm.
Hendelsen var ekte, de lå i sengen og en trekant hår deilig trimmet lik
en hageflekk pirret fantasien. Hun var stum, for barbert nedentil føltes
rart. Kanskje dette var generasjonsgapet? Kvinnen fra Lillestrøm hadde
sider og erfaringer hun var uten.

“Jeg har aldri vært miljøverner før nå: Stopp nedhoggingen!”

Sa hun keitet.

Her var det altfor mange elementer hun ikke behersket. Hun visste ikke
om Fab forsto, om hun skjønte selv tompraten for å si ett eller annet
betydde ingenting. Hun fortalte om reglene for å håndtere livet på
sjekkemarkedet. Første regel var aldri oralsex førstegang hun hadde sex
med en person, mens de to andre reglene gikk på alder. Målgruppe både
opp og ned stoppet på fem år, og hvis det var vanskelig ville hun gjøre
unntak for de innenfor ti år. Fab var yngre.

“Det er viktig og ikke å være fanatisk.”

Reglene sto ikke skrevet i stein.

Hun lå i sengen inntil en ekte person, illusjonene i nettsamfunnet var
borte. Alt gikk fort, for fort? Hun var ute av kontroll og ingenting
virket gjennomtenkt.

Hun elsket fantasi som ble virkelighet.

“Jeg har aldri angret noe jeg gjør impulsivt.”

Dekadens i system. Første tegn var fraværet av regler, anger, forståelse
for hva som er riktig og vil fungere. Hun strøk brystene til Fab,
fasinert av et arr hun ikke kunne forklare. Ganske snart etter ett
uskyldig spørsmål fra Fab brøt hun alle regler.

“Er det fortsatt første gangen?”

Sa Fab.

Morgenlyset endret fargen i gardinene. “Nei.”

Midt på formiddagen sovnet hun og våknet først når Fab dyttet. Hun lovte
fersk brød og måtte til butikken på hjørnet for å kjøpe. Solskinnet
varmet, men lyset gjorde henne en smule beklemt. Leiligheten var
åpenbart skitten, verre enn hun ville innrømme. Hun kjøpte brød i
nabolagbutikken og tok med begge tabloidavisene. Dagens Næringsliv kom
på dørstokken. Tilbake fra butikken lå lørdagsbilaget urørt. Fab begynte
å klargjøre retretten, snakket om livet som lå på etterskudd slik det
alltid gjør. Kvelden ville hun få alene. De lå på sofaen med føttene mot
hverandre og leste helgemagasinene.

Solskinnet gjennom vinduet avslørte alle lyter, og fra sofaen kunne Fab
se de skitne vinduene og eskene langs veggen. Hun lot perioden med
samboerens depresjoner og giddelause gå for langt. Slitasjen krevde
stopp, og hun gikk til streik. De maktet ikke følge opp den andres
grense. Kjærligheten døde. Resultatet var en dom fra gjesten hun ikke
planla. Hun maktet ikke rette måneder forsømmelse på de ukene Gaysir
skapte galskap og uforutsigbarhet i livet. Fab snurpet på nesen, sa
ingenting. Kvinnen brydde seg om utseende, vask og renslighet. Gjesten
var en pertentlig snelle, samvittighetsfull på renhold. Hun falt gjennom
på flere markører og skulle gjerne svart annerledes på flere spørsmål.

“Eier eller leier du?”

Sa Fab.

Hun skulle gjerne tjent mer, eid hus på Røa, kjørt en snerten Mini
Cooper, fortalt hyggelige ting om familien. Svarene var ikke perfekte.
De snakket, først løst rundt grøten, deretter direkte. Fab var åpen og
klar, brutalt ærlig.

“Det blir vel ingenting?”

“Nei.”

“Sannheten er at du er for gammel for meg.”

“Ja.”

Begge gråt, stille små tårer, nesten ubemerket trillet de ned kinnene
før hun fulgte Fab til bussen.

“Hvis jeg var yngre ville jeg kjempet for oss.”

Sa hun.

Hun følte seg for gammel til å ønske seg Fab. Enavnet fikk reise uten å
avgi et eneste løfte. Bruddet med kjæresten var for ferskt til at
følelsene kunne sies å være av verdi. Hun mistenkte en indre ubalanse og
mente å vite følelsene var upålitelige. Alt skjedde fort—for fort. Dagen
i sengen fylte kroppen med styrke til neste fase, singeltilværelsen
etter ti år samboerskap. Hun kjente varmen fra kroppen kvelden før, nok
sex til å leve i måneder på overflatisk flørting. Humøret var det ingen
grunn til å klage over. Betingelsene virket rimelig, hun sverget alltid
til rasjonalitet i slike stunder, sterk og høyreist. Hun burde visst
bedre.

Hun ringte bestevenninnen for å hekte seg på dem samme kveld. Rastløs
kunne hun ikke tenke seg å sitte inne. Else skulle på konsert på
utestedet Mir.

“Perfekt.”

Hun ville gjerne til Grünerløkka, gå på intimkonsert på Mir.

“Jeg kommer.”

Hun var ikke sen å be. Snart gikk hun nedover langs Akerselva. Bydelen
med de mange restauranter, barer og gressplener for slaraffenlivet.
Frisk til mots, struttende av selvtillit, rett fra dusjen. Hun stoppet
for å tenke hvor merkelig det var å føle seksuell tilfredsstillelse i
hver fiber, selv om natten og morgenen i høyeste grad handlet om den
andre personen. Hendelsene dukket opp i bevisstheten stykkevis, kun
sanselige uttrykk sprakk innimellom solstrålene i det varme fjeset.
Solbriller reflekterte lyset, varmen grep rundt naken hud på armene,
legger og den åpne halsen. Hun kunne se mot solen, stoppe opp og kjenne
pirringen. Slik var dansen i livet, energiflyt, perfekt balansert fra
morgen til kveld.

Hun følte seg bekymringsfri for første gang siden samlivsbruddet.

Spaserturen til Grünerløkka tok normalt tjue minutter, og på slike
dager, solskinnsdager gikk hun raskere. Halvveis følte hun behovet for å
stoppe, sette seg på en benk og tenke, tillate seg å sige sakte. Hun
skulle gå videre nedover dalen da mobiltelefonen ringte.

Hun følte energien og forsto Fab ble glad når hun tok røret i godt
humør. Gjensidig glede var takknemlig. Fab var hjemme i Lillestrøm og
ringte for å takke. Hun takket tilbake, fortalte hvor hun var på vei og
overrasket Fab som trodde hun satt hjemme og sturet. Hun likte å
overraske. Intuitivt forsto hun de siste dagene måtte by på mer, og det
skjedde raskere enn hun kunne ane. Kun dumme mennesker underkjente
nytelse de hadde følt sammen. Gavene livet kunne tilby Fab ville komme.
Ærlige svar ga ingen forventning, og hun undersolgte, men de skilte lag
og erfaringen satt perfekt. Hun var den Fab fikk, og mer. Overflater lar
seg justere, potensielt kunne hun eie mer, gjøre viktigere ting, leve
bedre. Grenser fantes ikke, og hun hadde en horisont til det uendelige.
Alt var mulig for de riktig motiverte, og hun var motivert og klar for
en ny fase i livet.

Hun skulle skaffe bil, men fortalte ikke Fab det der og da.

Bilen ville hun kjøre til Lillestrøm på tjuefem minutter. Livet gikk
lekende lett for de som levde, og det var ikke nødvendig å la to timer
på bussen drepe gjensidig glede. Mulighetene var uendelig. Alt de kunne
oppnå—sammen.

Hun stoppet aldri i samtalen og delte det euforiske skyggeteateret. Hun
innbilte seg gleden skinte gjennom. Stemmeleie lå jevnt, hun trakk
pusten og følte fornuften balansere godt på knivsegget, innenfor hva
ansvarlige kvinner i førtiårene kan tillate seg. Ytre faktorer avslørte
ingen diskrepans mellom fornuft og oppførsel.

“Jeg er glad du ringte.” (Jeg er gal etter deg.)

Sa hun.

De sa farvel en time tidligere etter en natt sammen. Det skulle være
slutt, men Fab ringte og endevendte alle planer. Hun var glad.

“Det er greit at du ringte. Jeg hadde det fint.” (Når kan vi treffes
igjen?)

“Jeg har denne innleveringsoppgaven.” (Jeg er ikke sikker på om vi burde
treffes.)

Hun forsto.

Fab ringte, det var viktigst.

Venninnene ventet, intimkonsert på Mir var en spontan ide og hun angret
aldri på ting hun gjorde spontant. Løftet om en hyggelig kveld var der
til tross for at de dro hver for seg.

“Vi snakkes.” (Jeg ringer i kveld.)

“Ja.” (Greit.)

Var svaret. Solen kunne knapt varme bedre, skinne sterkere, fylle henne
med mer energi. Fab fenget. Hun var avhengig, forelsket og fullstendig
gal. Gleden over å høre stemmen i andre enden løftet situasjonen høyere
enn hun noensinne kunne forestille seg.

“Jeg er glad du ringte. Virkelig.” (Jeg ringer straks konserten er
over.)

De la på røret og hun fortet seg til venner som holdt av en plass til
henne på bordene langs veggen på Mir. Hun betalte femti kroner i *cover
charge* og kjøpte før hun gikk derfra en musikk-CD med kveldens band.
Hun drakk tre øl og følte seg en smule fjollete. Bandet var dyktig, Fab
ville likt musikken. Hun pakket inn bandets album og ville gi det i
bursdagspresang. Fødselsdagen til Fab var om noen uker, hun fylte
tjueniår og det ville være en bra dag.

6
-

Status var nærvær, en identitet der hun sees av flere enn sine nærmeste.
Hun skapte en identitet og brukte et par kriger på forum for å heve
stemmen til å etablere status.

Dagene fløt mellom debattene. Tråden om pedofili startet forsiktig før
flere hundre forargede stemmer hoppet på den enslige selverklærte
pedofile. Mobben kunne herje, hun ville ikke beskytte mannen som sverget
han var ikke-praktiserende og trygg i barnehagen der han jobbet. Trym
hadde et svare strev å sensurere personangrepene før redaksjonen
erkjente debatt om pedofili er bra, ikke var en debatt. De stengte
tråden, og ingen bekymringsmelding ble sendt til politiet. Tre dager
hadde flere skrevet innlegg i den ene saken enn alle de andre trådene
ville samle tilsammen det året. Hatske, voldsomme og tidvis såre innlegg
fra indignerte forumshorer og offer for voksne kriminelle.

Hun styrte unna, kunne ikke og ville ikke toppe personlige vitnemål,
eller holde inne forakten mot stemmene forståsegpåeres med altfor
tolerant holdning til relasjoner der aldersforskjellen var syk selv om
den kriminelle lavalder holdt.

Homomiljøet ga hun korreks i de trådene der subtil og saklig kritikk
gjorde en forskjell.

Erkjennelse, det endelige bevis på respekt fikk hun fra Groggy. Han
sendte en invitasjon i en debatt bygd på samme lest som mange av de
andre, og enavnet hennes dukket opp på listen over hvem han ville se til
bords. Invitasjonen til forumsmiddag var et tegn stemmen satte spor. De
femtiår gamle Oslohomsen ville treffe henne. Han holdt datoer åpen,
klokkeslett og andre praktikaliteter. Forespørselen på forum fulgte han
deretter opp med en personlig melding i postkassen. Groggy simpelthen
spurte om middagsselskap var noe hun ville komme til og hvis det var et
tidspunkt som passet best?

Hun ga raskt svar, og ble overraskende oppløftet av invitasjonen.
Utfordringen ville være å samle de andre fra både Oslo og Vestlandet.
Han tok ansvaret, og hun stilte i ånden. Middagen skulle ikke stå på
henne, men hun visste innerst inne det var et tomt løfte. Samling av
forumshorene til middagsselskap var et forsøk som måtte havarere.

Topplisten pluss- og minustomler listet forumshorene etter et
poengsystem der aktive og populære profiler hang høyt. Hun lå innenfor
de femti mest aktive, men fraværet i debatten om pedofili gjorde utslag.
Hun lå skjermet på en anonym plass langt nede. Topplisten vekket
debattforum. Stemningen var amper og engasjementet rundt topplisten
skulle føre til mer heftig krangling. Ronda tronet på topp.

Ubehaget lå kaldt nedover ryggen. Tilværelsen på forum ble samlet i et
oppsett, og bare nærvær på listen skapte uro. Blant de femti sto hun
listet på en beskjeden plass, og likevel høyere enn hun ville være. Alle
stirret på Ronda, og enkelte var overrasket han sto øverst. Enkelte kan
tenkes var misunnelig. Groggy stilte i klagehjørnet.

“Ronda er grinebitt.”

Forklaringen på hvorfor lærhomsen fra Jørpeland trakk det lengste strået
og sto øverst på en liste over de med sterkest nærvær på forum var mange
og fantasifulle. Antall innlegg holdt ikke i den sammensatte logaritmen
Trym brukte. Antall tomler, enten de var negative eller positive ga
utslag. Tomler spredd jevnt, slik de gjorde på innleggene til Ronda
avgjorde. Han fikk tyn for plassen på topplisten. Ronda tilhørte
gjestelisten til forumsmiddag, han ble invitert av tilhengere og
fiender, men i nabotråden gikk forumshorene til angrep.

Hun hadde vært stolt da Groggy dro fram henne. Langt nede på topplisten
hang hun til tross for mange fler innlegg enn flere enavn over. Hun var
en av fire profiler på listen der tomlene gikk i minus. Hvert eneste
innlegg skrevet farget debatten, og hun var stolt over brodd i de fleste
innleggene, kranglefanteri egnet til å vekke aversjoner og ettertanke.
Hun mente selv argumentene lå innenfor det konstruktive, saklig og fra
et ståsted, hun drev ikke og tullet. Profilene på Gaysir skilte ikke
veldig fra det virkelige jeg.

Hun klarte ikke helt å ta Groggy alvorlig når han sendte invitasjon til
middag hjemme hos seg. Debatten rundt surrogasi skapte reaksjoner, og
det var stemmen han inviterte. Kranglevoren ble hun lagt merke til, og
sannheten var hun elsket happenings. Ting fylte livet og gjorde det
verdt å leve. Forumstreff representerte noe annerledes. Invitasjonen var
hyggelig, mens topplisten skapte en ømfintlig kvalme. Hun balanserte i
et ukjent landskap..

Andre tenkte på annet enn middag.

“Topplisten er tullball.”

Sa Ronda.

Han likte ikke oppmerksomheten plasseringen på toppen av topplisten ga
ham. Hun var enig, uten at hun uttrykte det like sterkt i forumets åpne
rom. Produktutvikling på Gaysir tilhørte kategorien saker hun ikke ville
kaste verdifull indignasjon mot. Ryggraden dirret i stunder hun tillot
harmen å herje, og idiotisk funksjonalitet på nettsidene kvalifiserte
knapt et hevet øyenbryn.

“De burde i det minste la debattantene reservere seg fra å bli listet.”

Sa hun, og det var alt.

Langt fra alle på forum syntes det var en ære å stå på topplisten.
Enkelte mente rent av en plass blant de øverste hadde aroma av skam.
Ronda var der, han stilte laglig til hugg, uttrykte seg uavhengig og
standhaftig. Han debatterte i det vide og brede, spredde tynt over flere
tema. Oppsummert skurret nærværet på topplisten. Han følte ubehag og
kviet seg for å beskue resultatet. Han ville helst behandle hver debatt
isolert, ikke leve i en diger gapestokk bygget på mystiske formler tatt
fra forumets mange tråder.

Protester falt på stengrunn. Opprøret møtte kjølig forståelse fra
redaksjonen. Trym forsvarte aktivt konstruksjonen og fikk mange med
blant forumshorene. Hun var ikke en av dem, og et forsøk på saklig
kritikk ble forbigått i stillhet. Hun ymtet om at det var forskjell å
representere ett standpunkt, innlegg og tråd—fra å bli knyttet til alt i
et jafs. Helheten reduserte totalen, og det argumentet falt i fisk.
Hovedpersonen i sentrum var Ronda, lederfiguren for de aktive likte ikke
topplisten og mobben snudde innover. Misnøye festet seg til forum.
Krangelen om topplisten ridde alle trådene, og Ronda ble fotfulgt.
Topplistens fornuft og ufornuft veltet bølger aversjoner i retning
skeptikerne. Hun gjorde mer. Ronda trengte støtte når tilhengere
mobiliserte. Forum luktet blod. Gribber sniffet og hakket når
toppdebattanten sa klart og tydelig fra hva han mente. Han ville ikke
stå på listen. Onde toner dempet stemningen på alt, en kilevink i tråden
for hvem det var verdt å invitere til forumsmiddag følte hun var
spesielt ondsinnet.

“Ronda vil jeg ikke ha til middag. Han er en fordømt stinker.”

Sa Kjøttstykket.

Aggresjonen oste misunnelse.

Stemningen snudde fra trivelig meningsutveksling til regelrett kriging.
Følt intensitet gjorde aktørene frosset, ute av stand til å forstå hva
som skjedde. Hun var i villrede, forsto simpelthen ikke hvorfor
topplisten hadde blitt hennes kamp.

Ronda sto i skuddlinjen. Groggy vasset i det trauste midtpartiet der
ingen fikk grep på hva han mente. Hun kjente dem fra gamle krangler, og
aldri før hadde hun sett kjempen fra Stavanger vise sårene. Ronda skrev
et ord i status og byttet profilbilde som sto til *deprimert*. Tråden
synes å skille mellom de mobbede og de som mobber; hun delte dem i
gruppene. Mobberne følte trygghet, de latet som om sammenstillingen på
topplisten der de tronet like under toppen var en god ting. De følte seg
trygge, ikke truet av en uforklarlig matematisk formel. Flest besøk,
aktiv deltakelse og tomler i begge retninger. Oppsummert fanget tråden
en totalitet og flertallet var såre fornøyd med tingenes tilstand slik
Trym skisserte løsningen.

Flest minustomler av dem alle hadde den gale tilbakeskrittpartimannen
med et enavn fra det norske kulinariske kjøkken. Trøndersodd startet
flest tråder, skrev endeløse innlegg og mottok definitivt flest
mishagsytringer. Han kunne skilte med tre ganger flere minustomler enn
plusstomler på topplisten. Ronda klaget over egen ulykke, mens rasisten
fra Trøndelag enset ikke kritikk når statistikk hang meningene hans
høyt. Motstand lå innebygget i forum, og surmaget personangrep på
ekstreme standpunkt var kanskje og forventet. Hun var en av de fire på
topplisten som skiltet med et minus på tommelregnskapet, en synlig
skamplett hun håpte skulle skli umerket. Hun visste nettominus på tolv
tommeltotter ikke kunne sammenlignes med de sekshundreognoe
nettominustomlene til Trøndersodd. De var imidlertid tilstrekkelig til å
vise profilen skjulte en kverulant, kranglefant og konfliktsøker. Hun
var en del av krigene, deltok i harde slag og lot sårene gro. Topplisten
vekket smerte. Hun brydde seg mindre hvis ikke det var for Ronda på
toppen som sto midt i ilden når skuddene ilte. Han hadde flere
plusstomler og synes å tilhøre det politisk akseptable på forumet.
Likevel smertet oppmerksomheten, og når tema ble han synes det som om
vennene var borte. Kjøttstykket og flere pekte på mannen fra Jørpeland,
misunnelige vridde kniven.

“Jeg vet sannelig ikke jeg. Min erfaring tilsier Ronda er reflektert og
hard motstander, alltid saklig og intelligent.”

Sa hun.

Kjøttstykket skulle ikke slippe unna overfallet. Hun hatet mobbere.
Taushet fra kjempen var øredøvende. Hun måtte trå til for en mann på
enognittiseksmeter. Hennes lodd i livet var å se urett, andre side av
samme mynt og flagge uenighet der en kvalmende symmetri kvelte de gode
samtaler. Hun skrev forsvaret og mente hvert eneste ord. Ronda fortjente
respekt—hennes og andres. Han visste i det minste når en tråd var
utflankert og når det var mer retorikk å melke fra flammen de kalte
uenighet. Ronda forsto hva hun begynte å ane, forumet var tiurleik i
avansert format. De struttet med fjær og viste farger for det homofile
samfunnet.

Topplisten ødela spillet.

De kalte det meningsutveksling, debatt. Illusjonen at trådene var egnet
til overtalelse, misjonering og søk i argumenter falt gjennom på
topplisten. Her skulle argumenter gjelde, og først deretter kunne de
kalle hverandre navn og stemple vilt. Forum trakk profiler ned ved bruk
av minustomler straks debattene nådde klimaks.

Intelligens, refleksjoner og retorikk var sjelden bedre enn bildet i
profilen.

“Pen jente.”

Hun tenkte tanken titt og ofte.

Hva sto skrevet, hvilken mening og hva slags tråder valgte hun. I løpet
av tre uker etablerte hun en stemme og plasserte fjeset på nok debatter
til å være forumsveteran. Hun satt ved middagsbordet—visstnok en
virtuell middag, tenkt for dem som brydde som om sånt. Invitasjonen
uttrykte respekt. Groggy tippet hatten for henne, hun takket for
omtanken. Han framsto i de fleste tråder som en forenklet debattant og
likevel var hun takknemlig.

“En saktmodig type og mye politisk korrekthet under huden.”

Sa hun om Groggy. Ronda nikket.

Hvis hun hadde vært psykolog og skulle skrive en innstilling var det hva
hun ville skrive i rapporten.

“Hvis du ikke har noe bra å si, la være!”

Mors råd klinget. Groggy gjorde mye fint og hun satt og dømte han.
Uforskyldt satte han dem i forlegenhet. Alt handlet om å være et bedre
menneske og hun gikk inn og ut i debatter om alt og ingenting. Filter
ble revet bort og forumsdeltakere blottet for empati ble ulver i flokk.

“Ronda fortjener honnør. Han er en engasjert debattant.”

Ros, heller enn ris. Hun var på riktig side, både for Ronda og
standpunktet mot topplisten. Funksjonalitet og statistiske formler ville
drepe debatt framfor å skape. Argumentet var solid.

“Skamlisten, kaller noen nyvinningen.”

“Nettopp.”

Hun følte faenskapen. Ronda og fler sa fra, selv om førstnevnte etter
lyskesparket fra Kjøttstykket lyste i sitt fravær. Tomheten talte volum.
Hun skrev støtteerklæringen for å sette en standard, strek for hva som
burde være tillatt.

“Takk for støtten. Jeg vet ikke hvorfor han sa hva han gjorde.”

Sa Ronda.

Meldingen i postkassen vitnet om at hun oppfattet situasjonen riktig.
Han led. Skadeskutt satt kjempen og leste kranglingen foran dem. Han var
deprimert og hadde det vondt. Sårene tok Ronda inn i hiet, og meldingen
hun fikk var en privat melding, åpen og lukket, takk for at hun
forsvarte ham når troppene fra topplisten grep ordet og nektet å gi
slipp. Sjokkskadet strakk han hånden, et villskudd lette etter sympati
hos den eneste fremmede i tråden som valgte forsvarslinjen. De sto
samlet mot rukla. Hun roste ham, tenkte støtten kunne trøste. Ronda var
trist tog status lå åpent. Hun tvilte aldri. Personangrepene på forum
kunne bli altfor meget, spesielt når mobben samlet gikk til angrep.

“Hold deg oppe! Kjøttstykket og de andre er misunnelige.”

Ronda ville trekke pluggen på datamaskinen, og hun manet til kamp. Hun
forsto ham godt. Støtteerklæringen hun ga var noe folk gjorde, de egget
og bortforklarte, skapte et trygt rom der han slikket sårene og gikk
videre. Hun tenkte aldri det ville være riktig å gi det hele og agiterte
heller for aktiv kamp. Empati var sjelden vare på forum, og hun ville gi
det lille nikket for å forlenge krigen. Han sultet etter å bli sett,
sugde opp analysene hennes, de vennlige ordene og støtten. De utvekslet
meldinger resten av dagen. Forum kranglet om topplistens eksistens.
Mange mente topplisten, men flere hyllet nyvinningen en lørdag hun lekte
psykolog for en deprimert kjempe.

“Topplisten river bort forumets siste egenart.”

Sa hun. “Internett for dumminger.”

“Kvalmende enighet.”

“Ja.”

Han sto rak på forum dager og netter. Han var en soldat og kunne gå
søvnløs i flere døgn, og han var endelig klar til å legge tastaturet til
side.

“Jeg trekker meg.”

Hun følte smerte og følte med ham. Timer senere slettet Ronda profilen.
Samtalene deres lindret smerten, men fjernet den ikke. Hun snakket ham
gjennom abstinensene og ble kjent med reserveprofilen der han hang
skjult, enavnet til en lærer fra Hordaland. Hun håpet samtalene lindret
tapsfølelsen, savnet. Begge visste tilværelsen utenfor forum var
vanskelig i starten. Han led av en alvorlig depresjon.

Hun ville ikke lide et sekund og var ikke klar til å forlate debattene.
Gi opp! Debattene trådde ikke nære nok, oppildnet av energi kunne hun gå
flere runder. Enavnet listet lenge på topplisten. Hun var mindre synlig
i den gemene hop. Minustomler hadde ingen effekt. Tvert imot, de var et
kvalitetsstempel og indikerte hun gjorde noe riktig i en verden der
unison enighet kvalte mangfoldet. Noen må banne i kirken. Fjorten år
gammel visste hun den personen kunne være henne. Hun var sterk, sto rak
og tenkte selvstendig. Mange sviktet, sleit seg halvt i hjel for å viske
sannheter når alle sto imot. Hun skrek for full hals løgnene, trodde
kraft utgjorde en forskjell og var stolt. Hun kalte seg kommunist i
tenårene og forsvarte invasjonen i Afghanistan.

“Hvis USA får lov til å være imperialistiske kunne Sovjetunionen gjøre
det samme.”

Begredelige minner mange år senere. Idealistisk ungdomssynd, tankegods
hun angret bittert. Minnet ville gjort de fleste fornuftige mennesker
flau hvis de orket å reflektere. Kreftene til å protestere der alle var
enig fant hun dypt begravd i selvet, og sikkerheten slapp aldri taket.
Hun skilte seg fra flertallet og var glad over en indre styrke. Hvis det
var mulig å isolere og fokusere viljestyrke i en retning ville hun rekke
langt.

“Synd, jeg håper du kommer tilbake. Forum taper hvis du blir borte.”

Hun mente alt. Ordene fløt naturlig.

De utvekslet meldinger resten av helgen, og fant tonen, respekt for
medmenneske. Hvis anledningen kom skulle hun snakke med Ronda på
forumsmiddagen. Han virket sterk og sår, en herlig kombinasjon, symbiose
i digre følelsesklumper på skrullesiden vekket ønske om å beskytte.
Spinnesiden var adskillig tøffere enn mannen oppunder to meter. Kvinnene
møtte forum og avviste følelser, stilte spørsmål i dyp skepsis og
kompromissløs nektet de medsøstre å se gapestokkens uheldige sider—ei
heller, erkjenne allmenn skue var implikasjonen. Ronda var en barsking,
noe eldre, en homsegutt som levde og intuitivt forsto mye. Hun ville ha
flere debattanter som ham på forum.

Han takket for de fine ordene der hun kalte ham reflektert. Ronda var
uforberedt de hatske angrepene og satt såret på en sofa i Randaberg. Han
var avvæpnet av misunnelige angrep på topplistetoppen. Deprimert alene
foran datamaskinen i en leilighet som ga nok luft i lungene til den
nødvendige pust. Han strakk seg etter henne—en vilt fremmed, knapt døpt
i forumets kultur for mishagsytringer. Komplimenter gjorde skulderen
hennes til en havn for den digre bamsemannen som lente hode over og
gråt. Hele lørdag satt hun og vekslet meldinger der de andre debatterte
topplisten. Forsvarere av nyskapningen lovpriste makkverket og fikk
gjøre det uimotsagt. Opposisjonen faset bort, og hun stilte seg på
sidelinjen med Ronda og vurderte de spinkle stemmene i skepsis. Slaget
var tapt.

“Trym har bestemt seg.”

Ronda sa det og hun tenkte det umulig kunne være slik

“Hvorfor ha en debatt da?”

“Jeg vet ikke.”

Sa han, men de visste. Sjefsutvikleren på Gaysir ville sole seg i et
slør han kalte brukerstyring, en illusjon han var der for dem—brukere,
de interesserte i nye og forbedrede funksjoner. Timer foran datamaskinen
for å utvikle nyvinningen skulle skape entusiasme i tillegg til å
rettferdiggjøre innsatsen.

“Du vet, Trym er et skritt fra fullverdig aspergersyndrom.”

Hun visste ikke, kjente ikke Trym.

Fornuftige mennesker måtte se topplisten i alle former, slik den ble
presentert snevret inn kranglerommet. De politisk korrekte ville rase
opp på topplisten med plusstomler i overtall, og de med forventning om
minustomler ville nøye vurdere om meningsytringer var verdt gapestokken.
Hvis det ble viktig å toppe listen ville debatter vokse fram med hensikt
ene og alene å løfte trådstarter i ovasjoner med plusstomler. Kjente
slagere, tema der antipatier slo jevnt i miljøet ville komme tilbake,
Israel var slemme, melodifestivalen favoritt ble snytt, surrogatbarn til
homofile menn og muslimer må ta aktiv avstand fra dødsstraff for
homofili. Temaene var kjedelige, godt etablert på forum og klare for
gjenbruk i det uendelige.

Politisk forflating av forum, og debattens beskyttelse var hennes
anliggende. Indignert skjøv hun bort tanker å forlate forum i
solidaritet med Ronda. Mobbekulturen der alle kastet seg over svakheter
lå lik en stein i skoen. Hun skrev en virvelvind av innlegg mot
topplisten og skilte seg fra de profilerte på forum som ikke var enige.
Hun mente topplisten var ond og sa klart og tydelig et rungende ja til
debatt. Meningsutfoldelse om topplisten skulle ikke bli en gjenganger,
men uten henne, for hun sluttet å skrive innlegg like etter at den gikk
offisielt gikk på luften. Boikott i solidaritet med Ronda. Hun leste
forum, fulgte innleggene og registrerte flere gjorde som henne, tenkte
to ganger før de skrev noe som helst.

Statistikk må ha talt, for det var færre, mindre styr på forum etter
kranglefantene sa farvel. Topplistedebatten var en gjenganger og de
gjenværende begynte å snu seg mot hverandre, lukte på moderatorene og
klage.

Hun skrev et hva-sa-jeg-innlegg i rusen av at flere i ettertid erkjente
topplisten dempende effekt på meningsutveksling. Aggresjon over å ha
mistet lekeplassen lå latent for hun skrev et innlegg hun kom til å
angre, et personlig og målrettet angrep på sjefsmoderator Trym. Han var
fienden, og bare en måned etter dette innlegget ville hun være slettet
fra forum minutter etter å ha gjentatt synspunktet.

“Jeg synes ikke Trym er spesielt reflektert. Han blander rollen og
inviterer til liksomdebatter der utfallet er gitt.”

Sa hun og tenkte på debatten om pedofili er bra. Utfallet kom på tampen
av en lang diskusjon, oste hatsk og markerte slutten på nok en tråd. Hun
fikk en tekstmelding fra Pikkihagen bare minutter etter å ha postet
angrepet på sjefsmoderatoren på forum.

“Vær forsiktig.”

Hun stusset på ordvalget fra sin gamle studiekamerat. Han tok sjelden
den alvorlige tonen. De skrullet, det var hva de gjorde. Han fortalte
noe hun ikke visste; redaksjonen blandet kortene hele tiden, de slettet
identiteter etter eget hode.

“Trym er den verste.”

Sa Pikkihagen.

“En kamerat fikk enavnet og to falske profiler slettet fordi han klaget
til redaksjonen da en i staben inviterte til deit og ikke ville
akseptere nei.”

Hun hadde vondt for å tro ham.

“Pass deg!”

Sensurert fra Gaysir fordi han nektet å gå på deit. Hun trodde knapt hva
han fortalte. Samtidig var det ingen grunn til å mistro vennen. De
kjente hverandre siden lenge, og det var få i miljøet han ikke kjente.
Debatten ble stille etter innlegget, og det var like greit. Forumshorene
var lei topplistedebatten, flere hadde trukket seg fra forum og hang på
hovedtavlen i lediggang. Historier om seksuell sjikane, klager og en
hevngjerrig stab som valgte å kaste vennen til Pikkihagen fra nettstedet
var vill.

Tråden på forum stilnet. Hun var fornøyd med å samle erfaringer i
personlige tekstmeldinger. Angrepet på forum var ektefølt og hun gikk
hardt på Trym. Han fikk alle frustrasjoner. En indignert sans for
rettferdighet hun følte på vegne av vennen Ronda var dråpen. Sannheten
var at hun savnet debattene. Trym måtte unngjelde.

7
-

Ukene blandet i overstadig lykkefølelse og sorg var i ferd med å treffe
kroppen. Hver ledige stund på nettet logget hun for å sjekke om det var
meldinger fra Fab. Betatt. Gjett om hun var betatt.

Dagen etter overnattingen våknet hun og ville ingenting annet enn å
ringe. Det var tidlig formiddag, Fab la seg alltid sent, og sov lenger
enn hun var vant til. Hun var lykkelig når Fab tok telefonen, hun hørte
stemmen, samtalen var knapp og de sa lite. Hun følte det gikk bra. Gode
samtaler er de som foregår utenfor hode. Hun enset ikke det meningsløse
innhold. Lykken lyste i alle rom.

For en herlig flukt fra virkeligheten.

Før hun sovnet om kveldene snakket de igjen. Innleveringspresset lå
strengt over dem begge. Sannsynligheten for at de ville treffe hverandre
de første ukene ble mindre.

Hun vasket alt hun kunne, ryddet gulvet og kostet bøss på kjøkkenet. Fab
skulle slippe å se leiligheten like møkkete som sist. Hun bortforklarte
uhellet, men innså full uvitenhet aldri ville være troverdig. Samboeren
forlot ræl i leiligheten, og det tok dager før hun rakk å samle tingene.
Hun sorterte og ryddet, kastet og samlet i esker hva som skulle kjøres
bort. Leiligheten var i urede, rent av ugjestmild. Rommene var skitne.
Hun hadde vist leiligheten på sitt verste. Redselen for at besøket ikke
ville gjentas vokste. Fikk hun svar på meldinger var beskjedene korte,
de kom sjeldnere. Hun følte usikkerhet. Flere dimensjoner entret livet,
problemer kom i veien for reprise. Svarte Fab på meldinger var de korte.
Kvinnen fra Lillestrøm tvilte. En dimensjon usikkerhet entret livet,
problemer som sto i veien for å gjenta hva hun husket var fantastisk
sex. De måtte vente, holde relasjonen på telefonen. Hun følte
frustrasjonen nappet hver gang Fab utsatte gjensynet. Ingen sa det var
slutt, slik begge hadde vært enige første dag. Fab ga verken beskjed om
det ene eller andre. Hun klamret til håpet om gjensyn, og den siste
patetiske handling var møysommelig å pakke inn albumet hun anskaffet
under intimkonserten på Mir. Bursdagspresangen ville hun overrekke
førstkommende lørdag. Mot hva hun lovet leste hun nettdiktet i Dagbladet
de skulle holde privat. Hun leste bemerkningene fra redaksjonen om at
forfatteren av det ubehjelpelige kjærlighetsdiktet ville ha diktet
slettet fra nettavisens poesihjørne. Forfatteren ville ikke være bekjent
diktet, påsto det var skrevet av en annen, en svindler, identitetstjuv.
Redaksjonen valgte å ignorere bønnen, lot diktet være for å sikre de
historiske loggene. Hun leste uttrykket Fab ønsket å glemme, de lyriske
linjene poppet opp først på googlesøket. Kontaktene hennes i avisen
kunne gjøre forskjell. Hun hadde skrevet til en venn i Dagbladets
IT-avdeling. Hun ville frigjøre Fab, hjelpe og fjerne det digitale
sporet fra en skjelmsk ungdomstid og vennen var i posisjon til å gjøre
grovarbeidet. Han var villig, men ville ha aksept fra redaktøren først.
Sporet måtte bort fra googlesøket og slettes fra redaksjonens
datatjener. Frihet fra et digitalt overgrep ville være den personlige
gaven. Hun skulle få slettet ti års magesår. Hun tenkte på kvinnen i
Lillestrøm, hva hun måtte gjøre for å rekke det hele til den store
dagen. Hun frustrerte over fraværet. Stumme telefoner, færre
tekstmeldinger og ingen besøk på profilsiden i Gaysir. Tvilen vokste.

Minnene sklei over til å bli nettopp det, distanserte inntrykk fra noe
som hadde skjedd.

Dagen før bursdagen fikk hun beskjed fra Fab om at de ikke lenger ville
sees. Daglig besøk på profilsiden, intensiteten i nærværet frastøtte.
Kontakten måtte stoppe. Toneleiet overrasket. Hun kjeftet tilbake og
spilte indignert etter prinsippet angrep var beste forsvar. Samtalen som
fulgte var vond. To voksne mennesker ble enig om at det var ingen
framtid for dem, akkurat som de hadde gjort første dagen på sofaen.

“Herregud, jeg har hatt sextreff på Gaysir.”

Sa hun.

Fab ble stum. Tenkte på deres ene gang.

“Nei, det var mer enn det.”

Sextreff hadde vært et mulig utfall. Hun visste ikke hvorfor
erkjennelsen var vanskelig. Hun sto på gaten utenfor restauranten og der
bestemte de seg for å ta bussen til Sagene. Hun ville aldri innrømme
annet enn at hun var åpen, tilgjengelig og spent. Telefonen lurte henne,
iveren tok overhånd, tankene om hva og hvordan gjenta kvelden sammen.
Ukene hun sto i stampe og konspirerte med en IT teknikker om å frigjøre
venninnen som hadde en dårlig erfaring på nettet. Han måtte klarere med
redaktøren, og gaven ville være et faktum. Hun visste ikke hva
redaktøren ville si, kunne ikke fortelle om gaven. Det ville ikke gjøre
en forskjell.

Musikkalbumett sto i bokhyllen, innpakket og klistret på en lapp der hun
i sirlig håndskrift skrev navnet til bursdagsbarnet. Det var ingen vits
å tvære mer på saken. Hun sluttet å gå inn på profilen til Fab, turte
ikke, redd for at hun skulle bli blokkert. Dagen for nådestøtet vandret
hun alene i den rene leiligheten, tidlig om morgenen og hun var lys
våken. Stemmen til Fab stoppet tankene som svirret, roet musklene på
høygir, og hun innså det var for sent. Begge gråt. Fab forsøkte å være
den kalde og kyniske, noen ingen av dem var bygd for kulde. Hun gråt
fordi det hadde gått akkurat slik hun visste det ville gå.

Månedene som hadde vært traff kroppen hardere enn hun ville erkjenne.
Hele eksistensen lå på skuldrene og de siste ukene følte hun ryggen
luske lavere enn vanlig. Hun unnskyldte seg til Ingebjørg som hadde
sendt en venneinvitasjon til profilen, den lå og ventet i meldingsboksen
ubesvart. Krangler på forum og hele historien der Fab vekket ukjente
perspektiv, hun ante tilværelsen i kyberlandsskapet var farligere enn
hun var villig til å tro i starten.

Hun sendte en melding til Ingebjørg og forklarte hvorfor det aldri var
aktuelt å akseptere invitasjoner på Gaysir.

“Ambivalent.”

Sa hun.

Følelser rundt hele tilværelsen på nettstedet lot seg beskrive i ett
ord. Sannheten var tilværelsen på Gaysir vekket følelseslivet og sendte
stemningen opp og ned like raskt som det kom nye profiler på
hovedtavlen. Veien til en ny identitet var kort. Ingebjørg behøvde ingen
lang utledning. Hun trengte å bortforklare hvorfor de ikke kunne være
venner. Forklaringen hun hadde til venninnen var enkel og vanskelig på
samme tid. Hun ville stå fritt til å slette profilen på kort tid, og
bønnfalt om forståelse for det..

“Selvsagt, det er opp til deg. Hvis du absolutt vil kan jeg akseptere
invitasjonen.”

Sa hun.

Trangen til å slette og opprette profiler, rastløs flytte for en stakke
stund usynlighet før hun igjen kan gjenoppstå i ny giv. Venninnen gikk
aldri til hovedtavlen mer enn tre ganger i uken. Pålogget tre ganger var
i knappeste laget for henne på en dag.

“Vi er venner i det virkelige livet. Jeg vil bare ikke ha noen hinder
hvis det blir nødvendig å slette profilen.”

Ingebjørg forsto ikke hva hun pratet om, sa ingenting, respekterte
valget og krevde ikke å bli gjenkjent. Hun ville spandere en øl neste
gang de traff hverandre på So. Sletting av profilen virket sannsynlig
hver dag, følelsene satt dårlig siden Fab sa det var slutt. Den første
hun rettet blikket mot på hovedtavlen var enavnet fra Lillestrøm. Hvis
hun ikke kunne se kvinnen søkte hun profilen og fikk klokkeslettet for
sist Fab var pålogget. Hvis andre visste hvor gal hun hadde blitt ville
Ingebjørg ikke ønsket vennskapet.

Siste måneds surfing innom profilen detroniserte sjanser for gjensyn.
Bruddet føltes hardt. Hun leste dagboken straks det kom poster, studerte
bildene i galleriet. Sladreloggen i venstre meny på profilen avslørte
hvem som besøkte, og hun ble sett snoke i fortiden.

Straks hun logget inn forlot Fab hovedtavlen. Hun forsto besøk på
nettsiden brøt en skjult kode, men hun strevde med å få tak på reglene.
Løsningen for å lære spillereglene var å innføre sanksjoner. Hvis hun
brøt reglen og gikk på profilen til Fab utviste hun en selvpålagt straff
og utelukket fra å delta på Gaysir ett døgn. Systemet fungerte ganske
greit. Siste måned krøp opp til fire blackoutdøgn. Frivillig boikott av
nettstedet på grunn av regler hun satte var vanskelig å etterleve. Det
gikk på et vis, mye fordi hun skaffet seg en falsk identitet til å gjøre
grovarbeidet.

Falske enavn kunne surfe og var lett å opprette. Kravet for å registrere
profiler var en epostadresse. Hun valgte fortrinnsvis mannlige alias,
eldre menn med fleksibel preferanse. Enavn måtte like både kvinner og
menn. Ingenting suspekt i en pervers som bruker tid på å spane på
kvinner. Profilene sto til stryk, de var lettvinte kopier for kortsiktig
snoking. Eldre menn fikk mindre oppmerksomhet, tenkte hun, og det var en
av de første logiske feilene hun gjorde. Profilene var hennes første
klumsete forsøk på å skjule gjøremål. Hun oppdaget slike profiler
fungerte som fluepapir på de med alternative ønsker.

“Søker. Sextreff.”

Skrev hun i profilen på et av de falske enavnene for å gi troverdighet
til søket på kvinner. Reaksjonene kom umiddelbart. Dagene etter
skapelsen hadde fantomprofilen fått flere blunk og meldinger enn hun
noensinne. Uken etter tilblivelsen hadde enavnet mottatt sju blunk og
tjuefem meldinger fra tjueen personer. Hun nølte, ville ri bølgen og se
hva som skjedde med mannen som søkte sextreff. En uke gikk og hun
slettet likevel det falske enavnet, den mest suksessfylte av dem alle.
Identiteten bygget på falske spor, stilte aktive ønsker og det syntes
nødvendig å gire ned tempo.

Falske profiler behøvde ingen sperrer, erfarte hun. Hun ville lansere en
mer nøytral oppmerksomhetsvindel.

Tilfeldigheter, en sjanse skapte noe nytt: En konkurrent i den falske
identiteten hun oppretter som både hadde pupper og matchet i alder.
Ubevisst hadde hun fulgt alle personlige data som stemte. Løgnene
interesserte henne ikke lenger. Besøkere skulle ikke være i tvil om
hensikten. Enavnet var *Fake* og profilen tilsto den var en fasade i
åpningssetningen—den første og eneste setningen på profilforsiden. En
tegnestripe av en måke satte punktum. Kopiert fra nettet ble strekene av
måken profilbildet til det falske aliaset. Måken fløy i tre ruter og
midt i sporet, halvveis, der fuglen flippet fingeren i ren frustrasjon
og klaget over et kjedsommelig liv strålte Fake. Snert og humor gjorde
henne glad.

“Livet mitt er dødskjedelig!”

Måkefingeren understreket poenget. Tidstjuveriet i surfet viste ledig
tid tilgjengelig, hvorfor måken eksisterte, og hvordan tiden ble fyll.
Liv i gjenge framsto som en gigantisk oppgave.

Fake var et blindspor, og hun visste bedre. Ganske snart følte hun
stolthet lik foreldre, eller en elsker, hun følte uttrykket var
autentisk og forelsket seg i den egne anonyme profilen. Livet er
dødskjedelig. Mest spennende var fuglen med skeivt blikk som flippet
gjennom profiler uten hemninger og brydde seg lite om
nysgjerrigpetraimaget. Fake fløy anonymt, kunne besøke alle og slapp
angst for å bli sett. Derfor fløy Fake—fritt.

Fløy. Fløy og fløy litt mer.

Måkeskitt over alt, sporene hang på besøkslogger. Kvinne i Alta besøkte
Fake for å se med egne øyne om hun var interessant. Altakvinnen var
riktig alder og bildet viste en jordnær og normal person, i den grad
slike fantes. Blikket rettet ikke den veien, og hun følte det var greit
da måken ble blokkert fra flere Finnmarksturer. Fab skrev i dagboken om
kåte menn med stor pikk og ville de skulle la være å ta kontakt. Hun
gledet seg over at kritikken var spesifikk, for ingen av hennes falske
identiteter skrøt av pikk og kom med aktive utfordringer til sex. Hvis
kritikk sies å falle på stengrunn måtte dette være denne advarselen. Det
kunne bare være en falsk identitet for henne etter måken. Fake fløy,
lettet, frikjent og tilfreds. Hun likte bedre å være ærlig i
uærligheten. Av de falske profilene forelsket hun seg først i måken.

Sånn gikk dagene.

Fake fløy og var den eneste gleden hun hadde. Innelåst, i et vidt åpent
trangt rom.

Helgene ble prøvesteiner for en tilværelse uten håp. Tungt, men
håndterbart. Ikke til å anbefale, men tålig. Hver dag var en seier.
Gleden av å se, føle forventninger og drømme forsvant aldri. Hun lette
etter poenget—det eksistensielle spørsmålet: Hva var liv?

Hun vurderte ståkerharakiri. Ingebjørg og alle hun kjente ville aldri
bli venner på Gaysir for hun skulle stå fritt til å flykte når smerten
var for stor.

Egenhendig å slette profilen lokket. Hun følte intuitivt en sletting
ville bli angret. Alternativet var å kaste seg inn med overdrevet lyst.
Hun kunne aktivt besøke profilen, se endringer i status og fjerne
sperrene. Slik kunne hun tvinge fram en blokkering, eventuelt ville Fab
ta kontakt og be om fred. Hun kalte strategien for
*blokker-meg-eller-ring* strategien.

Svaret ville gi en generell henvendelse og kjøpe ekstra tid. Utsettelse
på det uunngåelige farvel.

Slik tenkte en ståker. Såpass forsto hun. Hun tørstet etter å høre
stemmen til kvinnen hun begjærte. Rasjonelt sto hun imot, for kvinnen i
Lillestrøm hadde ingenting å gi. Straks hun levde galskapen ville de
være skritt nærmere det endelige bruddet der fornuften måtte vinne.
Øyeblikkene i galskap var deilig, vond og deilig igjen. De var forbi
punktet der de kunne finne tilbake. Konfrontasjon for å tvinge fram en
blokkering ble avverget fordi hun leste en artikkel i datamagasinet
Digi.no om sosiale nettverk der hovedpoenget i artikkelen hentet mottoet
til Microsoft med en oppfordring om å stå imot.

“Don’t be stupid.”

Regelverket mangemilliardær Bill Gates beordret ansatte å lese ba dem om
å spille på fornuften og bruke evnene. Hun levde, dro kroppen fra en
tilstand lik døden. Ferieplaner satt standard for hvordan framtiden
kunne være. Lett og ledig, dager i solen fylt i håp. Hun kjente ingen
krise, søkte noe mer passende, og mente en eldre. Hun ville forme
framtiden, ikke bli formet av vilkårlige følelser? En tilbakevendende
tanke hentet hun fra en fjern fortid, fra tiden før hun bygde reir med
samboeren. Spørsmålet hang der og besudlet løgnene om at livet gikk på
skinner: “Hvordan kommer du over en one-night-stand? ” Hun visste
svaret.

Helgen var det mulig å dra på byen. Utestedet So ønsket voksne lesbiske
i hovedstaden velkommen siste fredag i måneden. Hun hadde stått utenfor
på vei hjem—halvveis klar, og uforberedt. Hun hadde menstruasjon og
biologien avgjorde. Hele veien hjem til Sagene angret hun på valget.
Beste måten å slippe tak i et one-nights-stand var å ha ett nytt
one-night-stand.

Problemet var hun knapt kunne tenke seg andre, å ligge i armene til et
hvilket som helst vesen som ikke var Fab.

Alt dreide seg om kvinnen fra Lillestrøm.

Hun lå i sengen og tenkte på Fab, fortapt, og hva hun trengte å gjøre
for å sikre situasjonen var å tenke på reglen fra verdens mest
vellykkede forretningsimperium med det enkle rådet:

“Don’t be stupid.”

Hun holdt den giftige profilen på avstand, Sendte ikke epost, ringte
aldri. Tankene surret fritt i uker, og hun var forbauset over hvor
enkelt det gikk å skyve bort følelsene. Hun led under en mild og
håndterbar forelskelse, i verste fall ville tilstanden i limbo ta
måneder å riste ut av kroppen. Hun visste svaret på spørsmålet, og ante
det fantes en rask lindring.

8
-

Hun satt hjemme og livet var kjedelig. Hun redde sengen, vasket gulvet
og ryddet skap. Hun fant samboerens pass i en kjøkkenskuff og kastet det
i eskene broren endelig fikk tid til å kjøre. Malerier på veggen byttet
plass, plakater fikk egen ramme og hun fylte luftrommet i leiligheten
med møbler fra loppemarkeder. Hybelkaniner måtte ha plass for livet in
flagrant, og hun ville ha plass til å sanke de inn med en enkel klut når
anledningen bydde seg. Singeltilværelsen var mangelfull. Fattig. Et
vedvarende jaktlag på støvballer.

Vise ansiktet på Gaysir var en kunst, et stykke verbal ballett,
piruetter, tungekrølling, vinkelslipp og finere justeringer av posisjon.

Hun forsto ikke språket.

Var det derfor hun gikk på LLH-fest?

Med hode klistret til ruten tok hun bussen til Storgata, og halvveis,
mens bussen ennå ikke hadde forlatt Sagene bydel sendte hun tekstmelding
til Kasper, en gammel studiekamerat hun visste ville være på LLH-festen.
Han var leder i lokallaget og programfestet til å delta på alle typer
tilstelninger. Hun ville ha forsikringer om at det ikke var for tidlig å
dra ned til festen. Kasper kunne avverge situasjonen hun fryktet og
hindre henne fra å stige inn på Rockefeller alene. Stirret i senk av en
håndfull LLH-tillitsvalgte.

“Her begynner det å plukke seg opp.”

Sa han i tekstmeldingen.

Når folk hun kjente kom til festen var umulig å vite på forhånd.
Vorspiel var upålitelige, hun kunne ikke vite når det var rimelig å
forvente trygge folkemengder. Bussen til Storgata tok heldigvis litt
tid. Det mer hun tenkte på saken fristet det å bruke to timer på en
kinofilm framfor å gå direkte til Rockefeller. Niforestillingen sluttet
omtrent da festen våknet, elleve-isj, utvilsomt rundt midnatt. Siste
buss hjem var ikke et valg. Homser festet til siste slutt. “Hjem tidlig,
rekke siste buss? Eller kino før anstrengende cruising mye senere på
kvelden, når livet på dansegulvet kokte og nattbuss var
kollektivtilbudet.”

Hva orket hun?

Tekstmeldingen fra Kasper var tilstrekkelig. Hun kunne i beste fall
henge på ham. Det ville ikke være unaturlig, hun ville passe seg fra å
henge for lenge.

LLH hadde annonsert jubileet i god tid, og hun lagret datoen i
kalenderen og visste det ville være en personlig sak å stille. Flere
ville treffe hverandre på vorspiel, mens hun satt foran teven alene. Når
homsebefolkningen spiste en bedre middag og åpnet vinflaske nummer to
prøvde hun skjorter foran speilet for å ende i et høyst ordinært
klesplagg, noe hun kunne plukket på mindre enn to minutter. Hva slags
stemning det var på festen virket irrelevant for dem som kom i flokk,
ikke henne. Risikoen ved å komme for tidlig var å bli stående og kope i
timer før kjentfolk stoppet for å hilse. Løst bekjente ville øke faren
for å bli identifisert, stigmatisert, og hun ville ikke bli stående for
lenge slik. Hun forberedte å henge i baren, der kunne hun utveksle ord
med andre ensomme damer, ta vinnersporet, ledelsen. Hun mestret å mikse
med fremmede.

Bussholdeplassen i Storgata var et trist syn, mørket sank nedover byen.
De tunge skrittene til Rockefeller ble ikke lette i folketomme
gater—tvert imot. Hun var tidlig. Kasper hadde løyet, eller overdrevet.
Hun spanet etter folk, festen kunne umulig ha begynt, gatene rundt
Rockefeller preget de tomme dansegulvene innenfor. Populasjonen hun
kjente ved utseende var fraværende i gatebildet.

Vel framme ved inngangen til Rockefeller var hun ikke i tvil, og snudde
i døren.

“Nei, jeg går hjem.”

Tidsbruk utelukket siste forestilling på Saga kino, sjansen ble skuslet
bort og blikket flakket bakover, til bussen og veien hjem. Hun behøvde
ikke uttale ordene, si fra til vakten som stilte seg nøytral til faktum
at hun var feig og løp hjem før festen hadde startet. Ordene trallet i
hodet og tok følge til holdeplassen. Bussen kom og stoppet før hun fikk
tenkt seg om. Hyppige ruteavganger var et tegn på hvor tidlig det var på
kvelden, en siste bekreftelse det var feil å bevege kroppen mot sentrum.
Sete varmet, kjent, akkurat som hun forlot det. Hodet passet ikke like
godt mot ruten. Uro, beklemt og flau smak satt i munnen. Hun hoppet av
bussen etter bare to stasjoner. Ved Jacob kirke gikk skrittene raskt
tilbake til Rockefeller.

“Ikke i kveld.”

Tenkte hun. “Feig er ikke et valg.”

Møte i uteliv måtte skje før eller siden, og kvelden var bedre enn mange
andre. Singelstemplet tvang henne til handling. Et skritt i riktig
retning. Feil retning ville dømme henne til evig aleneskap. Resten av
livet ville hun leve som særing med dårlig ånde. Aleneskap var en felle
folk falt i altfor lett.

Minner gjorde vondt, et bilde på dårlig samvittighet. En liten pike sto
i døren og utropte til mors beiler at han aldri ville være velkommen.

“Aldri.”

Hun skrev ordet på veggen i brunt kritt slik at det knapt gikk å skimte
gjennom den mørke beisen. Hun var et vanskelig barn, og de var to.
Broren var rene gullungen til sammenligning. Hun mistenkte ham for å
ønske en far. Hun fikk Geir til å slutte å komme på besøk, og moren ble
tristere. Tristesse i aleneskap skremte hver gang hun stirret i speilet
og gjenkjente morens øyne.

Lesbiske er i faresonen, mer enn andre. Hun hadde forlest seg på
statistikk og levekårsundersøkelser. Rosa lampelys blinket like sterkt
som gul tobakklukt i renner under neglene. Lesbiske drakk mer,
skilsmissestatistikken vitnet om nære og intense forhold i konstant
oppløsning. Tallene talte imot de lesbiske. Legg på alder i
statistikken, blottstilt, hun forventet en framtid med mange åpne og
tomme dager, fylt håpløshet, fravær av drømmer og realisme.

Homobevegelsen skrøt på seg opptil tjue prosent av befolkningen, en av
fem fantaserte om andre med samme kjønn, homoseksuelle tilbøyeligheter i
nær sagt alle. Sosiologer, radikale likesinnede åpnet for muligheten
alle var homofile. Tankemønsteret definerte, kulturarv og språk.

Selvsagt løy de. Mest for seg selv.

Moderne og nøkterne forskere hevdet opptil ti prosent levde homofile
fantasier, og tallet ble en selektiv sannhet hos mange homofile. Hun
fant statistikken misvisende. Konklusjonen bygget på slutninger alle
heterofile venninner hun noensinne hadde nedlagt var lesbiske,
ungdomsårene inkludert, alle søkende, forført til en natts
eksperimentering. Hun mente homobevegelsen løp løpsk i det store
tallmaterialet. Spørreundersøkelser tok heller aldri høyde for de
perverse, en seksuell legning hun mistenkte bidro til å øke utvalget
betraktelig. Perverse og søkende tatt til inntekt for en homobevegelse
som tørstet etter legitimitet. Avvikere ville være der å drikke fra
homobevegelsens voksende aksept gjennom tiår målrettet arbeid.
Statistikk til besvær for LLH, data som betydde noe, de uoffisielle
tallene fra respektable levekårsundersøkelser viste praktiserende
homser, alle som gjentok homofile erfaringer og levde ut seksuelle
fantasier med samme kjønn var begrenset til rundt tre prosent av
befolkningen. Tre prosent, kanskje mindre? Ned til to prosent av folk
flest, kunne statistikken peke på som homofile, og hvor mye en
homopolitiker ville, i Norge var ikke prosenter med homofile i skapet.
Hun delte tallet, halvparten menn og kvinner, cirka en til to prosent
var kvinner. For ordens skyld sa hun halvannen prosent lesbiske bodde i
hjembyen. Totalt i befolkningen betydde det syttitusen lesbiske kvinner
i hele landet. Hovedstaden og alle omkringliggende byer antok hun
utgjorde trettifemprosent av norskingene. Dermed kunne hun utlede
tjuefemtusen kvinner som likte kvinner bodde nære. Utelukket hun
aldersgrupper mye eldre og yngre sto tjue prosent tilbake. Femtusen
kvinner reduserte utvalget betraktelig.

Femtusen lesbiske kvinner måtte holde for å finne den ene.

Hun søkte kjærlighet og drømte om en singel, attraktiv, sexy, frisk,
intelligent kvinne med ordnet økonomi.

Alt var ikke like viktig.

Hun var småspist, og tenkte det var viktig ikke å være fanatisk. Stilte
ingen urimelige krav til kjæreste, etterspurte ingen superkvinne. En i
et hav på femtusen kvinner i riktig alder og bosatt i byen. Singellivet
var viktig. Hun ville ha et ryddig forhold, ingen tredjeparter. Hun
holdt kravet strengt for dem begge. Kjæreste var målsetningen. Kravet
hun stilte til kvinnen i sitt liv var aleneskap sammen. De skulle være
et tospann. Flaks var den siste ingrediensen. Hav av mennesker i
utvalget på femtusen lesbiske kvinner måtte stå foran henne på riktig
tidspunkt. Hun gikk sjelden på byen, så godt som aldri vilkårlig.
Tilfeldigheter rådde og det var hennes oppgave å øke oddsene. Sitte
hjemme hver kveld å glane på teve hjalp lite på drømmen om kjæreste.
Surfing på nettet begrenset mindre enn sofatitting. Gjennom en
glassflate og kabler gravd dypt i jord fantes det et lite håp. Gaysir
skapte gjennombruddet og utvidet nettet for homofile og lesbiske.

“Livet skjer i det offentlige rom.”

Mumlet hun til seg selv.

Folk traff aldri kjæreste på nettet, og i alle fall ikke henne. Hun
hørte om tilfelle, leste intervjuer og utvilsomt skjedde det folk fant
hverandre. Bare ikke henne.

Hun gikk tilbake til Rockefeller for å gi livet en sjanse. Hver kveld
hun satt hjemme var en mulighet tapt. Rockefeller smalt, musikken slo
høyt i taket og veggen. En håndfull mennesker var ikke nok til å hindre
slagverket i dansemusikken fra å eksplodere i betongen. Straks innenfor
spottet hun Kasper ved et av de runde bordene inntil dansegulvet. Han
snakket med et par kolleger i LLH, to politikere hun bare kjente av
utseende og en litt eldre distingvert herre i dress. Politikerne
representerte fienden for en sosialist. Mannen var håpet for
konservatisme, og kvinnen skulle i følge politiske forståsegpåere redde
et av de mindre partiene fra utradering i neste Stortingsvalg.

Møter tidligere på kvelden måtte være forklaringen på hvorfor pampene
omkranset dansegulvet når lokalet ellers lå svært glissent i dunkel
belysning. Lokalet rommet hovedsakelig tillitsvalgte i LLH. Hun
gjenkjente et par aktivister, ingen av de store kjendisene, bare folk i
miljøet hun visste engasjerte bred støtte på tvers av partilinjer.
Homopolitikere skrev kronikker på Gaysir, satt merkelapper og flagget
tilstedeværelse. Det var bare rett og rimelig at de ville vise seg på
festene. Væren synes å bety mye for homopolitikere, og det var trolig
deres viktigste virkemiddel. Kasper var den fremste, en lederspire blant
likemenn i ferd med å bli oppdaget av mediene. Derfor sto to politikere
og pleide omgang med ham, og hun ville stille seg inntil fordi hun
kjente ingen andre der. Hun hentet lojalitet fra tiden de begge var
aktive studentpolitikere på hver side av fargespekteret. Felles historie
var åpningen hun gledet seg over å ha slike kvelder, årets LLH-fest
trakk mange tilskuere, Kasper hadde aldri vært bedre å ha i eget hjørne
og hvis ikke for vennen gikk hun hjem tidlig.

Hun kunne huske Kasper fra studietiden; varm og engasjert politikerspire
hvis største last var naturlig beskjedenhet på egne vegne.

“Hadde de vært i samme parti ville jeg løftet deg.”

Sa hun en gang til Kasper.

“Åh, ja.”

Han trodde selvsagt hun løy, og hun visste det var sant. Valg var
avhengig av hvilken hestehandel lot seg gjennomføre. De politiske
partiene sto klare til å absorbere studentpolitikere med hud og hår.
Arbeidslivet ventet, men ikke på Kasper. Han stilte til valg i LLH,
forlenget livet på lavgir og jobbet for homospørsmål for slikk og
ingenting. Han valgte å jobbe et år ekstra på kontoret til det liberale
fjollepartiet han elsket. Partier var tidvis trange slåsskamparenaer,
spesielt de små, verv kom sjeldent og dusinvis av tillitsvervjobber i
andre organisasjoner var forbeholdt de største partiene. Politiske
dinosaurer må ha steder de kan hvile, før og etter en karriere i det
norske kollektivet der demokrati betyr folk tar vare på hverandre—noen
mer enn andre.

Ledervervet i LLH ga Kasper en plattform der han gledet seg over
fullmakten til å feste. Timer før det ble liv sto Kasper på dansegulvet,
smurt i tålmodighet ventet han på livet vervet ga ham som
topptillitsvalgt. Kjekkaser i trange skjorter og glimt i øye til lederen
kretset rundt bordet deres.

Ledervervet gjorde han singel. Kasper hadde aldri vært mer omsvermet.
Tiden var feil for partnerskap.

“Kaller du dette liv?”

Anklagen var utplassert. Kasper leet knapt på skulderen, avslappet og
uten en bekymring. Sånn var det mellom dem. Hun rokket ikke ved
selvtilliten hans i kveld. Han sto der og underholdt to politikere,
omgitt av LLH venner og hun kom alene.

“Folk kommer. Bare vent.”

“Når?”

“Snart. Kjøp en øl.”

Hun gikk i baren, såre fornøyd. Introduksjonen gikk smertefri, hun fikk
leke med vennene hans. Vente på folkehavet, ha et lite vorspiel der og
tålmodig glede seg over kvinnene som ville komme. Politikerne ville gå
straks folkemengden tettet seg til, kanskje for midnatt, mens utvalget
av festglade homser vokste. Hun var glad for å være ute, takknemlig for
Kasper, tiden hun brukte på universitetet den gang, midt i smørøye, var
en kilde til næring—fortsatt.

Blant folk konverserte hun og spanet på andre lesbiske. Inntil Kasper
var hun trygg. Markedet var i endring. Hun hadde vært ute av sirkulasjon
en stund. Ferske sår tilsa det var tilstrekkelig for henne å henge i
baren og snakke med kjentfolk som hun knapt visste hva het. Hun
organiserte observasjonene rundt hvem gikk på deit med hverandre. Hvis
plakater med profilbilde hang på vært hvert gatehjørne kunne det ikke
hjelpe mer. Lesbemiljøet i hovedstaden ville se henne på hovedtavlen,
vite hun var der, ute blant folk. Hun var en de kjente valgt, en av dem.

Kasper fikk rett. Folk kom i hopetall til Rockefeller. Mange.

Samtalen rundt bordet gikk smidig. Hun ble gjenkjent fra bakteppe, hun
var et fjes de hadde sett før og tonen var lett. En transvestitt fra
Skeiv Ungdom sto og hang, en av Kaspers løsunger fra konferansen.

“Jeg er feit.”

Sa hen, og hun lurte på om det var flørt.

“Neida.” Hun hadde vært singel lenge. Her gjaldt det å fokusere og i
fullt alvor diskuterte hun mat og livsnytelse, gledene gourmetmat
tilførte hverdagen.

“Vi har for lengst sluttet å bry oss om babyfett rundt livet.”

Sa hun.

“Nettopp.”

Kasper slo seg lettere animert på magen. Hun var takknemlig for
homoskavankene hans skiftet samtaletema. Tross sosialisering og sjarme.
Lenge siden hun flørtet med heterodamer, og hen var verken det ene eller
det andre. Transvestitter manglet dessuten et mysterium som gjorde flørt
spennende. Hun følte ingen seksuell dragning mot menn i klær og uten.
Forsto hun tilbudet riktig? Hjernen spilte henne puss før, og hun hadde
lært intelligens, evner til konversasjon og sjarme var aldri nok hvis
koblingene stoppet ved nesetippen.

Tre dansere på LLH-festen, en mann i tiger tights, flankert av to
kvinner i miniskjørt og singletter fylte et hjørne på det tomme
dansegulvet, golvflaten nærest dem. Musikken skiftet til forårets
svenske bidrag i melodifestivalen. Noen ønsker å sette festen i gang.
Showet trillet. Folk strømmet mot dansegulvet og det tettet seg til
foran dem. Selskapet rundt bordet trakk bakover for å slippe folk forbi.
Kasper nikket og hilste elskverdig på alle. Hun lot blikket flakke og
gjorde ikke mer utav seg enn at mennesker slapp uhindret til på
dansegulvet. Takknemlig for tiden som løp. Bekymringer om å stå alene i
et glissent lokale glapp tak. Mengdene strømmet inn døren og
aktualiserte planene om å cruise lokalet. Kvinner, unge og eldre,
kvinner i alle aldre gjorde seg fjonge, feststemte lesber og
transvestitter i herreklær. Showet startet på scenen. Falske-Elvis fikk
godkjent, kortvokst, framtung og i parykk nitti grader rett opp og
publikum humret hver gang hoften rykket pelvisen. Stine, som Elvis
egentlig het, vrikket i takt med *Jailhouse Rock* til høflig klapping
fra homsene. Den andre ølen kjøpte hun i baren til akkuratpris.
Bartenderen hadde det travelt og la ikke merke til fraværet av driks.
Hun tok ølglasset og gikk tilbake til Kasper som var i en samtale med en
fremmed mann. Transvestitten gikk, og hun var takknemlig. Nå var det
fest. Et øyeblikk tenkte hun Kasper og mannen flørtet, og avviste tanken
som absurd etter å ha vurdert den fremmede som eldre, gammel, minst
hennes alder og muligens eldre. Mannen må være ti år eldre enn Kasper,
og hun visste vennen likte yngre menn.

“Dette er Roger fra Gaysir.”

“Aha, Gaysir.”

Roger gjenkjente lydordene fra en desillusjonert bruker.

“Har du profil?”

“Jo da, men jeg sletter meg med jevne mellomrom.”

Roger så spørrende på henne.

“For å minne meg selv om at jeg har et liv.”

Sa hun og løy.

Monoton bakgrunnsmusikk og stadig flere dansere på gulvet etter
transvestittshowet til Da Kings forlot scenen endret stemningen. Snart
begynte hovedshowet. Guttene begynte å ivre, flere på dansegulvet skapte
trengsel rundt bordet. De første ankomne, de med bord til ølglassene ble
skjøvet bakover, støynivået ville avslutte alle samtaler. Fast forankret
til Kasper sto hun og ventet.

Roger kunne hun snakke med om Gaysir, opplevelsene, traumene,
forhåpningene. Han kjente trolig miljøet bedre enn de fleste.

“Jeg er aksjonær.”

Sa Kasper.

“Var du med i Syklubben?”

“Ja, blant de aller første medlemmene.”

Hun stirret på Roger fra Gaysir og dummet seg instinktivt ned. “Hva er
aksjonær?” Hun visste hva en aksjonær var, men ville vite mer om
forretningsmodellen bak nettstedet.

“Syklubben møttes for ti år siden hver onsdag—en gjeng gutter, og de
startet Gaysir.”

Kasper oppsummerte kjapt og mye var utelatt.

“Er det mange som jobber der?”

Hun spurte Roger. Han holdt opp en hånd og fem fingre sprikende i hver
retning, mens han gløttet bort på Kasper opptatt av den unge mannen i
tigertights på dansegulvet. Folkemengden tettet til. Roger registrerte
våken rivalen.

“Fem heltidsstillinger.”

Roger ristet på hode: “Seks.”

Det var ganske bra, tenkte hun og nikket anerkjennende. Startsiden ble
solgt til Telenor for millioner og de var to ansatte i starten,
inkludert gründeren som tjente millioner. Dåttkommbølgen kom og gikk,
selskapet hauset opp aksjekursen. Mange gikk konkurs. Gaysir hadde
overlevd. De drev fornuftig og betalte lønn til et halvt dusin ansatte.
Hun husket tiden de startet vagt, og hvordan hun valgte bort Gaysir.
Nettsjekking passet henne dårlig også den gang. Hun tjattet hos
romlingene—jentealternativet, driftet på samme lest og i dag et hult
skall ingen besøkte. Hun dro på fisketur til Sogn og Fjordane, bodde på
gård og festet med romlingene, men det var ensporet sjekking i
guttesfæren som overlevde tiåret. Når Roterommet inviterte til
hjemmefester, kakebaking og venneprat, pulte guttene av hjertens lyst på
bakrommene til Herr Nielsens pub. De små rommene i kjelleren hang i hop
med homsenes mekka London pub. Puleboksene innkasserte huseieren en pen
profitt i kommisjoner fra velvillige thaigutter som jobbet og knadde seg
gjennom tre måneders turistvisum. Årevis fryktet hun kulturen og søkte
det trygge rendyrkede jentemiljøet. Kjærester traff hun via venner.
Tjatting gikk hun raskt lei.

En dag skjedde det utenkelige. Hun lå fanget, slukt av forum og grepet
av panikk. Gaysir og den nådeløse sjekkingen vant dagen. Hun traff Fab,
og mistet flørten like raskt.

Roger hevet glasset for ti års cruising, et liv i sus og dus, lønn og
moro pakket i ett. Han smilte tappert mot Kasper, men vennen lente seg i
en annen retning. Hun visste Kasper var kresen og målrettet. Unggutten i
tigertights ville vinne natten.

Når hun først sto der slapp hun ikke Roger. Hun ville vite mer.

“Bare kontakt meg.”

Sa Roger og rakte henne forretningskortet fra Gaysir.

Hun tok godt grep, og slapp han fri til å prøve å feile med Kasper.
Tiden var der for henne å mikse med Rockefellers voksende publikum. Hun
ga vennen en klem og rygget baklengs ut på dansegulvet med et skjevt
glis rundt munnen.

“Kom, la oss danse!”

Deits
=====

  Do not wait to strike till the iron is hot; but make it hot by striking

  — William Butler Yeats

9
-

Hun inviterer tre enavn på deit, den ene fordi hun fikk et blunk og
ville gi det frekke tilløpet en sjanse, og to enavn velger hun etter
kriterier på utseende, høyde og livsstil. Kravene er kort fortalt høyde,
fordi hun er høy selv, og aldri før har hun vært kjæreste med en røyker.
Skifte preferanse i forhold til tobakk virket ekstremt, for selv om
søket skjedde på nettet måtte hun anta de ville møte hverandre og
røyklukt er kvalmt. Byen ligger implisitt i søkekriteriet, et skille i
utvalget hun i praksis valgte måneder i forveien, slik hun skilte bort
menn. Alle tre deits var bosatt i Oslo.

Jevnt over er hun mistroisk til beskrivelse av livserfaringer og merker
de lesbiske kvinnene gjerne skryter av interessen for å gå tur i fjellet
og i marka. Hun tenker i sitt stille sinn at finnes det verre
sofagriser? Du kan ikke gå i marka hver dag. Skriver profilen på Gaysir
de er glad i litteratur mister hun tråden når de anbefaler *Havets
katedral* av Ildefonso Falcone. Romanen kjente hun som intetsigende
bestselger i bokhandelen. Hun forsøkte å lese suksessromanen og fant
hele øvelsen meningsløs etter en halv bortkastet dag. Hun elsker
Barcelona, byens katedral som romanen kretser rundt ville hun gjerne
besøke igjen. Historisk litteratur er et pluss, men debutromanen til
spanjolen kunne ikke måle seg med Jose Saramagos blodfordekte roman
*Beleiringen av Lisboa* eller alt historisk fra Umberto Eco. Hun ble
trøtt av de lange setningene til Falcone.

Ingen som anbefaler *Havets katedral* kan være glad i litteratur. Hun
uttalte seg gjerne kategorisk og liker det best når tankene kom klart
fram.

Invitasjonen hun tenker mest over er til en lesbisk dame i nabolaget der
hun bor. Riktig alder og en ærlig framtoning gjør det vanskelig å avvise
damen. Ingenting i profilen virker oppstyltet og falsk. Hun stirrer
blind på bildet og lurer fælt på hva som er galt. Ekte mennesker
fortjener en sjanse og hun har definert kriteriene. Damen er kvalifisert
på alle punkter. Hun huker av kriteriene en for en på sjekklisten. Like
fullt trykker hun ikke på send, invitasjonen blir hengende på skjermen
uten at hun forstår hvorfor?

Bilder fra marka, fjellet, på turbosykkel i naturen. Dette er en lesbisk
kvinne hun tror stemmer med friskusen på bildet, og realismen passer
dårlig. Fjellturer skjer i realiteten sjelden for hennes del, og de
kommer alltid i stand etter initiativ fra andre. Gode venner og broren
trår til og tvinger kroppen ut et par ganger i året. Med gru tenker hun
hva det vil bety hvis en kjæreste tar over maset. Tre profiler i
meldingsboksen fikk holde, og de lesbiske på Sagene kan puste lettet.
Hun vil teste om Gaysir er interessant, vite hva nettdeit kan være hvis
hun legger inn innsats og pågangsmot. Suksessmålet er definert: Ett
møte, en samtale, og det ble etablert kontakt derfra. Deit er håndfast,
og virtuelle utvekslinger enten det er epost, tjatt, små kybervink og
annet tull skal hun styre unna. Treff i virkeligheten, de behøver ikke
håndhilse, samtale live er alt og det absolutte kravet for å huke av
eksperimentet som vellykket. Fab inspirerte henne til å prøve, gi
hovedtavlen en sjanse, og tre profiler var utvalget.

Fripassasjer blir Blunke-Guri, en kvinne like under kravet til høyde og
med en blomstrende drøm hun ikke deler om å flytte på landet, kjøpe
småbruk og holde høner. Positiv egenskap hos Blunke-Guri er god smak og
et blunk i riktig retning. Hun visste å verdsette oppmerksomhet.

En snikende mistanke har slått rot og er i ferd med å ødelegge de første
ustø babyskritt på Gaysir og skape angst for å låse tilværelsen på
hovedtavlen. Kall det hva du vil, en kvalifisert gjetning fortalte
alderstallet i parentes bak navnet lå over en psykologisk sperre, en
usynlig strek der fine bilder, sjarme og vittigheter ikke gjør
forskjell. Hun mangler uansett alt. Fullklaff passer i alder, de er like
gamle, begge oppgir interesse for litteratur. Profilen ber om en
bokanmeldelse uten å gi hint om preferert genre. Vil hun vinne en deit?
Bakgrunnen er uklar og gjør satsingen risikabel. Teksten trekker henne
inn, og det sterke lyset som gjør det umulig å gjenkjenne trekk i
ansiktet skjemmer ingen. Nettkresen forstår hun godt enkelte velger å
være, og egenskap blir respektert. Selv bytter hun profilbilde ofte og
angrer noen ganger valget hun har tatt. Hun bytter det ene profilbilde
mer gjenkjennelig enn det andre. Anonymitet frister, virker forlokkende,
mer enn noensinne.

Forhåpninger til invitasjonene har hun først og fremst til Fullklaff.
Dark Horse er den tredje profilen hun henger på prosjektet helt til
slutt for å ha et utvalg på tre. Bildet er vagt, diffust og samtidig
klarere. Hun tenker det er en pen dame. Kanskje? Det lett forskremte
fjeset er tatt i farten på firmafest, og på toppen av hode sitter en
spiss papphatt. Dark Horse har høyde lik henne og det holder. Tre
profiler med fysiske attributter som passer. Hun vil gi dem sjansen til
et møte, rom til å takle utfordringen og gjøre hva hun kan for å skape
god og åpen tone. Hennes vei til suksessmålet gikk gjennom dem. Hun
inviterte tre deit, og resten i nettsamfunnet fikk ryke og reise. En av
dem skulle gi henne tro på at kjærlighet er mulig i kyberlandsskapet, og
det vet hun først når Gaysir beviser nettet er et sted å knytte
kontakter.

10
--

Timene etter invitasjonene forstår hun hvor lite forberedt hele
prosjektet er. Invitasjonene går spontant til tre profiler tilfeldigvis
pålogget i øyeblikket. Meldingene sendte hun på impuls, og det preget
resultatet. Tilnærmingen hun bruker er av det simpleste slaget.
Overflatiske kriterier valgt på instinkt: Profilbilder hun liker, høyde
over 170 centimeter og status ikkerøyker. Disse tre er verdige en deit.

Verdige, i betydning, heldige for de skulle få æren av å møte henne.

Søket favner hovedstaden, geografi begrenser. Invitasjonene går til en
smal krets.

Hun sender meldingene. Blunke-Guri svarer først, hvilket virker rimelig
siden det gikk et virtuelt blunk i hennes retning tidligere på dagen og
begge er pålogget. Grønne prikker inntil enavnet indikerer nærværet. Hun
synes det er en hendig oppfinnelse; blunkene alle kan sende med korte
hilsener, en invitasjon som glimter, humor innebygd i funksjonalitet.
Overraskende mange føler blunk er lettere enn sammenlignbare korte
tekstmeldinger. Et riktig timet blunk, straks før hun inviterer på deit
katapulter Blunke-Guri til toppen av listen. I en leilighet sitter
Blunke-Guri og drømmer om å eie et småbruk sier profilteksten.
Solbriller skjuler øynene. Fjeset er stivt, en maske i sollys. Blekt
ansikt, lyst hår, nesten hvitt og kort. Hun ville ikke snudd seg på
gaten. Andre ville stirret på en slank kropp, en blondine på sykkel.
Blunke-Guri bærer på en hemmelig drøm om småbruk. Hun fortryller over at
nummer tre på norskingenes ønskeliste er å eie et småbruk, etter hytte
og båt. Vi får ikke nok av eiendom i dette landet.

Hva de to andre enavnene gjør minutter etter at hun trykker på send er
et mysterium. Dark Horse er neppe på tur i fjellet, og Fullklaff sender
en kryptisk melding. Kvinnen bak enavnet med adresse på Frogner er på
vei ut døren og vil svare senere. Deretter forsvinner den grønne
prikken. Fullklaff logger tvert av. Hun hadde vært mest spent på
reaksjonen fra henne, men fortviler ikke etter det åpne svaret.

Fornektelse og rødmetoner, Blunke-Guri kommer henne i møte uten å
erkjenne det minste ansvar.

“Oj, jeg rødmet når jeg sendte blunk.”

Koketteri. Hun setter det på kontoen for flørting.

Juli måned, midt i norsk ferieavvikling forteller Blunke-Guri de først
kan møte over helgen. Hun ergrer seg innvendig over hvor dårlig planlagt
hele prosjektet er når det går opp for henne førstkommende lørdag drar
hun på ferie til Tromsø for å besøke familie. Bestemoren venter og
pikeværelse til tanten er ferdig oppredd. Morens rom var nedlesset i
skrot sist hun besøkte, og hun regner ikke med at det har endret seg.

Hun forteller Blunke-Guri situasjonen. Neste uke drar hun på ferie og de
må vente.

Enkle søk på nettet, fornavnet og en flom bilder dukker opp når hun
søker på Google. Informasjonen hun gir i de første meldingene er
tilstrekkelig for å finne det som er på internett om henne. Uautoriserte
bilder fra tilstelninger, dykk i lokalpressen der hun er intervjuet i
den lokale idrettsforeningen. Hun fikk slettet et umodent lyrisk uttrykk
fra Dagbladets poesihjørne, det skulle være bursdagsgave til Fab, men
når filen slettes har de ikke lenger kontakt. Hun skriver knappest mulig
en melding for å fortelle Fab diktet er slettet og det vil etter
cachsyklusen på tre dager forsvinne fra Google for alltid. Hun får en
takk, Fab smiler og ønsker henne lykke til videre. Kunnskapsbasen om
henne er full. Snart vet Blunke-Guri det meste, og hun vet ingenting—vil
ikke og nekter å spørre. Deit er inngangen til å bli kjent, og hun har
ingen forhåpninger utover vitnemål fra andre som forteller hvordan
nettet fungerte for dem. Hovedtavlen på Gaysir lister virkelige personer
tvangsrekruttert til et hendig eksperiment. Prosjektet vil gi svar på
livets store gåte: Finnes det noen for henne der?

Hun vil la de tre profilene overraske.

Når suksessmålet er nådd høster hun og vil igjen kjenne luksusen å bli
kjent. Glede, og hvis skikkelig heldig eufori. Deits vil trekke livet
forover, de er bærere av håp. Før hun kan senke skulderen vil hun kjøre
prototypetester og bestemme om Gaysir er bortkastet tid.

Hun avtaler med Blunke-Guri å møte om to uker, etter ferien i Tromsø og
når begge er tilbake i byen. To invitasjoner i fri flyt gjør det
ingenting å utsette deiten med blekansiktet bak solbrillene. Hun venter
på Fullklaff som lovte svar samme kveld for deretter å logge av.
Møysommelig utforming av invitasjonen gjorde henne spent på personen bak
det uklare bildet overeksponert i lys. Hun ga en bokanbefaling til slutt
i eposten—slik Fullklaff ba om. Ingen av de andre invitasjonene fikk
skreddersydd et personlig nikk. Følelsen hun må legge inn det lille
ekstra til profilen vokser fordi det er Fullklaff hun mest av alt vil
lære å kjenne. Hun anbefaler en norsk roman fra det siste tiåret og er
såre fornøyd. *Hav av tid* av Merete Morken Andersen fikk
litteraturpriser. Hun forstår det triste tema ikke passer alle.
Følelsene i den nøye sammenvevde historien appellerer forskjellig. Mange
hadde ikke oppdaget romanperlen blant de nyere norske utgivelsene.
Fullklaffs respons på litteraturtipset er minst like spennende som
svaret på invitasjonen til en deit på kafe.

“Fullklaff må svare ja.”

Tenkte hun.

Svarte Fullklaff ja til en invitasjon å være med på kafe, eller nei?

Nærstudier av bilde og profiltekstene i minuttene etter hun sendte
invitasjonene avslører hvordan hun slumset da hun plukket utvalget. Hun
venter. Sorthvitportrettet viser blondine nummer to. Hode til Fullklaff
er vendt mot vinduet der en katt står i vinduskarmen. Rasekatt, ikke som
den avdøde skogkatten hennes, påkjørt da han slapp løs i gatene utenfor
portrommet.

Fullklaff liker katter. Hun føler sterkt profilen virker lovende.

Invitasjonen gikk klokken 20:34. Kafemøte på Frogner i nitiden skjer
mellom venner, eller er det en deit. Flørt? Hun beregner når hun kan
vente Fullklaff tilbake foran tastaturet hvis det er det siste. Hun ble
lovet et svar samme kveld.

Blunke-Guri fyller tiden mens hun venter, en stund knoter de med
praktikaliteter ved avtalen, deretter går statusprikken inntil enavnet i
oransje. Hun venter på Fullklaff. Blunke-Guri har vært på ferie i
Tromsø, og hun skal reise samme vei førstkommende helg. Begge ser
tilfeldighetene og lar være å si mer om saken. De er ubestemmelig
opptatt. Stilletiene unngår begge det klamme møte mellom to kropper.
Deres avtale er grei, ryddig, sikker og det er greit å utsette selve
møtetidspunktet to uker. Nysgjerrighet knytter hun til kvinnen med den
perfekte profilen. Forvirring og forbauselse lammer kort hjernen. Hun
oppdager Dark Horse er den eneste av de tre profilene som fyller alle
kriteriene hun satte på utvalget. Tre profiler mottok invitasjon til
deit. Hun hadde det travelt, plukket tilfeldige enavn fra et søk på
hovedtavlen der hun allerede utelukket menn, kvinner utenbys fra og
røykere. De er pålogget, og det var siste kriteriet. Hun forstår hvor
raskt beslutningen ble tatt. Det må ikke påvirke avgjørelsen. Hun kan
håndtere en krise, og to kriser går enda greiere. Profilen til Fullklaff
avslører kvinnen er lavere enn 170 centimeter. Blunke-Guri er taus om
røykestatus.

“Jeg røyker ikke.”

“Godt.”

Sa hun.

Mildt lettet. Sjokkert over hvor slumsete hun velger prosjekter, hun er
altfor rask på avtrekker, og erkjennelsen skaper usikkerhet til hele
planen. Fjeset med solbrillene gjør ingenting for henne. Blunke-Guri ser
ut som en røyker selv om kvinnen nekter. Kanskje det var den hvite
huden? Fraværet av en status må bety det er mistanke om
nikotinavhengighet. Kynismen smitter. Hun liker det ikke. Det raskere
hun forlater nettdeiting med en kjæreste, eller visshet om at
tilværelsen på Gaysir er tullball, det bedre.

Deiten med Blunke-Guri er i rute. Hun unnlater å stille flere spørsmål
og der slutter meldingene. Lenge før Fullklaff kommer tilbake fra
kafetreff har de snakket ferdig.

Sengen er stor og dyp—hard slik hun liker. Trøtt. Hun venter i sofaen,
tastaturet står uvirksom og prikken går i oransje for uvirksom. En grønn
prikk foran enavnet til Fullklaff gjør at kroppen spenner, musklene
venter forventningsfull og det er ingen vei tilbake. Hun er klar.
Fullklaff uteblir lenge. Søvn må vente. Hun oppdager meldingen i
innboksen før den grønne prikken inntil enavnet. Lyssignalet forteller
kafebesøk på Frogner er slutt og kvinnen på Frogner sitter foran
datamaskinen. Hva blir dommen? Meningen med dagen koker ned til
øyeblikket foran skjermen, hun venter på et svar, minutter før midnatt
og før begynnelsen på en ny dag sitter de spent.

Fullklaff begynner forsiktig, skriver hun liker bokanbefalingen og
avslører den var godt kjent, allerede lest og fordøyd.

“Forfatterinnen har god psykologisk innsikt.”

Hun er glad de har noe å snakke om og tenker på skrivekurset hun tok med
Merete Morken Andersen. Forfatterinnen anså hun for å være Norges beste
redaktør. Hun sporer ikke i romanen spesiell psykologisk kompetanse og
stusser på ordvalget. Teksten fanger det varsomme i sorg, fletter
personlige prøvelser i en velskrevet øm skildring og viser stemmen bak
er en kunstnersjel. Grepet om leseren sitter fra første til siste side.
Leseren føler smerten på sidene. Merete Morken Andersen sa opp
redaktørstillingen i landets største forlag, første glimt av hvilke
talent hun besitter fant leser i uttrykk om et ungt menneskes selvmord.
Teksten er forskjellig fra alt forfatterinnen skrev tidligere. Hun lar
seg overbevise om forfatterskapet. Samtalen med Fullklaff flyter
naturlig når fokus er teksten.

Kvinnen bak profilen Fullklaff liker Merete Morken Andersen. Det har de
felles.

En anelse tilbakeholdenhet føler hun fra profilen håpet er hektet til.
Hun er forskjellig fra forfatterinnen. Kunstnersjel versus en
firkanttenker. Det var vanskelig å beskrive hvordan hun tenker, kjemper
mot strømmen og prioriterer.

“Nerd beskriver meg best.”

Sa hun til venner.

Folk legger mer i uttrykket enn hun vil, og i dyp konsentrasjon klarer
hun stå fram som alt annet enn usosial og ensporet. Ordbokdefinisjonen
på nerd. Folk tror henne ikke alltid. Mistenker koketteri, et spill for
galleriet.

Innsmigret og tilgjort nedsabling av egenskaper for å oppnå motsatt
effekt. Ofte får hun direkte komplimenter, folk aksepterer ikke raskt at
hun er nerd.

Mistenker de hun lyver?

Kort avslutter Fullklaff med konklusjonen hun må akseptere. Såpass
forstår hun.

“Beklager, jeg er allerede på vei ut på deit med noen jeg traff her
inne.”

Svaret treffer der det skal, og hun føler kraften i all styrke. Utslått!

Tre invitasjoner og to tilbake.

Kan hun la være. Kommentaren Fullklaff gjør om Merete Morken Andersen
tolker hun dithen bokanbefalingen falt pladask gjennom.

“Psykologisk innsikt, tja, sporer ikke det i teksten. Tvert imot,
dramaturgien er kjernen i bokens suksess. Jeg mener Merete personlig er
en smule fluffy. Fordi jeg åpenbart bommet, her er en ny bokanmeldelse.”

Skrev hun.

Hva betyr ordet fluffy? Innsikt i sjelen er uttrykket, altfor vagt og
ubestemmelig leter hun etter ord. Poenget er bokens dramaturgi,
fortellingen, dets indre oppbygging lar seg veksle i en dypere
forståelse av menneske. Hvis Fullklaff tror de har en faglig uenighet
vil hun lukke misforståelsen. Ferden starter med en bokanbefaling nummer
to, en bok skrevet av Odd E. Surén. Valget faller på *Diskrete
kulisser,* en av hans novellesamlinger. Boken er fylt med småtekster i
Kjell Askildsens ånd. Fullklaff må ha lest Askildsens samling *En
plutselig frigjørende tanke,* kanonlitteratur blant norske noveller
lovprist av åndseliten. Hun mener også neste anbefaling må tilhøre
språkgruppen, og Surén er odde nok til å klaffe. Svenske forfattere er
bra hvis hun overser krimlitteraturen. Valget *Hav av tid* feilet og det
måtte bli den skoløse bergenseren.

Nederlaget fra avvisningen sitter dypt. Erkjennelsen likeså. Håper hun å
gjøre om beslutningen? Tvinge Fullklaff til å innse hvis det er to deits
å velge mellom, da passer hun best. Håpet var rettet dit, og
tapsfølelsen sitter i kroppen.

Hun er en konkurransetenker og bortsett fra faktum nettflørting er
forskjellig fra konkurranse er det ingen grunn til å stoppe. Umiddelbart
svarer Fullklaff. Fluffy virker ikke bra, men det kommer ingen motstand
fra Frogner og påstanden står uimotsagt. Ingen kommentarer betyr ingen
krangel. Fullklaff presiserer hun mente psykologisk innsikt. De hadde
jobbet sammen—kjente hverandre, og hun tolker svaret. Fullklaff advarer
mot å si mer—si noe ufordelaktig om venninnen.

Svaret gjør tapet større. Hun liker Fullklaff mer. Faktum er vennskapet
og forståelse for litteratur er enda et bevis på kompatibilitet.

“Jeg gikk skrivekurs med Merete Morken Andersen og kjenner metodikken
der musikk trigger sansningen.”

Hun krydrer meldingene med frivillig ekstrainformasjon—byr på seg selv.
Nekter å gi slipp. Hun jobber ikke med litteratur. Skrivekurset strakk
seg over to dager og favnet ordflom, lyd og bilder, lukt og impulsive
skriveøvelser—uten stopp. Kort fortalt, inntrykket er at metodikken til
Merete Morken Andersen er fluffy, ikke for alle. Hun ville ikke påstå
den er preget av psykologisk innsikt, mer psykologisk intuisjon. Forstår
Fullklaff forskjellen? Skal hun utdype.

Nok sagt.

Trykk send. Se hva som skjer. Ingen respons i kveld.

Tre invitasjoner og to tilbake.

11
--

Skuffelsen av å bli avvist blander seg med fryd over å ha sikret avtale
om deit. Seieren vil hun bokføre om to uker når planene iverksettes. Hun
våkner og logger på Gaysir for å se spor fra natteaktivitetene. Hun
finner meldinger og søker på utvalget. Godt synlig på toppen av
hovedtavlen henger Blunke-Guri.

“Hallo, hallo, Blunke-Guri. Hvordan går det?”

Vil hun spørre, tenker i stedet og lar være. Avtalen var sikker, hun
ville ikke risikere å ødelegge målsetningen når hun først kom så langt.

“Jeg ser deg nok.”

Ville hun si, sende hei, men går i gang å sjekke status. Hun søker og
får bekreftet alle har lest invitasjonen. Hun har ennå ikke slettet
meldingene fra Fullklaff og finleser dem for å være trygg på at det ikke
er mer å svare. Hun bekrefter løse tråder er lukket. Blunke-Guri våker.
Hun har sikret deit og vil ha svar fra neste før hun feirer. Dark Horse
setter henne på sporet. Svaret fra den tredje og siste invitasjonen er
ikke et svar, snarere et spørsmål og invitasjon til flere.

“Hvem er du. For eksempel hva gjorde du i ferien?”

Tredje invitasjon til Dark Horse krever oppfølging. Hun frykter
konsekvensene av spørsmål og svar, den samme endeløse utforsking hun
gjorde med Fab før følelsene la seg lik en snor rundt halsen og kvelte
kritisk sans. Prosessen er omstendelig. Hun aner det ligger mer arbeid i
å pleie brikkene allerede på brettet. Ferien var ikke over. Regnværet
utenfor vitner om ventetid før snøværet kommer og avgjør. Lange dager
markerer årstid. Luften har en varm bris fra sommeren. Kolleger for alle
vinder. Ingen spør hva hun skriver. Definitivt skriver hun ikke på
dokumenter om teknologi, eller kundegrunnlaget på regionskontorene.
Kravspesifikasjoner. Hun sender ingen epost forespørsler om status—tar
ingen initiativ og skjeller ikke ut noen. Jobben i juli er en rimelig
meningsløs øvelse i flaggstangtitting. Bokstavelig talt. Vinden blåser
opp tre av selskapets logoflagg som henger utenfor i allslags vær.
Bortreiste kolleger kan verken veiledes og beordres til lydighet.
Smiskes, briskes og trikses med. Tiden fordriver hun lett på Gaysir.

Spørsmålet til Dark Horse er konkret, det er enkelt å svare. Årets
feriedager, alle som en, bruker hun til ferieturer med venner til
historiske steder. Hun besøkte et vennepar i Reykjavik februar.
Førstkommende uke skal hun til bestemoren i Tromsø. Oktober booket hun
inn Bulgaria. Hun vil besøke den gamle romerske byen Plovdiv der fjorten
venninner planlegger antikkferie og studier. Sant nok, hun kjenner bare
tre i reisefølge, men det er ingenting å si på organiseringen.
Billettene er kjøpt, og hun står klar samme kveld ferien starter. Turen
til Plovdiv skal nytes til fullt, og påvente avreise sitter hun i Oslo,
alene på jobb.

Ærlig og redelig svarer hun Dark Horse. Der sitter svaret, tenker hun, i
sannheten. Svaret er ikke egnet til å imponere. Svaret vekker
nysgjerrighet, kanskje?

“I sommer har jeg lest til førerkortet.”

Slenger hun etter for å motvirke inntrykket hun sitter hjemme og gjør
ingenting—for det har ikke vært ferie i år å snakke om. Hun forteller om
Reykjavik i februar, Plovdiv i oktober og Sibir i september for å gjøre
det mer interessant. Sibir er ikke bestemt enda, og hun tror
historieforeningen vil vente med å bestemme endelig til neste år. Svaret
er egnet til å utdype og dekke over fraværet av fridager i juni og juli.

Alt er sant. Tygg på svaret: Er hun interessant?

Dark Horse har en beslutning å ta, og hun vil roe tempoet. Gi dem tid
til å tenke. Det er flere veier til suksessmålet. Blunke-Guri er en. Hun
vil takle et par spørsmål fram og tilbake uten å la følelsene løpe
løpsk. De forsiktige skrittene appellerer, hun forstår dem, og
gjenkjenner tvil. Dark Horse vokser i den ustøe tilnærmingen i et farlig
landskap.

Aldersforskjellen er et tema. Hun var selv i tvil, men valgte å sende
invitasjon for å få tre. Problemet er Dark Horse har de samme
betenkelighetene. Spørsmålene kommer på løpende bånd. Hun tenker de er
en høflig avvisning, et forsiktig forsøk på å finne en hake,
unnskyldning for å takke nei. Hun svarer etter beste evne og kjemper for
å sikre deiten. Dagene er de samme, tomme små dykk til dagens spørsmål.
Hjemme skal hun møte Gaysir og gå på deits. Hun tar kontakt etter
hjemkomsten fra Tromsø, sender meldingen som minner om invitasjonen hun
sendte. Planen er bekreftet. Det lyser grønne prikker inntil alle
enavnene på hovedtavlen. Bilder henger uendret. Blodfattig hvitt
spøkelse i sorte gjennomtrengende solbriller stirrer tomt, får ikke høre
de endeløse fortellingene hun gir Dark Horse som suger dem opp uten å
love noe som helst. Annie Lennox utseende gjør ikke inntrykk. Redselen
for å ødelegge hva hun har oppnådd sitter i hver pinefylte
uthalingsteknikk. Hun må til Tromsø, deretter kan de snakke når hun er
hjemme igjen.

Skjermflimmer. Kolleger beveger seg i øyekroken, de kjenner ikke slitet
og stresset med å nettdeite. Fullt oppreist kan de skue skjermen, se
nettsidene til Gaysir, grelle og glorete, blå bakgrunn med et klumpete
ikon øverst. Bilder av bare overkropper, annonser i glam, ben, skjelett
og andre blikktjuver. Folk i seriøse bedrifter surfer ikke på Gaysir.
Hun ergrer seg over å sitte i kontorlandskap. Heldigvis er kollegaen som
sitter bak sjelden på plass. Stadig mer tid foran hovedtavlen i vente på
svar begynner å bli pinlig. Paranoid veksler hun mellom vinduene med
hurtigtaster og blar mellom skjermbildene. Hun venter forgjeves på en
reaksjon som aldri kommer. Hun sitter med en lysende grønn prikk og
venter på meldinger fra Blunke-Guri. De er enige. Skal de snakke før
avtalt? Hun er ikke nysgjerrig på kvinnen, spør ingenting og får ingen
spørsmål tilbake. I stedet føler hun nysgjerrighet mot den andre
profilen; uro, spenning, eller sitrende forventning. Hun får ikke ja,
spørsmålene er høflige, forsøk på snill avvisning og hun forstår
posisjonen—respekterer det til og med standpunktet. Dark Horse er en
utfordring der Blunke-Guri er flat og uinteressant. Uken løper. De tar
verken direkte kontakt, eller søker andres profilforside. Suksessmålet
er alt hun evner, og hun enser ikke hvordan blikket blokkerer for
framdrift.

Ballen Blunke-Guri glipper fra første stund.

Hun stiller ikke spørsmål. Vet knapt om det er fordi hun ikke aner hva
hun skal spørre, eller hvorvidt hun vil vite svaret. Mulig hun
simpelthen er redd for følelsene.

Overflødig tid går med til å skrive lange svar til Dark Horse om hva hun
gjorde i ferien. Oppgaven synes enkel. Hun skriver på rams hva hun kan,
redselen for å grave i andres drømmer strekker seg lik en fjelltopp over
skyene. Hvis hun framprovoserer en reaksjon der krav og ønsker vil
utløse større forhåpninger. Hun holder enkle suksessmål. Dark Horse spør
om skriveriene hun gjør på fritiden. Svaret hun gir er dulgt, for å
erkjenne hun skriver og hva hun skriver blir altfor privat. Dark Horse
har truffet en grense. Hun kjenner ubehaget og søker på eget navn.
Resultatet bekrefter hvem som helst kan google og finne hva de trenger
etter to epost. Søket åpner mapper, bilder og avslører hovedlinjene i en
lang og brokket dannelse. Skriveriene må være privat til de blir bedre
kjent. Hun nøyer seg med generelle svar.

Suksessmålet er en deit med noen hun treffer på nettet. De trenger ikke
bli kjærester. To mennesker møttes og avgjør om de har framtid. Enkelt.
Håp er målet.

Hun bruker for tid på svarene til Dark Horse. Svar på svar, dråper av
selvet bygget på et overflatisk lag med tilknyttinger. Det er deilig å
prate om seg selv, og hun får løfter om deit. Suksessmålet sitter langt
inne. Dark Horse kvier seg for å si ja. Hun har ingen sjanse. Hun gjør
en innsats for at det skal det gå bra. Heldigvis har hun Blunke-Guri i
bakhånd, en sikker deit. Feiring før tiden er ikke lov. Målet i sikte må
aldri gå miste. Et instinkt forteller det er viktig å holde dører åpne.
Kort og presis, to epost til to enavn. De som er igjen etter Fullklaff
høflig og kontant avviste deit på en kafe.

“Jeg er tilbake i Oslo, inviterte til deit og jeg mente det.”

Uken i Tromsø bytter hun daglig profilbilde, gjerne oftere. Skriver små
statustekster og tar god tid foran de grønne prikkene. Overveldende
tomt, Blunke-Guri sender ingen meldinger i løpet av uken, spørsmål eller
blunk. Fraværet av humor gnager. Spøkelsesbildet er for alvorlig. Øye er
sjelens speil, sier klisjeen. Hun holder armlengdes avstand til
Blunke-Guri. Hun er ikke nysgjerrig. Kanskje det er hennes oppgave å
være nysgjerrig, stille spørsmål, blunke og slenge en kommentar. Hun
holder armlengders avstand, oppdatert daglig status, publiserer bilder
fra fisketuren med onkelen og fjellvandring i Tromstinden like ovenfor
huset til bestemoren. Hun deler villig vekk ferieminner i statusfeltet,
og sender ingen direkte spørsmål til meldingspostkassen. Hun får ingen
spørsmål heller.

“Jeg mente det.”

Gjorde hun? Suksessmålet kan ta uendelig tid og hun må forsere.
Venteleken er ikke noe for henne. Hun har vondt og vil flykte. Seire en
siste gang, og finne ro til noe annet.

Historien ligger og vugger i bakgrunnen, skremmer og driver framover.
Men en grønn prikk lyser nærværet og følger i øyekroken. Irritasjon
vitner om noe der—en dragning mot profilbilde og prikken, grønn og
oransje blandet.

Fri kastet hun seg inn i debattforumet og der krangler hun så busta
fyker.

Avviser alle henvendelser. Raskt. To meldinger, maksimalt tre og hun
forklarer høflig nettdeit er uinteressant. Aldri uten å sjekke bilde og
profiltekst. Utseende, røyker de? Avvist. Gaysir kunne like godt
utstedte en stemplingsfunksjon, en hake de kunne hekte straks
vurderingen er gjort.

“Veid og funnet for lett, beklager.”

Hun skriver til det ikke lenger er mer å si på forum, debattinnlegg og
de høfligste avvisninger til damene på hovedtavlen som tar kontakt.
Deretter er det stopp. En kvinne fra Lillestrøm stiller et årvåkent
spørsmål, tar henne på kornet, og hun får seg aldri til å avslutte
samtalen deres. Heller lar hun den løpe og slik går ukene til et
spørsmål for mye, det som skaper følelsen av å være beglodd,
gjennomsiktig. Damen fra Lillestrøm visste for mye. Hun ble googlet og
avslørt.

Hun visste en ting etter tiden på Gaysir; makten er skjevt fordelt.
Ubehag og angst. Slik føler hun det må være for gasellen når tigeren
sniker seg opp og biter i halen.

“Hva heter du så jeg kan google deg?”

Hun bestemmer seg for å sjekke vem hun er som henger i kulissene. Fake
har lært henne det er viktig å sikre svar, falske identiteter som leker.
Hun har et privatliv å redde. Døgnet hun ventet på navnet til Fab var
nervepirrende. Hun skulle google navnet raskt og finne bilder. Fab er en
person, et ego, beviselig bosatt i Lillestrøm og nysgjerrig på henne.
Smilet virket kjent, bølgene i silhuett, intelligent presisjon gjemt i
skygger. Gjensidig googlet jevner det seg ut. Like raskt som enavnet lot
seg bekrefte sender hun mobiltelefonnummeret og inviterer til å ringe.

Ingen ringer. Tre invitasjoner utfordrer Gaysir, søker gjensyn av
opplevelse og bekreftelse på kanalen. Neste gang vil hun gjøre ting
bedre. Grønne prikker lyser opp hovedtavlen. Rekker i grønt, oransje
signaliserer inaktive profiler pålogget. De stirret, ser på når
Blunke-Guri bestemmer hvordan svare på invitasjonen.

“Jeg er tilbake i Oslo, inviterte til deit og mente det.”

Sa hun.

“Var det fint i Tromsø? Jeg simpelthen elsker sånne som deg her inne,
som vil bli kjent. Det er bra.”

Urolig. Svaret fra Blunke-Guri har for mye tekst. Hun leser videre.

“Beklager, men jeg har fått kontakt med en gammel fasinasjon her inne og
vil vie oppmerksomheten min der.”

Svaret fra Blunke-Guri kom i det minste kjapt, og følte hun ikke
lettelse ville hun smakt skuffelse. Halvannen uke har skiftet det sikre
suksessmålet til avslag og rungende nei. Utseende lik Annie Lennox gjør
ingenting for henne. Kanskje øyene bak de mørke solbrillene alltid vil
være skjult. Og bra ville det være. Alle dører må ikke åpnes. Besk
ettersmak kommer fra faktum invitasjonen sendte hun på grunn av et
blunk. Et klart og tydelig blunk fra Blunke-Guri. Er det lov å trekke
seg fra et tilbud du selv initierer? Reglene framsto som uklare. Hun vil
legge inn en protest. I det minste å spore et opprør og sette grensene i
ettertid.

Uten å framstå som surmaget returneres hun et svar.

“Lykke til videre. Det går fint, husk du kontaktet tross alt meg først.”

Husket du blunket? Ingen er såret, og det er unødvendig å si mer. Stiv
maske, ingen vil omgjøre beslutningen tatt.

“Du kan ikke tvinge noen å elske deg.”

Tenker hun.

Tre invitasjoner og en tilbake.

12
--

Vil hun bli elsket, eller ønsker hun bare å bekrefte for seg selv at det
er mulig for en som henne å finne friere på nettstedet. Sannsynliggjøre
en sannsynlighet for å gjøre opplevelsen meningsfull. Klisjéer bulker
seg opp i hode, hun lurer fælt hvorfor energien sprudler og presser fram
spørsmålet. De har stirret på hverandre en uke, hun har svart på
spørsmålene til Dark Horse og unngått blikket i det grønne lyset til
Blunke-Guri. Hun vil høste gevinsten i et prosjekt satt i gang før
Tromsø.

Hun ville mer og søker svar.

“Hvordan var familien i Tromsø. Når skal vi møte?”

Var det et ja? Dark Horse fortsetter å overraske.

Og glede.

Spørsmålet er der klar til å gripes. Hun snakker gjerne om ferien.
Skrive svar var enklere enn å finne et sted de kunne møte hverandre.
Rammene for deit hadde hun ikke tenkt over før, og stilt ovenfor
anledningen er det de første kritiske sekundene umulig å feste seg til
en ramme. Hvis dette faktisk skal skje må hun foreslå et sted. Dark
Horse venter et svar.

Hun hadde vært overbevist den tredje invitasjonen ville gå samme vei som
de to andre. Fullklaff takket nei første kveld. Timene ferske etter hun
avskrev Blunke-Guri som ville vie oppmerksomheten mot en annen.

Det var ingen såre følelser. Spøkelsene var visket bort, de bekreftet
bange anelser om Gaysir, og selvtilliten strutter. Hun er smart, pen og
frister de beste. Her var det ingenting å si på selvfølelsen.
Intuisjonen sier sterkt ifra, og utfallet er klar tale: Gaysir suger.
Ingenting gleder mer enn å motbevise knirket i bakhode som sier Dark
Horse er utilgjengelig, umulig—en fangst. De utvekslet navn og hun har
gjort et kort og overflatisk søk. De har likheter bortsett fra der hun
faller stiger Dark Horse. Profilen bygger rundt en dagbok i lette
anekdoter og på vennelisten gjenkjenner hun fjes fra So tilhørende de
andre. Hun er sjanseløs. Når vil denne damen som gjemmer seg bak
nysgjerrige spørsmål treffes. Hun strekker seg og nyter rått det flotte.
Høy. Smart. Streng. Uoppnåelig.. Hun ville elske å ha feil.

Dark Horse venter på svar.

To avsnitt er nok til å beskrive oppholdet i Tromsø. Ivrig rekker hun
ikke å reflektere over senteret i bevisstheten. Heller ramser hun opp en
liste med barer og kaféer i Oslo, en for hver bydel i sentrum og lar det
være opp til Dark Horse og velge sted. Valget er simpelthen for
vanskelig fordi i hode foregår et kaos inntrykk og blikket flakker.
Hjernen klarer ikke stoppe før hun trykker på knappen for å sende
meldingsvaret.

Veikt. Feigt. Kleint.

Hun angrer på svaret straks meldingen er sendt. Mer spesifikk kunne hun
være. Dark Horse fikk et utvalg på et halvt dusin kaféer spredd over
byens ulike hjørner. Mangel på struktur og kraft i svaret skjemmet. Hun
ville gi et mindre sluskete og ubestemmelig inntrykk, og banner i sitt
stille sinn. Hun må gi et annet svar.

Hun re-sender en ny og kortere melding.

“Glem siste melding. Ring og vi blir enig!”

Mobiltelefonnummeret ligger ved, og hun sender beskjeden fylt av en
barnslig glede over at hun ganske snart vil høre en ny stemme. Har Dark
Horse dialekt? Mange spørsmål og en forventning som gnager og sliter mot
intuisjonen. Hun seirer, suksessmålet er innen rekkevidde. Et møte, en
katastrofe, eller noe annet.

Hun lurer på hvor lenge det vil gå før telefonen ringer.

Erfaringen med Fab tilsier et døgn. All erfaring hun har.

Fullklaff mistenker hun går på deit, og mistenkt er nettopp Fab. Tanken
er ny. Hun skyver bevisstheten vekk, og tenker det er et stort sjalu
spøkelse, bortsett fra hun føler ikke brodden. Lykketreff er å slippe
forpliktelsene—pakken til Fab, utilsiktet og motvillig ble hun droppet,
alltid like heldig. Fullklaff kunne ikke være perfekt. Minnene av å være
vraket virker sideordnet. Sjalusien stikker ikke dypt. Mye energi
kanaliserer hun inn i suksessmålene. Snart vil teateret spille sirkelen
til ende. Én samtale går raskere enn tre til sju meldinger for å avtale
tidspunkt og sted. De må finne en dag, og om de møtes til lunsj eller om
kvelden vil si mye på tonen. Meldinger tar tid og hun oppfordrer Dark
Horse å ringe for å gjøre neste skritt enklere. Grønne prikker lyser opp
søket og trekker blikk. De tjue besøkende skuer mot de samme tre til
fire gode enavnene som frister dem alle. Fab trekker blikk og krever
oppmerksomhet. Fullklaff synes å synkronisere tilværelsen rundt
Fab—derav mistanken og stikk av lettelse.

Hun har en plan.

Fab er forbudt for henne, profilen utilgjengelig og innesperret, eller
rettere sagt hun er sperret ute. Alle andre kunne se profilen fra
Lillestrøm av hjertens lyst. Blokkeringen er frivillig. Hun kanaliserer
energi mot Dark Horse for å bevise hun rett og slett klarer dette fint.
Planen er i ferd med og lykkes. Det er lenge siden hun har tenkt på Fab,
det føles som måneder, men det er bare to uker.

Første erfaring på Gaysir på åtte år og hun har forbud mot å se, høre og
drømme. Trusselen om å blokkere profilen hennes er nok og setter alt på
vent. Veloppdragen og bundet av konvensjoner, respekt for de personlige
grenser har aldri vært viktigere. Førtiår overrasker hun over hvor
selvgod tilværelsen har blitt. Kortsiktig selvbedrag, knutepunkter med
regler brutt sekunder etter de er uttalt. Falske profiler gir en
inngang. Profilbildet til Fab endrer seg til en vågal utfordring i
singlet, puppekontur, kropp—silhuett, og et skjelmsk blikk mot kamera.
Samtalen dem imellom skvulper i bevisstheten, forbannet hviler minnene.
Hun lar seg ikke lure til å besøke profilen. Fabs grønne prikk lyser
bortenfor øyekroken—utilgjengelig.

Følelser av medlidenhet, vemod? Hun skulle gjerne kontaktet Fab og gitt
en klem. Borte er det innelukkede forsiktige vesen, sorthvit portrettet
til en lærer. Desperasjon uttalt i en singlet, åpenbart for ingen
bortsett fra henne. Jobben med å omfavne og trøste gikk til neste
profil. Hun mistenkte den perfekte profilen hun aldri traff for å være
kvinnen Fab lener seg mot. Fullklaff virker stormforelsket og poster
meldinger i statusfeltet, små hjerter og dulgte hentydninger om den ene
sanne kjærligheten, og hun gjør koblingen raskt. Driver hun med
sjalusidrevet paranoia, eller sanselig intuisjon som nesten aldri tar
feil? Forestillingen om at Fullklaff treffer Fab danner hun over tid og
tankene fester seg ettersom hun registrerer de grønne prikkenes mønster
og ser dem danse på hovedtavlen hver kveld.

Kammerspillet som utspiller seg kan hun ingenting gjøre med. Hun har
sine egne mål. Dark Horse ringer ikke. Suksessmålet virker nært og
likevel langt borte.

Galgenhumor står det i ordboken er forsert munterhet i en fortvilt
situasjon. Hun trenger mye av det—galgenhumor for å komme gjennom dagen.
Skuffet. Utvilsomt. Nedturen sitter i kroppen og nekter å riste løs.

“Jeg har tenkt og det vil være feil med møte nå.”

Skriver Dark Horse.

Deretter skriver hun noe om en ekskjæreste som vaker i bakgrunnen. Ønske
om å være ryddig. Tiden er ikke riktig. Unnskyldningene står i kø, hun
gjenkjenner dem og ser fornuften i hvert argument for hvorfor de ikke
bør møte til deit. Avvist med tre ulike grunner, høflighet i mange lag,
en konklusjon er alt hun mangler. Eneste måte å avslutte direkte, rett
fram og ærlig. Hun svarer som sant er at intuisjonen advarte. I bakhode
lå en mistanke om at bak dialogen med Dark Horse var høflighet.

“Jeg sendte tre invitasjoner.”

Erkjenner hun.

Og hun forteller hvordan Fullklaff og Blunke-Guri takket nei på grunn av
andre de ville vie all fokus.

“Du sier i praksis det samme.”

Knapt. Bittert. Hun forsøker å skjule skuffelsen, men det er vanskelig.
Tonen i anklagen inneholdt mye sant. Galgenhumor og ærlig skuffelse. Hun
kommer ikke utenom. Tilståelsen at det var flere invitasjoner, en bukett
enavn måtte sies for renselsens skyld. Naken har hun alltid tydd til
sannheten, eller hennes versjon av hva som er sant.

Suksessmålet unnslapp i alle tilfelle. Punktum. Tre invitasjoner, ingen
tilbake. Hvorfor var den siste fiaskoen mye hardere å svelge?

13
--

“Shit happens, but why does it always happen to me?”

Tapt hadde hun gjort før. Mange ganger. Slums og latskap gjør et tap
ikke er å unngå. Utfordringen slik hun møter livet er å gjøre sitt
beste. Seire hentes i det å gi opp, aldri låse seg helt og ta ekstra
skritt i pur motstand for å telle avstanden til målet. Slik visste hun
bedre til neste gang hvis det var verdt å strekke nakken.

Skrittene er seier alene, hver dag i live, latter og glede i bølger av
lykke.

Suksessmålene unnslapp, konklusjonen måtte være farvel til Gaysir: Ta de
siste endelige skrittene. Forlate nettsamfunnet og finne noe bedre å
gjøre. Erkjennelsen som ligger i nederlaget smerter, skvulper i
bevisstheten og blokkerer handling. Kroppen er paralysert og ute av
stand til å feie det hele til side.

Konkurranseinstinkt versus Dark Horse. Veien kan like gjerne være
tålmodighet. Hun venter på noe skal skje.

Og det gjør det.

Fab logger av nitten minutter over midnatt og tar ikke bryet å slette
profilbilde. Singletten står igjen som et åpent sår. Ingen merker Fab
lyser inaktiv før neste dag. Fullklaff forsøker tøft å holde masken,
utestengt i følt smerte. Hun har medlidenhet for dem begge. Fullklaff
søker tilbake, men den korte stunden hun kjente Fab var det åpenbart
damen fra Lillestrøm hadde smak for drama. Hun forsøker ikke å dømme.
Det er menneskelig å spille den sårede part og snu ryggen når skremt.
Straffen Fab utøvde er hard, nådeløs og alle lider.

Hvem gråter hun for, Fullklaff eller seg selv?

Hun gjenkjenner et mønster hos kvinnen fra Lillestrøm, ser hvordan
Fullklaff strekker egen nakke og møter en kald skulder. Minst fornøyd er
Fab, og midt i natten gjør hun kort prosess og slukker det grønne lyset.

Følelse av medlidenhet for Fab.

Hun tenker: “Et barn.”

De hadde bare kjent hverandre de månedene. Hun surfer på hovedtavlen
hver dag og er såre fornøyd med hvor lett det er å unngå kvinnen fra
Lillestrøm. Hun ser bedre enn Fab forsto. Dagene etter de sist snakket i
telefonen og hun ble nødt til å skalere ned besøkene på profilen gjorde
henne søvnløs. Fornuft vant dagen og hun kuttet tvert. Energien gikk til
prosjektet å skaffe deits, og kun sporadisk registrerte hun sporene til
Fab og gjenkjente mønsteret. Responsen var taushet. Fraværet gjorde
vondt, men var håndterbart. Gjenkjennelse fant hun i Fullklaffs
desperasjon, og suget etter oppmerksomhet fra barnet i singlet. Hun lot
seg ikke lure. Personligheter på hovedtavlen danset rytmene de alltid
gjør—en tango for kvelden, en sjanse til reprise. Tjueniåringen Fab
lever som om det er talløse sjanser å skusle bort.

“Stakkars kvinne.”

Tårene for Fab er ikke lenger hennes anliggende.

Hverdag og venting lyser i grønne og oransje prikker—med kjedsommelig
rutine legger hun innloggingstidene til faste tidspunkt. Morgenen, like
etter å ha skiftet pysjamasbukser, tisset på do og skrudd på
frokostteve. Etter lunsj. Ettermiddag for utlogging fra jobben, og
straks hun kommer hjem.

Prikkene er irrelevant. Hun søker målrettet, finner tidspunkt og sist
innlogget gjennom øremerket søk. Tre enavn blir til fire og fem. Hun
søker på dem og resultatet gir oversikten hun trenger for å følge
rytmene. Tilhørende profiler beslektet og like får samme behandling.
Detektivarbeid og intuisjon, beslektet kobleri i nye søk. Tidslinjene
avslører. Den ene går ut av hovedtavlen, den ene går inn. Tomrommet er
ikke tomt. Hun får meldinger, små hilsener og blunk. Skrik og subtile
budskap i statusfeltet kverner hun gjennom en dekoder for oversettelse.
Hun tviler aldri hvis budskapet er til henne, at det er til henne, og
tenker kanskje det beste ville være å stille flere kritiske spørsmål.

Kjedsommelighet i system skaper virkeligheter enhver kan tolke etter
eget hode.

Gleden av å kunne forelske seg gir håp og er verdt tittingen. Profilene
som avviste henne, og flere, får besøk av måken. Fake flyr og erter:
“Tittitt.”

Else svarer ikke på telefonen når hun ringer. Hvis heldig får hun svar
noen timer senere, straks bestevenninnen rekker å høre beskjeden hun
legger på mobilsvareren. Hun trenger å snakke, og hun kan ikke få
kontakt raskt nok. Normalt tar det en dag. Venninnen er psykolog med
aversjoner mot å ta telefonen.

“Hallo, hva vil du?”

Venninnen er i godt humør.

Hun vil forklare hva som skjer, gi en rasjonell og kort beskrivelse av
livet på hovedtavlen og den falske identiteten Fake hun bruker til
dekke. Fjolleri, egentlig. Ingenting og alt har skjedd siden sist. Hun
oppdaterer venninnen, forteller om Dark Horse—den ukjente, en flørt hun
mistet.

“Hva med tjueniåringen?”

Sa Else.

Hun vinker bort alle tanker om Fab. Situasjonen er mer alvorlig. Else
trenger fakta, påfyll til hjelp i å gjøre riktig vurdering. Hun trenger
råd. Hun visste svaret, men trengte noen andre til å si det. Hun ringer
målrettet venninnen som kan plassere lidenskapen. Else er uforberedt og
trenger bakgrunnsinformasjon. Tiden flyr, mye endret seg på kort tid.
Hun forteller om prosjektet, suksessmålene og de tre profilene som slapp
unna.

Hvorfor var avslaget på den siste invitasjonen hardere å svelge? Hun
trengte svar. Tomrommet ligger klamt over en følelse, hun nekter å
slippe tak og ta konsekvensene av å bli avvist. En snill, høflig og åpen
avvisning—like fullt var hun avvist. Hun mangler akseptable alternativer
og flyr på autopilot. Det står om livet på Gaysir. Resultatet viser
eneste riktig er å slette profilen og gå videre. Rasjonelt ligger
løsningen opp i dagen.

Tredjeparter skal være objektive. Psykologer er en typisk tredjepart, og
hun kjenner bare en følelsesdoktor. De beste råd gis alltid fra
venninner, og i Else har hun to løsninger i en person. Psykologen hun
stoler på etthundreprosent. De snakket sammen uker i forveien, spiste
lunsj, drakk en milkshake hver på The Nighthawk Diner og drodlet løst
rundt tilstanden å være singel. Prosjektet hun tenkte på den gang var
løst tankespinn, men siden de sist møttes hadde hun eksekvert. Hun ville
avgi statusrapport. En vag idé før turen til Tromsø ble en historie. Hun
fortalte og ga smakebiter i samtalene. Hun hadde en tanke om at tre
deits ville skape noe nytt. De satt på et bord og drakk cola—en matbit
på restaurant. Mest sannsynlig spiste hun en salat. Det hele minnet hun
bak et slør, vage inntrykk underordnet historien hun ville fortelle.
Personen bak enavnet Dark Horse skremmer henne. De har felles kontakter,
likheter og forskjeller. Oslo er en liten by. Største trusselen synes å
være et vellykket liv, der Dark Horse gjør suksess og har noe som ligner
en karriere lever hun et ordinært liv. Hun er sjanseløs. Beklagelsene
sitter tett. Innimellom smatting på sugerøret klager hun høylytt over
nettets ubarmhjertige dom.

“Fordi det er nettet er jeg sjanseløs.”

Hadde de truffet hverandre på et utested, ville hun og Dark Horse
muligens hatt et øyeblikk. Attraksjon og tenning var mulig, argumenterer
hun, der nettet siler og sjansene renner mellom fingrene lik sandkorn.
Forskjellene blir større, lister med egenskaper i framtidig vennskap
viktigere. Feilene vokser og er store, varselflagg lettere å unngå.
Nettet stopper forhold før de begynner. Nå går toget, sjansene er
forspilt. Hun er desperat etter å snu.

De har utvekslet navn og googlet hverandre. Hun fant to bilder og lite
annet. Nok til å forstå hun er sjanseløs. Else må hjelpe i miseren.
Klagesang før fakta, den endelige dommen. Alltid et instinkt foran, og
like fortvilet hver gang.

Sjansene viste seg å være større enn beklagelsene på The Nighthawk Diner
tilsa, for Dark Horse sa nesten ja. Hun møter Else på Grünerløkka for å
forklare. Hun står ovenfor et dilemma. “Hjelp!” Venninnen er vant til
stemningsskiftene, vet hvordan parere ordvekslinger etter år med
trening. Hun har tapt, men er høflig avvist med et halmstrå.

“Kanskje til høsten, når jeg får hode over vann. *Hvis* jeg får hode
over vann.”

Sa Dark Horse.

Dommen er nådeløs.

“Det er akkurat hva jeg sier når jeg ikke vil se dem igjen.”

Hun tenker det stemmer. Sånn måtte hun forstå alt, slik Else ser ting.
Hun er sjanseløs.

Hun har fortsatt å fly, høyt og lenge. Fake gir luftrom og en kappe av
anonymitet ovenfor de fremmede. Trøtt og lei, alt virker meningsløs, og
hun vil vite hva Else mener.

Ergrelsen over å ha spilt spillet og tapt sitter dypt. Eksil i Elba,
Napoleon aksepterte det heller ikke. Nederlag. Fly for harde livet, og
finn et lysglimt—håp—noe enda bedre. Let, finn, forfør og glem.
Rekkefølge irrelevant. Søket etter sex går via overflyinger og flere
stoppesteder for å gjøre opp status. Tre invitasjoner, tre avslag.
Status er rødt for fare på ferde.

Stilt ovenfor nederlaget tilstår hun, forteller om suksessmålene hun
aldri vil oppnå. Hendelsene har blitt til selvoppfyllende profetier.
Gaysir framstår som en korrupt kanal der mange dramakviins gir tomme
løfter, serverer livsløgner for de verpesyke. Tre invitasjoner tok all
fokus og hun vinker farvel til den siste av dem, for alltid vil hun lure
på hva som skulle til for å sikre suksessdeiter, møter i gjensidig
respekt med håp om kjærlighet. Ildvann hjelper. Tålmodighet. Hun mangler
is i magen for det ene, og evner til det andre.

“Jeg innrømmer å være skuffet.”

Hun sa det slik til Dark Horse. Kunne ikke tenke seg en annen måte å si
fra. Ærlig og rett fram. Hun er veldig skuffet, og kan fint bruke større
ord, fortalt deit behøvde ikke bety annet enn halvtime over en
kaffekopp, minimal utveksling av informasjon og aldri behøver de å se
hverandre igjen. Hun vil gripe tak i Dark Horse og riste til det står
klart for alle de er ment for hverandre. Hvorfor skusle bort slike
følelser? Perspektivene veksler, vitner om mangel på balanse i
tilværelsen der hun er singel for første gang på ti år. Hun vet livet
står på hold. Tiden vil gi håp, eller er dette slutten på glede?
Følelsene river i kroppen.

Fri som fuglen, og objektet foran henne har et navn. Hun tilstår og
trygler, ber om nåde fra dommen.

“Bare kontakt meg hvis, når, om du føler deg fri fra ekskjæresten.”

“Ja.”

Nok sagt, minimalt påklaget fortsetter tilværelsen. Et par dager med
stillhet og Fake dagboksnuser som ingenting har skjedd. Dybdesøk stopper
ikke på Facebook, Linkedin og nettsteder for profesjonelle søk. Inntil
dette øyeblikket hadde hun begrenset snokingen, men slusene åpner i
desperasjon, vel vitende at økt kjennskap fjerner magien, dreper flatt
romantiske ideal. Hun vil ha slutt på torturen. Kunnskap er en farlig
ting, sannferdig klisjefylt forbannelse. Hun roter i ligningsverdier,
styrereferat og annet kjedelig stoff langt over grensen. Hun finner en
video fra arbeidsgiver der de ansatte bedriver selvforherligende skryt.
Presset er vanskelig å stå imot når hun vil vite mer om den tredje
markøren i kjærlighetsprosjektet. Hun vet Dark Horse flyttet nylig til
egen leilighet. Badet mangler lys, alt er nytt, seng, den bærbare
datamaskinen er skiftet til MacIntosh. Hun leser og tolker fra dagboken
og vet ikke hvorvidt elektronikken var byttet, eller skulle bli det.
Stikkord fra Gaysir åpner verden til en som sa nei, og veien inn i de
upålitelige notatene i dagboken sier ikke nok. Stemme, dialekt, keitet
gange—videoen fra arbeidsgiver på nettet er uvurderlig. Fake får studere
Dark Horse, se henne tale til kamera. Hun sporer ingen dialekt å snakke
om.

Hun vet plutselig en hel masse om dette vilt fremmede menneske.

Dark Horse legger informasjon i status og lar det ligge i dager. Budskap
alle ser. Adferd hun begynner å kopiere. Fake får dybde når status
skifter innhold. Kulturkanalen på teve viser det russiske dramaet bygget
på Mihail Bulgakovs roman *Mesteren og Margarita,* og hun gir måken et
gjøremål i status. Med et halvt øye følger hun sorthvitserien, en
russisk trøyte i pretensiøs billedspråk, intellektuelt runkeri hun
følger passer bra i en måkes eksistensielle væren. Over tid og etter
flere søk vet hun masse om offeret fordi Fake skuffer aldri. Måken
fanger essensen i galskapen, og presenterer resultatet i hele sin prakt.

Vemod og tristesse, denne kunnskapen. Tapet vokser. Prisen blir
uoppnåelig.

Toget går, gikk.

Må hun forklare, vil hun fortelle—finnes det en fornuftig forklaring?
Hun vil snakke og offeret setter grensene de aldri krysser. Hun søker og
luksusen å bli kjent forsvinner for alltid. En deit ville låst dem til
en kaffekopp. Blikket rettet ned, blottet for ord og hemmeligheter. Hun
har ødelagt framtiden som fungerer.

Dark Horse kontakter henne en dag i det blå. Hun mottar en melding og
blir minnet om invitasjonene som aldri ble sendt.

“Husker du?”

“Jo da.”

Vil hun si kjølig. Hun minnes Dark Horse, fri fra forpliktelser, bånd,
historie; etter tiden gir dem en sjanse vil minnene være igjen. Hvordan
hun svarer på invitasjonen baner veien for framskritt. Hun står ovenfor
den endelige testen. Suksessmålet endret innhold, fiaskoen var
tilbakelagt og arkivert. Nå står hun klar for scenen, hun vil møte Dark
Horse.

“Jeg kan møte deg på restaurant Druen.”

Omtenksomt velger hun møtested like i nærheten av arbeidsplassen til
Dark Horse, gjør spaserturen kort, bygger en lavere barrierer der to
mennesker har færre betenkeligheter. Valget avslører henne, forteller
hvilke retninger fokus har tatt. Suksessmålet endret seg, fiaskoen er
tilbakelagt og arkivert. Hun vil ikke gi Dark Horse en fluktvei, vil de
skal møtes og frustrerer når den mystiske kvinnen som avviste henne
bestemmer tidspunkt. Fantasien spiller små puss. Stedet for en deit
avslører hvor mye hun googler. Detaljer hun må erkjenne før de kan gå
videre, og aldri før har det vært viktigere å si ting som de er. Det må
være ingen mysterier. Hindringer river ned og gir færre rom å trekke
til. Smidighet i ryggmargen kan ikke redde situasjonen. Hun taper
diskusjonene i tåkepraten, samtalene i hode som ingen ende tar. Hun vet
hvordan hysteriet vil ende. Unser selvfølelse kryper inn, og svaret til
Dark Horse står klart.

“Toget har gått, har det ikke?”

Svarer hun.

Hvorfor i det hele tatt inviterer hun til deit hvis utfallet er bundet.
Slik må hun møte Dark Horse. Løgner vokser fra desperasjon og små glimt
folkevett reflekterer som lyset i øyekroken.

Forklaringene virker rotete, hun graver etter fornuft inni ordskyen, et
mylder av tanketråder koker ned til angst over å vite for mye. Dark
Horse er intelligent og vil forstå. Hun lar seg overtale, ønsker å bli
forført og vil aldri avvikle den dødfødte deiten. Dark Horse får lære
henne å kjenne, den virkelige henne, med og uten fornektelser.
Tankesprangene. Hun yter motstand for å overvinne demonene som alltid
har vært der.

“Å bli kjent er en luksus og jeg føler vi mister sjansen. Hva gjenstår
er et sexpress.”

Korte øyeblikk i våken tilstand fortviler hun og vil forlate traume. Hun
sletter profilen bare for å innse det gikk for lett. Hun angrer og
oppretter ny profil neste dag. Dumheten sender enavnet i karantene og
hun må skrive fornavnet mor ga henne i dåpsgave. Måken får være i fred,
for falske identiteter har ingenting med henne å gjøre. Blottstilt
nekter hun i det minste å bruke eget bilde og laster opp et stilistisk
portrett av usanernes president Barack Obama. Både bilde og familienavn
blir for mye virkelighet å takle.

Hun tenker hvor frustrerende det er å være uten enavnet, hvor dumt det
var å slette profilen der enavnet hun hadde brukt i alle år blir lukket
en måned framover. Hun vil tilbake og angrer på at hun slettet
impulsivt. Hun tror ikke kjærlighet på nettet er mulig, søker fornuft,
et liv utenfor, bare for å oppdage hun savner forum, hovedtavlen og
enavnet hun alltid brukte. Fotografi av selvet blir utenkelig når hun må
ty til det kristne navnet fra dåpen. Enavnet ga et minimum privatliv,
samtidig er det unikt, en del av henne og hva hun ønsker tilbake i samme
øyeblikk som det glipper.

En måned er lenge å leve med en profiltekst sensurert og profilbilde av
en fremmed—president for alle usanere, Barack Obama. Hun ville tilbake
til en klar og tydelig sjekkeprofil, bruke eget bilde, enavnet hun
kjente best og full ærlighet.

Et døgn naken hadde hun allerede startet å frykte andre ville stjele
identiteten.

Hun kan skrive til Roger i redaksjonen til Gaysir. Han kunne korte
karantenetiden, han sa noe sånt. Skjult bak et ansikt i hovmod planla
hun en retur mer ærerik.

Direkthet avvæpner alltid. Taktikk, en overlevelsesstrategi som skremmer
vettet av folk, og selv de modigste sto uten forsvar. Hvis motstanden
var innkapslet i forsvarsmekanismer ville klar tale hjelpe, som det
eneste virkemiddel. Hennes løgner danset polka mellom innsikt og
galskap. Ansikt til ansikt ville de møte hverandre, ærlige svar skaper
hakeslepp og gjennombrudd, men i stedet logger Dark Horse av. Den grønne
prikken forsvinner og ventetiden strekker seg over timer, netter og
døgn. Kvinnen hun forguder er sint. Nettets iboende treghet rir kroppen
som en mare. Svar ligger åpent i fraværet av en grønn prikk, treghet som
følge av sjeldne besøk til hovedtavlen. Hun redder unser med selvfølelse
og holder fantasiene innenfor et kontrollert rom. Fantasien forblir i
hode, og rykk i musen får ikke hjelp. Enhver på hovedtavlen løper samme
risiko. Hun trenger ikke sex herfra, ville ikke ha det, og sextreff var
uaktuelt hvordan hun enn spant scenarioet. De vil aldri deite på Aker
Brygge, og hun får aldri sjarmere Dark Horse.

Begjæret er kontrollert, hun puster lettere og venter bak Obama. Hun
henter krefter og gjenvinner balanse. Slik er det. Galskap får bare
herje i perioder—klart definert og innenfor hva hun finner rimelig.
Skjult kan hun vente på andre tider. Ting må slutte, og det er uklart
hva hun trenger for å bevise status er frisk.

14
--

Dagboken varslet i forkant, en kort bemerkning om sommertid og et varsel
om bevegelse. Dark Horse var på vei for å se Dogtooth. Hun leste ordene
på nytt og desifret betydningen av hva som står i dagboken. Budskapet er
noe så sjelden som en melding om noe som skal skje, ikke har skjedd.

Ordet virker kjent, i dagboken finner hun filmen i søket: *Dogtooth.*
Svaret gir mening. Kvinnen er på vei til en forestilling på Ringen kino
der de viser hovedprisvinneren fra filmfestivalen i Cannes. Filmen er
anbefalt av filmanmeldere verden rundt. Hun leser anmeldelsen som kaller
filmen årets mest perverse opplevelse. Dogtooth har femtenårsgrense.
Dagboken forstår hun slik at om femti minutter er Dark Horse i kinosalen
på Ringen kino for å se den greske *kunst*\ filmen. Dark Horse vil være
alene eller i selskap og se en smal film med glitrende omtale fra
publikum og filmanmeldere. Dagbokreferansen reiser seg lik en annonse.
Hun søker. Salkartet viser det er få seter og salen er liten. Selskap er
påkrevd, tenker hun, hvis det er aktuelt å dra. De vil bli sett, tanken
om anstand faller helt naturlig. Hun vil gå på forestillingen og ringer
venninner, to av dem svarer ikke, en venninne feirer bursdag hos moren.
Hun får tak i Else og vet hun må gi en forklaring på hvorfor det er
viktig å se filmen akkurat i kveld.

“Den virker interessant.”

Kunne hun si. Det holder ikke for Else er oppvakt, kjenner godt hvordan
hun tenker og stiller spørsmål.

“Filmen har fått glimrende anmeldelser.”

Når dagboken varsler i forkant er det mening bak ordene, et tegn hun må
handle. Dette er innenfor hva hun finner naturlig å gjøre. Hvorfor
virker kort varsel mistenksomt på Else? Venninnen vil vite hvorfor hun
på død og liv skal på kino. Invitasjonen gjelder en tur for to på Ringen
kino om førti minutter. De må gå straks skal de rekke forestillingen, og
hun forklarer hvorfor.

Else stopper ettertenksom.

De er ventet, argumenterer hun. Dark Horse vil at hun skal gå, dette er
viljestyrt forsøker hun overbevise. Hun trenger hjelp, selskap i
forbrytelsen. Foreldreansvaret stopper dem.

“Kan ikke la Nurket være hjemme alene.”

Sa Else.

Hvor urimelig virker situasjonen når alle i samtalen straks forstår
alvoret. Gudbarnet vokser hver dag. Sist hun traff Nurket hadde de sett
på hårspenner med rosa og lilla glitter, hun kjøpte et grelt eksemplar
til samlingen. Barnet frydet seg stort. Hun innser barnevakt trengs i
mange år.

“Nei da, du har rett. Jeg må bli hjemme.”

“Jeg gir deg en tre på skalaen til ti, der en er normal og ti er gal.”

“Hvorfor tre. Hvorfor ikke fire.”

Ingen svar. Else er ikke i humør for vennskapelig krangling. De prater
om kvinnen som går skrittene over Grünerløkka og inn på Ringen kino. Hva
slags selskap har Dark Horse med seg i salen, det tenker de aldri over.
Samtalen handler om hvordan det ville vært å sitte på bakre rad og spane
på en fremmed. Tilfredsstille en nysgjerrig det er rimelig å fore for de
gale. Hun er gal. Venninnen har rett.

Hun er ingen tjuv. Tapet måtte erkjennes, hun må fase seg vekk fra
Gaysir, finne tilbake til livet, og gir Else rett. Hun sier noe i den
stilen.

“Nettopp derfor er du bare tre, fordi du sier sånne ting.”

“Så hvorfor ikke to?”

Urimelig hadde hun aldri vært. Bestevenninnen lar seg ikke lure.

Argumentere mot seg selv tar masse energi. Støtten hjelper. Så langt har
sikkerhetsventilene i livet fungert, reddet henne fra de største
blemmene. Beviset på egen fortreffelighet fant hun i form av tre
avvisninger. De er bevis på styrke i den indre intuisjonen, konklusjonen
hun visste og fikk bekreftet. Som forventet før marerittet startet
svermer hun i brakkvann. Resultatet er valid. Noe mer udugelig sted å
skaffe seg deit enn Gaysir finnes ikke. Hun har beviset.

Fake fører ingen dagbok. Måken lei av livet legger standarden for
jukseidentiteter. De som går lenger vil se tilståelsen på
profilforsiden.

“Ingen meldinger bør komme inn for ingen meldinger skal ut.”

Hun får en melding i løpet av natten. Postboksen lyser med en peker i
rødt til avsender. Overraskelsen treffer når hun stirrer på
varslingslyset. Fake har fått en melding fra Dark Horse hun forstår lite
av.

“Strålende profil.”

Begynner Dark Horse.

“Likte du *Mesteren og Margarita?* Hvis ikke kan jeg anbefale
*Dogtooth.*”

Kort, presist og til feil person. Statusteksten med forrige ukes
traurige teveunderholdning får en kompliment. Men Fake er falsk. Hun
ringer Else mindre enn tjuefiretimer etter sist samtale der
psykologivenninnen gir en temporær galskap berettiget diagnose.

“Hjelp.”

De ler.

Utkonkurrert av den falske nettprofilen ment for å fly og ingenting
annet. Hun lever en nettklisje. Dager etter all håp forsvant, et døgn
etter venninnen ga diagnoser fra galskapsskalaen. Hvem er gal?

Hun er gal.

“Vi må gå og se filmen—\ *Dogtooth.*\ ”

“Jeg har ikke Nurket i kveld.”

“Da er det avgjort.”

Etter jobb skulle de boltre seg med analyser, frustrasjon og grave dypt
i meningen bak meldinger fra Dark Horse, gjerne forstå hvorfor den
tredje invitasjonen er vanskelig å legge bak seg.

15
--

Er vilt forelsket, opphengt i en idé. En stemme hun traff på nettet.
Irritert ung dame stilte spørsmål og slapp detaljer om seg selv,
konkrete detaljer, nok til hun lurte og vil vite mer. Hvordan svarer hun
på spørsmålene, hvilket nivå intimitet er riktig, hvor åpen og
gjennomsiktig kan hun være i kyberlivet? Svarene og spørsmålene til Fab
driver relasjonen framover. Hun flyter viljeløs.

Fab åpner for spørsmål tilbake. Hun stiller dem bare ikke, visste ikke
hvordan.

“Vent!”

Strategien å vente og se virker lettvint. Hvis hun tar tiden til hjelp
går alt over. Hun bestemmer seg for å møte Fab og forstår ikke hvordan
det skjer. Hun føler usikkerhet. Tålmodighet er nøkkelen. Hun er langt
fra å finne balanse i livet, og det må hun ha for å høste. Kanskje hun
er grådig? Hvilket spørsmål tipper og avgjør forelskelsen er verdt å
gripe etter? Hun husker knapt hva Fab sa, hvordan den unge kvinnen fra
Lillestrøm vant dagen. Kvalmen fra spørsmålet henger igjen. Ubehag i
lyset. Følelsene renner over i et sug der magesekken snur opp ned på
verden hun kjenner. Fab graver dypt og svarene skaper hulrom.

Hun er googlet og føler en syk kulde spre seg i kroppen.

Tiden står stille.

Spørsmål og svar, det meste er sagt og tilbake sitter uvisshet. Eget søk
avslører hvor eksponert hun er på nettet. Dialogene er overflatiske,
profilbildene svake, tre linjers profiltekst og en liste fremmede
venner. Hun skaper en flunkende ny profil der Obamas hovmod er henne, en
venneløs sjel i limbo. Hun skriver minimalt for å beskrive det
personlige, registrerer ingen grupper og unngår dagboken og alt
identifiserbart. Enavnet hun alltid bruker er borte, det er en president
i veien der det før hang et bilde tatt på mobiltelefonen, nærbilder,
fortrinnsvis ovenfra og uten de sedvanlige grimasene. Karantene på
enavnet gjorde hun måtte vente før det igjen ville være mulig å føre en
fornuftig dialog. Obama identiteten er utvannet, skjult og sier minimalt
til forbipasserende. Den falske måken hun bruker til å snoke på
hovedtavlen er like mye henne som dette speilbilde fra usanernes
politiske kjempe.

Hun ville ha enavnet tilbake, bruke mobiltelefonkamera og lage et bilde
der hun stråler. Bilder fra mobiltelefonen i åtte megapiksler der et
dusin slørete alternativ i appen redigeringer ujevnheter. Profilen skal
være en salgsplakat, i stedet har hun skremselspropaganda.

Alder plager henne. Gjennomsnittsalderen på Gaysir synes å ligge og vake
rundt tretti år, og hun er tiåret eldre. For gammel, likevel mistenker
hun akkurat gammel nok til å leke, henge i ytre ring og plukke smuler.

Fab tilhører hovedbordet—er blant tavlens lekreste damer som henger og
frister. Tjueniår ung fra Lillestrøm. Faktum er at jakten startet nå de
møtte hverandre, og hun er bytte når Fab retter blikket i hennes
retning. Ta tak og jakte målrettet er forunt de få. Hun mangler trening,
uvant å sjekke damer virrer hun inn på hovedtavlen. Svarene siger
passivt til hun snur flisa. Spørsmålene hun knebler velter alt over
bord, følelser innvendig vokser. Hun tar et digert jafs, lik et rovdyr
hopper opp. Svaret kommer et døgn senere.

“Du kan ringe meg.”

Sa hun.

Hun skriver ikke: “Ring!” Ønsker over alt på jorden er Fab vil ringe.
Kort beskjed på mobilsvar er det hele, enklere kan hun ikke gjøre det
uten å avsløre eksplosjonen av følelser.

Fab ringer et døgn senere.

Etter det ble hun en ståker. Er det mulig?

Døgnet før i samme kinosal satt Dark Horse, og hun følger forestillingen
dagen derpå. Hun ble vurdert et døgn tidligere til å være en tre der ti
er splitter pine gal. Psykologen talte, hun respekterte venninnen mer
enn de fleste. Else visste godt hvor skoen klemte. De lekte i diagnoser
og lo av situasjonen. Besøket i samme sal og tilskuere til filmen Dark
Horse anbefalte gjorde ingen forskjell. De er begge enig om at hun lider
av en forstyrrelse. Diskusjonen dem imellom handler om skala og
alvorsgrad. Hvis de blir enig finner de veien bort fra uføret.

Hvordan følger hun profilene på hovedtavlen? Else spør og graver, og hun
erkjenner livet etter hun ble singel er redusert til dette livet i
uvisse. Snuser hun på profiler, eller er hun død innvendig. Død er
alternativet, og hun vil leve.

“Du tror kjærlighet er en form for galskap. Slutt med det!”

Sa Else.

Hun gjenkjenner gode råd. Bortforklaringene er innøvd.

“For mye teve som liten.”

Hun sverger til det sentimentale og mener populærkultur har skadet henne
for livet. Venninnen taler til en vegg romantiske idéer om evig
kjærlighet. Konstruerte forestillinger lite egnet for nettsjekking,
mener Else, og spesielt ikke Gaysir. Hun må si seg enig.

Først Fab, deretter Dark Horse for å slå i hjel følelsene fra den
første. Rasjonelt vet hun hvorfor spørsmålene graver dypt i sjelen. Hun
lar dem grave. Svarene er gull og gråstein, irrasjonelle forelskelser
bygget på virtuelle samvær og galskapen slipper hun løs på intetanende
og uskyldige kvinner.

“Jeg er trolig ikke bygget for nettsjekking.”

Fenomenet Dark Horse representerer er kjent og faresignalene er der.
Sitter hun trygt—så vidt, slik hun forstår situasjonen. De har ingen
kontakt etter hun ble forvist til høsten og relegert til å vente.
Omfanget av galskapen er personlig, kunnskap forbeholdt henne og
venninnen. Kilden til samtalen sendte det falske enavnet en anbefaling
på en gresk film, gullvinneren i Cannes og dermed skapte tilfeldigheter
latter og smerte. Først ler de. Analysene er mer komplisert. Overflatisk
tyder meldingen fra Dark Horse på at hun er utkonkurrert av eget
speilbilde, en fatalistisk måke med vulgært fingerspråk vinner dagen.
Jukseprofilen hun bruker for å spionere på andre anonyme vekker en
annens oppmerksomhet. Måken fikk meldingen der hun fikk beskjed om å
vente til høsten. Profilen som forviste henne til en ubestemmelig
framtid liker måken bedre.

Else ser hun lider. Konfrontasjon er dumt, feil og nytteløst, sier Else,
og det motsatte av produktivt. Else prater til døve ører. Hun nikker og
later til å forstå alvoret. Smerten ligger og skvulper. Hun vil ha
garantier for kontakt over sommeren. Høsten er evigheter til, og Else
krøller pannen i bekymring når hun nevner sjansene for at noe kan skje.
Suksessmålet er tapt, og skubb i samme retning vil blokkere alle
muligheter for vennskap, eller høflig nikk når hun en gang i framtiden
møter Dark Horse på gaten. De vil se hverandre før eller siden. Miljøet
for lesbiske i Oslo var ikke større enn at det var sikkert. Hun forstår
faren ved å følge tråden meldingen til Fake åpner.

“Er ikke hele poenget med falske identiteter du må aldri tilstå noe som
helst.”

Sa Else.

Venninnen er ikke på Gaysir og forstår innretningen bedre enn hun
noensinne vil gjøre. Hun hører advarselen fra en god venn. Fortsatt er
hun bare en tre på skalaen for gal, og situasjonen er under kontroll.

De ler.

Visst er tilværelsen fjollete.

“Må jeg lese Cyrano på nytt?”

“Se filmen først.”

Klisjéene ligger tykt, og de har nok å le av. Filmen må sees. Hun
spanderer billetten og takker for støtten. Venninnen bruker overskuddet
i kiosken og grabber alt hun kan bære av godterier. Søtsakene er nok til
å gjøre hvem som helst kvalm, og det meste blir liggende urørt. Gode
venner i kriser er viktig. Hun er glad Else står i billettkøen når
tårene triller. Hun forsøker å holde dem inne bak latterbølgene. De er
bare synlig for Else.

“Forholdet var dårlig lenge?”

Hodenikk.

“\ *Dogtooth* er årets mest perverse film skrev filmanmelderen.”

Hun siterer ordrett fra avisen i det de stiger inn til forestillingen.
Tid for reklame.

Mett før middag var luften tett og varm. Sommerens siste dager gikk hun
nedover Torshov til Ringen kino. Samtalen strakk seg lenger enn de tror
mulig, og latteren sitter løs. Hun vet ikke hva de ler av, hvorfor hun
er i en fortvilt situasjon. De har en samtale som går i sirkel der enden
er den samme. Elses styringsiver vaker i bakgrunnen. Straks hun
diskuterer strategier for å kontakte kvinnen bak enavnet Dark Horse
stiger temperaturen adskillige grader. Else tillater ikke avvik. Hun
visste hvor de skulle, handlingsalternativene var begrenset og femten
års vennskap og fullført eksamen i faget psykologi gjør Else best egnet
til å gi rådene hun ikke vil høre. Makten lå hos venninnen.
Profesjonelle har mer pondus, slik er det bare. Midt i galskap trenger
hun en fornuftig stemme der ivrige og utspekulerte idéer og taktikkeri
blir møtt med riktig mengde fornuft. Else kjente de emosjonelle
sprangene. Hun svikter i trygg forvisning venninnen beskytter. Viten og
forståelse medvirker til at Else har en solid tre der skalaen er ti for
gal.

Hun føler redsel for føleriet, desperasjon og den iboende driv til å
slippe fri alt når det gjelder kjærlighet—det viktigste, opphøyde,
sublime. En fremmed, et vesen bestående av bits og bytes i
googlemaskineriet. Profilen til Dark Horse er en stemme uten substans,
lukt og smil. Irrasjonelt tenker hun stadig oftere på Dark Horse. Hun
faller dypt. De sitter i en kinosal og ståker nettprofilen tjuefiretimer
for sent.

“Hva betyr meldingen til Fake?”

“Hun liker helt klart å ta initiativet.”

Sa Else.

“Hva betyr det?”

“Hysj, se filmen!”

Smiler hun, gråter hun. Else retter blikket bort og fokuserer mot
lerretet. Husets datter i filmen bøyer seg ned på kne, stikker hode
under skjørtekanten og betaler med tunga en vennlig gest fra en av
familiens gjester. *Dogtooth* spinner pirrefølelser til mellomgulvet og
videre nedover i kroppen uten å være en film for lesbiske av den grunn.

Når de reiser seg for å gå gjør Else opp status.

“Vil du egentlig ha kontakt med noen som anbefaler denne filmen?”

Latteren treffer mellomgulvet og går videre til gapet som hyler fra
kjelleren, forbi trappen til fotgjengerovergangen der de skiller lag.

“Nok ingen god idé. Nei.”

Sa hun straks de slutter å le.

De er skjønt enig på alle nivåer da Else går nedover Vogtsgate, og hun
forlater Grünerløkka. Ingen av dem tror forskjellig fra den andre ting
har endret seg. Hun sitter i saksen.

Mer etter *Dogtooth* enn før.

Søk er annet enn Google, eller Bing. Gule sider leverer telefonnummer og
adresse, karttjenester henter fotografier, selvangivelser avdekker
økonomi og arbeidsgivere avslører ofte ubetenksom informasjon. Alle
ivrer etter å dele informasjon og fortelle ting. Timer blir til dager,
dag blir til en dato og flere datoer. Hun har ingen utveier. Luksusen å
lære andre å kjenne forsvinner når hun vil bli kjent med Dark Horse og
søker dypere. Lenge står hun imot, dragningen blir for sterk og
frustrasjonen vokser. Hun vil vite mer enn de første spor av engasjement
på universitetet, og det første søket for å bekrefte Dark Horse
eksisterer blir til flere søk. Kvinnen er ekte, en kropp i nærmiljøet.
Lesbisk og glad i litteratur. En person hun gjerne kjenner på vanlig
måte, ikke besudlet av snoking på nettet. Tiden forserer alltid, og det
var ikke lenger mulig å vente. Døgnet har timer hun må fylle.

Fab var tjueniår fra Oslo før hun søkte. Straks samtalen begynner er det
blitt til tjueåtte år og Fab er fra Lillestrøm.

Alle lyver på nettdeiting.

“Ingen ville snakke med meg hvis jeg skrev Lillestrøm.”

Sa Fab da hun konfronterte henne.

Hvis ikke for løgnene ville hun ikke en gang ha oppdaget det nysgjerrige
menneske. Søket hun har på hovedtavlen begrenser seg til Oslo. Strenge
kriterier står ved lag. Lokalisering står sentralt og er et kjernepunkt
når hun søker kjæreste. Store deler av landet blir forkastet og nettopp
derfor framsto løgnen som en hun måtte unnskylde. Hvis hun holder søket
innenfor Ring 2 ville utvalget simpelthen være for lite. Finne den rette
er plutselig viktigere enn ærlighet. Hun gjenkjente egne løgner.
Tilgivelse er det enkleste i verden.

Forskningen hun leste om i en avis viste at usanere valgte attraktive
profilbilder til nettsjekking. Bilder som i gjennomsnitt var fem til sju
år gamle portretter. Folk lyver på seg to centimeter i høyden og
inntektsmessig ligger de ti prosent over reell inntekt. Løgnene og
rapporten som avslører dem gjør inntrykk.

Tre invitasjoner og tre avslag. Løgnen hun forteller framstår over tid
som usann. Den miserable troikaen avslutter en historie. Enavnet til
kvinnen ga aldri et svar da hun slo fast de skulle på deit. Hun spurte
aldri høflig, lot heller en erklæring i meldingspostkassen proklamere
intensjonen. Hun delte fritt flere umotiverte avslag bygget på
overflatiske kriterier som utseende, høyde, eller bilde før de tre
utvalgte enavnene fikk gjengjelde. Hun er skyldig. Avvisninger er kjent,
og hun er selv skyld i flere av dem. Hun har sendt patetiske
unnskyldninger om å være på Gaysir for forumsdebattene.

Fab endrer alt. Profilen hun ikke dytter bort, kvinnen som ringer. Hun
mister gaven Fab gir, og dermed var det eneste som gjenstår et siste
stunt, tre invitasjoner der hun leder an. Kunne hun ta tyren ved hornene
og avsløre svindelen ville hun finne frihet.

Alle lyver på nettet, for alle andre. Hun lyver og stopper ikke. Hun
lyver for seg selv.

Det er ingen slutt på Gaysir etter de tre invitasjonene avslår å gå på
deit. Hver dag tar hun bryet å legge eposter og dokumenter til side og
logge på Gaysir for å sjekke de utvalgte tre profilene og se om noen
finner det bryet verdt å følge henne.

Holde masken har noe å si, puste sakte og sitte i ro. Tidsnok oppstår
uro i kroppen og hun står imot eksistens slik hun kjenner livet.
Impulser og galskap jobber mot henne, personlighet forsøplet av
sentimentale griller om å kjempe for kjærligheten mot all fornuft. Gå et
skritt for langt er alltid faren, og hun kjenner seg selv godt. Hun kan
ikke være en annen. Dragningen mot trafikkulykker, å se, stirre framover
og kjøre videre er ikke henne, og likevel sitter hun stille. Blodbadet
på hovedtavlen er magnetiske grønne prikker. Hun fikserer all
oppmerksomhet og trykker innover de mange pekerne.

Fake flyr og mangler kontroll. Hun er i fritt fall og synker for hvert
klikk på musen. Tilståelsen hun er måken, måken er henne, sender hun til
Dark Horse like etter hjemkomsten fra Ringen kino. Else sa klart og
tydelig, kontakt ville være en dårlig idé, og hun ble advart. Hun svarer
før døgnet er over. Historien hun forteller sier alt; hvordan de gikk
for å se *Dogtooth* og erkjennelsen avslører mer enn at Fake er henne,
hun legger ut om hvordan det komiske og patetiske møtte hverandre i
kontakten.

“Vil du virkelig være kjent med noen som anbefaler denne filmen?”

Sa venninnen, og hun forteller vitsen videre til Dark Horse.

“Vi lo godt av det spørsmålet.”

Hun fortsetter tilståelsen, legger kortene på bordet og vil la det hele
ligge åpent. Hun forteller hvor nære de kom å spane på Dark Horse
kvelden før, og slik blottstiller hun galskapen. Hun har kort fortalt
alt. Ærlighet og åpenhet er veien gjennom minefeltet foran dem. Hun
håper Dark Horse legger godvilje til i tolkningene og tilgir alt hun
skriver. Veien til Dark Horse må være usensurert, direkte og risiko for
å stå igjen naken foran en fremmed er verdt det ubestemmelige.

“Ante meg. Glad du likte filmen.”

Kort svar, vennskapelig og ingenting mer.

Dermed er det ingen vits å ha den falske identiteten. Fake har utspilt
sin rolle. Smileikonet punkterte avsløringen av avsløringen. Hun jaktet,
og gikk i fellen. Hun strever med å forstå språket, imens danser Dark
Horse ring rundt og peker nese. Hun blir sett og lekt med.

Fake er forbi, flytimene opphører. Hun er en amatør i kyberlivet, og hun
er avslørt.

16
--

En scene fra Dogtooth begynner å spille i hode og vil ikke slutte.
Datteren i huset på alle fire skyver hode inn mellom bena til brorens
innleide gledespike, sleiker låret og graver møysommelig dypere.
Redselen for å henge igjen alene veier lettere enn den deilige smaken av
lår.

“Kom inn!”

Hun nøler. Leiligheten er lys og sparsommelig møblert. Mye finere enn
hennes. Samtalen har en fin flyt og straks de setter seg på kaféen, tøff
i tryne velger hun en aggressiv og direkte tone, nekter å skjemmes for
fantasier.

“Tror ikke vennskap går, du skjønner, jeg har allerede hatt den første
seksuelle fantasien om oss.”

Skjelmsk smiler hun. Forfører.

“Du kan ikke se *Dogtooth* uberørt.”

Minst en av dem er fanget. Dark Horse inviterer hjem fordi det er
nærmest. Hun bor lenger opp i dalen og drømmekvinnen bor i gangavstand
fra kaféen. De har ingenting å snakke om. Oppsummeringen av skammen
etter en måned på Gaysir er over på femten minutter. De er gjensidig
googlet, kjenner hverandre altfor godt, føler hun. Nysgjerrig på å lære
mer, og samtidig fornøyd med utsikten å lette spenningene i kroppen.
Utseende og bilde stemmer. De har ingenting mer å snakke om.

Hun visste hvor de skulle, spilte rollen i hode hundrevis av ganger og
ser etter steder å sitte slik at skuespillet kan begynne. Steget inn i
leiligheten strekker suksessmålet deit sju mil lenger. Kjøkkenet
tilknyttet stuen har to høye barstoler, opphøyde krakker i god avstand
til gulvet. Sofaen er flunkende ny, design i lysegrønt og ligger lavt i
terrenget. Bred, fin å ligge på for to personer. En seng ville være mer
komfortabel.

Skrittene inn sklir av seg selv og tonefallet fra kaféen må sitte,
stødig og trygg, med en snert humor for ikke å ødelegge den gode
stemningen. Glimtet i øyet, drømmer om øyeblikket der underbuksen sklir
nedover skjørtekanten og hun ser det lange, hengslende menneske med rødt
hår krype over gulvet, hode først og rett i skjørtefolden der en
fonteneeksplosjon vrir kroppen hundreogåttigrader i ren nytelse.

Dark Horse har egne idéer. Kvinnen entrer leiligheten, går skrittene til
nærmeste kjøkkenkrakk, snur seg og retter blikket. Dark Horse trer av
seg en turkis truse som sklir ned ankelen.

“Ja?”

Stum beundrer hun den vakre figuren som erobrer hovedrollen, stjeler den
fra henne med største selvfølge.

“Hva får jeg for det da?”

Hun er der, deltar i leken, forsøker å gjenvinne kontroll og vil ikke gi
opp uten kamp. Der står de ansikt til ansikt. Dark Horse forstår
forskjellen mellom dem og behøver ikke forklare før knærne hennes bøyer
av. Seirende lener Dark Horse seg tilbake mot kjøkkenstolen og skuer
over den villige kapitulasjonen. Rommet er lite, fire kryp til føttene
der Dark Horse flipper av et sett høye hæler i lysebrun sky. Skoene har
et rødlig skimmer i plastikken som speiler hårfargen. Dark Horse ruver
langstrakt under taket, hvis hun bare hadde sett så langt. Foran henne
ligger en mindre busk, lysere og kortklippet, uten den distinkte røde
tonen på hodet.

Smaken av skritt sitrer forventning. Lukter melk, hårtuppene stikker
først, men tungen utløser et skred saft som demper følelsene. Hode
ruller for bedre effekt. Dark Horse stønner, skifter tyngdepunktet til
det andre benet for å stå stabilt mot kjøkkenstolen. Hun kroker ryggen
og griper tak rundt låret for feste til en oppstigning. Hånden på hode
griper fastere og dytter henne ned. Lyden fra Dark Horse stiger og
drukner for hvert dykk i dåsa.

Hun legger ekstra tenning i innsatsen og vibrerer leppene i fri dressur,
tunge, nesetipp og hode kjemper for flatene. Tuppen av tunge streber for
å finne et mindre punkt der veggene vibrerer.

Utålmodig vil hun høste, bestige skuldrene og rekke leppene ennå ukjent.
Åpen tillit å krølle tungene i hverandre og vise seg i hele prakten
naken. Hun har fine pupper og var stolt av det. Slank kropp og kledd for
deit i den fineste brune-bh kjøpt på Sten & Strøm. Sengen er det nye
suksessmålet, straks Dark Horse slipper tak i håret.

Grynt og stønn sprenger lydmuren mot naken hud.

“La oss flytte inn på soverommet.”

“Ikke stopp.”

Hun suger fast og lette med tungen etter punktet der lysten sprenger
rammene hos Dark Horse. Hånden glipper.

“Ikke stopp.”

Bønnen blir hørt. Hode hviler på låret der hun tørker tungen og lar
sanseinntrykkene synke i takt med at pulsen til Dark Horse finner
balansen tilbake. Latter. Hun ler også. Skuespill og fyrverkeri sitter i
kroppen og sitrer gjennom de minste nervene.

“Mer.”

Det er hennes tur.

“Nei, du må gå.”

“Hva?”

“Gå!”

Fullt påkledd, yttertøyet ligger på gulvet, men ellers intakt er det for
henne bare å kle seg i jakken og plukke opp ranselen på vei ut døren.
Blikket til Dark Horse er klar tale. Betingelsene i leken er å gjøre som
bestemt. Dark Horse bestemmer i eget hjem. Hun sitrer mellom bena, rede
til å ta en dose tungegymnastikk, fornøyd med fingre. Ikke på denne
deiten. Hun må forlate leiligheten etter det ene klimakset og banner
innvendig.

“Faen, det var min fantasi.”

17
--

Hun tok faget markedsføring i skolen og besto lekende lett eksamen.
Faget tilhørte dem hun omtalte som kverulantfagene. Forakten for
poengene de ga kunne bare måles mot dårligere karakterer i avansert
matematikk, operasjonsanalyse og finans fordypning. Hun var ingen
ekspert på kommunikasjon, mer en halvstudert røver. Gjennom prøving og
feiling lærte hun hvor vanskelig det er å bli forstått. Styrke i
fagkretsene for kverulanter lå ikke i evnen å pugge lister, hvilket hun
sannelig kunne, heller lå kompetansen i forståelse for betydningen av
trynefaktor. Bestevennen demonstrerte poenget da han gikk kledd i rosa
skjorte og nyklippet hår til muntlig eksamen i faget internasjonale
forhandlinger. Han klarte full uttelling på karakterskalaen. Smilet
smittet. Hun kunne lekende lett møte utspørrer, plukke ned nøkkelord og
tale uanstrengt i førtifem minutter. Gode karakterer ga ingen glede, hun
valgte tall og statistikk der latskap var det eneste hinder for sublim
viten.

Normale kriterier for kommunikasjon sikret henne måtelig suksess.
Frustrasjon oppstår fordi hun vet fiasko kan unngås hvis hun finner den
markerte linjen mellom sender og mottaker. Hvis blikket og smilehullene
får sjarmert Dark Horse på andre siden av tåkehavet de kaller
Internettet.

Hun surrer og stokker ord, pøser på med ekstra informasjon, støyer for å
understreke poenger som går tapt i streben med å unngå det. Møter i den
fysiske verden er i det minste håndterbart. Budskap på Gaysir er verken
lineært eller tydelig, hun forstyrrer, digresjonene hennes er vanlig.
Utfordringene i dialogen ligger i lag utenpå det personlige som for
henne er tøft nok fordi budskapet om kjærlighet er vanskelig og dandert
i riktig mengde galskap.

Fra første stund har tilværelsen på Gaysir vært en kilde til bekymring.
Hun takler dårlig subtile og skjulte hentydninger. Hun elsker tvert imot
direkte og klar tale.

“Jeg vil gjerne gå på deit med deg.”

En troskyldig henvendelse, åpen og direkte. Hvor vanskelig behøver det å
være? Hun sender flere invitasjoner og glemmer å signere, eller vise
overdrevet høflighet. Det burde ikke være en overraskelse når svar
uteblir. Bak hver profil skjuler det seg personligheter med ulike grad
av modning til uformelle og gjeldende regler for oppførsel. Hun kan
etter flere måneder en håndfull skjulte regler. Kvinnen bak Dark Horse
er en veteran i forhold.

“Hun skylder deg ingenting.”

Sa Rachel, og forsvarte Dark Horse.

“Sant. Jeg synes likevel folk kunne være mer høflig.”

Hun hørte hvor patetisk det var, og sutret frustrasjon. Rachel må smile.

“Kommunikasjon har aldri blitt bedre av at ingen kommuniserer.”

Hun furter seriøst.

“Hva skjer mellom deg og mysterie mannen? ”

Sa hun.

Dulgte profilbilder, anonyme enavn og fri adgang til enda flere, mange
skjulte enavn. Rachel delte ikke fra livet.

“Vi får gå ut en dag og ta en lengre venninneprat.”

Mulighetene skrumper åpenheten og preger samfunnet, gjorde regler for
god oppførsel ugjenkjennelige. Rachel kunne umulig forstå. Subtile
undertekster begrenser antallet svar det er lov å gi, hvor raskt hun kan
svare. Tok det to dager før hun fikk svar måtte hun tilsvarende vente
minimum en, helst to dager før hun sender melding tilbake. En rørete
samling schizofrene sjeler utvikler uformelle regimer, sett med regler
for hvordan og hva er tillatt. Hun kommer ikke utenom. Pannerynker
avslører hvor betenkt historiene fra Gaysir gjør venninnen.

“Snart.”

Hun sitter i klisteret til den bærbare datamaskinen og venter på den
grønne prikken når Dark Horse gjør entré. Alltid forgjeves. Hun
ignoreres, møter den kalde skulder.

Lyset fra en annen profil med en obskur opprinnelse vekker blikket.
Hemmeligheten sprekker i lyset straks hun merker Snusehøna på listen
over de sist besøkende. Profilen skiller seg fra Dark Horse, fire år
forskjell i alder, men Snushøna mangler identifiserbart bilde. Bortsett
fra hjemsted på andre siden av landet hvor Dark Horse vokste opp er det
lite som tilsier de er en og den samme. Søk og loggføring bekrefter de
to enavnene rullerer nærværet på Gaysir. I løpet av en kveld er hun
overbevist om at Snushøna er den falske identiteten Dark Horse bruker
for å surfe på intetanende offer, slik hun tar måken Fake for å fly
formasjon.

Flyturene hadde opphørt. Hun tilsto for Dark Horse måken var hennes, og
straks bekreftelsen lå i bordet endret livet karakter.

Dark Horse visste før hun tilsto.

“Hei, du er virkelig hyggelig. Jeg vil gå på deit med deg.”

Ingenting mer komplisert til fugl nummer to i rommet. Meldingen ville
sitte, tenker hun, og går bort fra tanken like raskt. Bortsett fra
faktum Snushøna har hjemsted på andre siden av landet er det lite som
vitner om en varm personlighet. Kort og godt ville hun aldri invitert
enavnet hvis ikke for en klar idé om at dette er alter ego til Dark
Horse. Hun visste ikke hvordan det skulle være mulig å invitere profilen
og bevare troverdighet. Tanken er å snu bordet og gjennom en
sjarmoffensiv mot den falske fuglen overbevise om at de to har livets
rett. Hjemstedet alene utelukker kontakt. Snushøna bor på andre siden av
landet. Hun hadde ekskludert Fab fra Lillestrøm. Galskapen gikk for
langt og det er viktigere enn noensinne å verne om hva som betyr mest,
huske personligheten hun gjenkjenner som gjør henne stolt. Hun ville
tape hvis kriteriene endret seg på Gaysir. Aldri skulle hun igjen
henvende seg til andre, aller minst profiler som ikke omfattet
kriteriene. Ondskap er krefter som river ned standarder, tenker hun.
Nederlaget ligger tungt i kroppen. Følelsen frister ikke til
gjentakelse. Åpen, kanskje naiv, slik liker hun å være. Smerten sliter i
viljen til å dytte framover.

Snushøna er ikke for henne.

Dark Horse bestemmer fra dette øyeblikket. Markørene er forstått, den
grønne prikken forsvinner og hun venter til desperasjon går over i
fortvilelse.

Hun har ingen mulighet til å bekrefte mistanker uten å miste seg selv.
Snushøna får være i fred. Hun kryper på knærne foran den ubestemmelige
premie. Tilværelsen på sofaen higer etter grønne prikker. Livet er tomt.
Sjansene øker hver dag den klamme følelsen vokser, en redsel for å bli
utradert oppstår. Fullstendig kapitulasjon hjelper ikke.

Timene etter hun kom hjem føler hun motet løfter humøret, akkurat
tilstrekkelig til at hun sletter Fake. Måken har spilt en rolle og lurer
ingen. Hun kan være bare en.

Vemod og tristesse renner fritt når hun sletter den falske profilen.
Venting er verre. Følelsen av desperasjon sniker umerket inn. Dark Horse
strekker tiden, de spiller et spill og hun taper i hvert minutt uten
respons. Hinderløyper er fylt med feller, små klikk i dagboken betyr
flere skritt i mørke. Hun drømmer om å gjenta suksessen, finne sammen på
deit der bare hun kan ødelegge.

Vakuum, det nye må opp og fram.

“Farvel til Fake, velkommen Dogtooth.”

Sa hun.

Like konstruert og gjennomsiktig, en voksen mann, gammel som faren i den
greske filmen, gal og pervers, laget for den eneste som kunne forstå.
Glimt i øyet er ledestjernen. Hun fyller profilen med en YouTube-video
om hundedressur. Detaljen burde glede Dark Horse. Kennelopprett er et
motiv i filmen, og humor holder spillet i gang. Hun skaper en fyldig
profil og målet er kun å trekke Dark Horse tilbake til livet de delte.
Hun kunne ikke kjøpe blomster, og griper etter halmstrå; for ingenting
med Dogtooth er morsomt.

“Se på meg!”

Det er for lettvint å si forstyrrelsen er galskap.

Hun kan ikke lenger leve uten en falsk hinne utenpå den andre. Arbeidet
hun legger i å bygge en troverdig identitet gjør den falske framtiden
framstår mer ekte enn det virkelige jeg hun skjuler bak et bilde av
Barack Obama.

Hun lengter etter applaus.

Abrasjonen Dogtooth er den mest romantiske gest hun noensinne har
tillatt seg å slippe løs på verden. Straks byggingen av en
erstatningsprofil er ferdig går hun til Dark Horse og gjør
sjarmoffensiven kjent på den brutale besøksloggen. Gaysir lister de som
stikker innom nettprofilen enten de vil bli sett, eller være anonym. Hun
har alle intensjoner om å vise fram Dogtooth. Hun sveiver innom og
sitter rolig og venter på respons. Dark Horse vaker, venter, strekker
tiden, men ingenting skjer. Dogtooth får ikke besøk. En grønn prikk
inntil enavnet Dark Horse lyser kompromissløst, deretter går den i
oransje. Sjarmoffensiven står besvart—ubesvart. Hun logrer
skuffet—fattigere uten Fake, og det gjør det samme. Dark Horse kommer
aldri tilbake.

Dagene går. Lediggang. Hun våkner om morgenen og går på jobb, kommer
hjem og skrur på teven. Logger inn. Rekkefølgen i de daglige gjøremål
har endret seg. Hun løfter den bærbare datamaskinen og plasserer denne
på databordet inntil sofaen før hun rekker å ta av seg skoene. Innkjøpt
på IKEA gir bordet den perfekte flate for datamaskinen når
operativsystemet fyrer opp og hun henger jakken i skapet. Hun rekker å
gå på badet for å tisse før det er påkrevd å skrive passord. Den bærbare
datamaskinen er klar straks hun slenger ned på sofaen, og først etter å
ha lest meldingene på Gaysir er det tid for teve. Hun orienterer seg i
kveldens programmer med halvparten så mye vigør som det tar å studere
hovedtavlen på Gaysir.

Dark Horse svever innom, og forbi, logger av straks blikkene møter
hverandres grønne prikker. Et par ganger blir Snushøna stående og
blinke, stille i lyset, lik en som tror seg usynlig og paralysert. Hun
spiller med, gjør sitt ærende og logger av.

“Hvis du vil være usynlig, vær så god.”

Hun kjenner igjen Dark Horse, visste hvordan kvinnen bak enavnet tenker,
gjenkjenner i marerittet en sjelevenn fanget mellom mulighet og behovet
for å beskytte selvet. Stolthet og skam blandet med et ønske om å
skjerme spillet og dets regler. Hun kjemper mot sterke krefter. Hun er
bannlyst, beskjed mottatt, her stopper alt. Veien tilbake går gjennom
Dark Horse, og bare det. Vent og lytt er hennes oppdrag. Hun gjør lite
annet. Periodevis lever hun gjennom dagene med letthet, og innimellom
kollapser hun fullstendig og klikker inn til dagboken. Jakten på Dark
Horse er manisk.

Finnes det i grunnen verre tortur?

Hun lurer, stryker indeksfingeren mykt over Enter-knappen.
Galleribildene frister. Hun har dem lagret i mapper på datamaskinen.
Dark Horse ville aldri ringe. Hun vet det.

Dogtooth svever i intet, kan verken fly eller få fram smilet på samme
måte som Fake. Et besøk vekker oppsikt, og selv om det ikke er
adressaten retter hun ryggen og merker hver eneste detalj. Nærværet
blottstiller kvinnen, geipen i status etter besøket og dagboken der
hendelser samsvarer på dato. Minutter går, status endrer seg til en sur
geip og de blir fiender i samme øyeblikk. Kvinnen er en konkurrent til
Dark Horse. I eget hode leder hun løpet ved å være den nye i formelen.
Hvordan kan en fortapt sjel, fullstendig utenfor, uten sjanse være i en
bedre situasjon enn ekskjærester? Spekulasjoner driver historien videre.
Verden fortryller, og slik ser hun ting, og slik er det. Hun har forlatt
rasjonalitet, og leter etter tegn som bekrefter at hennes falske profil
til hyllest for kjærlighet ble bygget på et gjensidig løfte. Dark Horse
dukker unna, og det gjenstår etter en uke bare å slette Dogtooth. Hun
klipper navlestrengen til de falske identitetene og sitter igjen med
dåpsnavnet og bilde av Obama på profilen. Venteleken er oppdraget, og
det virker uoverkommelig, nesten umulig.

Hun får nok, sletter Dogtooth, som hun tidligere slettet enavnet.
Frieriet levde bare en kort uke. Identiteten i kyberverden hun alltid
bruker er ikke lenger tilgjengelig. Smilerynker, skjevt nikk der
panneluggen ligger halvt over fjeset, det personlige fra et menneske som
inviterte til en samtale. Venteleken knekker tilliten. Hun oppnår ingen
deit, møter en vegg taushet, oransje prikker og hun lever med ryggen til
hva som er virkelig. Spekulasjoner dreper lysten til å fortsette.
Sjalusifølelser flyter fritt i spor og hun legger for hat ekskjæresten
til Dark Horse, venninnene, og nettstedets kalde kynisme som flyter
fritt i elektroniske spor.

Full utradering virker mer sannsynlig. Hun vil ikke lenger være seg
selv.

“Ja, slett profilene.”

Hun stopper ved Dogtooth. Heier fram ekskursjonen og angrer ikke med det
første. Mørke kveler siste rest av sommersol lenge før Kveldsnytt.

To dager, kanskje tre dager pause. Hun går inn på profilen der Obama
står igjen som siste representasjon for det personlige. Hun ergrer seg
over kostnadene ved å slette impulsivt. Gaysir trigger impulsene raskere
enn hun rekker å vurdere konsekvensene. Det ordinære og elskede enavnet
er blokkert, i karantene. Hun kan ikke være seg selv før om minst tre
uker. Inntil videre må et substitutt representere hva som er igjen å
vise verden bestående av grønne og oransje prikker.

Fantasiløst brukte hun navnet moren ga henne i dåpen. Bilder er
bannlyst, de er forbeholdt det digitale enavnet hun slettet. Kobler hun
fjeset til det virkelige navnet blir Gaysir for mye virkelighet. Hun må
ty til triks og fanteri, skrive en profiltekst knyttet til engasjementet
hun viste i presidentvalgkampen på andre siden av Atlanterhavet. Hun
velger profilbilde av seierherren Barack Obama i stiv positur foran et
speil. Inntrykk, uttrykk, arroganse og skuespill. Falske refleksjoner
speilet i foajéen til det hvite hus. President Obama kan ingen forveksle
med en lesbisk kvinne i førtiårene. Heller vil de mistenke hun
idealiserer mannen, når sannheten er det stikk motsatte. Han fungerer
som det perfekte skalkeskjul i mangel på det personlige bildet. Valget
av presidenten er verken originalt eller egnet til å forlede. Ideen får
hun fra frøken Grønn. Damen fra Askim dyrker Obama på linje med de
ivrigste tilhengere i USA. Hun velger å skrive om idealismen hun fant i
presidentkampanjen på andre siden av Atlanteren, lojaliteten til
presidentens motstander i de innledende rundene, grenseløs dedikasjon
til hva som skulle bli usanernes første kvinnelige president og
skuffelsen når det ikke gikk som planlagt. Historien sier alt om henne.
Nettene foran dataskjermen for å skrive innlegg på fremmede nettsteder i
USA. Hun følte ingen glede da Obama vant nominasjonen. Hun gråt.

“Jeg tror engasjementet i presidentkampanjen sier mer om meg enn alt
annet som kan stå i en profiltekst.”

Skrev hun.

Fordekt bak en mørk maske, hun synes det er vanskelig å lyve. Villskap
og rusfølelse beskrevet gjennom engasjerte innlegg for politikk på andre
siden av verden. Galskap hvis ikke for døde norske soldater, offer for
storpolitikken. Engasjement er rasjonelt, sant—ekte, personlig. Henne i
ett nøtteskall. Profilen der Obama er talsperson for henne virker
naturlig, men ganske snart går hun lei av det forsiktige tilløpet. Hun
vil gjenoppstå i drakten som ble slettet, og inntil den dagen var mulig
måtte Barack Obama stirre på de grønne prikkene. Hun kan ikke lenger
stikke under en stol det er vanskelig å forlate dem.

Frigjørelse sliter i kroppen mer enn hun trodde mulig.

Flere profiler blunker. Hun sklir unna, redd de vil trekke henne inn.
Britt75 bruker ordet avslappet i status og hun avslår straks
invitasjonen kommer.

“Nei takk, for tøff profil for meg.”

Sa hun.

“Synd.”

Neste dag skifter Britt75 status til kåt og kosete. Hun ler, for hele
natten ligger hun søvnløs og tenker mye på hvorfor profilen var for tøff
og vekket uro. Er det mangel på bilde, faktum Britt75 oppgir piercing på
kroppen, eller det harmløse ordet avslappet i status? Intuisjonen
tilsier hun må avvise kvinnen. Først neste dag når statusteksten skifter
vet hun intuisjonen er det mest verdifulle hun har for å overleve på
hovedtavlen.

Trafikkulykker har mindre appell enn skrikene i lystavlen. Hun stirrer,
spekulerer og registrerer den minste bevegelse i enavnene som henger på
hovedtavlen i limbo. De puster i den samme luften, og kveles. Sykelige
drifter pulserer, hun tar kontroll, viser hva som er i vente. Lik en
narkoman vil hun trappe ned, ta små skritt og når enavnet er
gjenopprettet vil hun til opprette det normale bare for å flykte med
ryggen rak. Hva som er normalt tenker hun nøye over, og bestemmer det er
en tilstand der hun som seg selv rekker å bygge en kjølig distanse til
alle, før hun faser bort fra denne verden.

Fab og Dark Horse er historie, de tilhører ikke framtiden. De er der i
nåtid, sykelige symptomer på tilværelse i stress. Lærdom og erfaring,
tilbakelagt.

Viktigst er følelsen av endelig å være tilbake etter dager og uker
forvist. Abstinenstørsten er tilfredsstilt med Obama, midlertid slurper
hun de siste dataene hun trenger for å sove i fred. Tilværelsen fri er
nære. Det er frihetens tid hun lengter etter, en tilstand der hun kan
redde ansikt. Sporadisk logger Fab inn og ut; ikke lenger den samme
kvinnen hun kjenner fra Lillestrøm. Dark Horse følger vanlig mønster og
nekter å la nærværet forstyrre. Hun ser og puster lettere. Uforstyrret,
den samme.

Hun er persona non grata.

Alt er som før. Hun finner komfort i det, i alle fall for en stund.

18
--

Hun strever for å gjenvinne balanse. Ingenting er lett. Hun boikotter
enkelte profiler, innfører sanksjoner hvis hun bryter reglene, og nekter
å logge på Gaysir i lange perioder. To enavn, mannfolk fra Østfold og
fleksible i søket erstatter en kort stund Fake og Dogtooth. Mennene skal
skli inn i bakgrunnen på hovedtavlen og hun gjør dem eldre og ekle.
Målet er å skape to falske enavn som verken vekker entusiasme, eller
oppmerksomhet i søket etter kvinner.

Veien tilbake går i gamle spor.

Tvangsmessig søker hun ned i alder og stopper der hun fanger opp Fab.
Andre ganger gjør hun søket mindre og følger aldersmålet. Fornuft slår
sjelden nysgjerrigheten. Søk begrenset til lokalisering rundt Oslo og
innenfor aldersgrensen på fem år yngre og eldre er knapt verdt tiden det
tar å skrive passordet. Dark Horse er åtte år yngre, samme alder som
ekskjæresten og hvis søket stopper på ti år yngre og eldre, koster det
lite å legge listen hakket lenger ned. Hun rasjonaliserer for å se Fab
blant lysene, men kvinnen fra Lillestrøm logger stadig sjeldnere inn.
Søket inneholder i det minste ingen av de falske mannfolkidentitetene
det er nødvendig å bruke for å sirkle usett på hovedtavlen. Etter måken
og hundekreket blir hun kvalm av å se sine egne falske enavn blant de
håpefulle som søker kontakt. Mannfolkene fra Østfold lever sjelden
lenger enn noen uker.

Sightseeing i fristende profiler, harmløs moro og tidsfordriv.
Pikkihagen hevder det ikke går an å ståke noen på Gaysir, akkurat som
det ikke finnes uønsket seksuell oppmerksomhet i et rom for de frilynte.
Perspektivet er absurd, for forum debatterer jevnlig de unge pikenes
ubehag når de mottar meldinger de spesifikt ber seg frabedt.

“Ensomhet og perversitet eksponert den intetanende er like ubehagelig på
Gaysir som noe annet sted.”

Sa hun.

Hun utveksler høfligheter med vennen når de først møter hverandre i en
debattråd. Han besøker ikke profilen, og hun gjengir tjenesten. Plassen
er bare for små meldinger, for alt annet kan de ringes. Pikkihagen har
skiftet jobb og de avtaler dato for å slarve over middagen han tilbyr
seg å lage på hennes nye kjøkken.

“Skyss!”

Kun en sjelden gang bruker hun de falske mannfolkene til å klikke inn i
andre dagbøker. Hun er ensporet, fortapt, prisgitt ventingen.
Tilværelsen krever noe nytt, en interesse og fasinasjon. Søk på Dark
Horse og spor på sist oppdaterte logging tar mindre tid enn døgnets
tjuefiretimer.

Hun er glad for å være i live. Tilstede i det minste, deltaker i et
parallelt univers der Dark Horse puster og søker svar.

Angrer lenge gjør hun over å ha slettet profilen, enavnet hun elsket og
måken hun speilet. Ferske profilnavn er avskåret fra å delta i
debattene. Nye og falske identiteter danner et sammensurium. Hun gjør en
innsats for å rydde i profilene og få oversikt i kaoset. Uker er igjen
til det blir mulig å skifte enavn til det hun blir gjenkjent for.
Presidenten i USA fungerer som oppmerksomhetsavleder. De som vil vite
hva hun skjuler får ta kontakt.

“Pad.”

Tre bokstaver i status til Dark Horse hun ikke forstår.

“Hva forsøker kvinnen å si?”

Galskap er å tolke, overtolke, en øvelse hun får trent innimellom søkene
etter spor fra rundt regnet fire til fem favoritter på hovedtavlen. Dark
Horse vekker sterkest følelser, en driv bare mulig å mane fram i
seksuell ekstase.

Hun strekker tiden og gjør hva hun kan for å være tilstede, bli sett og
delta.

Etter den greske filmen Dogtooth ruller en scene i hjernebarken. Hun er
drevet av et bunnløst begjær. Hun står på alle fire og krabber inn i
Dark Horse, hode først og selv om det harde gulvet presser mot knærne
sluker hun i en kraftanstrengelse den nakne hårfloken foran klitoris.
Rødt, piskete hår. Skjørtekanten flakker i øyene og hode må tilte
halvparten av gangene mot høyre for tungen å treffe punktet mellom bena
og utløse skjelvet i bekkene. En flod veller over når Dark Horse fullt
påkledd, truseløs, sitter på sofaen og hersker. Hun på alle fire gir seg
hen til sitringen.

Med høyre hånd i skrittet må hun holde balansen på tre ben når
madrassene gynger. Mot sengegavlen slår hun hode og fantaserer om
skrittet til Dark Horse, kjenner ristningene vokse.

Fantasi er bedre enn virkeligheten.

Orgasmen som kommer etter tre korte minutter er tilstrekkelig for å
befeste en vilje til å være tålmodig. Mer tålmodig enn hun noensinne har
tenkt det er mulig å være.

“Vi kan være venner.”

Sa Dark Horse når de treffer hverandre på deit.

“Nei.”

De kan aldri bli venner etter Dogtooth—etter den første seksuelle
fantasien. Hun står oppreist på alle fire i sengen og slår hode mot
sengegavlen. Deilig, søt, eim av eplesjampo befester velvære og
ingenting er vakrere og mer begjærlig enn Dark Horse med rødlig hår og
den lange slanke kroppen.

De vil vinne.

Det glipper, hun føler sklien, en merkbar eim av usikkerhet griper tak.
Litt mer hver dag. En enkel forespørsel, hun spør venninnen med mest
erfaring fra ulike nettspill og sjargonger om å si hva Dark Horse mener
med status. Et enkelt mysterium for en som forklarer henne venstrehake
etterfulgt av tallet tre betyr hjerte—elskede. Hun vil vite hva brukere
som skriver *pad* i status sier. Utledet kontekst, blottstilt galskap er
utelukket og til venner går et konkret spørsmål som søker svar.

Hun får ingen hjelp. Vennene visste verken ut eller inn. Hun sender
tilbake en tvetydig spørresetning.

“Nei.”

Neimen om noen visste. Søk på trebokstavelsesforkortelsen gir mange
muligheter, og hun fester blikket på en av dem. Medisinsk betyr
forkortelsen *pad* på engelsk symptomer på hjerteproblemer. Kjennetegnes
spesielt blant folk på høy alder og dersom det tas tak i, gjennom
trening og livsstil vil hjerte fungere normalt i mange år ekstern.

“Hun ble sett. Galskap og det hele.”

Tenker hun.

Dark Horse kommenterer henne via status. Hjertet banker for en fremmed,
en sjel der profilbilde og en ugjennomtrengelig mur av forsiktig
skjerming. Ved å banke, daglig å logge på og raskt besøke profilsiden
til Dark Horse for nye dagbokoppdateringene viser hun seg fram på
besøksloggen. Hun forteller om en interesse og nærværet blir oppfattet.
Det er mulig hun krysser grensen for sjikane? En gang i framtiden, til
høsten lovet Dark Horse hun kanskje ville takke ja til deit?
Beslutningen har en eier, og hun bønnfaller om å bli tatt i nåde.

Hjertelidelsen rammer fortrinnsvis de eldre, men enkle grep skal til for
å sikre livet og gå videre. Pad er avslaget hun har ventet, kommentaren
sender et direkte budskap bare til henne, og tolkningen er klar som
dagen. I det minste framstår det klart etter et par dager, når venninnen
rister på hode og gir opp å gi nærmere forklaring på nettsosiologiens
meningsbærende idé bak ordet pad. Dark Horse ler hånlig av
hjertelidelsene, falsk elskov, kaller henne gammel og ber henne ta til
fornuft. Hun holder ikke ut.

Hun vil slette profilen og vet bedre.

Savnet etter eget enavn river i kroppen. President Obama er det nærmeste
hun kommer en ekte framstilling av personene bak galskapen. Han er
uegnet i den ærerike retretten hun ønsker; skrittvis utmeldelse fra
hovedtavlen på egne betingelser er en drøm.

Hun vil kontakte Dark Horse for å få status fjernet. To dager lette hun
etter forklaringer på en annen betydning på teksten, noe annet enn det
medisinske uttrykket, sjablonger og koder kjent bare for de vant til å
kommunisere med hverandre i digitale kanaler.

Dark Horse ville aldri ta kontakt. Hun visste det. Mobbingene måtte i
det minste stoppe.

Tilværelsen der hun venter på en grønn prikk og deretter skuler
optimistisk mot meldingspostkassen virker tom og fortvilelsen gjør henne
yr og forvirret. Hun må vite, tvunget av spørsmålene som humrer i
bakhode gjør valget åpenbart, for hun får aldri hvile.

“Om kommentaren er til meg. Vær så snill stopp.”

Skriver hun til Dark Horse.

Hun føler seg mobbet. Høy alder, ubesvart kjærlighet og andre
tvangstanker framstår som hjertelidelser. Hvis Dark Horse har
medlidenhet må de tre bokstavene bort. Deretter kunne de igjen ignorere
hverandre, stirre på grønne og oransje prikker, uten å handle.

“Pad? Som i mitt nye lesebrett fra Apple.”

Flau leser hun forklaringen fra Dark Horse. Skammen over å være
selvsentrert, egenkjær og uten stolthet kommer senere, siger inn om
kvelden og treffer hardest da hun møter venninnene på bar. Hun forstår
hvor skamfull alt har blitt når skandalen er hemmelig for dem. Hun vil
ingenting fortelle. Dialogen med Dark Horse skal opphøre, hvis hun kunne
kalle den ensidige realitetsbrist hun opplever fra enkeltstående
setninger på status en dialog. Graden av galskap har degenerert til noe
skjult, og alt som gjenstår er Golgata—marsjen i ydmykelse til enden.

Hun må drepe en drøm. Det er ingenting hun heller vil, og hun elsker å
drømme.

Før hun dro for å møte venninnene på Aku Aku sender hun et svar, en
melding til besvær, en anklage. Unnskyldningen for fullstendig å ha
misforstått status mangler. Hun graver i stedet fram trivielle
bemerkninger egnet for å provosere. Avskjære. Trå vann. Distrahere.
Innhold framstår meningsløst og er siste vers på kapittelet Dark Horse.
Setninger kun tillatt mellom nære venner, absolutt ikke mellom fremmede
på et nettsted for sjekking. Der treffer de anger og panikk hver dag.

Det var ikke hennes beste stund når hun anklager Dark Horse for skjulte
budskap. Barkrakkene sitter høyt og venninnene kakler uforståelige
trivialiteter. Hun drukner i egen forlegenhet når trivielle fakta sklir
forbi og knapt enser forandringene i henne. Hun planlegger å trappe ned,
velger strategier for ærefull retrett og har ikke tid til å høre
venninnene sladre. President Obama må gjøre hva hun tenker vil fungere
bedre i full åpenhet, vandre kontrollert fra hovedtavlen. Det eneste hun
ønsker er å trekke profilen i et spill der det fortsatt er verdighet for
alle parter. Planene utviklet seg. Gjenvinne stolthet i ordveksling
mellom to enavn som aldri har truffet hverandre utover meldinger i enkle
setninger. Hun trenger å svare på fornærmelsen. Anklagen hun sendte var
grunnløs, rotet i bunnløs fantasi og kokt sammen i hode. Hvorfor skriver
Dark Horse ikke simpelthen *ipad* i status? Hun ville forstått ipad;
forsøker hun å innbille seg. Hun ville aldri mistenkt et skjult
budskap—tolket en medisinsk forkortelse og tillatt seg å bli fornærmet,
latterliggjort for lidenskapen hun føler.

“Åh, for et rot.”

Hun kunne bare tilstå. Legge saken fram for Dark Horse og unnskylde de
siste meldingene, anklager om skjult mobbing som ikke finnes. Forklare
feilen er hennes, og problemet likeså.

Åpenbart!

Selvsagt for dem begge og likevel bærer hun et håp om at det ikke blir
slutten, heller en begynnelse gjennom ydmykelse. Slik er følelsenes
natur. Forelskelse klamrer til halmstrå.

“Dette er ikke meg.”

Nettopp. Kunne hun selge det, løgnen, forklare det irrasjonelle og i
beste fall gjøre det vakkert, eller mer sannsynlig, utslag av keitete
klumserier fra en tulling forelsket i en idé.

Hun er flau og beste kur er å tilstå, gjøre seg enda flauere i
programmet for kjærlighet. Hun vil vise det irrasjonelle blir forstått.
Tilståelsene viser fornuften bak galskapen, og slik gjenvinner hun
fotfeste—-en unse respekt. Endelig kan hun gi Dark Horse fred, hvis de
bare får snakke og felle dommen selv. Slik raljerte hun i hode, og
visste lidelsen var hennes og hennes alene.

Hun må anta at slik ting sto er det ingen redning. Klar beskjed tilbake
ville være første skritt til full helbredelse. Visshet var hva hun
trenger for at kuren til friskmelding kan starte.

Skrittene hjemover over Bentsebrua minner om løp. Iveren etter å komme
hjem er en del av galskapen som setter henne i uføret. Hun vil rette
meldingene tidligere på dagen, legge kortene på bordet—igjen. Hvor mange
ganger kan hun erkjenne overtramp? Hjemover har hun hastverk. Galskap
vil ikke fjerne den forsmedelige og flaue smaken. Kald bris varsler
høsten er på vei til Oslo, mørke rundt gatelysene langs Akerselva
bekrefter faren som lurer.

Hun setter seg foran den bærbare datamaskinen lettere andpusten etter å
ha småløpt hjemover. Venninnene snakket i ett, og hun kunne ikke dele
den flaue smak fra selvutlevert paranoia. Anklagene hun sendte til Dark
Horse om å ha skjult meldinger i status, hets fra en forstyrret, absurde
påstander sannferdige først når meldingene ble sendt der hun framstår
både som gal og selvsentrert.

“Alt handler ikke om deg.”

Sa mor.

Hun visste moren hadde rett, men protesterte alltid. Logikk tilsa det
motsatte.

“Hvis ikke verden handler om meg, hva er meningen?” Hun mistenker
spørsmålet er enkel filosofi egnet til rasjonalisering etter slett
oppdragelse.

Flau. Smaken stikker i tungen. En barstol på Aku Aku satt hun halvt og
trippet, over Bentsebrua og opp bakken til Sagene løp hun andpusten.
Planen på vei hjem er å tilstå, sende meldinger og uforbeholden
unnskyldning. Blottstille mors advarsel. Starte en nedtrapping og styre
ett farvel, gå i dialog hvor hun ikke framstår som dum.

Dum er det verste. Forelsket er en tilstand hun kan forsvare.

Årevis med teve gjør det romantiske ideal til noe sublimt som henger
høyt. Hun bar i seg sentimentalitet som en medaljong, et ideal over alle
andre. Sterke følelser. Dum ville være det aller verste. Tilståelsen
ville rense luften. Kunne hun bare forklare?

Tastaturet knatrer meldingen da Dark Horse logger inn. Kort og like
raskt er den fremmede kvinnens enavn borte fra hovedtavlen. Minuttet
etter lyser den grønne prikken inntil nyankomne Snushøna. Paralysen
setter seg i ryggraden når den falske profilen viser seg på
besøksloggen. Meldingen hun sitter og skriver blir hengende i det åpne
vindu og musepekeren får hvile. Kroppen er stram og venter på å sende.
Hun tviler, lar heller Snushøna lese profilen i fred. Anklagene bringer
alteregoet til Dark Horse fram i lyset og profilen blir studert. Hun
blir lest og følelsene av at unnskyldninger haster mer en før ringer
bjeller i bakhode. Tross det lar hun meldingen henge usendt til Snushøna
logger farvel og den grønne prikken forsvinner.

“Hva skjedde?”

Planen er å unnskylde. Hun vil sende enda en melding, legge seg flat og
erkjenne galskapen er langt framskredet og alt som gjenstår etter en
selvsentrert feilkobling er å skyve dialogen inn i en spiral med
trivialiteter for deretter å trekke seg ut med skadeskudd ære—noenlunde
hel. Vond og smertelig dialog der hun går spissrotgang og finner en vei
bort, fokus og nye suksessmål. Alene sitter hun og skuer på meldingen
til Dark Horse som starter slutten, det er ingen grønne prikker igjen på
hovedtavlen å fiksere blikket mot, bare en masse fremmede. Hun trykker
på send for å forfølge neste planlagte skritt. Veien fra av en deilig
besettelse er en renselse der hun ligger langflat, prater og saklig
trekker seg tilbake. Slik ville det ikke gå. Hun treffer nettsiden for
blokkerte brukere.

Sjokket sitter.

Dark Horse har blokkert henne og vil verken godta meldinger eller besøk
på nettsiden.

Hun vil ikke få sjansen til å unnskylde. Innlede en dialog der hun får
trappe ned og trekke seg bort. Blokkert. Beskjeden er utvetydig. Klar.
Brutal.

Dark Horse skylder ingenting og den ensidige tiggingen tok siste rest
tålmodighet. De har ingen felles historie, ingen framtid. Omgivelsene
tilhører dem begge, men all kontakt, kommunikasjon, skjulte budskap
eller henvendelser må stoppe.

Tristhet synker inn. Hun er i sjokk.

Galskapen brakte henne hit. Budskapet var ikke til å misforstå. Sånn er
situasjonen avklart og hun føler merkelig nok en lettelse. Tålmodighet,
venteleken er ingenting for henne. De kunne møtt hverandre på en kafé,
snakket som to rasjonelle mennesker og funnet dette var ingen match.
Ønsket hverandre lykke til videre i livet, eller valgt andre veier.
Samtalen fyller tiden, og det kunne vært et hyggelig bekjentskap.
Spennende.

I stedet sitter hun igjen med en anstrengt og trist dom.

Dark Horse blokkerer henne minutter etter Snushøna gikk inn i profilen
og hjelper til med å gjøre opp en mening. Forlatt alene på hovedtavlen
seiler hun sin egen sjø. Hvis galskapen må bekreftes finner hun det
nettopp i tidslinjen som avslører at hun vet profilen er blokkert snaue
ti minutter etter grensene ble satt. Hun trykker på enavnet til Dark
Horse og varsles, hardt og brutalt. Det foreligger en blokkering og hun
kan ikke se brukeren, lese profilen, dagboken, eller sende
unnskyldninger. Hun kunne ikke lege seg flat, skjelle, spørre, eller
foreslå noe som helst. De er fremmede.

Kun ti minutter. Hvis ikke tiden det tar fra hun blir blokkert til hun
oppdager sperren på enavnet er det endelige bevis på at hun trenger å
bli blokkert. Hun er gal. Det er ikke bare en floskel hun smykker seg
med. Hun har blitt en ståker med for mye tid. Hun mangler filter.
Subtile meldinger, forsiktige skritt og tilnærminger fungerer dårlig for
slike som henne. Nettsteder for å sjekke ville aldri fungere. Beviset lå
i tre invitasjoner, tre avslag og profilen blokkert.

19
--

Tilværelsen etter å ha blitt forvist fra et liv gir klarhet. Hun vil
ikke slukke lysene. Grønne og oransje prikker viser de andre, alle hun
aldri kan få. Lakonisk skriver kvinnen med eim av kynisme i status en
kommentar om lykke.

“Dette kaller jeg lykke.”

Hvordan skulle hun forstå det?

Lykken flykter. Hun makter sjelden å finne entusiasme og glede i en
profil. Halvparten mangler bilde og den andre halvparten kjenner hun
godt.

“Zombie time!”

Skriver hun og håper Dark Horse fanger dobbeltbetydningen fra filmen de
delte—\ *Dogtooth*, som leker med begreper og gir en alternativ
betydning til ordet zombie: Blomst. Hun blomstrer og visner i
forlegenhet. Blokkert og avskåret fra å ta kontakt.

En blokade er ingen kur.

Galskapen sitter i kroppen og utfordrer hver dag. Fjesene i rekken
kommer i alle versjoner og ulike former. Standard er et bilde tatt foran
speilet på badet, eller som utkledd fugleskremsel på firmafest. Lesbene
liker å skue fra fjelltopper og vandre på en sti i skogen. Eldre
profiler, hennes jevnaldrende har sjelden bilde av ansikt og velger
estetisk natur.

Dark Horse er fjeset i mengden utenfor rekkevidde.

Fab ser hun aldri på hovedtavlen. Kanskje hun ikke ser Fab lenger,
kvinnen fra Lillestrøm som nesten blokkerte henne. Fasinasjonen hun
unnslapp. To katastrofer på kort tid. Smerten av å bli blokkert, forvist
fra livet til Dark Horse griper tak og lager knuter i magen.
Beslutningen er tatt, hun må møte venner og livet. Hun fyller kalenderen
framover, det er et par fine datoer og løfte om liv foran dem, og hun
kan ikke vite den første av dem er alt hun trenger for å finne en
kjæreste—et menneske i kjøtt og blod, en jevnaldrende med tale, lukt og
lys pannelugg.

Livet er ikke i balanse. Hun må gjøre noe galt. Litt sent for
erkjennelser. Ulykke, misere og forlegenhet fyller livet. Deiting på
nettet framprovoserer det verste og beste i henne.

Det blir ingen deit.

Hun vil tilbake til det opprinnelige enavnet, bruke bilder av skallet
rundt egoet, kle profilen i et sannferdig og disiplinert ytre som gjør
marsjen troverdig. Usanernes president skjuler godt, og populasjonen
lærer henne å kjenne, finner hva som ligger bak. Han gjør jobben, men er
ikke fullstendig. Alle liker ikke det ivrige menneske de finner. Hun er
takknemlig, glad i egoet. Med enavnet tilbake kan hun vise verden, vise
seg fram og begynne på nytt. Ting blir annerledes. Bilder av henne i
stedet for Barack Obama vil lokke fram nytt håp.

Paralysert.

“Hva gjør hun nå.”

Tenker hun.

Livet er ikke syntetisk. Hun vil elske—og like viktig bli elsket.
Kravene finnes, stolthet og en grense. Inni finner hun alt i et
menneske. Også i dem hun tenker og drømmer om å eie. Dark Horse har krav
og nettopp derfor er umotiverte meldinger fra en fremmed et overtramp.
Fornærmelsene som fulgte villskap. Blokkert og ubønnhørlig plassert på
sidelinjen. Budskapet er utvetydig. Vondt. Rasjonaliseringer er det
siste forsvar.

Kjedsommelighet, ønske om å late som, det er gode grunner for å ha et
nærvær. Det er uaktuelt å forlate hovedtavlen med grønne og oransje
prikker, å vise sårene, blottstille mangelen på en plan B, fortsatt står
livet utenfor. Det er som om ingenting har skjedd.

Livet er preget av å være innkapslet. Være er å vise seg fram, beføle
datamusen og holde prikken grønn. Aktiviteter, dialog og søk. Såret er
åpent, stort og kroppen krymper seg i brekninger fra minner. Ti
minutter. Det kunne umulig ha tatt mer enn ti minutter før Dark Horse
blokkerer brukeren til hun trykker send med neste trivielle melding
anklager og patetiske unnskyldninger—bare for å få beskjed om at
meldingen ikke kan leveres.

“Du er blokkert.”

De ti minuttene viser bedre enn noe annet hvorfor en forvisning er
nødvendig. Hvor fortjent og skyldig. Skam. Ingen av disse følelsene får
være i fred når dommen er fullbyrdet, for dommen må dokumenteres. Her er
beviset. Nærværet blir plagsomt for dem begge. Spørsmålene det stiller,
usikkert og trinnvis. Dark Horse må titte, besøke nettprofilen og sjekke
om ståsted forblir uendret.

Gjensidig snoking gjennom agenter fra Østfold, slibrige og blottet for
troverdighet. Falske profiler manet fram i desperasjon og fortvilelse.

Er Dark Horse verdt å elske? Hun vil aldri vite sikkert. Kvinnen hun
googlet, stemmen som stiller spørsmålene og forsiktig slipper små biter
informasjon bærer i seg et håp. Potensiell kjæreste er kanskje den beste
beskrivelsen av forventningene hun har til kroppen på bildet. Sju bilder
fra profilens galleri og et par fra nettet. En tynn, hengslende kropp
med halvlangt rødlig hår. Intelligens, utvilsomt. Innsiktsfull? Dark
Horse virker vellykket. Suksess gir et strengt blikk på verden, mens hun
har blitt mildere med årene. Hun har lært å mislykkes, og
mislykkes—ofte. Kravene til livet er justert deretter. Spesifikke
grunner uteblir, forventninger over tid reduseres automatisk. Det må til
et par smeller, ydmykelser i full offentlighet, når eksamen går fløyten
eller hun vraker drømmepartneren på en ubetenksomhet. Alderen øker
realismen. Respons forsvinner i blikket, og smil, hun markedsfører seg
selv ovenfor vennene med slagordet:

“Mitt liv er en åpen bok.”

Uttrykket faller flatt. Hun vet det er en løgn. Salget går den ene
veien, og hun må overbevise. Tjueniåringen kan ikke kjempe i forholdet,
den oppgaven tilhører de over førti. Hun glansfarger håret for første
gang. Et og to grå hår la seg på toppen og den mørkeste brunfargen i
butikken absorberer deres lyse glimmer og gjør alt som før.

Forberedelser til et møte som vil skje. Hun har ingen vilje, krav eller
betingelser.

Situasjonen etter å ha blitt blokkert er herlig befriende. Knefall
ligger ikke til slekten og hun mener alltid åpen kommunikasjon er å
foretrekke framfor dulgt symbolisme. Bruk av bilder, pekere til
musikkstrimer med klar tale i sangtittel, eller de melodiøse sangtekster
hun må tolke. Det er vanskelig å forstå det sublime, eller brutalt
enkle. Dialog er fraværende og henger etter i beste fall. Timer og
dager, tålmodighet i prøvestadiet, en øvelse i utholdenhet.

Kjærlighetstesten hun stryker i hver gang.

Dager etter hun gikk inn i limbo gjør hun en tilfeldig oppdagelse.
Dagbøker ligger åpen for alle å se i lister over de siste oppføringene.
Der henger de personlige tekstene til allmenn skue for innloggede som
vil lese dem. Det er aldri nødvendig å bevege seg inn i profilen og
avsløre nysgjerrighet på besøksloggen. Oppdateringer i dagboken kan
leses umiddelbart på samlelisten, og det er ingen grunn til å avsløre
seg for de begjærte. Private nedtegnelser på profilen til Dark Horse
ligger åpen for hele verden å lese, akkurat som alle de andre
dagbokskriftene.

“Hvilken amatør, novise.”

Hun visste det var sant straks det ble sagt høyt. Ingen i rommet lyttet.
Hun faller for grep en bruker med minimal kunnskap om funksjonalitet på
Gaysir kjenner, og hvis noen som henne mangler den kulturelle ballast
til å justere når uformelle regler setter grensene for sømmelig adferd,
ville verktøyet veilede bort fra besøksloggen.

“Alt kunne vært unngått.”

Tenker hun.

Tanken må komme, men realiteten er ståkere vil bli oppdaget. De skal
tas, de vil tas. Politiet i fortellingen er dataloggføring og
systemutviklerne i syklubben som eide det hele. Offentlige linjer, åpen
for verden viser hva hun søker og uten avsløring gjør de mulig å følge
et liv parallelt med det egne.

De har sikkert ikke intensjon om å bli politirapporter. Besøksloggen på
nettprofilen er til hjelp og gir det enkelte medlem veiledning og tips
om hvem som følger dem.

For noen er besøksloggen en plage. Majoriteten lar seg kue og slutter
straks de oppdager den lammende effekten overvåkningsapparatet har på
andre. Surfe fritt er en illusjon. Snart er det ingen igjen. Kun
amatører. Falske identiteter har oppgave å snoke. Selvet, nettprofilen
med egenverd og selvrespekt må holde seg for god til å kikke. Spørs om
det ville hjulpet å oppdage dagboklisten? Se og bli sett er like mye en
funksjon av ståkerens program. Hun ville gi seg til kjenne for Dark
Horse. Ta en sjanse, ha en sjanse. Nå er alle sjanser tapt og i
avstumpet kynisme skuer hun verden på hovedtavlen.

20
--

I vanvare ståker hun hovedtavlen i måneder, fremmed for finere finesser,
rett på sak går hun i gang med å treffe nye mennesker. Lenge tror hun
meldinger må fyres fra profilen. Først senere forstår hun meldinger kan
sendes direkte fra ikonet, konvoluttene som lyser grønne og oransje
inntil profilbilde. Konstruksjonen på Gaysir er slik det er aldri
nødvendig å vise ansikt i besøksloggen. Hun oppdager dagbokskriverier er
listet på en samleside, for sent; uken før blokkerte Dark Horse profilen
fra besøk. Hun føler skam og forlegenhet. Stresset jager hun spor,
nytteløst og rent av skadelig, det er ikke til å unngå. Et av livets
paradoks er at retning frastøter der linjene stopper. Isolasjon er
uunngåelig.

Og for hva?

Hun gjør forberedelser for og tre fra skyggen til presidenten i de
forente stater og gjenoppta enavnet. Planene å trappe ned
tilstedeværelser dveler, men er avhengig hun gjenvinner identiteten og
finner balanse.

Hun holder seg til vennene. Livet flyter, deits i flokk åpner sjansene
og pirrer nysgjerrigheten. Fab gjør en sjelden visitt på hovedtavlen
bare for å slette profilbilde. Kroppen i singlet slutter å skrike etter
blikk, og hun kjenner stinget av misunnelse. Planen er å begrave Obama
for deretter å gjenoppstå i egen drakt; mer komponert og samlet,
frigjort fra livet på Gaysir og alt det har å tilby. Da vil det være
mulig å puste, pause og snu ryggen til forum og de grønne prikkene på
hovedtavlen som indikerer nærvær. Hun forlater de andre, bedre til å
kontrollere eget selvbilde og fri fra flommen følelser og impulser.

Hun venter. Karantenetiden løper, det er vanskelig å forlate Barack
Obama. Beslutningen å forlate identiteten var tatt, eksekveringen gikk
treigere. Exit må være på eget initiativ og føles riktig. Flukt er et
stygt ord.

Forumsdebattene ligger med brukket rygg. En debatt om topplisten og dets
skyld lurer og gjør et gjensyn. Hun trår vann og i et kjedelig øyeblikk
skriver hun et hva-sa-jeg-innlegg. Negativt oppgulp og forumets forsømte
sjel, fraværet av den frie og anonyme debatt, rensket fordi gapestokken
levde til besvær gjennom topplisten. Profilerte kverulanter har forlatt
forumet, eller de kutter innlegg for å unngå de høyeste plasseringene.
Innlegget hun skriver er krast, har en bitter brodd, og likevel må anses
for å være fullt forsvarlig innenfor hva er rimelig på et nettforum som
det på Gaysir. Spesielt i lys av konflikten som oppstår. Hun kaller
topplisten et politisk korrekt makkverk. Stikket til Trym fordi han tok
fra henne debattene en måned i forveien ligger i teksten, dårlig skjult
mellom linjene. Hun angriper på nytt fordi en gjenåpnet debatt bringer
tilbake smerten i det tomme rommet. Innlegget skal svi. Hun burde visst
Trym ville stikke tilbake: Det tok mindre enn to minutter.

Profilbildet forsvinner. Hun tror det er en feil, en teknisk glids, og
henter bildet opp igjen før hun rekker å se meldingen i postkassen med
retningslinjer for bildebruk. Barack Obama er forbudt, og bildet
forsvinner like raskt som hun får det opp. Hun ser standardmeldingen i
postkassen. Det er to av dem, identiske og helt etter skjema. Hun tenker
straks innlegget på forum er årsaken til reprimanden fra moderator.
Redaksjonens trofaste overvåker forum. Det er bare de utnevnte
moderatorene som kan slette bilder, innlegg, debatter og i visse
tilfelle hele profiler. Hun skriver ett kritisk innlegg, saklig og
likevel et angrep. Hun anklager alle som mente topplisten var en
forbedring for å ha gravlagt siste rest fornuft i et surrealistisk
trådnettverk der debatter herjet er erstattet av tøv. To minutter etter
det sure innlegget blir bildet av Barack Obama støtende og moderator
sletter. Hun leser bildereglene og forstår hovedregelen er å være
portrettert på en slik måte ingen vil ta feil og misforstå.

Hvem vil tro bildet er av henne?

“Dette er et illustrasjonsbilde.”

Skrev hun i gulgrelle bokstaver over Obamas dress før hun publiserer på
nytt.

Han forsvinner likevel, like fort som bildet kommer opp, og vedlagt de
generelle bildereglene får hun denne gang en forklaring fra moderator.
Regelen er forbud mot åndsverkbilder og dette er tatt fra VG, skriver
moderator, som gir henne en nøtt.

“Nei vel.”

Hun velger et bilde brukt tidligere, en karikatur av Obama hun har
liggende i mapper på datamaskinen. Vitsetegningen er morsom, lastet ned
fra en avis, men brukt før på Gaysir over flere uker uten at det ble
slettet.

“Tegningen er tatt fra Dagbladet.”

Skriver moderator straks han sletter det tredje bildet.

Hun bruker eget navn og vurderer en kort stund å ty til fotografi.
Enavnet hun brukte til eget bilde er i karantene. Moderator er nære å
forsere planene og komme tilbake i full figur. Ilter nekter hun å bli
diktert. Hun synes Trym er urimelig og mistenker ham sterkt for å skjule
seg bak moderatorstemplet. Hun koker. Følt motvilje fra debatten om
topplisten, personangrepet hun overmodig gjorde mot Trym en måned
tidligere og all motbør veller over. Hun ville ikke gi etter og velge et
annet uttrykk en Obama. Tegningen hun velger, en karikaturer av
presidenten tatt fra Aftenposten er neste forsøk. Bildet er mer harmløst
enn de andre, og like mye brudd på bildereglene. Hun visste om profiler
som også ble representert av presidenten, et antall tegneseriefigurer og
en håndfull peniser i fri ereksjon. Overtramp mot bildereglene finnes i
talløse eksempler. Fokuset på bilderegelbrudd, minutter etter hun
skriver et negativt innlegg på forum om topplisten får henne til å
frese. Hun vil ikke gi Trym seieren.

Hun velger en ny tegning fra Wulff og Morgenthaler, også hentet fra
Dagbladet og brukt i profilen tidligere.

Deretter er det stopp.

Først forstår hun ikke hva har skjedd. Hun åpner nettleseren og logger
på Gaysir på vanlig måte, og kommer ikke videre. Deretter forsøker hun
profilen til mannfolka fra Østfold. De falske identiteter hun bruker
stadig sjeldnere, og fra dem søker hun bare for å oppdage profilen er
borte—slettet.

Hun vil spørre Roger om mistanken er korrekt, og skriver til mannen hun
traff og gjorde til bekjent på LLH-fest tidligere den sommeren.

“Jeg håper du vil svare på det enkle spørsmålet om det er slik at jeg er
*kastet ut* av Gaysir?”

Vedlagt i meldingen legger hun ved innlegget skrevet før kontroversen
med profilbilder startet. Hun vil vise innlegget er langt innenfor hva
som er både saklig og konstruktivt. Hun sier ingenting om personangrepet
mot Trym, og seiler i villfarelsen dette er et hinder det er mulig å
komme over med bedrevitersk konfrontasjon.

Svaret fra Roger kommer raskt. Han benekter kategorisk innlegget på
forum har noe med slettingen å gjøre.

“To profiler er slettet av to grunner, og ingen av dem har med forum å
gjøre. Du bryter bevisst bildereglene og har falsk profil.”

De vil nekte for alt; skyve ansvaret på henne.

Hun forsøker å logge på igjen. De falske profilene har like lite
suksess. Moderator sletter henne, profiltekster, bilder og de skjulte
identitetene. Hun kommer ikke inn på hovedtavlen. Indignert svarer hun
Roger, vil ikke akseptere fakta og klamrer seg til forestillingen det er
maktmisbruk å fjerne identiteter uten noen form for advarsel. Hun vil
forstå konsekvensene etter alt som har skjedd.

“Jeg slettes mistenkelig raskt etter et kritisk innlegg på forum,
innenfor timen. Jeg tillater meg å tenke mitt. Betyr det jeg er dermed
bannlyst fra Gaysir for alltid?”

Hun gruer seg til svaret.

Raskt oppretter hun i ventetiden en profil og bruker en ubrukt epost fra
jobben. Hun blir umiddelbart kastet. Redselen fra debatten om surrogasi
ligger og valker i bakhode.

“Jeg kommer aldri til å ha sex igjen.”

Denne frykten, angsten for å bli utstøtt fra det politisk korrekte
landskapet i hovedstadens homoliv tror hun er mye større enn alle myter
om den ensomme homsen på landsbygden som gruer seg til å komme ut av
skapet.

Spak gjentar hun en bønn.

“Jeg må få en advarsel.”

Hun klamrer seg til håpet. Hvis de hadde advart henne først, ville hun
holdt kjeft og tatt en kort pause uten bilde?

“Du får tro hva du vil.”

Roger er hardere i tonefallet.

“Fakta er du ble advart av en av våre moderatorer om ulovlig billedbruk,
noe du begynte å kverulere på, og senere fant vi at du operer er med
falsk profil i tillegg.”

Redaksjonen tok side for Trym, og de hadde alt på det tørre. Hun
forteller hendelsesforløpet til Else og får mindre støtte enn forventet.
Gaysir er et privat foretakende og kan gjøre hva de vil. Hun må finne
seg i dommen og la saken ligge, mener vennene, ikke ta konflikten til
homopolitikere og andre aktivister. Hun må lide i stillhet, for
eiendomsrett gjør det sosiale nærværet på Gaysir til et privilegium hun
blir nektet på grunn av brudd på billedreglene og arroganse. Det
stemmer, alt sammen, og hun visste med seg selv slettingen trolig var
best for alle. Hvis hun samler bitene fra en ulykkelig forelskelse, roer
kroppen og får kontroll på hjerteklapp fra redselen som løper løpsk vil
det være mulig å finne veien fra uføret.

“Når du oppretter en profil hos oss krysser du for at du har lest og
forstått regelverket. Advarsler bør være overflødige. Når du i tillegg
ikke viser snev av ydmykhet i etterkant får jeg følelse av at du ikke
har særlig respekt for regler. Slike brukere trenger vi ikke.”

Skrev Roger fra Gaysir.

Sort på hvit. Hun gikk til google og lager en anonym epostadresse,
bruker denne til å opprette en profil og bekrefter Roger har rett i alt
han skriver. Svaret til Roger resignerer, ville ingenting tilstå, og hun
forklarer i tydelig lag hva hun tenker om sensur av legitim
meningsutveksling på forumet. Hun omskriver hans siste setning for å
avslutte poenget: “Slike fora trenger vi ikke.” Det var ikke mer å si.
Situasjonen sto bom fast. Skjelven tenker hun hva det vil si, hvordan
hele tilværelsen blir endret ved å være forvist til usynlig på Gaysir.
Hun forstår ennå ikke hvor sint Trym er, ikke før to dager senere vet
hun. Trym ser sporene i den anonyme epostadressen og sjanser, han
sletter den siste falske identiteten nyopprettet og uten profilbilde.

Utraderingen er komplett.

IRL
===

  But I, being poor, have only my dreams. I have spread my dreams under
  your feet; tread softly, because you tread on my dreams.

  — William Butler Yeats

21
--

Benjamin spurte lenge før helgen om hun ville sette av fredagen.
Bokdagene i Oslo sentrum fylte byen med arrangementer hun yndet å
besøke. Humørfylt elsket begge å bryne seg mot den pompøse og
selvopptatte bokbransjen. De var frekke og arrogante, strålte i egen
bedrevitersk fortreffelighet og brukte overmotet unge studenter har til
å snike inn på gratisarrangementer for kultureliten. Største bragd, og
de beste minner, var det ene året Benjamin overtalte henne og de sjanset
å snike seg inn på hageselskapet til forlagshuset Aschehoug. De passerte
sikkerhetssjekken ved porten problemfritt. Hun sto avsondret hele
kvelden og snakket med en barnebokforfatter for ikke å bli oppdaget.
Benjamin tok større sjanser. Han oppsøkte kulturredaktøren i
Morgenbladet og pratet om tyske filosofer. Året etter unnlot hun å dra
fordi det regnet. Begge fikk jobber og måtte bruke tiden på andre ting
enn ulovligheter. Oslos åpne arrangementer ga nok underholdning.

Planen var de skulle stille lag på Buckleys Pub der de kickstartet
Bokdagene ved å holde krimquiz i lansering av siste bok fra Unni
Lindell. Hun likte quiz, begge kunne briljere i egne emner. Benjamin var
spesielt god på obskur kunnskap, de klassiske emnene. Hun likte træsj.

“Jeg ville vært god i quiz hvis jeg husket navn, titler og årstall.”

Lo hun halvt på fleip.

Benjamin flirte. All god humor bygger på en dæsj sannhet sier klisjeen.
Sannheten var hun tilførte quizlaget mest når det var behov for å
verifisere mellom to mulige svar. Hun dømte sjelden feil.

“Du styrker laget, du leser faktisk krim.”

Sa hun.

“Vi kan være fem. Hvem skal vi spørre?”

Med Rachel var de tre, laget trengte to forsterkninger med kunnskap i
krim som likte quiz. Hun avviste raskt tanken på Else, venninnen hatet
quiz.

“Jeg og Rachel er lagfyll, vi trenger noen bra.”

“Hvem leser norsk litteratur egentlig?”

Sa hun.

“Du vet hva jeg mener.”

Benjamin tygde på problemet noen dager. I løpet av uken overtalte han
konkurrentene på Litteraturhusets månedlige kulturquiz til å gjeste
laget deres. Marthe og Tove fra Sherlockskolen tapte de for på en dårlig
dag.

“Du blir eneste hane i kurven.”

Begge visste at erfaringsmessig var det best med spredning både i alder
og kjønn for å dekke allmennfeltet. Ensidige sammensatte lag vinner
sjeldnere.

“Jeg tror vi har et godt lag, vi leser krim og du regner som mann.”

Sa Benjamin.

Fredagen i ilden var alle omforent med oppgaven. Hun og Rachel stilte
som fyllmasse på laget, de skulle supplere og gi mulige svar uten
påståelig å trumfe gjennom feil, undergrave det riktige svaret, eller
ødelegge prosessen i diskusjonen. Alle antok ryggraden i laget var
Benjamin, Marthe og Tove—lagets meritterte quizzere. De kunne vinne hvis
de balanserte innsatsen riktig og hadde flaks på spørsmålene.

Arbeidsfordelingen fungerte fra første spørsmål. Hun drakk en pils og
ventet på fjerde spørsmål da hun skimtet en gjenkjennelig silhuett bak
en søyle ved inngangen. Benjamin var opptatt med å skrive. Tove
resonnerte med Marte og Rachel om hvorfor det ikke var et annet svar.
Dark Horse sto i en klynge ved døren, omgitt av to venninner stirret de
innover i lokalet etter ledige plasser. Dypt innkapslet i tunge
skinnsofaer langs veggen kunne hun ikke flytte seg når Dark Horse
stirret rett på.

Venninneflokken på gulvet virket forvirret, de var ikke forberedt på at
Bucksley Pub arrangerte krimquiz selv om begivenheten ble annonsert i
programmet til Bokdagene. Dessuten ville minst en av dem gå umiddelbart.

Hun beskuet rotet og lyttet samtidig til neste spørsmål. En potpurri med
kjenningsmelodier fra tevekrimserier ga alle muligheter til å bidra. De
to første melodiene var lett gjenkjennelig. Kvinnene fra Sherlockskolen
bekreftet svarene hun skrev på blokken. Tredje og fjerde melodi måtte
hun grave dypere etter for å huske. Melodien lød kjent. Quizmaster
avrundet lyden når hun stirret etter Dark Horse som krysset gaten
utenfor på vei bort.

“Tredje er Rockford Files, fjerde er jeg litt usikker—hva med noe fra
syttitallet, Matlock?”

“Ja til Rockford Files.”

Sa Tove, klart tevekyndig og helt klart en konkurrent på tema
tevekultur.

Hun lot seg imponere av begge jentene fra Sherlockskolen. Tove listet
teveserier, gamle slagere fra sytti- og åttitallet. Borettslaget der hun
vokste opp fikk kabelteve tidlig, og hun gledet seg over å være spesielt
god på usanernes brokete kulturlandskap. Tove og Marthe var dyktigere.
Sherlockskolen imponerte på Litteraturhuset i filmkultur. Hun følte de
var tøffe konkurrenter.

“Sherlockskolen er kjempegode på træsj.”

Sa hun.

Hun ville identifisere kjenningsmelodien på den fjerde teveserien. Dark
Horse hadde vist seg i levende live på Bucksley Pub. Hun lurte på hvor
Dark Horse og venninnene gikk, hadde lagt merke til at de ikke gikk i
retning So i Arbeidergaten, eller utestedet Elsker som de lesbiske delte
med homsene. Venninnene virket heterofile og de kunne boltre seg over
hele Oslo.

Tove lot kommentaren henge og spilte fornærmet.

“Træsj. Træsj. Hva er det egentlig?”

“Vi kan heller si Sherlockskolen har høy grad av kultursensitivitet.”

De smilte. Laget var på glid. Marthe brøyt inn og foreslo en nyere
teveserie. Hun gjenkjente *Jack and the Fatman* som riktig svar og
visste laget ville få full pott på lydsporene.

Benjamin førte svarene direkte inn. De ville ligge høyt. Eneste usikre
var om de ville ende på pallen, eller toppe og vinne bokpremien fra
forlaget. De vant. Hun drakk tre øl på Bucksley Pub. Triumferende
besluttet laget å gå til Bohemen i andre enden av gaten. Quizzere møtte
fredager på Vålerengapuben. Tove skulle dit åkkesom for å treffe
kjæresten. Rachel strålte og følte for å dilte uten mål og mening.
Bokpremiene de fikk var signert av forfatteren. Hun ville gi eksemplaret
i julegave til moren. Vel utenfor ymter Rachel straks et ønske om å dra
hjem å spise pizza og se film, men glemmer like raskt. Benjamin ville
avslutte kvelden mer triumferende. Han krever offentlig feiring blant
quizkolleger og løp etter Tove. På Bohemen ville de sole seg i glansen
av å være seierherrer i faglig dyst med forlagsfolk.

“Proffene er best.”

Strålte Benjamin. Det var de.

Quizkolleger på Bohemen skulle høre hvordan de møtte uredd i en quiz for
forlagsfolket. De hadde ingenting å frykte i den høyst merkelige og
konkurransefrie hverdagen til spesialister. Hun smilte, likte å feire,
bar bokpremien stolt i venstre hånd og pilsen i den høyre. Siste slurk
før hun tuslet etter de andre mot Bohemen gikk på styrten, og i det hun
tar igjen Rachel roper en kjent stemme navnet hennes.

Rachel strente etter Benjamin opp trappen til puben. Hun vinket til dem
og stoppet utenfor So, kjellerklubben under Bohemen. Ingebjørg sto på
gaten og røykte. De kjente hverandre siden ti år tilbake. Hun gikk
sjelden ut, men kontakten med Ingebjørg ville hun ikke miste. En sjelden
gang gikk de sammen til byens lesbiske begivenheter. Hun likte
Ingebjørg. De oppdaget henne i løp etter Benjamin og ropte på gaten. Hun
stoppet forfjamset. Ingebjørg lo når hun fortalte hvordan de vant
krimquiz og fikk signerte bøker.

“Du er full jo.”

Sa Ingebjørg.

“Nei. Bare glad. Jeg trengte virkelig å vinne. Være ute i dag.”

“Jeg tror jeg aldri har sett deg skikkelig full.”

Hun kikket nysgjerrig inn i lukevinduene til So for å se om det var liv
der. Klokken var kun kvart over ni. Ingebjørg sto og røykte utenfor So
med venninner. Hun møtte blikkene og tok alle i hånden.

“Kommer du senere?”

Spurte Ingebjørg.

“Ja, jeg tenkte det.”

Nå som hun visste Ingebjørg var på So, ganske sikkert. Hun trengte å
feste, flørte og kjenne duften av ekte kvinner av kjøtt og blod.
Venninnen var god å lene seg mot. Quizlaget var fine, men heterofile og
det fantes et skille som aldri forsvant, en usynlig demarkasjonslinje
hun ikke kunne krysset. Ingebjørg var en lesbisk kvinne hun ikke fordrev
med hodeløs flørt, de ble venner. Ti år senere festet de gjerne, men det
var lenge siden. Hun ville hekte på dem nede i So-kjelleren. Restene av
quizlaget kunne feire uten henne i etasjen over.

“Jeg kommer straks jeg har sagt farvel til Benjamin. Bestill en øl til
meg!”

Det ville bli hennes fjerde.

Hun hang med Ingebjørgs selskap på røykehjørnet. Hun sa hei, håndhilste
og konsentrerte seg for å huske navnet til Ingebjørgs venninne. Hun la
merke til at kvinnen var utrent røyker, fingrene holdt sigaretten stivt,
typisk festrøyker. Den gule panneluggen hang ned og skjulte halve
ansiktet før kvinnen flippet håret og festet luggen bak øret.

“Hei.”

Ingebjørg fikk rett, hun var lettere i hodet, mildt beruset og glad
etter en hyggelig kveld på byen.

Smilet til panneluggen avvæpnet.

“Har du et enavn på Gaysir?”

“Player.”

Svaret kom raskt, det var kun Ingebjørg som mente det var et frekt
spørsmål å stille en hun ikke kjente. Hun reagerte på enavnet, tilla
profilen en masse egenskaper hun normalt ville forbeholdt de aller mest
aktive på Gaysir, men kunne ikke huske å ha sett Player før.

Slik hun følte var status mest presserende å formidle. Hun ville ha alt
åpent.

“Jeg er slettet på Gaysir, utradert, fjernet for alltid.”

Sa hun.

Gaysir brukte hun hovedsakelig til å utveksle meldinger til venner,
Ingebjørg og den tidligere samboeren. Alt tok slutt et par dager
tidligere etter redaksjonen kastet henne ut for brudd på bilderegler og
arroganse.

Den siste karakteristikken slang hun med fordi det var sant. Ingen grunn
til å utelate denne mindre attraktive egenskap. Hun mente alt sagt.
Slettet for brudd på reglene i Gaysir, en bagatellovertredelse hun ikke
var alene om. Korreksjonen kom tilfeldigvis minutter etter hun skrev et
kritisk meningsinnlegg på forumet. Hun lettet, ølet virket for hun
raljerte fritt fra det triste kapittelet Gaysir viste seg å være.
Ingebjørg pattet på sigaretten og tenkte kanskje på invitasjon om
vennskap på Gaysir med påfølgende avslag. Hun forklarte hvorfor,
nærværet skulle være dynamisk, dersom hun hektet på venner var det
vanskeligere å slette profilen og bygge ny hvis det føltes nødvendig. Om
Ingebjørg forsto logikken var en annen sak. Frihet til å slette profilen
sto sentralt i hvorfor hun valgte bort vennekoplinger på profilen.
Ingebjørg reflekterte muligens over hvordan ironisk nok var det
redaksjonen som slettet profilen. Hvis hun hadde hektet på venner ville
de kanskje tenkt annerledes og gitt en advarsel først.

“Du er sammen med Ingebjørg?”

“Nei, vi er ferdig deitet vi.”

Sa Player.

Hun tenkte hvor trist for Ingebjørg. Panneluggen var søt. Bortsett fra
det grelle enavnet med en klar advarsel om å trå forsiktig.

“Sånne som deg på Gaysir, proffe spillere må jeg passe meg for.
Nettsjekking er simpelthen ikke for alle.”

“Og hvordan er vi?”

“Hæ?”

Hun visste ikke hva hun skulle svare, for det sa seg selv, gjorde det
ikke? En flørt på Gaysir måtte være en person med spesielle egenskaper,
en med evne til å knytte seg til mange og på et nivå der det var lett å
bryte. Proffene kunne gå fra seng til seng uten besvær. Noen klarte det.
Ikke hun. De gikk til hovedtavlen på Gaysir og plukket hva de ville for
en midlertidig tilfredsstillelse og var kuttet fra et annet stoff. Med
et navn sendte profilen signaler, slengte de på en tekst i status gikk
ting raskere. Ordet kåt for noe må skje straks, eller avslappet for å
takke for sist. Ingenting signaliserte å være tilstede og åpen for
forslag som ordet avslappet i status.

Hun merket den direkte tonen på Gaysir, de med målrettet fokus på
seksuell tilfredsstillelse. Sikker var hun ikke, men profilene uten
personlig bilder, anonymisert, med enkle, åpne og direkte preferanser om
sextreff vekslet mellom ordene kåt, kosete og avslappet i status. Hun
ville testet teorien, turte ikke, men mente hun forsto kodene som
signaliserte faser i de rendyrkede og direkte initiativene.

“Jeg er aldri på Gaysir, og enavnet er fordi jeg spiller fotball.”

Hun husket ikke Player, forklaringen på enavnet var logisk sammenstilt
fra ungdomsidrett. Hypotetiske spekulasjoner kom ingen vei. Ingebjørg
stumpet røyken og de gikk inn. Straks hun fikk sagt farvel til Benjamin
kom hun tilbake. Hun kjøpte fem Carlsberg, en til seg selv og drikke til
de andre rundt bordet. Det skulle bli flere utover natten. Like før hun
presset seg ned i sofaen, mellom Ingebjørg og Player smilte blondinen
med pannelugg, grep tak i låret og visket i øret.

“Jeg er ikke farlig. Fullstendig harmløs.”

22
--

Rasjonelt var det ingen grunn til å opprette en profil på Gaysir. Hun
var i et perfekt forhold, del av et pent par, presentable og fine i
tøyet. To voksne. Hun kunne ikke huske om profilen ble opprettet etter
hun slo opp første gang, eller mellom andre og tredje gang de gjorde
slutt. Det kunne være det samme. Nysgjerrighet drev henne, en pirrende
undring der den store usikkerheten var om hun kompromisset i livet og
nøyde seg med mindre. Kraften i spørsmålet trakk mot hovedtavlen der
livene fylt med tomme løfter spredde seg kaotisk.

Surfet Dark Horse bort nettene i søket etter noe ubestemmelig? Hadde
noen av de gamle favorittene skiftet status siden sist, og kanskje
sivilstand? Hvor lenge siden søkte hun på objektene for begjær?

En stund.

Hun fant kjæreste blant folk, og visste det var flaks. Spesielt fordi de
var svært forskjellige. Hvis ikke for krimquiz og faktum laget vant, hun
strålte og i godt humør drakk en øl for mye, ville hun ikke møtt
Ingebjørg i selskap med den lyse panneluggen. Livets tilfeldigheter var
årsaken til at de traff hverandre punktum.

Hun og kjæresten *Player!*

Betenkt var hun.

Deiten de avtalte forsøkte hun å avlyse to ganger. Player lot henne ikke
stikke, ga henne ingen unnskyldninger. Hun ble forkjølet etter den
første natten. Sykdommen var av det mildeste slaget og gikk over etter
to dager. Dager etter første natt skulle de møte på deit, og enhver
antydning om å skyve det framover avviste Player. Tidspunktet for deiten
lå tidlig på ettermiddagen og gjorde det mulig å gå videre til andre
avtaler, hvilket hun trodde var hensikten til Player. Derfor bestemte
hun de skulle spise på restaurant *Mat & Mer* i gangavstand fra
leiligheten. Slik ville det være lettvint å gå hjem skulle møte bli en
katastrofe.

Nesedrypp i snørr de første dagene samme uke utelukket sex, sjarme,
enhver sjanse for å gjenta første natt. Forkjølelsen forsvant like fort
som den kom. Hun hadde ment en utsettelse virket fornuftig og tekstet
meldinger som foreslo nye datoer. Player slapp ikke tak, hun ble frisk
og gikk på jobb. Player ville holde avtalen deres.

Helst ville hun utsette det hele til over helgen, til hun ble friskere
og tryggere på valget. Hun ville utforske om det fantes håp om å
realisere det umulige. Lørdag var det stor fest på Rockefeller, og alle
av betydning ville være der, eller gjemme seg i So-kjelleren. Hun ville
være begge steder, danse og spane, fri til å flyte fritt i folkehavet.
Hun måtte ut i verden, en håndfull datoer i året var det mulig å se Dark
Horse, Fab, Fullklaff og alle av betydning på samme sted. Byens lesber,
tilreisende, likesinnede tuslet på de samme arenaene førstkommende
lørdag. Nachspielet på So ville garantert være fylt av kvinner. Gjentok
hun impulsene fra natten med Player ville det være ni måneder til Skeive
Dager der hun fikk sjanser igjen; sjanser til å spane i galskap ville gå
tapt. Lot hun natten stå alene ville det være greit også.

Var Player verdt sjansene hun skuslet bort ved og deite. Ville forholdet
vare?

Lærte hun panneluggen å kjenne ville forholdet selvsagt ha en sjanse, de
måtte finne hva fungerte og bygge en framtid fra virke. Player fikk fram
smilet. Håp fantes midt oppi miseren.

De avtalte møte rett etter jobb. Restauranten lå i nabolaget og
forkjølelsen forsvant like raskt som den kom. Deiten var on.

Hun behøvde ikke gå. Valget å slippe løs byttet, kople bort kroken og la
fisken svømme til neste dam fantes. Rollespillet forvirret. Tanken hun
var fisken på kroken trykket. Dagen derpå knyttet Player skolisser og
gjorde seg klar til å forlate leiligheten. Ingen sa et ord. Det var ikke
snakk om å utveksle mobiltelefonnummer. Fullt påkledd og de manglet en
avtale, det var en reell mulighet å kutte kontakten før de startet. Hun
håpet Ingebjørg ville ha hva hun trengte for å ta kontakt, og betraktet
gjesten som sto og knotet med skjerfet, luen i den andre hånden. Hun
hadde brukt morgenen til å ligge og stirre på ryggen til Player, rumpen
og det bølgete blonde håret som dekket ansiktet. Hun studerte gjesten
som vridde seg i smerte fra en Carlsberg for mye, samtidig som Player
beklaget at familien holdt selskap og det var nødvendig å gå dit.

Timer før besteg hun ryggen og strøk den med begge hender før hun løftet
en utslått gjest med en jevn omgang støt bakfra. Hun brukte hele hånden.
Player krøp i vellyst rundt på madrassen og grep etter tak bakover til
hun fant det. Halvt oppreist på knærne rant det en stri strøm ned lårene
når hun med andre hånd fortsatt å stryke ryggen til Player og dytte
kroppen ned i sengen.

Smerten neste morgen kunne like gjerne være frustrasjon over at natten
var for kort, som en kraftig baksmell.

“Jeg må gå, skal til foreldrene mine til middag, hele familien er der.”

“Jeg skjønner.”

Sa hun, og mente det.

“Du må gå.”

Hun drakk, og dagen var blant de sjeldne etter en tur på byen. Hun følte
seg merkbart opplagt. Hun levde, svevde, gledet seg over tilværelsen.
Nypult var hun kjedelig forutsigbar.

Alkohol fungerte, øl var smøringsmidlet for å lette stemningen. Hun
drakk for fruktene den brautende og selvsikre tonen høstet. Sju til ti
år borte fra folk, uteliv, igjen forvist til barkrakker. Hun følte
draget. Tøffhet i fasade. Alderen ga grunn til uro, men det aller beste
med Player var alder. De var jevnaldrende. Player måtte sies å være den
definitivt peneste trofé hun kunne skryte på seg. Veltrent
fotballspiller. Hun fikk ikke sove, studerte kroppen inntil og alt var
fint.

“Absolutt nydelig.”

Hun møtte ikke krav om å gjenta kvelden neste morgen. Dersom de lot alt
skli og behandlet hendelsen som et one-night-stand ville det være greit.
Synd å skusle bort, men greit. De behøvde aldri å se hverandre.

Ti år i forhold med perspektiv til pensjonistalderen tok fra henne
lysten til flere sprang inn i et livsløp. Fokus var hva de fant i
natten. Kropp, nærhet, synsinntrykk, sex i rendyrket ekstase. Endorfiner
på høygir. Tvilen var der på dansegulvet når panneluggen svirret rundt
dansepålen på So. Player var en overraskelse, festrøyker og fylte ikke
kriteriene sånn sett. Timingen føltes feil. Begge levde gjennom traumer
i året som gikk. Hennes tanker var en kø i kaotiske inntrykk, mens
Player ventet på en nøkkel og bodde på sofaen til søsteren. Faktum hun
ble gjort kjent med tidlig på kvelden og lot ligge til stengetid. Alt
var opp til henne, hun eide en seng for dem.

Tilværelsen hopet seg opp på Gaysir der hun ble blokkert og forvist.
Følelsen var hun måtte rydde før det ville være mulig å friskmelde seg
til et nytt forhold. Hun holdt igjen.

Fristelsen lokket. Hun lot det være opp til Player.

Hun ville klare alt livet hadde og tilby—sikkert. Player sa ingenting,
satt på gulvet i gangen og knyttet skolisser. Overnattingsgjesten med
lys pannelugg ville forlate leiligheten og før hun lot det skje blunket
hun. Stillingskrig fikk være det, for hun kunne ikke annet enn å spørre
om de skulle utveksle telefonnummer.

“Selvfølgelig.”

En postitblokk på kjøkkenbenken ble det første Player grep, og omkranset
et hjerte noterte Player mobiltelefonnummeret. Hun tastet tallene og
sendte en melding for å gi det egne nummeret straks Player forlot
leiligheten for å småløpe mot Sagene bussholdeplass. Neste dag sendte
hun en nøytral takkemelding og var ferdig med hele episoden. Mandag
ringte Player og spikret en avtale hun skulle bruke tre dager på å
forsøke unngå. Hun hadde ventet i måneder på dagen, siste sjanse å ståke
Dark Horse lå og lurte. Forvist fra kybersporet måtte det tregere
alternativet, virkeligheten trå til. Lørdag var sjansen hun ventet å se
Dark Horse *in real life* – IRL – uten filter.

Deit to dager før var usikkert og farlig, og det føltes som å besøke
andre profiler på hovedtavlen uten å være interessert. Identifisert på
besøksloggen kunne hun sende gale signaler. Hun gikk på deit likevel.
Risikoen for å bli fanget vekket redselen, angst for å ekskludere
drømmeprofilen. Hun visste ikke om det var vilje, eller
rasjonalitet—fornuften hamret alltid inn når det sto på som verst.
Fornuften beskyttet fra å forelske seg i illusjoner og usannsynlige
kjærlighetshistorier utspilt i hode. Festen hun ventet på var høyst
sannsynlig ingenting å satse mot. Hun ville se tomme blikk og kjenne
traumene fra å bli ignorert. Dark Horse blokkerte henne og ville være på
So, høyreist, ruvende. Rasjonelle argumenter for hva hun visste var ekte
løp side om side med tilfredshet i galskap. Organisert og samlet visste
hun lengselen etter Dark Horse var drevet av et indre behov etter å
bryte med hverdagen, feste følelser og energi til hva hun aldri kunne
slippe løs—flykt fra virkelighet. Panneluggen ertet endorfinene.

Reddet fra impulsene av et smil som skinte gjennom lyse lokker.

Langs et av bordene på So sto hun og kjæresten Player, bortenfor vaket
Dark Horse.

Baren strakk seg langstrakt gjennom lokalet tettpakket med feststemte
gjester. Avstanden mellom henne og Dark Horse var såpass at de bare
kunne se toppene av hodene glimte. Player gjorde sporadiske dykk inn i
kroppen, to dager nyforelsket, og hun kunne verken flytte på seg eller
snu ryggen. Aller minst kunne hun spankulere bort til Dark Horse for å
slå av en prat. Det var ikke tid og sted til å rydde i en forferdelig
situasjon, erkjenne de gjenkjente hverandre og bryte den tredje muren
mellom kyberlivet og realitet. Beklage oppførselen, og kanskje å få
tilgivelse. En siste gang, før de gikk hver for seg og hun kunne puste
fritt igjen. Hun var klar til å bygge et liv. De var i gang, alt gikk på
skinner, luksusen å lære å kjenne neste erfaring startet før hun var
klar. Panneluggen fikk fram smilet.

Dark Horse gikk rundt midnatt, før de fleste, lokalet kokte til
dansemusikk. Bak musikkanlegget sto verten og tok ønsker. Dark Horse
kjente djen, fikk ønsket oppfylt, en gammel slaget fra Spice Girls
spilte luften ut av lokalet. Gjestene surret til refrenget da en rak og
høy kvinne med rødlig hår sveivet forbi ryggen på vei bort. Hun følte
hjernen spinne rundt egen akse til første verset.

 | *If you want my future forget my past,*
 | *If you wanna get with me better make it fast.*
 | *Now don’t go wasting my precious time,*
 | *Get your act together we could be just fine.*

Det var sent og tolkninger følte hun var irrelevant. Hun gikk hjem med
Player. Tid og realisme løp fra dem. Hun valgte livet, en framtid, den
korteste veien til lykke. Overnattinger, ferier og små gaver. Elskov og
middager. Tosomhet framfor det uvirkelige og todimensjonale livet på
nettet.

De var lykkelig, trodde hun. Sammen hånd i hånd nedover stien langs
Akerselva gikk hun og Player til Grünerløkka, de spiste søndagsbrunsj på
The Nighthawk Diner. Søstrene var med på konsert og hilste høflig. Hun
traff svigerforeldrene, og hun fortalte alt om broren og skilsmissen,
private hemmeligheter. De reiste til London med søstrene på jentetur.
Hun kjøpte sminke, adidassko og fråtset på Kentucky Fried Chicken, mens
de voksne drakk kaffe på Starbuck ved siden av.

Player luktet godt.

Hun elsket panneluggen, smilet og de små brystene. De var lykkelige
trodde hun, og en god stund var de lykkelig. Begge kastet seg
tilsynelatende inn i forholdet med begge føtter, totalt oppslukt av den
andre, fokusert på tosomhet til metamorfosen startet kraften som skulle
gjøre dem til par. De sov sjelden alene. Endring startet smerten,
forutsigbart, som alltid skremmende. Ingen vil erkjenne de har
forbedringspunkter. I livet finnes forskjellene du kan leve med og
forskjellene du ikke kan leve med. Hun og Player snakket forbi
hverandre. Snakket de ikke overhode gjorde det mest skade. Sår stank
blod, begge ble såret, trakk seg inn i skallet. Alder spilte kanskje
inn, motstand mot alt annerledes, grunnleggende ting som fokus og grad
av konformitet i forhold. Sedvane. De var like gamle, og ulike.

Endringene synes å angripe identiteten, ikke sjelden var det hun som sto
i skuddlinjen. Såret sto hun skolerett for et fjes halvt snudd bort for
å skjule skuffelsen over en impuls—ingenting annet. Største overtramp
hun gjorde var å avbryte Player midt i en historie, raskt slo
bommerangen med anklager og trusler om å isolere fenomenet.

“Jeg må skjerme meg. Møte venner uten deg.”

Sår. Sint. Skuffet.

Hverdag var å vaske, mat, henge av seg klær, se på teve, ligge i sofaen
og stirre ut av vinduet, tømme vesken, betale regninger i nettbank,
kysse kjæresten i nakken når hun gikk andre veien. Hun satt i sofaen og
Player spurte om ting, og spørsmålene virket mer som kritikk.

“Hva gjorde du før det fantes datamaskiner?”

Før dette var bøker og studier, teve med en kanal, musikk på vinylalbum
og leker i skogen utenfor huset. Barndom, uskyld, masse læring, det
samme. Livet forandret seg ikke meget.

Hun ville se, prøve om det var mulig å opprette en profil i enavnet ute
å bli slettet. Sjekke om Trym husket og ville slå hardt ned på forsøket
å bli del av hovedtavlen. Måneder borte fra Gaysir forandret ingenting.
Hun levde, sneik seg inn, ærlig med profilen hun brukte før Trym slettet
alle spor. Fullt enavn og bilde fra tiden før hun lot seg representere
med et profilbilde av Barack Obama.

Planen var å unngå forum, delta i det stille, skli unna øynene til
sjefsutvikler Trym i redaksjonen. Daglig hang han foran hovedtavlen.
Farligste tidsrom var de korte minuttene sist påloggende hang på
forsiden, synlig for alle. Sneik hun seg inn på ukurante tider, unngikk
forum og reduserte tiden innlogget burde strategien fungere. Alt hun
trengte var en epost, anonymisert og eksklusivt til bruk for
medlemskapet på Gaysir. Enavnet gjenoppsto på to minutter. Hun
gjenkjente straks de grønne og oransje prikkene. Hovedtavlen var lik,
men føltes større fordi hun ikke rakk å begrense søket. Kvinner i alle
aldre hang på utstilling når blikket skannet nedover skjermen etter
kjente fjes. Første uke fikk hun tre meldinger fra venner som visste om
konfrontasjonen med Trym. Villgutt spurte hva skjedde, og hun erkjente
inntreden på Gaysir var uautorisert, et spinkelt forsøk på lavmælt
oppstandelse for å utveksle meldinger med venner. Hun ville ikke vise
seg på forum. Skulle de diskutere politikk måtte det skje over et glass
på byen. Villgutt forsto, sendte et blunk og ønsket lykke til med
opprøret.

Pikkihagen sendte en oppfordring om å holde ro, unngå å bli gal.

“Produktkritikk av Gaysir må være lov i debattforum.”

Sa hun.

“Ja, men du må ikke være så gal.”

“Hva mener du?”

Pikkihagen brukte irriterende ironi på alt og gjorde narr fordi han
visste det var en anelse galskap i livene deres. Han, fordi Pikkihagen
gjorde alt for å tråkke over grenser. Hun, fordi hun gjorde alt for å
holde seg innenfor grenser. Forumshorene oppdaget tilbakekomsten selv om
hun ikke skrev innlegg. Hun rettet tommelen. Snart ramlet kommentarer
ned i meldingsboksen. Illusjonen at Trym var uvitende forsvant raskt,
den første uken. Eget enavn, det hun alltid brukte kom til liv på
Gaysir. Hovedtavlen var ikke stor. Trym jobbet daglig i redaksjonen,
profesjonelt, mens hun var en gjest. Uinvitert sådan. En gjest som sneik
rundt i det skjulte etter det ble bestemt av andre kundeforholdet skulle
opphøre. Hun ble slettet fra Gaysir for brudd på billedreglene og
arroganse.

Første gang pålogget merket hun frøken Grønn på hovedtavlen og
profilbilde av en seierssikker Obama. Hun tenkte vemodig på hvordan
redaksjonen slettet henne for å bruke et likedan bilde. Frøken Grønn
skrev aldri på forum, hang bare på hovedtavlen og var lett å spotte på
grunn av det brede smilet til Obama. Topplisten hun skrev de hatske
ordene mot og kalte et politisk korrekt makkverk hang på menylisten.
Flere måneder løp før Trym avsluttet eksperimentet med egen toppliste.
Gamle kjente fylte de øverste plassene, men flere var borte, deriblant
Villgutt listet langt nede. Han debatterte mindre. Ronda var helt borte.
Topplisten besto av Groggy, Løken og Babytoy. Diskusjonen var like
tåpelig som før, Trym justerte småting fra den første listen. Grunnen
til at Trøndersodd ikke listet lenger var flere minus enn plusser.
Profilen med nettminustomler slapp rangering slik Trym definerte
designet.

Lenger ned på hovedtavlen fant hun Dark Horse hengende—innlogget.

Hun søkte på Fab. Profilbilde på tjueniåringen i tettsittende hvit
singlet var slettet. Fab dato for sist pålogget viste et par måneder
tilbake. Hun motsto fristelsen å klikke innover for å sjekke om
profilteksten var endret. Søket rettet seg heller mot Dark Horse. Hennes
nye profil kunne umulig være blokkert. Hun respekterte grensen mens hun
var ved sans og samling. Blikket, smilet og hårfarge virket kjent. Hun
følte uro, et savn fordi hun aldri fikk snakke eller beklage—etter eller
annet. Vemod over tapte sjanser, hun fikk ingen. Dumme tanker og drømmer
om hva som kunne vært hjemsøkte henne.

Alt var som før, bortsett fra alt sto bra til i livet, kjæresten satte
smilet på fjeset, glede og lengsler i en definert og kjent framtid.

Opprettelsen av profil på Gaysir ble proklamert høylytt hjemme i stuen,
hun gikk inn på profilen til Player og forlangte oppdatert status. Begge
registrerte *Har kjæreste* og hun sendte et par blunk i slengen. Players
profiltekst var uforandret de siste to årene, og kjæresten logget på
hovedtavlen høyest en gang i måneden. Hun bombarderte Player med blunk,
brukte kvoten det var lov å sende hver dag og fylte med små
hverdagsmeldinger. Frekvensen på antall besøk økte, men Player brukte
dager mellom besøkene, og hun sendte tosifret antall blunk før
kjærlighetserklæringene ble lest. Hun roet kontakten, føltes for dumt å
sende blunk når kjæresten satt to meter bortenfor og leste
treningsmagasiner.

Blunk til Player fjernet ikke farene, gjorde ikke skyldfølelsen mindre.

Foran datamaskinen skrev hun fritt, tjattet med venner og søkte status
på gamle fasinasjoner. Fab, Dark Horse, Fullklaff og inne på Gaysir
startet hun samtaler med den tidligere samboeren. Vennskapet overlevde
bruddet, avklart og ferdig, begge i nye forhold lot på fleip blunk
sprite opp tilværelsen. *Har jeg sjans?-* og *Du er drømmetypen*-blunk
satte tonen. Player lo med.

De traff hverandre, ingen følte seg truet.

Hun kunne våknet, dratt kroppen vekk fra selvbedraget og ringt
gamlekjæresten på telefon, tjattet på google og aldri kastet blikket inn
på Gaysir. Heller sneik hun seg inn som en tjuv i natten, søkte profiler
som ingenting ville ha med henne å gjøre. Studerte bildene og leste de
skjulte signalene i grønne og oransje prikker, tempostyrt, impulser lik
morsetegn som proklamerte en intensjon. Puls med budskap hun ikke
forsto. Knatringen på tastaturet gikk i jevne strømmer, og med
foruroligende rytme, per hele time logget hun på Gaysir og gjorde søket,
sjekket hyppig pålogging og studerte profilbilder i tilfelle endringer.
Korte dykk i dyp konsentrasjon, før hun trakk blikket tilbake til stuen
med Player, syttenkanaler på teve, platespiller, peis og et tykt
Vosseteppe Player hadde fått til jul som varmet føttene.

Plutselig rykket Player i skjermen og ristet løs den bærbare
datamaskinen fra bordet. Instinktivt grep hun tak når kjæresten tilranet
seg et glimt fra skjermen. Oppblåst i stort format hang og dinglet
bildet av Dark Horse. Hjertet banket. Blikket møtte Player, taus stirret
de på hverandre. Forfjamset samlet hun tankene og snakket til kjæresten
i den mest hverdagslige tone. Kontrollert hjertebank. Det handlet om å
holde en fasade for å lure alle i rommet.

“Hun har skiftet profilbilde.”

De sa ingenting mer. Sjokkert, avslørt, alt sammen? Kort var hun
uoppmerksom kjæresten. Intuitivt dro Player i skjermen og trakk blikket
bort fra prikkene. Hun fortalte om utkastelsen fra Gaysir første kveld
de møttes, galskapen som ridde henne, små minner i forlegenhet. Hun
fortalte alt de første dagene, ærlig blottstilte hun alt, til et punkt.

Forsto Player dybden i følelsene, og forsto hun dybden i følelsene til
Player. Hun måtte snu seg bort for ikke å se smerten i ansiktet.

“Hva vil du?”

Søndag formiddag ville hun sitte for seg selv og slappe rolig av foran
datamaskinen. Player trippet rastløs og trengte å gå ut i solen.
Nettleseren lukket raskt, og det var bare dem hjemme.

Hun kunne sagt elskede, omfavnet panneluggen, kysset henne på tinningen
og klemt kjæresten hardt. Øm tatt hendene og strøket fingrene kjærlig,
noe som skjedde sjeldnere. Rasjonelt forsto begge de kunne være
fantastiske sammen. Ingen ønsket å slå opp med den andre. Umodne gikk de
inn i forholdet, godt voksne, brent og uvillige til å sverge evig
troskap. De snakket om å være uforberedt på samlivet, bruddene de kom
fra og de ulike målsetningene for framtiden. Drømmen til Player om et
hus ved sjøen var ikke på hennes topphundre liste hvis penger var ingen
hinder. Når leste Player sist en bok? Ulikhetene stirret dem i senk.
Løftet om en framtid strakk seg to måneder fram, og når hun nevnte
ferieplaner i Costa de la Luz et halvår fram i tiden ble alt stille.

| “Vil du gå en tur, gå ned til løkka?”

23
--

Hun elsket klisjeer. De har i seg at de er sannferdig, og nettopp derfor
er de klisjeer. Gjentakelser til besvær, prakteksempel på mennesket som
aldri lærer av feil. Definisjonen på klisje. Hun aksepterte ikke
premisset at en klisje er tom for betydning, for det var sannhet i
utsagnet.

“Ikke så galt at det ikke er godt for noe.”

Sa hun.

Sjokket var ikke tilværelsen som singel, det var hverdagen.

Kommentert nærvær etter hun ble singel og igjen hang på hovedtavlen
store deler av fritiden. Lakonisk observasjon. Bruddet var sivilisert, i
betydning det ikke ble diskutert. Begge parter forventet og aksepterte
det uunngåelige mellom to så forskjellige. En uke etter bruddet forførte
Player en venninne på fotballtrening, to uker etter visste hun Player
var forelsket i en annen enn henne og mindre enn fire uker gikk det før
status på profilen til Player lyste *Har kjæreste*. Sjalusi gjorde det
umulig for dem å snakke sammen, men tenkte hun, hvorfor skulle
relasjonen være forskjellig bare fordi kjærlighetsforholdet tok slutt.
Hun klarte ikke befri seg fra tanken dersom de hadde snakket sammen,
vært ærlige og valgt hverandre ville tosomhet være mulig. De var fortapt
i mangel på samlivsterapi.

“Nåvel, da var det bare et mellomforhold.”

Sa hun til Rachel.

Hun rasjonaliserte, valgte ikke å tvære foran venninnen, ødelegge
stemningen når Rachel svevde euforisk fordi Benjamin flyttet inn
leiligheten på Majorstua.

“Takket være deg, du foreslo ham. Husker du?”

“Nei.”

Løy hun.

Hemmeligheten slapp det lykkelige paret straks Player absorberte den
ledige fritiden. Rachel tenkte idylliske parmiddagsselskaper var tingen.
Hun tenkte det var en dårlig idé, og Benjamin var ombord. Det var
vanskelig å si hvem var mest skuffet når Player dro, hun eller Rachel?

Ved å se livet i en serie monogame forhold ville endestasjonen være en
spasertur på perrongen før neste tog igjen brakte håp om tosomhet. Mest
irriterende var ikke å vite hvor lenge hun måtte stå på perrongen å
vente. Erfaringsmessig tok det alt fra året til måneder. For henne aldri
en kort uke. Hun var ikke skrudd på den måten, kunne ikke snu ryggen
uten vemod og anger. Hun ville kjempe. Sårt innså hun Player følte ting
annerledes. Tom, uten noe å gi skrapte hun følelsesregister for å finne
argumenter for å snu, satt taus en uke. En uke ubesluttsomhet var nok.
Player satt aldri stille. Tiden rant altfor fort og hun hang igjen i det
gamle. Bergtatt av det romantiske ideal, låst til løfter avvist, kjempet
og slått ned med hard hånd.

Ubehaget var det vanlige, redselen for aleneskap, følelsene av å ha
tapt, avvist og skyld for hun lot det skli såpass lenge, halvveis,
målløs uten forpliktelser og løfter. Alt usagt plaget nattesøvnen.
Tankene gikk til den lyse panneluggen og et par kraftige overarmer før
svette og kjønn kastet henne inn i fortvilelse.

Gjensynet med Gaysir viste seg å være spasertur på kjent territorier.
Hun valgte å holde til seg til en profil, intensjonen var falske
identiteter skulle hvile i fred og aldri gjenoppstå. Besøk på profilene
hun fulgte måtte begrenses, hun ville legge fra seg minst mulig spor.
Savnet etter falske identiteter var lettere å håndtere i denne omgang
fordi det siste hun lærte før redaksjonen slettet tilgangene var de
siste føringene i dagbøker var til på en samleside alle kunne nå fra de
globale menyene. Hun behøvde aldri å blottstille besøksloggen. Dark
Horse skrev få innlegg månedene hun var borte, men i sommervarmen kom
det ti korte poster.

Hun leste alt Dark Horse skrev og behøvde ikke åpne profilen. Hun slapp
avslørende logger.

Fab var borte, manglet profilbilde og logget på sporadisk, ikke overhode
den siste måneden. Hun ventet et par dager i uvisse, opprettet ett falsk
enavn og gjorde besøket, bare for å slette den falske profilen like
raskt som det oppsto. Status, dagbøker og profiltekst var uendret. Alt
lå inaktiv.

“Godt for henne.”

Hun kjente en anelse misunnelse, og innerst inne visste hun hvor det
gikk. Hun utviklet en daglig rutine å sjekke før endringer i statusen
til Player. Naboen og broren fortalte villig vekk oppdateringer fra
Facebook hver gang hun møtte dem, små drypp, hint om at profilen endret
seg.

“Ja, jeg vet. Player er i et forhold.”

Sa hun.

Naboen dro fram en iphone og hentet opp skjermen for å vise sladder om
Player ikke var en skrøne. Hun skulte mot skjermen for å se kjæresten.
Spilte kjølig interessert. Medlidenhet kunne hun være foruten.

“Sånn er det.”

Hun nektet å registrere seg på Facebook, var overbevist om det var en
fase og brukere ville flykte derfra i hopetall. Hun ventet i årevis. Hun
ventet fortsatt.

Når Facebook kom hadde hun allerede forlatte profiler på SixDegrees,
Friendster og den norske varianten Nettbyen som til tross for nesten
full dekning i markedet kollapset på ett år når Facebook slo rot. Cirka
firehundre kontakter på SixDegrees viste hun tok sosiale medier seriøst
helt i starten. En kort periode konkurrerte hun med Benjamin om hvem som
klarte flest koblinger, helt til vennen dro på seminar i utlandet og kom
hjem med hundreåtti visittkort. Han gruset vennelisten på to korte
dager. SixDegrees ville bevise teorien i novellen til ungarer Frigyes
Karithy som hevdet alle mennesker i verden er seks ledd fra å kjenne
hverandre. Konseptet slo rot, spesielt i USA der noen oppfant
selskapsleken *Six Degrees of Kevin Bacon*. Nasjonens mest
hardtarbeidende B-skuespiller dannet navet i en lek der vinneren var hen
med færrest ledd. Hun hørte om spillet i 1996 etter den nittien år gamle
bestemoren til en venn fra New York vant med bare fire koblinger til
Kevin Bacon. Familieselskapet kåret den eldre jødiske kvinnen som jobbet
bak scenen på et Broadway teater på 1920-tallet der hun traff Archie
Leach, bedre kjent som skuespiller Cary Grant. Filmstjernen ble senere
gift med Dyan Cannon, som spilte mot Burt Reynolds, en av medspillerne
til Kevin Bacon i klassikeren *Deliverance* fra 1972. Publikum ville
alltid huske sjokket i filmens kultscene der en mann blir voldtatt.

En kort stund var hun fasinert av tanken på nettet som nettverksbygger,
men da Facebook økte i oppslutning var hun ferdig og sto imot presset å
registrere en profil. Moren registrerte profil for å holde kontakt med
barnebarnet, hun nektet. Broren sutret fordi hun ikke forsto hvor godt
Facebook fungerte. Hun mislikte tanken på nettverk som omfavnet massene,
dekket alle tenkelige behov. Monopoler. Hun trodde simpelthen ikke på
makkverket.

Framtiden for nettverk er nisjer, og hun registrerte profiler på
Linkedin og Gaysir. Sosiale medier for nettverk viet jobb og kjærlighet.
Dette var spesifikke nettverk rettet mot et formål.

Bekymret registrerte hun verden ble avhengig sosiale nettverk. Vår
sårbarhet dukket under overflaten. Hun var betenkt fordi selskapene
driftet etter kommersielle kriterier. Usanere beklaget mangel på moral
når dominerende selskap ofret ytringsfrihet og anonymitet for å entre
det kinesiske markedet. Hun fikk en vekker når Wikileaks kom på bankenes
sanksjonslister og ble nektet overføringer av penger. Over natten ble
det vanskeligere for givere å donere penger. Ingen protesterte. Tiltak
for kriminelle, diktatorer og terrorister tatt i bruk for å kneble
kraften i en fullt lovlig organisasjon myntet på åpenhet. VISA,
Mastercard, American Express og vanlige banker nektet å ta imot penger
til Wikileaks, en organisasjon dedikert ytringsfrihet, og utviklingen
gikk bare i gal retning. Maktovergrepet manglet sidestykke i historien,
men ingen protesterte. Bankene og Wikileaks var ettersigende private
næringer med rett til å gjøre som de vil og betale konsekvensene.
Politikere sklir unna, toer sine hender og later som om det ikke var de
som ga bankene instruksjoner. Makt utøvd på bakrommet.

Hun gjenkjente følelsen da Trym slettet enavnet, den siste falske
identiteten og for å understreke poenget fulgte opp dagene etter for å
slette den nyopprettede identiteten hun skapte i opprør. Hun ville
protestere, klage, løfte saken til politikere med krav om å rette opp
uretten. Hennes behov på beskyttelse hadde homopolitiske konsekvenser.
Stilt ovenfor valget var hun aldri i tvil hva enhver fornuftig skapning
finner mest urimelig. De unge ville i valget mellom å bli stoppet på
gaten i identitetskontroll, eller få profilen og alle vennene slettet
fra Facebook foretrekke det første.

Venninnene hørte utleggingene, parallellargumentasjon, og ristet på
hode. De kom alltid tilbake til samme problem. Livet i en kultur med
opphøyd tro på fri konkurranse ville aldri sette grenser. Slike tanker
var umoderne.

“Gaysir kan kaste ut hvem de vil, de er privateid.”

Ingen protesterte på urett. Private overgrep langt verre enn de statlige
redsler latent innebygd i uniformer var det ingen vilje å skrike om. Hun
følte seg ikke høy i hatten, singel forlatt av kjæresten og med
begrenset bekjentskapsflokk blant de lesbiske—flere av dem ekskjærester.
Slettet fra Gaysir, to minutter etter et ganske tilforlatelig innlegg på
forum. Påskuddet for å slette profilen var brudd på billedreglene fordi
bildet av usanernes president i fullt sømmelige former smykket enavnet.
Hun vekket antipatier i redaksjonen og det var ingen vei tilbake. Hun
måtte leve fri fra Gaysir, for de levde bra uten henne. Alle hadde en
profil på det sosiale nettverket. Ti år var over og Gaysir dominerte
alene. Redaksjonen var den eneste profesjonelle med fokus på homofile og
lesbiske nyheter. Hovedtavlen lå åpen for spaning, blunk, invitasjoner
og orienteringer for alle bortsett fra henne.

Maktovergrep. Utvilsomt.

Hun satt stille. Fikk kjæreste, levde og festet som om ingenting
skjedde. Aksepterte redselen som hang over hodet på alle homofile i
landet Norge.

Pikkihagen kom på besøk og lagde salat. Hun gjorde innkjøpene og plukket
ingredienser fra en liste han beordret henne å kjøpe. Dyrest var en
eksklusiv olivenolje. Han skulle lage maten og hun stilte med kjøkken.
Matlaging var hans domene.

“Du er der inne nå, ikke sant?”

“Jeg sneik meg inn og venter halvt å bli slettet av Trym hver dag.”

“Du må ikke være så gæren.”

Sa Pikkihagen.

Hun sprakk da han koketterte i gammel stil. Humor avvæpnet situasjon til
et punkt, deretter måtte vennen se alvoret i situasjonen. Politiet i
Bodø arresterte en pedofil for overgrep i en sak med over to dusin barn
og intim kontakt oppimot seksti stykker. Han voldtok noen av barna etter
først å ha lurt dem til hotellrom med løgner. Jaktmarkedet var Gaysir.
Politiet begjærte og fikk utlevert logger over mannens aktiviteter.
Profiler merket *Utlandet* kontaktet henne ukentlig, og kom med
umotiverte tilnærminger. Mange lovpriste profilen selv om besøksloggen
viste de aldri tok tid til å klikke inn dit. Spor på prostitusjon,
omsetting av narkotika og bondefangeri, rubrikkannonser flagget ved
stedsangivelse. Gaysir var en markedsplass for ytelser lokalisert *in
natura*. Norske ekspatrierer storkoste seg, humpet og sto i, enkelte mot
betaling hvis hun forsto kodene rett. Hun var utestengt.

“Hun!”

Pikkihagen var ikke dum, han forsto det politiske, sosiale og
stigmatiske. Forsto han at hun var redd?

Profilbilde han brukte var et i utvalget på tretti i galleriet, han sto
ved hagegrillen i bar overkropp og med en pils i ene hånden.
Soldagssmilet til gladgutten lyste mot verden. Alt lå innenfor det
sømmelige, han fulgte billedregelene til Trym og tilfredsstilte
kroppsdyrkelsen mange av de homofile mennene søkte. Trym poserte selv
med et lignende bilde.

“Jeg søker ingenting og alt.”

Skrev Pikkihagen.

Sjarme og kunnskap, frekk, snerten slo fritt rundt og hun var ikke i
humør. Alt han disket opp av lattermilde bortforklaringer ville ikke
fjerne angsten i rommet. Hun innså han var like redd, Saunatreff,
crusing i badstuer der fremmede blikk avkler siste plagg som dekker hud.
Håndkledet rundt livet, eller en trang tanga. Pikkihagen kjente Oslos
bakgården, parker og lekte med grenser. Han krysset grensene flerfoldige
ganger. Frekkhetens nådegave hjalp han å plukke fruktene, menn tjueår
begge veier med kapasitet til å skjemme ham bort på den ene eller andre
måten.

Mens de spiste salat fortalte Pikkihagen om ferieplanene sine og
komplikasjonene med en tjueognoeåring han likte som ikke forsto hvorfor
det var nødvendig for ham å dra på ferie med en sexpartner fra Berlin.
Reiseplanen lå i kalenderen planlagt i måneder, billetten betalt.
Hvordan det var mulig å la være å dra var mysteriet for vennen.

“Drar du til Nice med en sekstiåring ute å vite hvordan han har det
der.”

Sa hun.

“Jeg har sett leiligheten hans i Berlin.”

“Da så. Og hva sier typen.”

Ekkelt spørsmål på ett sårt tema tidlig i forholdet. Pikkihagen var
aldri eksklusiv. Han balanserte det åpne forholdet med en ung mann og
avtalte synder planlagt gjennom året, en hårfin line i fråtseri. Paret
sto foran den første virkelige testen på det åpne forholdet, langt
tidligere enn det var rimelig å forvente. Ingenting av dette var hennes
sak.

Gaysir tilhørte den homofile og lesbiske offentligheten, slik de skeive
ønsket å framstå i miljøet. Pikkihagen var tilgjengelig for alt, men
presiserte på profilteksten andre single.

“Er lei av å være krydder for lykkelige par.”

Skrev Pikkihagen.

Han proklamerte i store bokstaver: “Singelpride.”

Skrullefakter hjalp. De avvæpnet og han slapp unna de vanskelige
spørsmålene, hvor mye krydder var det i posen.

Hun kunne latt han skli, gitt faen og spist salaten.

Situasjonen var at hun klødde i fingrene etter å føle seg fri på forum.
Hun fordelte tomler, topplisten var fjernet. Trådene kuttet ventetiden
og gjorde livet på hovedbordet levelig. Hun kokte, var genuint sint på
vennens nonsjalante behandling av problemet.

“Hva slags bilde hadde du egentlig?”

Sa han.

Hun beskrev bildet av Barack Obama, akkurat som hun hadde fortalt før.
Blikket gikk til det samme glassaktige uttrykket, en blanding av
forundring og ettertenksom bekymring. Hun spredde frykt, redselen hang
på dem og Pikkihagen ville skli bort. Han og alle homoer som levde i
nettsamfunnet. Ubehaget han følte var lik klisset på offentlig toaletter
du helst vil unngå komme nær. Han ville ikke vite om konfrontasjonen med
Trym.

De hygget seg. Pikkihagen skiftet tema. Vinen smakte godt. Hun sjekket
etiketten for å huske til neste gang hun gikk forbi en taxfreebutikk.

“Jeg er aksjonær.”

Sa han.

“Er du? Kasper og Håkon også.”

Hvor mange venner med fortid i Syklubben kjente hun? En betimelig tanke.
Hun ville fortelle historien til dem alle.

Gaysir lå åpen, ingenting var privat. Profilene frontet hvem de ville
være, det private selvet i et lukket miljø, hjemmeværende, trygge på
begjæret. Her var de homofile og lesbiske med en tilpasset identitet.
Han forsto forskjellen. Pikkihagen var singel. Lettere tilforlatelig
søkte han andre singel, men takket aldri nei til garnityr i hverdagen.
Han skrev det bare ikke på Gaysir, gikk ikke lenger enn å skrive
dementi. Et minne bak øret for de som glemte profilteksten skjuler mer.

“Jeg kan være bedre, og jeg kan være verre.”

Det skrevne fornektet. En *shakespearsk* fleskepostei fra en homsegutt
på kaninføde og perspektiv om å leve mange år til tenkte hun: “\ *The
lady doth protest too much, methinks.*\ ” Hans lettere sider, skrullen,
humor og vri var godt kjent. Han gløttet inn i et hemmelig liv. Gaysir
var stedet med en identifiserbar, tilgjengelig og åpen profil. Saunaer
inviterte til mer.

Enavnet endret han for flere år siden til noe tilforlatelig, selv brukte
hun det gamle for å erte. Brystkassen bar pilsen i hånden, gladgutt med
grenser klart definert innenfor hva det offentlige Gaysir aksepterte.
Ungdomssynder fikk bli mellom dem.

Pikkihagen var på vei til Frankrike på ferie med en elsker.

“Hvor traff du ham?”

“Sauna. Selvsagt.”

Samtaler mellom venner om livets underfundigheter, håp og ambisjoner,
hun elsket de spennende krumspringene til vennen. Livet hans var ikke
kjedelig, humøret sjelden dårlig. Han ble med i Syklubben en kort stund.
Fellesskapet avvek fra hans primære instinkter. Organisert sjekking lå
utenfor hans interesse. Han flyttet fra byen etter å ha truffet en mann
i København. Aksjen han kjøpte på stiftelsesmøte lå brakk i en skuff.
Middagsselskapet paret holdt en gang hun besøkte dem endte i en merkelig
skrytebrudulje om hvem levde mest, tok for seg av hva livet hadde å
tilby. Pikkihagen og den danske partneren praktiserte frihet i byens
sexorgier. Den bebrillede kulturredaktøren, dansken oppnådde mye hos
kjendisdyrkerne. Vennen tok hjem mer fordi han var tolv år yngre og
smilte til alt og alle.

“De kaller det sjarme.”

Sa Pikkihagen.

Gliset avslørte en perlerad med hvite tenner. Hun forsto ikke
attraksjonen med å bli tatt bakfra på et offentlig toalett av en vilt
fremmed. Hans muligheter var så mangt. Forholdet til dansken tok slutt
og vennen flyttet tilbake. Før han dro fra Oslo rakk Pikkihagen å være
med på stiftelsesmøte til Gaysir og i likhet med de andre homsene i
Syklubben betalte han andelen og ble aksjonær.

“Ga du virkelig penger til en datatjener.”

Han trakk på skuldrene. Pikkihagen ga en beskjeden sum, det personlig i
oppmøte og miksing i pausene trakk han til møte til å begynne med.
Tilfeldigheter skulle ha det til at dagen var starten,
generalforsamlingen til bisnissen. Pikkihagens anliggende handlet om den
kjekkeste fyren han tok hjem, premien til den mest utadgående og
sjarmerende. Normal kropp, brede skuldrer, høyde, han trakk til seg
oppmerksomhet ved å smile selvsikkert. Syklubbens tid gikk mot slutten,
saunaer fulgte, jaktmarkeder åpnet seg der nettet løy.

“Noen foretrekker personlig møte.”

“Ikke du.”

Sa hun. “Du foretrekker å vinne sex som om det er en konkurranse—hver
gang.”

Klinisk forpliktet før avtaler finner sted har ingen sjansespill over
seg. Nettet utfordrer ikke Pikkihagen før han ser objektet i veien for
tilfredsstillelse. Han tenner på å vinne. Når homsene i Oslo samlet seg
var det en kampsport, offentlig vant de som tok premien med hjem.
Taperen satt alene og debatterte på forum. Seieren sødme tente
Pikkihagen. Gaysir leverte daglig, lettvint, uten konkurranse for de som
søkte. Største utfordringen var å unngå falske identiteter, gjennomskue
løgnere og finne egne grenser, hvilket var det vanskeligste fordi
Pikkihagen levde etter regelen det finnes ingen grenser. Grensene på
Gaysir var for syns skyld, koketteri, fasade. Profilen lå åpen for alle
å lese.

“Gaysir skiller ut klinten fra hveten raskere.”

Sa hun.

Argumentene for å bruke nettdeiting framfor sauna, eller de
tradisjonelle samlingene—slik de gjorde det i Syklubbens tidlige dager.
Vennen rynket på nesen.

“Hvorfor vil du absolutt skille ut?”

Sa Pikkihagen.

Såpass enkelt, et spørsmål der hun for en kort stund tenkte forundret
det finnes ikke dumme spørsmål, bare dumme svar.

“For å skille ut hvem du liker.”

“Tenner på alle jeg.”

Sa han.

Og mente det.

Argumentet synes å være når han ville ha sex var det langt på vei
irrelevant hvem han møtte. Hans behov sto i høysete. Mannen, motparten,
objektet for begjær i øyeblikket var et instrument for
tilfredsstillelse.

“Har ingen vekket entusiasme, bortsett fra Dogtooth?”

Hun brukte kodenavn på Dark Horse til venner for å skjerme
opplevelsen—-identiteten, og katalysator for et forstyrret følelsesliv,
kvinnen hun ståket med nådeløs målrettethet.

“Bare Insane, men det er et blindspor.”

“Hvorfor?”

Hun antiflørtet med en artikulert og morsom kvinne fra Sandnes, kunne
ikke forestille meldingene de utvekslet hver dag skulle bli mer enn
artig anekdoter fra sjekkemarkedet.

“Sandnes. Jeg har forsøkt avstandsforhold før. De fungerer aldri.”

Sa hun.

Sannheten var hun ville for enhver pris unngå kompromiss. Player var et
ni måneder langt kompromiss, urettferdig for alle. Hun ville strekke seg
langt og kalle det et feilgrep. Et vidunderlig feilgrep. Hun ville
avslutte Dark Horse, ryddig sette seg ned og snakke, avklare en gang for
alle om de kunne snakke om tiden som var.

“Jeg sa nei til deit forrige helg fordi jeg var bortreist. Insane
besøkte venninner i Oslo.”

Kvinnen dro på byen, flørtet, og straks helgen var over spurte hun
Insane om det ble noe på henne? Svaret var nei, de lo, og hun forsto
straks kvinnen bak enavnet satt strandfast på Sørlandet og flørtet i
alle retninger. Insane var ikke eksklusiv.

“Kan du ha sex med Insane?”

Sa Pikkihagen.

“Ja.”

Vennene kokte alltid ting til den ene sannheten, punktet av betydning
for sansene. Hun svarte ja, og visste ikke, for profilbilde var uklart,
grenset til ubrukelig og de verken utvekslet telefonnummer, etternavn,
eller bilde. Kun fornavn. De var som kjentfolk å regne, og utvekslet alt
om hverandre, lokaliserte treffpunkter for felles bekjente. Vurderingen
av Insane gjorde hun utfra hvordan de skrev, bakgrunnen var ikke ulik,
begge med brokete studieløp og skiftende interesser. Basert på
språkføring kunne hun sikkert si kvinnen var en person hun tente på
seksuelt. Insane var en attraktiv kvinne.

Pikkihagen traff blink.

Insane fikk henne til å akseptere en coladeit, og alt kvinnen behøvde å
gjøre var å sette seg på rutebussen til Oslo.

Hun måtte gjøre et kompromiss først, ofre drømmen om det perfekt.
Invitere. Det siste leddet satt langt inne, og hun var paralysert.

“Jeg er nok ikke klar. Jeg vil ikke slippe tak i et ønske om mer.”

Sa hun.

Hun tenkte mye på Dark Horse. Andre hun var i kontakt med på hovedtavlen
var underordnet.

“Smak og behag.”

“Smaken er som baken, den er deeeeiiiiiiilig.”

Sa Pikkihagen.

Han trakk på det siste ordet for å understreke poenget, skrulle, tilføre
samtalen det vanlige alvoret og samtidig halvveis avvise alt med et
smil. Pikkihagen tok sjelden en samtale seriøst. Hun strakk gjerne
poengene, avsluttet barnsligheter og gjentakende mas; denne gangen ville
hun slippe han løs. Tanker om tilfeldigheter og hvordan de bestemmer til
slutt lå brakk. Salaten tok slutt og vinen måtte holde siden det var
ukedag og begge skulle på jobb neste morgen.

Kvelden fløt sakte, hun satt på sofaen og stirret på klokken, talte
timer før hun kunne legge seg. Hovedtavlen hang uforandret i timer, hun
ble en av flere gjengangere. Britt75 fikk en ny venn og var oppe i ni
stykker på kort tid. Status skiftet mellom avslappet, kåt og kosete.
Stemningsleie ble etterfulgt av nye venner.

Hun fikk en invitasjon til vennskap. En profil hun besøkte kort for å få
overblikk, ingen spesielt, hun leste teksten og gikk videre. Likte hun
profilbilde og kriteriene var oppfylt tok hun gjerne en samtale, gjorde
forsiktige tilnærminger og åpnet for vennskap. Vennene visste hun ville
være fri. Hvis hun unngikk forpliktelser var det enklere å slette
profilen. Derfor var det nytteløst å sende en invitasjon, for dem ville
hun avvise. Invitert av en fremmed fristet enda mindre. Hun lurte
hvorfor overhode noen kom på tanken å be om vennekobling når det ikke
var kontakt. Undringen vekket spørsmål. Hun tenkte umiddelbart på
Britt75. Ni venner på kort tid. Hun spekulerte om det var sextreff, en
til to i uken, til sammen ni overmodne frukter plukket av en kvinne rett
på sak. Hun var vant til blunk, meldinger og uskyldige spørsmål. Korte
tilnærminger hun raskt avsluttet. Knappe og utilnærmelige svar, bortsett
fra Insane som flørtet på alle frekvenser fra første stund og bød på god
underholdning pakket i smilefjes. Forespørselen fra en fremmed om å bli
venn på Gaysir overrasket. Alle teorier hun måtte ha om Britt75 måtte
revurderes.Britt75 smykket med ni venner var en av profilene som vekket
nysgjerrighet. Tiden hun tilbragte på hovedtavlen hadde oppsamlingen av
venner skjedd på rekordtid. Nysgjerrig skrev hun til kvinnen som blunket
og ble avvist fordi profilen var for tøff.

“Ni venner. Jeg er imponert. Hvordan klarer du det?”

Sa hun.

“De bare spør.”

“Har du truffet dem alle i virkeligheten?”

Rett på sak spurte hun Britt75 om det var ekte liv bak tegnene. Hun
ville vite hvordan en ekspert gikk fram og vite om mistanken stemte. Hun
fikk det ikke til å stemme alle ni var sextreff. Britt75 tilhørte skaren
profiler mistenkt å være skalkeskjul for Dark Horse. Riktig alder,
høyde, tilhørende riktig bydel og anonymisert, mangel på bilde og
personlig informasjon. Kvinnen hadde ni venner og var definitivt en
mistenkt. Først og fremst fordi hun blunket to ganger, andre gang
utvekslet de vennlige ord og hun inviterte til deit trygg på at de aldri
ville møte hverandre. Britt75 avslo forutsigbart. De visste hvor de
hadde hverandre. Stillingskrig mellom profilen som søkte kos og hennes
ønske om deit, tro på kjærlighet og nattesøvn. Hun besøkte profilen
trygg, uredd, som hun besøkte vennene. Besøksloggen skremte ikke fordi
forholdet deres var avklart. Nysgjerrigpetra kunne hun forsvare, og
profilen behøvde ikke svare. Det hadde heller ikke Britt75 tenkt å
gjøre. Hun måtte unnskylde spørsmålene.

“Jeg er nabokjerringa som følger med og lurer på hva alle gjør. Følger
deg og er kraftig imponert.”

Erkjente hun.

“Huff da.”

“Ja, nettopp. Hva kan jeg si: Sladder er livets krydder.”

“Snik… ☺”

Sa Britt75.

Sjarmert og lukket. Hun fikk ikke svar, ville aldri vite om de ni
vennene koblet til profilen etter langsom tjatting, eller om de faktisk
traff hverandre utenfor Gaysir sitt todimensjonale fengsel.

24
--

Hun syklet til galehuset for å treffe Else. Været var strålende på femte
dag og Else jobbet lange økter for å dekke mangel på personale i
sommerferien. Sykkelen rullet gjennom sykehusområdets flate og brede
veier, bilfrie, bilene sto i parkeringshuset der en helikopterplass på
toppen var det travleste punktet med landinger annenhver time.

Sykkelen parkerte hun utenfor lukket avdeling på Ullevål Sykehus der
Else jobbet som psykolog. De hadde tre slike på området, og venninnen
jobbet med de psykotiske i en gammel gul mursteinbygning fra
femtitallet.

Bygningen lå skjermet til forbi Kiwibutikken og inntil gjerde på
baksiden. Boligene på andre siden strekker seg langs dalføre oppover til
Tåsensenteret. Familier som sitter godt i det lever her skjermet for alt
som skjer på sykehuset og resten av verden. Raskest vei til Sagene gikk
gjennom en port i gjerde, via øde bakgater og tvers over lyskrysset ved
Voldsløkka. Hun kunne veien hjem, men ringte Else. Hjemme ventet sofa og
den bærbare datamaskinen. Gaysir fristet ikke, hun ville holde seg
utendørs lengst mulig. Foran mursteinbygningen lå plenen fredfylt, to
benker inntil en forminsket plass der sykehuset hadde montert en
basketballkurv. Friluftsområdet til pasientene sto tomt såpass sent på
ettermiddagen. Else møtte henne i døren og de satt en stund på benken
for å suge til seg sommerens siste solstråler. Været surnet fort, det
kjølige lyset skapte kraftbobler og de sitret under huden. Kroppen fylt
med liv fikk utløp for energi i hvert pedaltråkk gjennom byens gater.
Solbrun, slankere etter hun ble singel, bedre trent. Hun manglet
prosjekter for all denne innebygde energien og svippet innom venninnen.
Fellesferier satt Else og betjente dobbelt antall pasienter og fikk bare
støtte fra halve sykepleierstaben. Avbrekket var velkomment.

Hun var nysgjerrig på Elses kontor.

“Skal vi gå inn?”

Sa hun.

Else låste dem inn gjennom en sidedør og de endte i en trappeoppgang
innkapslet av ståltrådnett lik det som ble brukt i sykehusets gjerder.
Kontoret lå bak dem, i en lang korridor og bak en jerndør de låste seg
videre gjennom fra trappeoppgangen.

“Er det slik et galehus ser ut?”

Veggene var lyse, dørene farget, gulvet vinyl og bortsett fra de befant
seg i kjelleren virket alt som hvilken som helst korridor. Hun kjente
seg igjen, men hadde aldri vært der før. Jobben til Else var ukjent
territoriet og detaljer gjorde inntrykk. Hun slapp inn til et digert
kontor der det var en sofa og bord i ett hjørne. Skrivebordet fylte
resten av rommet. Else skulle skrive ferdig en journal før de kunne gå
for å spise.

“Avdelingen er rett over oss.”

Sa Else.

Hun satt dypt i stolen og kjente godt etter, stirret i taket og innbilte
seg låste celledører med matluke i etasjen over. Deretter studerte hun
Else bak pulten og forestilte seg pasientene i samme stol når de
fortalte historier. Else lyttet, ga små råd og stilte kontrollspørsmål,
satte diagnose og bestemte hvor lenge pasienten måtte gå på avdelingen
før de skulle videre til lokale institusjoner, eller hjem for å kjempe
videre.

“Dagen har vært et mareritt.”

Else hadde mye å si. Problemene hun opplevde på Gaysir ble bagateller
sammenlignet med skjebnene Else møtte hver dag.

Morgenmøte hadde startet en dårlig dag der sjefssykepleieren stilte
spørsmål med Elses diagnose og tvangsinnleggelse. Foruten den
profesjonelle dissingen og mangel på respekt avskydde Else tendensen til
å skyve vanskelige pasienter rundt omkring for å unngå jobben de var
ansatt til å gjøre. Diagnosen sto solid. Senere fant de glass på
pasientens rom til tross for advarsler og strenge ordre om å holde
kuttgjenstander borte fra selvskadende pasienter. Frustrasjonen økte
gjennom dagen og hun kom tidsnok til å overvære sluttrapporten og trøste
venninnen som brast i gråt, desperat etter å fortelle. Hun måtte vente
med egne frustrasjoner, de begynte å bli slitne, kjedelige historier fra
tilstanden å være, livets tyngdekraft.

Hun opprettet en falsk identitet. Målet var å sjekke status, lese
dagbokmeldinger skrevet i månedene hun var ute av sirkulasjon, bannlyst
fra Gaysir av redaksjonen. Hun ville se hva som hadde skjedd med Dark
Horse. Opprøret var også et pek til Trym, et skrik der hun nektet å
akseptere tyranni. Nettet tilhørte folket. Hun tilhørte det lesbiske
Oslo og de kunne ikke nekte henne adgang.

Uker etter galskapen startet kunne hun resignert erkjenne alt var seg
lik.

Hun ville vite hvorfor status var uendret, forklare hvorfor Dark Horse
var singel. Hun spekulerte i altfor høye krav, eller verre. En mistanke
hun idylliserte kvinnen bak enavnet dukket under overflaten. Var alt en
vrangforestilling? Dark Horse kunne være et av rovdyrene, de tok for seg
og kastet like lett. Snushøna red flanke, avlastet når Dark Horse hengte
i skyggene og følte behov for å skjule selvet. Avslappet skrev Snushøna
i statusfeltet. Hun var like overbevist enavnet var identisk med Dark
Horse som tidligere. Den falske identiteten ble værede; hun innså
behovet for sikkerhetsventilen. Singeltilværelsen skulle ikke gå til å
gjenskape galskapen fra tidligere, slippe løs utøylet begjær mot en
uskyld. Dark Horse blokkerte enavnet, satte en klar grense hun måtte
respektere. Hver dag utfordret henne. Sprekker i viljestyrken, hun var
følsom på tegn til svakhet. Mangel på impulskontroll preget livet.
Skamfølelsen satt, hun gikk tom for skam. Enavnene på hovedtavlen var de
samme, og hun ville bare titte. Nysgjerrig lå i lag utenpå kroppen og
søkte tilfredsstillelse. Blottstilt på besøksloggen ville opprøre, hun
visste bedre, søkte kontroll. Falsk profil var en praktisk innretning
rasjonaliserte hun, til beste for alle.

Livet fløt komfortabelt. Hun levde bra uten galskap. Vemodet gikk til
Player, kjæresten hun sviktet og som sviktet. Hun søkte profilen,
sjekket status, søkte spor etter forelskelse. Panneluggen deitet en
annen, hun visste såpass. Naboen kom på besøk, logget på Facebook for å
vise henne. De var Facebookvenner, koblet gjennom henne i en kanal hun
ikke kjente, eller ville vite om. Gaysir var arenaen hun tilhørte, og
Trym måtte tro om igjen hvis han trodde hun ville forsvinne. Dyret ville
ikke ha henne, men bildene lå der, stemninger fra livet og enhver status
hun ønsket å dele med offentligheten. Oppført på sivilstand satte hun
singel og ventet på at Player skrev det samme, slapp alle forestillinger
å skjerme følelser og skrev sannheten.

Player var forelsket i en annen. Hodestup og totalt involvert. Hun sto
igjen på perrongen, ledig, tilgjengelig for den som vil ha en kjæreste.
I det minste var hun tilgjengelig for deits, trodde hun. Fiaskoen fra
året før i mente skapte tvil. Hun sendte tre invitasjoner til deits og
det gikk galt. Hun slikket sårene, følte ingen panikk, nysgjerrig var
hun først og fremst over hva var annerledes.

Følte hun forskjellene.

Ganske snart visste hun ingenting endret seg, bortsett fra hun.

Forandringene skjedde på overflaten, minimale bølger over stormen
innvendig. Dark Horse skvulpet i bakgrunnen; blant alle på hovedtavlen
fristet profilen aller mest.

Savnet etter Player hang igjen, kroppen hjemsøkte drømmene og slapp ikke
tak, lengsel etter svette, fossefall, hun ville gripe etter håp og
stoppet. Realitetene var Player gikk rundt i Oslos gater. Hun ville ha
kjæresten tilbake, fikk hun sjansen tok hun panneluggen i armene på
minuttet. Gaysir var fokusadspredelse, forum redningen, distraksjoner i
tomrommet rett inn i stuen. Spriket splittet, forvirret, skapt for kaos.
Ubehag.

Hun kunne prate om opplevelsene i timer, og Else lot samtalen flyte
fritt. De bestilte middag på den lille fortausrestauranten på Sagene, og
det ga et avbrekk i praten som gikk ustanselig fra de forlot Ullevål
Sykehus.

“Jeg må finne ut hva jeg vil.”

Sa hun.

Else nikket.

“Gaysir er akkurat som før. Ingenting forandrer seg, bortsett fra at Fab
er borte.”

Den siste måneden nysingel gjorde henne mindre redd for blunk,
tekstmeldinger og tilnærminger. Hun var tøffere. Emosjonelt handlet
søkene om de gamle historiene, og det eneste nye søket gikk til Player
for å sjekke den tidligere kjærestens status. Noe var endret.

“Player står uendret på Gaysir, men er registrert i et forhold på
Facebook.”

Sa hun.

“Hvordan vet du det?”

“Naboen.”

Else himlet med øynene, visste alt om naboen, de snakket tett og sladder
sklei raskt ned.

“Jeg følger med på Dogtooth.”

Else visste hvem hun mente, dekknavnet på kvinneidealet på Gaysir.
Samtalen dreide alltid innom besettelsen som ga henne et tall på skalaen
for gal kontra normal.

Hovedtavlen listet profilbilder ettersom de entret, de lengst innlogget
lå lengst ned, de aller siste inne hang øverst. Hun vekslet mellom korte
og lange intervaller, hyppige søk og lengre studier på hovedtavlen.
Likte aldri å bli hengende for langt ned, eller verre, flytte til side
to. Rastløs gjorde hun hva som var mulig for å sikre plass øverst på
siden. Hun trykket jevnlig på tastaturet og leste på forum. Hun stirret
på prikkene og sporene til Snushøna, lurte ikke lenger, de var en. Med
et enkelt triks kunne hun få Dark Horse til å logge ut for kortere og
lengre perioder. Hun spanet, deretter gikk hun ut og inn igjen samtidig
som Dark Horse viste seg på hovedtavlen. Slik la bildene deres seg
inntil hverandre. Eksperimentet lyktes når Dark Horse logget av, flyktet
fra skjermen og avslørte stresset. De sto i veien for hverandre, de ble
fiender, årsaken til hvorfor livet ikke beveget seg fra A til Å. Livet
på hovedtavlen var en lek, henvendelser fikk ikke høflig og konsis
avvisning som før, hun svarte på spørsmål, responderte kort og presis,
holdt kontakten åpen og lukket i lengre perioder. Forsøkte å unngå å
oppmuntre, men ville ikke lukke samtalene for tidlig, miste muligheter.
Tilværelsen lettet, flere samtaler, spørsmål, liv og søkene på Dark
Horse ble mer levelig. Nummen grafsing i et følelsesliv sklidd til side
for hverdagen, mindre viktig vasket bort for aldri å dukke opp igjen.

Redselen for å forelske seg var mindre.

Til en lunken pasta og et glass hvit vin fortalte hun hvordan symptomene
var der, men hun måtte friskmeldes for det fantes ikke lenger noe håp om
følelsesmessig nirvana i søkene. Profilbildene var som før det sentrale
på hovedtavlen, samt de grønne og oransje prikkene. Hun søkte andre
profiler, og ville gjøre seg kjent for dem som en fornuftig og rasjonell
person.

Fullklaff hang i ny drakt, enavn og bosted beskrevet mindre spesifikt,
ikke knyttet til bydel. Synet av profilen åpnet en følelse av deja vu.
Mye var annerledes, og noe var kjent. Kun intuisjonen gjorde hun
gjenkjente minnene. Beskrivelsen på profilteksten hadde knapt endret
form, og som før ville en bokanbefaling åpne dører. Fremmede sto fritt
til å satse, og hadde hun ikke visst bedre var sjansene reell og hun
kunne spane på enavnet. Hun spekulerte om det var redelig å legge seg
etter kvinnen som var uvitende om at hun feilet før, når hun måtte feile
igjen. Selvsagt måtte hun anbefale en annen bok enn Merete Morken
Andersens roman *Hav av tid.* Lureriet å skifte enavn, tillatt av
redaksjonen å gjøre en gang i måneden falt ikke i smak, for hun het hva
hun het. Skifte navn skapte nye problemer, forvist og uglesett følte hun
Gaysir var vanskelig nok. Fullklaff hadde den perfekte profilen—bare
ikke for henne. Ny drakt gjorde ingen forskjell.

Fri fra gammelt åk markerte hun selvstendighet. Kanskje hun ville
hjelpe. Hun fant boken på biblioteket, en novellesamling oversatt fra
tysk, og derfor bar kvaliteten. Tyske forfattere var Europas nye
hemmelighet på totusentallet. Første kapittel overbeviste, det var alt
hun trengte for å skryte forfatteren opp i skyene. Hun gledet seg til å
lese resten. Fullklaff fikk en kort melding, knapt verdt å skrive.

“Jeg anbefaler Ferdinand von Schirach og hans bok *Forbrytelser*.”

Sa hun. Ivrig.

Samme dag svarte Fullklaff, og var glad for anbefalingen.

“Det høres ut som krim?”

“Nei, det er noveller. Meget bra.”

Bedyret hun.

Fjoråret ble aldri nevnt, ei heller bokanmeldelsen den gang. Hun trengte
ikke spørre om Fullklaffs meninger om Odd W. Surén. Hun forlot
språkområdet og anbefalte en tilfeldig valgt tysk novellesamling plukket
på biblioteket, og hun førte samtalen innenfor det lille rommet som var.
Nytt enavn, pusset fasade og fortiden lå skjult. Det var opp til andre
enn henne å ta minnene fram i lyset. Hun valgte å se framover, ingen
tilbakeblikk. Hun utviklet aldri relasjonen til den perfekte profilen,
for det var Dark Horse hun ertet. Stresset. Spanet på i flere kanaler.

Hun var singel. Ingenting, bortsett fra egne valg holdt tilbake. Alt
hadde endret seg mellom dem.

Gi en bokanbefaling! Deretter stikk derfra, gå videre og se aldri
bakover. Fullklaff tilhørte historie. Hun håpte Fullklaff likte boken,
det var alt.

Samtalene fløt friere, uten å løsne av den grunn. Britt75 var nesten en
venn å regne, og Insane flørtet uhemmet i en flora smilefjes og lett
latter. Tilværelsen var trygg. Gaysir var slik hun ønsket det i
øyeblikket. Hun var i sonen. Følte seg attraktiv, fri, utilnærmelig fra
å bli såret, samt åpen for forsøk. Redselen for aldri å forelske seg var
igjen større enn redselen for å forelske seg. Slik ville hun ha det, for
posisjonen var fylt med håp og framtid. Hun søkte noe, en person verdt
innsatsen, en å elske når blokkeringer hindret kjærlighet å flyte fritt.
Hun tenkte mye på Dark Horse. Hun ville være en som hørte klare
tilbakemeldinger og sluttet å plage andre med uønsket seksuell
oppmerksomhet. En ståker elsker ikke, de tillegger andre egenskaper og
gjør den til noe eget, uvirkelig, blottet for empati dytter de et
program. Dark Horse var katalysator for et ødelagt følelsesliv.
Urettferdig plaget hun kvinnen med en til to besøk fra en falsk
identitet i måneden, og langt verre var meldingstekster på status til
besvær. Vage tekster i status, umotivert, eller med en kobling til
endringer hun ikke hadde noe med. En dialog i ett hode. Skamfølelse over
å være egosentrisk og selvopptatt ridde kroppen i mørke. Hun lå i sengen
og slet i putevaret, fikk ikke sove, våknet hver morgen rundt klokken
fire, logget og søkte på Dark Horse, Snushøna og alle tenkelige og
utenkelige enavn. Objekter for begjær, eller mistenkte falsknerier,
potensielle alteregoer for den eneste av betydning. Mistenkt var alle
som dukket opp på besøksloggen og viste interesse. Menn gadd hun aldri
følge opp, lot enavnet skli sakte ned på loggene og forsvinne uhindret.
Kvinnene fulgte hun til profilforsiden, veid og vurdert. Brorparten av
dem falske, eller private, uten avslørende informasjon.

Hovedtavlen var som før.

Ettermiddagen hun ble blunket til for andre gang dukket mistenkt nummer
to opp på radaren. Profilen var ny, en ung kvinne eldre enn Dark Horse.
Brit75 manglet bilde, skrev kort på forsiden en banal forklaring hvorfor
tilstedeværelse på Gaysir og oppga status *singel*, ikkerøyker med høyde
lik Dark Horse. Interesse for hennes profil, det andre blunket etter hun
først avviste tilnærmingen sendte Brit75 til topps på listen over
mistenkte falske profiler. Kvinnen var tøff. Hun avviste Brit75 blankt,
forklarte høflig hvorfor profilen var gal.

Forklaringen ble akseptert, de steppet rundt hverandre, nikket, trakk
tornene i gjensidig respekt.

“Synd.”

“Synd.”

Måneder etter satt hun å bruke fritiden og spanet, sporadisk falt det
naturlig å snakke med kvinnen med den altfor tøffe profilen. Hun sluttet
aldri å fiske etter detaljer fra livet, og lurte om det var noen hun
kjente bedre bak den billedfrie personligheten. Samtalen deres sklir på
siden og ligger på overflater, og hun aner en redsel for å slippe løs.
Frykten stinker, hun føler Britt75 gjennomskuer sansene og tar
forhåndsregler. Spørsmålene trår forsiktig. Hun gis ingen foranledning
til å kutte dialogen, og finner ingen grunner til det. Følelsene
fokuserer en annen retning, og deres tidsfordriv fungerte. De gikk på
lavgir, blottet for emosjonell bagasje og forpliktelser. Hun visste hva
hun ville vite om Britt75, og en dag identifiserte hun kvinnen fra et
besøk på Linkedin, spurte og fikk en introduksjon til en baker med eget
konditori og forkjærlighet til søtsaker.

To blunk var historie, i alle fall ikke suspekt. Hun tenkte profiler
kunne vise interesse uten å være et alias for Dark Horse, og aksepterte
smigret invitasjonen til en samtale.

“Takk, takk. Vennlig. Jeg må dessverre fortsatt si nei til treff.”

Blunkene hjalp på humøret. Hun kjente varmen, hadde gladelig spekulert
og holdt i livet mistanken Dark Horse var Britt75, men aksepterte det
fantes en mulighet for noe annet. Bakeren var rett og slett hyggelig. De
forhandlet om premissene for et møte. Hun kjente en forsiktig
oppstemthet. De snakket, skrev og steppet rundt grøten.

“Vi kan alltids møte på en deit?”

Sa hun og lurte på om hun jaktet på suksessmålene.

“Nei, hehe.”

Smilene gikk rundt. De gjenkjente situasjonen, tenkte hun, og svaret var
tydelig. Mistenkte Britt75 ville like gjerne på deit som den skyldige.

“Nettopp, sist jeg gikk på blind deit fikk jeg som fortjent, en
nittenårig jusstudent med kviser. Det var en han. I vårt tilfelle var
jeg aldri bekymret.”

De traff et ærlig øyeblikk, gjenkjennelse innbilte hun seg.

“Hva er det med deg og deits?”

Sa Britt75.

“Jeg stoler ikke på kanalen. Bli kjent med folk er en luksus forbeholdt
virkeligheten, og alternativet vil aldri gjøre meg tilfreds.”

Et sukk føltes gjennom elektriske kanaler via øyet til en grop dypt i
kroppen. Britt75 forførte, ble forført. De kom ingensteds. Fortrolighet
gjennom gjensidig identifisering på Linkedin åpnet i det minste
samtalen. Stillingskrigen der de hadde klare fronter ble mykere. Hun
fikk invitasjon til 17-mai frokost med vennene, og takket nei fordi uken
var eksepsjonell travel og nasjonaldagen ble den første fridagen på
lenge. Møte bare mellom dem var ikke aktuelt. Hun ville ikke bøye av,
eller vise svakheter. De søkte ulike ting, og vennskap mellom slagene
fikk holde. Dark Horse brukte billedløse profiler til å spane, og hun
tittet på vennene til Britt75. Listen vokste og tæret i en rasjonell
romantiker, følte hun. Identifisert på Linkedin avsluttet uroen, og hun
gledet seg over å ha funnet en venn å prate med på hovedtavlen.

Hun gjenkjente andre mistenkte mer sannsynlig alteregoet som ridde
drømmene.

Bakeren gikk fri.

Snushøna var den første blant en rekke falske identiteter. Hun visste om
fire til sju andre profiler skapt for å forvirre, spionere og boltre seg
i forbudt flørt. Aldri før hadde hun reflektert over hva slags kvinne
hun elsket, bare akseptert mistankene, stirret på profiler og tenkt de
var skalkeskjul for Dark Horse alle sammen.

Hvem ville se bare henne, søke optimistisk og fryktløs på en stemme med
selvopptatt naivitet?

Mistenkte dukket opp på besøksloggen, og det var oftest der det startet.
Dark Horse lo. Gjengangere på besøksloggen gransket hun med kritiske
blikk, søkte sporene etter humor og sunne distraksjoner, verdige
erstatninger for den destruktive spiralen det var å skule til en kvinne
som aldri ville ha med henne å gjøre. Hun ble blokkert. Kontakt avvist,
forvist til et annet tidspunkt.

Aldri!

Refleksjonene fungerte dårlig, for hvis alle disse anonyme profilene hun
skjøv bort var Dark Horse burde spørsmålet i det minste tenkes gjennom:
“Hva slags kvinner falt hun for?” Hvis de verste mistanker mot den
stadig voksende vennelisten til Britt75 stemte ville kvinnen være et
sjarmerende rovdyr som sveipet gjennom de åpne trådene på hovedtavlen.
Hun fikk aldri et svar fra Britt75 hvor mange av vennene var personlige
treff, og brydde seg ikke heller. De sto trygt, for hun var ikke
interessert i sextreff og var såre fornøyd med å pludre bort kveldene
innimellom søkene på kvinnene som pirret fantasien og bar håp.

Skulle hun i ettertid stilt spørsmålet, undret hvor mange lesbiske bor i
Oslo sentrum, akkurat i høyde, på centimeter lik Dark Horse, Britt75 og
henne?

“Bakere har ikke profil på Linkedin.”

Sa Else, og det var alt hun sa.

Hun hadde forsøkt å fortelle hvor lett det var å ljuge på seg en annen
identitet på Internett, og skape en løgn. Hadde hun bare vært i stand å
gjenkjenne svindlerne. Livet kretset rundt en løgn som sa en gang i nær
framtid ville Dark Horse ombestemme seg og de ville endelig møte
hverandre. Blikket stirret seg blind på spor, mikroskopiske bevis på
kontakt der det var ingen.

“Kanskje senere, en gang på høsten.”

Hadde Dark Horse sagt.

Siden ikke tenkt mer over saken.

Hvordan tolket hun utsagnet, fantes det andre måter. Hun var forvist til
en ubestemt tid, og hele tilværelsen hvilte på Dark Horse
forgodtbefinnende. Slik venninnen hadde sagt, og hun først, fikk hun en
høflig avvisning: *Takk, men nei takk!* De egne handlingene, hun fant
ingen formildende omstendigheter, spilte puss og bygget videre på en
skala i galskap. Enten var hun fortapt i det romantiske ideal, forsvarer
til døden av et håp om å finne en sjelevenn, eller led hun av en
psykologisk lidelse lik den de strir med på galehuset. Sofistikert
selvskading uten glass løsrevet diagnosekartet. Ståker på koffein og
sukker.

Hun gjorde feil, og det handlet om å se syner og handle deretter.
Vrangforestillinger fører ting skikkelig galt av sted. Hvor galt visste
hun ennå ikke.

25
--

Hun lo hjertelig. Mistanken mot Sexdyret slo ned og festet straks, som
lyn fra klar himmel. Øyeblikket med innsikt der hun forsto alt og var
sikker, et sekund hun var villig til å satse på en virkelighet kom
sjelden. Hun visste tankegodset var søkt og likevel ville hun gjøre hva
hun som helst for å bevise hva var rett. Lenket foran skjermen i dager
burde vært nok til å la alarmen kime. Hun kjente farene. Galskapen kom
fra tryggheten alltid å ha rett. Sentrum for det hele var et gigantisk
ego til besvær. Isolert i egen verden gjorde ikke sannsynlighet for
feilskjær mindre. Hvor lett fantasien gikk amok hadde hun erfart før da
broren sto midt i en særdeles stygg skilsmisse. Monsteret hun vokste opp
med lå tungt på samvittigheten når svigerinnen kjempet for å få barnet
til skolen. Broren eksploderte i selvmedlidenhet under skilsmissen.
Familie begredelig kalte hun dem. Moren gjorde ikke saken bedre med
bortforklaringer. Hun visste synspunkter var hule i møte med
virkeligheten. Samtalen med svigerinnen glapp små bilder av uhygge, og
hvor lite de enn stolte på hverandre fikk hun bruddstykker. Broren slo
med ord. Ordflom truet daglig. Kone, barn og han truet både andre og seg
selv. Veien ut av huset om morgenen var en hinderløype i følelser.
Svigerinnen fortalte hvordan broren lagde en scene og satte skrekken i
barnet, sa farvel en siste gang, klemte ekstra lenge og sendte familien
på dør. Barnet skulle på skolen og svigerinne fikk illevarslende beskjed
om at barnet må stå utenfor på og slippe å se hva ventet i hjemmet. Han
tegnet på fantasiens blanke tablå med trusler og artefakter, pistolen i
skapet og tårene rant. Selvmordtrusselen satte en støkk i svigerinnen og
hun dro ikke på jobb, snudde bilen etter å ha sluppet av barnet på
skolen og kjørte tilbake for å snakke ned mannen. Svigerinnen trengte
hjelp. Hun fikk vite om uføret, skulle hun handle. Hun kjente ikke igjen
gutten fra oppveksten.

“Hva kan jeg gjøre?”

Sa hun.

Svigerinnen visste broren satt med nøkkelen, og de måtte strekke seg for
å gjøre ham sikrere, trappe ned konflikten og la tiden reparere. Hun
flyktet fra det hele.

Ingenting forandret seg.

Skilsmissen var stygg, og det var i perioden hun begynte å tenke broren
var noe langt mer enn en hustyrann. Avisoverskriftene skrev om en nedrig
blotter som spredde frykt. Forsømmelse fra politiet tidlig i
etterforskningen gjorde antall observasjoner av blotteren til talløse
nummerlister, forseelser ble forglemmelser, helt til avisene klistret
hva de visste om foreldreskrekken på landets forsider. Publikasjonen av
de første hundre gjerningsstedene føltes familiært. Kjente steder,
lokalt, fra barndommens lekeplasser. Ukjente adresser for de fleste, men
ikke henne.

Blotterjakten i nabolaget hun og broren vokste opp var angstfylt.
Følelsene gikk på sukkerpiller når familiens traume synes å være
fasader. Hun overbeviste seg selv om at broren var en forbryter, hun
vurderte å ringe politiet og tipse dem. Hvor mye hun ville tro han var
uskyldig i alt fælt avisen skrev. Lojalitet til barna veiet tyngre. Tips
fra allmennheten var hva politiet trengte for å fange de vanskeligste
kasusene. Avisene skrev markablotteren herjet, tegningen politiet
publiserte i avisene bar en vag likhet med broren. Svigerinnen lo ikke
av teoriene, tenkte trolig på barnet de oppdro sammen, de kjente begge
familier levde utrygg. Skrekken lå der og valket. Skissen,
gjerningsbeskrivelsen, alt føltes nært. Moren med sans for sladder tygde
på teorien om broren og ringte tilbake for å forsvare markablotteren.
Det var ingenting broren gjorde moren ikke ville bortforklare. Tre dager
i uvisse, på tampen til å ringe politiet var det bare en hun ikke hadde
fortalt sine mistanker til. Hun blandet ugjerne samboeren inn i
familiens problemer, men det var ikke til å unngå.

Samboeren lyttet. Stirret. Lyttet til hun snakket ferdig og ga et
umiddelbart svar.

“Du er gal.”

Hun hørte hva samboeren sa.

Tre dager bar hun på overbevisningen om at broren bar på ondskap, mer
enn trusler og selvmedlidenhet, verbal vold mot barnet og hevet stemme
truende til alle i nærheten. Samboeren svarte resolutt, ideen fikk aldri
feste. Hun hadde en fiks tanke brorens forbrytelse var mer enn
privatliv, og en kjæreste som kjente henne, forsto uroen og hvordan
skilsmissen satte tilværelsen ute av gjenge. Søvn fløt i rykk og napp.
Broren fylte et sort hull, sortere gikk det ikke an å bli. Hun ville tro
det verste.

Straks samboeren sa fra stoppet redselen. Broren var ingen blotter og
voldtektsmann.

Hun var gal.

Diagnosen virket riktig straks den ble uttalt. Kjærestepar i sju år,
gode for hverandre fordi assosiasjonsbølgene lot seg identifisere på et
blikk, tonefall, avkledd i tre enkle ord. De skulle være par til
pensjonsalderen. Foran lå fredelige år, harmoniske, alt sammen ble
endevendt når kjærlighetsforholdet tar slutt. Feilslutningene hun gjorde
om broren blottstilte først, dramakviin løst fra en neurose før
samboeren slapp fri, fri til å herje omgivelsene. Her hang hun på
hovedtavlen. Politiet arresterte en annen, en mann fra bilverkstedet
nedi gaten og tok ut tiltale.

Sexdyret søkte bamser å suge i parker nære hjemmet. Hun leste profilen
og fant den absurd. En tjueniårgammel mann gjorde gjentakende besøk på
profilsiden og preferansene strakk begreper lenger enn hun var i stand
til å tenke. Sexdyret var tjueniårgammel og likte alt. Invitasjonen lød:
“Aktiv grov bamsemann søkes på Sagene.”

Når tanken slo henne profilen var falsk satt det umiddelbart, like
plutselig som at broren var markablotteren.

Sexdyret var en falsk identitet for en kvinne på hovedtavlen som brukte
dette motbydelige vesenet til å spionere. Hvilken kvinne var spørsmålet,
for hun mistenkte ikke Dark Horse for å være skyggemonsteret. En annen.
Snushøna, Britt75 og et par andre fargeløse og anonyme profiler i riktig
alder knyttet hun til Dark Horse. Sexdyret tilhørte en annen verden.
Kvinnen måtte ha sans for humor, det pikante, for det var utenkelig
Sexdyret var mann i sine beste år ute etter å suge macho homser med
skjeggvekst. Mannfolk med øye for kvinnelige profiler på Gaysir gikk inn
et par ganger og sluttet etter de leste og forsto preferansene. Hun
søkte eksklusivt kvinner. Lesbiske profiler fikk besøk fra menn, de
klaget på oppmerksomheten, og likevel roet de seg alltid. Mennene trakk
seg tilbake i stillhet etter å ha lært. Kontakt ble kontant avvist.
Profilen var lesbisk, uinteressert, amper mot uønskede invitasjoner.
Sexdyret gjorde seg bemerket ved at han i tillegg til å være fra
nabolaget var uvanlig slitesterk. Han kom tilbake for ente gang, ett år
etter han besøkte første gang. Han var en gjenganger på besøksloggen,
hun ville vite hva slags skapning figuren var?

Hun lo godt. Sannheten sto fram og Sexdyret sluttet å være mistenkt. Hun
var sikker. Sexdyret var en falsk identitet for en kvinne med særs
sublim humor. Respekt for kreasjonen virket naturlig.

“Haha. Morsom fake id.”

Sa hun i et blunk til Sexdyret, og tenkte ikke mer på saken.

Hvis det var mer ville hun følge intuisjonen. Glansen å gjennomskue
bløffen ville skinne fikk hun rett. Smaken av seier vekket humøret,
trakk opp stemningen og var verdt måneder ekstra i posisjon. Sexdyret
svarte med blunkemeldinger.

Svaret kom umiddelbart.

“Hvaforno. Fake id? Morsom du?”

Skjermet hen masken? Alt var mulig.

Identiteten bak Sexdyret ville ingenting erkjenne.

Nedsiden vifter hun bort. Hun var ikke bundet av noen, løfter til en
gal, pervers mann som snoket på nettsiden der han ikke var velkommen
Hvis kvinnen bak Sexdyret ville holde fasaden skulle profilen utfordres.
Beviset ville være hvis en mann kom til syne, og hvis ikke var hun
overbevist om at preferansene i søket var for spesifikt til å være
sannsynlig.

“Ja. Hvis ikke kan du ringe.”

Sa hun. Og mente det definitivt ikke.

Hun var åtti prosent sikker. Sånne menn finnes ikke, med høy
sannsynlighet. Lojalitet gikk ikke til Sexdyret. Han løy hun til,
svindlet. Hun kunne nekte for alt og gå videre. Lovlig, med ren
samvittighet. Rachel tok henne en gang på kornet, gravde til hun ble rød
og forlegen. Uforsvarlig traff hun et følsomt punkt.

“Du altså. Du er som en hagle; skyter i mørket og en sjelden gang blir
det blink.”

Sa Rachel.

Venninnen kjente henne. Snakketøyet gikk og traff et sårt punkt, hjernen
zoomet for deretter med største selvfølge å erklære hva var sant.
Sexdyret tilhørte en feminin kameleon og var intet annet enn en falsk
identitet. Ingenting noen kunne si ville overbevise om noe annet. Hun
spilte dårlig poker. Første respons avslørte bløffene i spillet der
hemmeligheten ikke å bløffe unnslapp henne. Ryggmargen kriblet slik når
hun gjorde seg klar til total syn. Hun ville gå inn hud og hår.
Spenningen når hun satset sitret. Sexdyret var synet. Kvinnen bak
Sexdyret kunne erkjenne og ta æren for å ha skapt en sublim perversitet,
en mann ingen ville mistenke for å være falsk. Kvalene lå implisitt
innen rekkevidde, som en del av fortellingen simpelthen. Hun hadde ikke
tid til meningsløse betraktninger om skrittene fra barnsben til voksen
moroklump. Parallellene til en spillegal tante som brukte de to
årslønnene i sluttpakke på tre måneder. De var likere enn hun ville
erkjenne. Hva så? Sannheten unnslapp.

Hun betraktet profilen til Sexdyret, stilte aldri spørsmålet hvorfor en
falsk identitet behøver kompliserte dekkhistorier. Hun tenkte ikke at på
Gaysir trengte ingen skjule falske identiteten i annet enn anonymitet.
Tomme hoder inntil grønne prikker fylte hovedtavlen. De opptok talløse
arkfaner, flik av normalitet var profilene vanskelig og skjule—ikke de
utagerende livsglade i utallige drakter. Antakelser om framtiden føltes
feil. Hun overvurderte egen evne til forræderi. Løftene hun ga ville
holde, enten de gikk lett, eller med tungt hjerte. Tap var ikke et
alternativ.

Livet gikk i en rett linje. Løgner satt aldri godt, hun tydde til dem
når sannheten var ubehagelig.

Femten år eksklusivt lesbisk. Når hun ble singel søkte hun bare kvinner.
Mennene i livet hennes fortjente respekt, de var gode relasjoner,
ærlige. Derfor valgte hun når anledningen passet merkelappen bifil.
Sannheten var at kvinner var alt hun ville ha. Menn var uaktuelt. Dialog
med tjueniårgamle Sexdyret på Gaysir tilhørte en særegen og obskur
blindvei i et liv levd kjedelig. Profil åpen for det meste, spesifikt
sugejobber på Sagene. Straks! Fortrinnsvis søkte han storvokste menn med
mye hår. Hvorfor ikke henne?

“Hvorfor skal jeg ringe deg?”

Sa Sexdyret.

“Fordi du er interessert i alt, og jeg vil også bli sleiket.”

Hun slapp løs fanden, ertet tilbake. Hvis dette falske menneske nektet å
legge kortene på bordet ville hun utfordre Sexdyret til kamp om
sannheten, tvinge ham ut i lyset.

“Kødder du med meg? Du er bare interessert i kvinner.”

“Det også. Spesielt her inne. Men er bifil.”

Dette var en av de få tilfeller merkelappen var god å ha for å oppnå
ønsket effekt. Mannen i profilteksten ville bite på invitasjonen.
Fortsatt reflekterte hun lite over hva det ville si hvis Sexdyret skulle
vise ham. Helgen lå åpen. Innholdsløse timer, hun stirret på skjermen,
grønne og oransje prikker og følte søndagene rant vekk. Hun fant ingen
glede i livet. Opplevelser hadde mening. Hun kunne lyve, trekke tilbake
når som helst, la han fare videre på jakt etter hårete bamsemenn å suge.
Femten år uten menn gjorde ingen forskjell, hun var bifil og det var
sant. Hun følte ingen angst. Seksuelle erfaringer med andre enn kvinner
var kjent stoff.

Dessuten… Sexdyret var en kvinne, det var sikkert, alt som gjensto var å
bevise faktum.

“Nei, tror du jeg gir deg telefonnummeret mitt bare fordi du sier det.
Tror du jeg er dum?”

Sa han.

Hun følte trygghet i seier. Spillet var der for henne å vinne. Sexdyret
klamret identiteten til anonymitet, og hun fant ingen grunn til å nevne
det ikke ble spurt om telefonnummer. Skulle de trenge en kanal kunne hun
oppgi eget nummer—registrert på jobben. Hun var halvveis skjult i en
masse på tretusen ansatte. Sexdyret ville aldri gi fra seg anonymitet.

Hun høynet innsatsen.

“Jeg er fortsatt på at du er en falsk id, jeg er villig til å vedde sex
med en pervers mann for å bevise jeg har rett.”

Sa hun.

Direkte ville hun sette han mot veggen, friste, skape en opplevelse på
søndagsettermiddagen.

“Hva vil du gjøre da?”

“Siden jeg er lesbisk og det er lite sannsynlig jeg vil sette pris på
seansen må jeg styre det hele. Alt må skje i mitt tempo.”

Hun lurte hva det var som gjorde det sannsynlig at situasjonen kunne
kontrollere. Hvis hun stilte mot en ung mann, og ting gled ut, og hva
skulle forsvaret være? Hvis alt gikk galt og han gikk over streken ville
hun stå forsvarsløs. Tankene vokste, og hun skjøv dem bort med enkle
bortforklaringer.

“Jeg kan alltids lyve.”

Tenkte hun, og forskjøv komplikasjoner bak i bevisstheten.

“Jeg liker å bli sleiket.”

Sa hun.

Og det var sant.

Hun skrev ikke å bli tatt bakfra, fordi det ville hun se an. Kunne det
tenkes hun var nysgjerrig på hvordan det ville være å ha sex med en
mann? Upersonlig sex kunne fylle søndagsettermiddager, hun visste
såpass. Hun husket ikke hvordan menn var. Hun likte dem bredskuldrete.
Deilige Eivind, aktivist i epostfeministene. Han smadret kiosker som
solgte porno i Gamle Oslo. Pressemeldingene skjulte aktivistene og gikk
omveier via anonyme porter satt opp på universitetets datatjenere. Han
slo et slag for feminisme, trygg identiteten aldri lot seg avsløre.
Eivind hjalp alle, og hun følte han sto i veien. Tre ganger over to år
fant de sammen, men var aldri kjærester lenger enn to måneder. Han
elsket kompromissløst. Siste gang ringte hun fra bussholdeplassen på
Grønland, forsinket og kvalm før besøket, ute av stand til å dra den
siste biten med trikk. Oppløst i tårer ringte hun Benjamin for å forstå
hva som var galt. Energien rant fra henne og hun ville ikke snakke,
nøyde seg med en tekstmelding og en kort linje i beklagelse. Vraket for
tredje gang og det måtte gjelde, hun ville aldri mer ta Eivind i armene,
den perfekte mannen, hodeløst forelsket, 191 centimeter høy,
bredskuldret. Perfekt!

Svaret på hva gikk galt unnslapp.

Sånt skjer, svarte Benjamin. Hennes historie forble privat.

Søndagen var nåtid, alt sto klart for et besøk fra Sexdyret hvis det var
nødvendig. Hun trodde veddemålet var sikret. Kroppen trygg. Sexdyret
ville ikke gi fra seg telefonnummeret. Han trakk ut tiden og hektet seg
opp i detaljer. Hver presisering økte vissheten om at profilen var spill
for galleriet. Hun ville aldri møte Sexdyret, snakke med ham. Mennesker
som dette finnes ikke. Profilen er usann. Kun en kreativ kvinne tenkte
slik innfløkt perversitet for å fly under radaren på Gaysir, skjule det
sanne jeg.

“Hvordan gjør vi dette?”

Sa Sexdyret.

De kom ikke lenger. De var ved veiskillet, gikk så langt Gaysir kunne
bringe dem. Han ville ikke videre. Hver gang hun sa mer ble avviket
sant, hun var et hederlig menneske og det var tid for ettertanke. Hva
skjer etter Gaysir?

Hun reiste seg og logget av, undret om hun tok dialogen for langt.
Kjedsommelighet får bare bringe deg til et punkt, ikke lenger, og
tankene tilbake til en tid hun snudde ryggen til var ikke smertefri.
Mennene i livet hennes hadde vært fantastiske. Hun sa bifil for dem.
Merkelappen var deres hedersmerke, og likevel fantes det i hver relasjon
en fundamental mangel—alle som en. Hun kom aldri fra faktum å være
lesbisk vokste fra seksualitet. Lesbisk legning tilhørte kroppens
sannhet og var seksuelt betinget. Sanselig sjeleliv, kropp og hode som
ville dit og ikke lenger.

Kjøkkenbenken var full av brødsmuler, et tomt glass og to tallerkener lå
slengt i oppvaskkummen. Hun skylte tallerkenene, satte de i
oppvaskmaskinen med glassene og tørket smulene med en fiberklut. Badet
var heller ikke strøkent. Hun sprayet speilet og tørket med papir, og
sveivet over toalettskålen med børsten. Håndklede skiftet hun med et
nytt. Ting endret seg fort. I tilfelle besøk ville hun støvsuge gulvene,
soverom og stue, skifte sengetøy måtte hun gjøre etter. Vaske rent ble
viktig først når sengetøyet måtte anses for å være skittent. Ville hun
våge seg til Gaysir, stå løpet, tvinge Sexdyret inn i lyset.

“Det var det jeg visste, bare prat.”

Sa Sexdyret.

Hun satte seg og svarte med en kort tekst, ett ord og en sekvens med
tall. Ring og ‘\ *telefonnummeret’*. Nå var det opp til Sexdyret.

For hvert minutt det gikk tenkte hun det var tid hun seiret. Sexdyret er
en kvinne. Hun synet og bordet fanget. Nummeret lå utenfor, de kunne
forlate Gaysir og vise seg for hverandre dersom Sexdyret vil. Men han
spør og graver.

“Hva er dette?”

“Mobiltelefonnummeret mitt. Jeg sa aldri jeg måtte ha ditt. Du kan ringe
meg?”

Sa hun.

Stille igjen. Lenge. Hun gledet seg over hvert minutt for det føltes som
hun vant. En kort stund var det et snev av usikkerhet i luften, hun
forestilte seg hvordan det ville være å ha sex med en mann. Husket hun
rett, at sex med det maskuline kjønn var en grei teknisk øvelse, ikke
fullstendig meningsløs hvis hun tok initiativet og kanaliserte kreftene
dit hun trengte for å få en fysisk reaksjon av seminytelse. Sexdyret var
den som nølte.

“Jeg vinner. Jeg vinner. Takk og lov.”

Sexdyret kommenterte tekstmeldinger på Gaysir og ringte ikke, hun tenkte
lettet seieren var innen rekkevidde. Tankene i usikkerhet forsvant,
samtidig gikk alarmen i stillheten. Impulsene tatt igjen av fornuft.
Bordet fanger i poker. Øyeblikket, sannhetens time var kilden til
spenning. Det mest kaotiske hadde skjedd, og søndagen engasjerte,
underholdning for halvåret kokte ned til her og nå. Med hud og hår
satset hun selvet.

“Okay. Jeg skal dusje først og ringer.”

Hun ville ikke innse nederlaget. Hun logget av Gaysir. Startet å vandre
uten mål og mening rundt i leiligheten. Deretter slo hun helt om og
støvsugde gulvet i stuen, soverommet, og tok en sveip med tørkemoppen på
badet. Hun dusjet. Leiligheten var strøken.

Håp var alt som hang i luften. Sjansen var Sexdyret bløffet, han ville
ikke ringe. Hvis han ringte ville hun være klar. Profilen bodde på
Sagene. Sexdyret kunne være der på fem til femten minutter. Hun ville
respektere avtalen hvis mannen bak enavnet var ukjent. Redselen for at
Sexdyret var vaktmesteren, eller den digre feite familiefaren i
hjørneoppgangen surnet ventetiden. Hun kunne vinne. Hun pleide håpet,
selv om det kimet alarmer i bakhode. Telefonen ringte og det ville være
en manns stemme i andre enden. Hun ville fortelle hvor hun var og han
ville komme, stige inn i soverommet.

Hun måtte ta føringen, artikulere hva han fikk lov til å gjøre, styre
unna penetrering. Hvis hun tok grep, gjorde det beste av en dårlig
situasjon kunne hun leve med veddemålet. Sexdyret ville ringe, være i
samme rom og ta instrukser. Hun lå på sengen, kjente duften av sjampo.
Håret fuktet putevaret, hånden fuktet klitoris. Hurtig spente hun
kroppen rundt hoftene, før hun slapp tak og følte muskelen mykne i ledig
hviletakt. Ville hun takle Sexdyret, i verste fall hvordan ville livet
bli hvis vaktmesteren ringte på døren. Hun måtte flytte.

Kroppen sa fra, varslet at hun lekte i et sanksjonert spill, alt var
ikke forgjeves.

Tanken fløt fritt, hulter til bulter, et skimmer med håp om at
veddemålet var aktivt. Åpent. Avtalen lå fast, ingen tanker om å flykte.
Hun løy ikke. Spent svermet hun om fortiden, forsøkte å huske hvordan
det var å ha sex med menn, glemme angsten for å treffe vaktmesteren. Hun
reiste seg og spankulerte rundt i sirkel på stuegulvet da telefonen
ringte.

26
--

Hun trykket på knappen med telefonrøret og legger mobiltelefonen opp mot
øret.

Søndagen forløp rolig, hun puslet i leiligheten, spilte av et par
episoder av teveseriene lastet i løpet av uken. Pysjamasen hang løst på
kroppen når hun spiste lunsj og leste dagens avis. Før solen snudde
logget hun inn på Gaysir og stirret på hovedtavlen, studerte mønsteret i
grønne og oransje prikker, skrev innlegg ned trådens slutt til hun ikke
kunne si mer om emnet.

Deretter fikserte øynene på besøksloggen der Sexdyret gjorde et gjensyn.
Han lurte alltid i bakgrunnen og studerte henne. Irritasjon fantes ikke,
mer et lakonisk smil, betraktninger og hoderist over en av de mest
overdrevne statuser på Gaysir. Sikrere hadde hun aldri vært på at
profilen Sexdyret var en kvinne som skjulte seg bak overdrevet
obskønitet, og honnørblunket var ment som én kompliment til stemmen.
Sexdyret holdt fasaden og hun synet, deretter høynet hun, det var et
personlig valg gjort på impuls—slik mange valg hun gjør. Hun var villig
til å betale prisen hvis hun tapte veddemål. Prisen var sex med en mann.
Femten år hadde gått siden hun sist lå med en mann. Den avskyelige
handlingen lå begravd i glemmeboken, vage minner valket nysgjerrig i grå
masse. Fram til telefonen ringte var håpet om å vinne der. Hun skulle i
sannhetens øyeblikk vite om antakelsen at Sexdyret tilhørte en
sofistikert plan for å skjule perversitet stemte. Profilen hadde blitt
utviklet av en intelligent kvinne med humor og altfor mye energi.

Stemmen i røret var en ung mann, tjueni år gammel. Nølende svarte hun
knappest mulig.

“Hei.”

Han lyttet på stemmen og trakk på ordene.

“Du er en stemme. Da tapte jeg veddemålet.”

Sa hun.

“Ja.”

Skrekkslagen forsøkte hun å tenke raskt, unngå ta lederskap når avtalen
var krystallklar. Hun så etter klausulen bort fra avtalen. Slik begge
forsto opptrinnet ville dagen resultere i sex.

“Jaaaa.”

Sa hun.

“Har du et sted å være.”

“Ja.”

Var svaret. Deretter brøt linjen.

“Hallo. Hallo?”

Samtalen ble brutt. Hun studerte mobiltelefonen og konstaterte at
batteriet var fullt og samtalen ble ikke avbrutt fra hennes side. Skjult
nummer viste mobiltelefonen på skjermen, hun tenkte han reserverte
nummeret og fikk det ikke til å stemme med kvalene Sexdyret viste
tidligere mot å si nummeret til egen mobiltelefon. Hun rakk å søke på
nettet. Nummeret ukjent, fastlinje, hun tenkte det måtte være en
telefonkiosk?

Reservasjonene fra Sexdyret overbeviste om seier, i stedet var han
veldig diskré. Han ringte ikke fra egen telefon, men et annet nummer—et
myntapparat. *Fantes de fortsatt*? De ble brutt.

Hun forsøkte å tenke.

Raskt tok hun en runde i stuen for å roe nervene. Leiligheten var
strøken, klar for besøk. Nyvaskede gulver. Han, Sexdyret var mann og
ville ringe tilbake. Mobiltelefonen ringte der hun slapp den ned på
sengen. Hun gikk tilbake til soverommet for å svare. Satte seg på
madrassen der hun en halvtime tidligere gnukket seg selv til
vibrasjonene, bare for å bekrefte kroppen fungerte. Ingen funksjoner var
defekt.

Stemmen hans var ukjent. Sexdyret var ikke vaktmesteren. Avtalen
gjelder.

“Hei.”

“Vi ble brutt. Det går bra nå.”

“Ja.”

Sa hun.

“Har du et sted å være.”

De sluttet der, og hun visste ikke hvordan de skulle gå videre.
Soverommet lå åpent, døren til gangen ville lede han inn, forbi speilene
på veggen og eksponere kroppen før han tar av seg klærne og stiger ned i
sengen. Hennes seng.

“Har du et sted å være?”

Sa hun tilbake.

“Nei, i grunnen ikke.”

Han ville til henne, og hun erkjente det virket enklest slik. Han ville
vite hvor hun bodde. Svarene var enkle, og likevel trakk det ut i knappe
og upresise formuleringer.

“I samme nabolag som deg… Sagene.”

Hun fulgte med.

“Ja. Hva slags sted har du?”

Sa han.

Hun måtte invitere ham hjem, han sto praktisk talt på dørstokken. Et
sted nære ventet han på svaret. Han var i nabolaget og ville ringe på
dørklokken om ti minutter. Om en kort stund var hun tvunget å slippe han
inn og de ville ha sex. Hun kunne be ham om alt innenfor visse rammer og
mannen ville stille opp til alle typer teknisk sex, blottet for
følelser, forpliktelser. En dag, helg, en søndagsettermiddag var
tilstrekkelig og hun følte selvbilde rase i jakt på spenning. Nytelsens
farer.

Hun visste ikke hvordan han så ut, luktet. Han smakte sikkert vondt.

“Hvor er det?”

Adressen lå på tungen.

Samtalen ble brutt på nytt. Denne gang forsto hun straks hva som
skjedde, mobiltelefonen lå musestille på øret, hun satt stiv av redsel
og det var ingen sjanse for at linjen ble brutt derfra. Apparatet hang i
luften, bevegelsesfritt og med perfekt mottak.

Tankene strømmet raskere, fornuften var i ferd med å seire. Hun reiste
seg og gikk inn i stuen til den bærbare datamaskinen der hun logget seg
på Gaysir. Hovedtavlen var nesten uendret. Hun søkte på Sexdyret og kom
først fram. Raskt skrev hun meldingen.

“Dette var tydeligvis vanskelig. Jeg beklager. Det blir for rart, jeg
trekker meg fra dette. Spesielt fordi det er en tredje person å ta
hensyn til.”

Sjansespill.

Muligheten åpnet seg når han erkjente at det fantes ikke et sted for
dem. *Hva sa han*?

“I grunnen ikke.”

Kortkort svar med nødutgang klart som dagen, blodomløpet rakk fram til
hjernen og det sto tydelig at dette var veddemålet hun ikke kunne hedre.
Femten år med kvinner ville ikke ende i et slibrig sextreff med et dyr
fordi hun kjedet seg en søndag ettermiddag.

“Hvorfor?”

“Det ble mye rot og med en tredje person i ligningen vil jeg ikke.”

Sa hun.

Ville aldri ville. Slutningen var logisk, rett på sak, redningsplanken
var en gjetning på at Sexdyret bodde med en annen og måtte ut av huset
fordi han levde i samboerskap. Han aksepterte unnskyldningen, hun
gjettet riktig og slapp konfrontasjonen.

“Er du sikker? Jeg har barbert meg nede for deg.”

Hun husket søte, deilige Evald. Stjerne på operaen i Wien, russer fra
St. Petersburg. De delte bungalow ved stranden på den greske øya Lesbos.
Han dyttet henne inn i drosjen som tok dem dit, hun lot ham avtale pris
og ta imot nøklene i resepsjonen. Når de våknet og han ville ha sex
orket hun ikke tanken på den lille lyse penisen inni kroppen og hun
sugde ham for å slippe. Deretter stakk hun av på moped til Elassos der
butsjlesber lå i rekker på stranden, døddrukne fra gårsdagens pilsner.
Bare synet av dem slapp livsgleden tilbake. Minnene av Evald på Lesbos
snudde magesekken hundreogåttigrader, kvalme, ubestemmelig ubehag vokste
i rasjonaliseringens stygge skygger. Hun ville aldri mer suge pikk.
Tillot hun seg å huske ville mannfolkeim aldri ødelegge livsgleden.
Kjedsommelig fant ingen kur i handlinger, og kunne aldri skyve bort sur
vemmelse i synet av en perfekt, lys og ren penis. Hun slapp å suge
Sexdyret, alt for å slippe spre bena for ormen de har hengende mellom
beina.

“En annen gang.”

Sa Sexdyret.

“Nei, aldri. Dette var en gang. Et stunt. Ikke personlig ment, men jeg
vil blokkere deg.”

Hun kunne ikke sperre brukeren Sexdyret raskt nok, og følte han tok det
fint når hun sneik seg ut av hovedtavlen for kvelden. Hun var sikker, så
feil kunne hun ta.

Hun kjedet seg og ukens fridager gikk alt som vanlig, venner fredag
kveld satte helgen i gang og korte turer lørdag formiddag for å spise
frokostlunsj ble sporadisk byttet ut med kinotur, eller en utstilling på
museum.

Reddet fra syndefall trampet hun rundt i leiligheten flere timer for å
analysere hva skjedde? Hun kjedet seg. Forklaringen kunne ikke være
enklere. Tantens unnskyldninger for alle løgner og feilinvesteringer i
lotterispill holdt ikke vann i tilfelle Sexdyret. Tidsfordriv forklarte
ikke tilstrekkelig hvor nære katastrofen Gaysir tok henne, fornektelse,
fremmed for paralleller mellom spill og sex fantes det ingen rasjonell
forklaring. Sjansespillet. Hun tilhørte en familie gamblere.

Andelen heterofile jegere på Gaysir, fornuftige folk på jakt etter
pikante opplevelser gjorde resten av populasjonen engstelig og folkesky.
Anonymisert med dulgte bilder, gjemt bak enavn egnet til å skjule satt
de skeive. Homofile redde for å bli synlige når trafikken på nettstedet
gjorde alle til skue framfor seriøse søkere av det tradisjonelle, den
store romantikken fra litteratur og film. Hun antok kjærligheten fantes
på hovedtavlen. Hun var optimist av natur. Debatter om hvem de var,
hvorfor, regler for konversasjoner på Gaysir dukket opp på forum og
forsvant. En gjenganger var de vanlige demarkasjonslinjer mellom de med
ønsker om å være i fred fra pågående grensebrytere og de som ikke brydde
seg over nærværet av heterofile. Liberale regler for å tillate alle, å
ønske alle velkommen kom i konflikt med etterspørsel om å stenge ute
større grupper. Forumsdebattene avslørte epidemier med uønsket seksuell
oppmerksomhet. Feilplasserte henvendelser skjedde i konflikt med
uttrykte preferanser, og flere ropte forferdet, krevde respekt.
Personlige standpunkt varierte fra de plaget av fenomenet, yngre jenter
og gutter, og eldre veteraner som hadde vært ute en vinternatt før.
Forumshorer lærte å neglisjere, eller delta selv, leke med mulighetene.
De levde, delte erfaringer og aksepterte liv i mange former og fasetter.
Flertallet av forumshorene var menn, skjermet ville hun tro for
umotiverte invitasjoner til sextreff fra det annet kjønn. Ungdommen
lekte med grensene bare for å bli plaget av tilnærminger fra eldre.
Lesbiske kvinner var yngel for gifte ektepar. Grensesprengning var en
del av tilværelsen alle måtte tåle, mente flere. Forum viste ingen nåde.
Bare si nei og ignorer dem, rådet forståsegpåere.

Aldri mer ville denne debatten være tilgjengelig. Hun kunne ikke flagge
synet Gaysir var for dem, homofile og lesbiske, eksklusivitet for de få.
Hun måtte avstå fra å kreve et fristed og forlange respekt for legning.
Homofile, heterofile og de perverse var de tre legningene og ingen
skulle ekskluderes. Tredje gruppen var jevnt fordelt i de to første, og
eneste felles for de perverse var søket etter å sprenge grenser, lure
andre til noe som handlet mer om skam enn seksuell tilfredsstillelse.

“Alle samtaler jeg har her inne har jeg med Dark Horse.”

Sa hun til seg selv.

Overalt stirret kvinnen rett på og krevde kald likegyldighet. Iver, håp
og drømmer var et overtramp mot grensen satt i stillhet, og det var noe
hun måtte respektere. Tilbake hang rester fra kollisjonen der fantasi og
realitet utviklet mistenksomhet lik en psykiatrisk diagnose.

“Hva skjedde?”

Sexdyret manglet finesse, det eneste sublime var overdrevet perversitet
hun innbilte seg måtte være feminin list—ikke Dark Horse, men trygt
likevel. Et veddemål hun ikke kunne tape.

Hun tapte.

Dårlig samvittighet for å ha trukket seg fra et veddemål. Nei, hun følte
dårlig samvittighet for å ha tatt feil, satset og tapt, gitt en
heterofil mann et håp om seksuell ekstase med en lesbisk.

Når sjansespillet lønner seg vil de heterofile aldri slutte, forsvinne
fra kanalen for de skeive. Gaysir blir stedet for de frilynte, mennesker
med erfaring og ønske om å krysse grenser. Alle homofile og lesbiske har
på et tidspunkt tatt et skritt lenger enn de fleste, og derfor er det
naturlig at de i et svakt øyeblikk villig eksperimenterer. Hun tenkte
slik, den stive penisen til Evald roterte i hjernen, hun lurte på om
Sexdyret hadde en maken. Illusjoner brakte henne til kanten av stupet;
først vrangforestillingen Sexdyret var kvinne, deretter ønsket bak
finurligheten satt Dark Horse og gjorde djevelens verk. Løgnen hun i det
hele tatt var i stand til å lyve, love bort og snu ryggen til ordene,
som om bordet ikke fanget straks veddemålet lå der når Sexdyret ringte
for å innkassere løfte. Skulle hun kontrollere situasjonen de var drevet
inn i etter et ønske om å skli? En vilt fremmed mann står nybarbert med
penisen i hånden på soverommet, hun brer seg forlokkende på madrassen og
oppfordrer ham til å stupe hode ned mellom beina og han gjør som hun
kommanderer; hva mer var det? Gjensidig bytte av seksuelle tjenester, å
gi tilbake? Hun hadde ingenting å gi. Kunne hun snudd baken i været og
beordret anal penetrering? Situasjonen framsto uoversiktlig, uklar,
Sexdyret var sterkere, han kunne gjøre som han ville. Bare fordi du kan
betyr ikke at du gjør. Med Evald valgte hun minste motstands vei og
knadde pølsesnapsen, lekte med tungen og gulpet kroppen dypt. Slapp hun
unna?

Undertrykket angst flommet fritt, hun gispet etter luft, takknemlig for
flaks og tilfeldigheter. En ubestemmelig glede reddet kroppen fra
lidelsen.

Hun spilte scenene i hode, marerittet vokste, lattermilde minner om en
mann med lommene fylt av mynt avbrutt når den grådige telefonautomaten
svelget kronestykker i rekordfart, åttikroner i minuttet—lett.

De ble avbrutt. Sexdyret kastet seg inn i kiosken på Advokat Dehlis
plass og vekslet en femtilapp i mynt, ringte, bare for og blir brutt
igjen når den siste tjuekroningen rant ned sluket på telefonautomaten.
Andre ufrivillige pause forårsaket av mangel på mynt i streben etter
full anonymitet. Sexdyret ville skjule seg bak telefonsystemets
anonymitet og mistet sjansen, hun reddet kroppen i et lykketreff og
ville alltid være takknemlig for tiden til å tenke som automatslukene
til Telenor ga.

Det også.

27
--

Hun klikket seg inn i profilen til Insane for å se hvem ble koblet til
på vennelisten. Overrasket gjenkjente hun fjeset, en venninne til Else.
Hilde var pen, smilte, og til beklagelse sto en nonsjalant stil de tre
gangene de møtte hverandre i veien for vennskap. Hun kalte Hilde Heidi
når de møttes, gjentok stadig samme feil og det satte tonen.

Hun slurvet på førsteinntrykk, og hentet inn godvilje senere i
overdrevet sjarme. Offensiven mot Hilde kom sent. Hun følte det gikk
greit. Felles dem imellom var seksuell legning og vennskapet til Else.
De møtte hverandre på So og nikket høflig. Slike sjansetreff var
sjeldne. Hun var samboer og vekke fra uteliv i årevis. Hilde var skilt
mor til en tiåring. Barnet hadde gått i barnehage med Nurket, Elses
datter som hun adopterte i hjerte og oversvømte med gaver på bursdager.
Nurket og Hildes datter var bestevenninner. Else fortalte om feriene de
tok sammen, to alenemødre på tur, og hun fikk høre godbiter fra
skilsmissen til Hilde når konflikten sto som verst. Ut-av-skapet
historier vekket nysgjerrighet. Hun sugde til seg pikante detaljer om
hvordan Hilde forelsket i en kvinne ba om skilsmisse fra far til barnet.
Lenge før de møttes visste hun altfor mye om samlivsbruddet og krisene
som fulgte. Elses gjorde et klumsete forsøk på å matche dem. Deit med
Hilde ble for tett. Hun fulgte aldri opp forslaget. Hun mente den
ubehjelpelige heterofile tilbøyelighet å matche single lesbiske var
søtt, og avslo like fullt alle forsøk. Gode intensjoner er ikke nok. Hun
sjekket damer best på egenhånd. Tilfelle Hilde tente ingen entusiasme,
og mangel på tenning var gjensidig. De var kanskje for like, hun visste
for mye om Elses venninne, visste nok til å styre unna. Hvem vet i slike
saker hvorfor en sti er uten frukter?

Barna fylte ti år og gikk ikke lenger i barnehage. Hun likte rollen som
filletante for Nurket, lengtet ikke etter egne barn, eller stebarn.

“Er jeg for egoistisk for barn?”

Insane koblet Hilde til vennelisten, og nysgjerrigpetra våknet. De hadde
flørtet fritt på Gaysir, sendte meldinger og utforsket fortid, nåtid og
potensialet i en tenkt framtid. Intuitivt forsto hun Insane ikke flørtet
eksklusivt med henne, de holdt en direkte og åpen tone, ærlig på
gjensidig attraksjon—sjenerøse med komplimentene. Avstand nøye oppmålt;
der og ikke lenger. Hun utsatte coladeiten. De snakket om singellivet,
ekskjærester og håpet for framtiden. Hvis hun registrerte venner på
Gaysir ville Insane fått en invitasjon. De var fornøyd med å flørte
hemningsløs, og hun gledet seg til neste gang kvinnen fra Sandnes skulle
til Oslo. Det fantes et løfte om å vekke liv i avtalen om et coladeit.
De sendte meldinger ustanselig. Hun reiste bort, vekk fra byen, deiten
måtte vente til hun kom hjem. Den helgen traff Insane Hilde i Oslo.

“Kjenner du Hilde?”

Sa hun.

Insane stusset, ventet minutter før hun fikk et svar. De hadde deitet,
en kort halvtime. Hilde og Insane var sammen søndag formiddag i Oslo,
bare timer før hun kom hjem. Søte-Hilde innrømmet flørtete lesbe fra
Sandnes, og stikk i sjalusi truet å ødelegge den gode stemningen. Hun
forsto de var forelsket lenge før forholdet overhode ble erkjent. De
pratet om felles bekjente på den lange vennelisten, fjes hun husket fra
tiden hun var ny i det lesbiske hovedstadmiljøet. Hun hang ved
biljardbordet på lesbeklubben Potta. Hilde tilhørte ikke kjernen.
Alenemor bosatt på Grüerløkka, kjente Else—bestevenninne i
omgangskretsen som visste mest om det meste. Hun vanket på Potta mens
Hilde var i skapet, gift i et heterofilt ekteskap med mann og fødte et
barn med ham før det ikke lenger gikk å fornekte: Hilde elsket kvinner.

“Hun er en bekjent, god venninne med en av mine beste venninner.”

Sa hun.

Insane kom fra sjokket. Sammenfallet moret mer enn bekymret. Hun
erkjente Hilde var en fremmed, en aktør i nærmiljøet. En hun ville vise
respekt.

“Du burde konsentrere deg om søte, omgjengelige Hilde.”

Sa hun. Og angret.

“Mener du det. Hva med coladeiten vår?”

Insane var skuffet, eller høflig. Litt begge deler.

“Tilfeldigheter hadde det slik at jeg ikke var hjemme når du var i byen.
*C’est la vie.*\ ”

Tøff i tonen dyttet hun siste månedens flørt og tidsfordriv på
hovedtavlen i armene til Hilde. Hun trengte ikke dytte hardt.
Singellivet var åpenhet. De flørtet trygt, fritt og utvekslet ærlige
ordspill. Begge visste veien de vandret ledet til sex. Var hun bygget
for enkeltnatten? Søkte hun tilfeldig sex ville kvinnen i Sandnes være
perfekt. Motivert. Pen, intelligent og morsom, fra distrikten—en
avstandsflørt. Forhold over telefonlinjer visste hun ville svikte, knake
straks entusiasme ble tynt fra hvert ord sagt. Fem timer med bøker og
dagens aviser mistet sjarmen når samværet føltes forutsigbart, hvilket
alltid er tilfelle når kilometeravstand og tid får prege mennesket.
Rasjonelle voksne tror de vil det annerledes, de går i forhold avstumpet
fra drømmen om tosomhet. Enkelte tror det er lett å finne punktet der de
klarer å snu, flytte, endevende livet for den ene sanne kjærligheten. De
var to voksne, tre hvis hun tok hensyn til Hilde, og det måtte hun jo.
Hun ville aldri flytte til Sørlandet, Insane kjente Oslo, visste hva
byen innebar på godt og ondt. Byen var en stor godteripose, et besøk med
fråtsing i kropp og hodeløse forelsker til det gikk over.

“Du vet hva sex er?”

Spurte hun.

Insane visste ikke svaret, ikke hennes versjon, hørt og fordøyd fra en
film langt tilbake. Hun fortalte sex var intimitet utledet fra samtale.

“Først prater du om det, deretter gjør du det.”

Såre enkelt.

“Du mener Hilde er søt og omgjengelig.”

Hun kunne ikke nekte for at Hilde var pen, enkelte ville si flottere enn
henne, og viktigere, Hilde var noen år yngre. Insane ville knapt kjenne
aldersgapet.

“Jaaa.”

Sa hun, og fortsatte å dytte.

Hun nevnte aldri barnet. Bestevenninnen til Nurket, barnet hun var
filletante til. Ville ikke ødelegge med skår i gleden. Hun lot løfte om
forelskelse like rundt hjørnet boltre seg i fred. Det var sikkert. Etter
Insane ble forelsket i en alenemor stoppet effektivt all flørt dem
imellom. Vesenet hun skrev til ropte etter endorfiner, opplevelser, håp
om en framtid. De var like sånn. Svært kompatible. Synd. Hun tenkte
mange tanker og lot kvinnen gå videre til beste for dem begge. Smerten
etter Dark Horse lå latent og blokkerte.

“Jeg ville elske en coladeit når jeg er klar. Ikke nå. Jeg er hengt opp
i en annen her på Gaysir. Dessuten, jeg utvider aldri søket ut av Oslo.”

Sa hun.

Tankene ville ikke slippe tak i Dark Horse, og det ville være egoistisk
å stjele en kortvarig opplevelse fra noe sannferdig og ekte. Siste
setning sa hun mest på fleip. Minnene om kriterier og regler, deres
disiplinerende effekt var nettopp hva de trengte. Insane lot sparkingen
skje, viste ingen tegn til overraskelse. Samtalene var om dem. Smilene
lå bredt. Hun visste Insane skulle besøke Oslo, og da ville hun komme
bakerst i køen.

“Vi kan ta coladeiten senere. Må bare komme i buksene på søte,
omgjengelige Hilde først.”

Nå ertet Insane.

Hun kunne gjøre en forskjell, kjempe, men følelsene lå kaotisk og
skvulpet på overflaten og hun ville ikke snu alt på hode når kriteriene
var feil. Tilværelsen truet selvet, eksistensen hun kjente var trygg og
varm. Hun måtte avklare ting med Dark Horse, lukke galskapen, snakke med
kvinnekroppen bak enavnet og unnskylde et turbulent følelsesliv søkte en
katalysator uskyldig fanget på hovedtavlen. Hun tenkte på samboeren.
Usikker følte hun på normalitet og lurte om det var noe som helst sant i
livene deres. Var hun noensinne normal? De håndgripelige tegn på fornuft
og rasjonalitet kastet hun opp i luften på det minste løfte om
forelskelse og straks hun sniffet sex. De lo sammen. Nå også om Hilde.
Venninnen de begge kjente, fremmed for dem begge, en person de visste
ingenting om.

Når de avsluttet for kvelden ante hun ting hadde endret seg. Neste dag
var begge innlogget, hun stirret på den grønne prikken inntil enavnet og
blikket sklei til den tomme meldingspostkassen. De var der alle sammen,
og enda et enavn haket av henne på mottakslisten.

Rachel stoppet og lente seg forsiktig fram, snakket i lav rolig tone og
krevde med blikk og kropp full oppmerksomhet. Hun visste bedre enn å
ignorere venninnen. Første dag tilbake på jobb var det mange epost å
lese, enkelte meldinger måtte besvares og andre lå i dager. Flere
meldinger var orienteringer fra kolleger og fikk fint å arkivere. Hun
satt i ro og mak, kontorlandskapet var halvfullt og Rachel talte lavt
når normal stemmeleie fungerte. De få rundt i kontorlandskapet ble mer
forstyrret av visking.

“Kan jeg spørre deg om noe privat?”

Sa Rachel.

Hun tenkte en stund hvilket dumt spørsmål, egnet kun til å fylle tid og
varsle om neste ledd i setningen, en invitasjon til blindedeit. Rachel
møtte vennene til Benjamin, flere av dem bekjente fra studiedagene knapt
minner i Outlook. Hun hadde presentert dem for Benjamin, og de snakket
over middag, delte bekymringsmeldinger og ga venninnen en idé. Rachel
skrøt på seg kjennskap til en singel mellomleder i Legemiddelsentralen
viss sladder i synagogen mente var lesbisk. Den klassiske historien der
venner kobler de eneste lesbiske de kjente smøg fram. Rachel sto ved
pulten såre fornøyd, tenkte forslaget var mer revolusjonerende enn hva
var tilfelle.

“Dette er en utmerket idé. Alle synes det.”

Sa Rachel og tenkte på Benjamin.

Hun følte seg beæret. Alle i venneflokken var enig om at de to lesbiske
i kretsen var sporty damer og verdt å matche. Vennene ropte i kor.

“Ja.”

Var alt hun måtte si før Rachel forklarte hvorfor en deit var velkommen.

Hun forsto ikke hvilken interesse og energi Rachel la i planene,
erkjente imidlertid venninnen meget mulig satt med riktig medisin på
tresket i livet.

“Ja, jeg vil gå på deits igjen.”

Sa hun.

Klarere kunne hun ikke melde en interesse. Venninnene smilte fornøyd,
klappet henne på skulderen og lovte å komme tilbake. Rachel løp videre
og ga ingen nærmere forklaring. Hun merket nysgjerrig rykninger i
kroppen. To invitasjoner på deit i løpet av en uke, og ikke et konkret
tidspunkt, avtale, løfte om noe deit faktisk vil finne sted. Hun håpet
på to deits, coladeit med Insane og den mystiske sjefslesben Rachel
trakk fra hatten. Hennes oppdrag i det hele synes å være kjølig
distanse, spille for galleriet. Nonsjalante betraktninger av
invitasjonene falt inn i oppgaven, hun var positivt imøtekommende, men
også, fullt i stand til å avvise hva som helst venninnene kom rekende
med.

Oppdraget føltes vanskelig. Hun forsto en ting etter måneder i
skjebnekamp på Gaysir, nemlig at tilværelsen som singel taklet hun
dårligere enn de fleste. En iboende redsel for å dø alene, aldri å bli
elsket og elske tilbake lammet hjerneflimmeret, tok fra den siste rest
av selvrespekt og beskyttelseslaget med fantasi rundt selvet gikk i
høyspenn.

“Jeg går gjerne på blindedeit.”

Sa hun.

Først da Rachel gikk lurte hun på hvem og hvor, detaljer i planene; hun
ville vite hvordan deiten skulle avtales? Hun trengte et punkt å
fokusere energi mot. Slå fra seg alle håp om å treffe Dark Horse sto
øverst på listen over ting hun måtte gjøre. Hvordan slå opp med noen du
aldri har truffet, et menneske som blokkerte henne på Gaysir?
Hovedtavlen lå foran henne hver dag. Blikket sklei mot magneten på
brettet. Premien hang høyere enn å vinne millioner i lotto. På denne
tiden fikk hun tre rette i lotto. Gevinsten lå innenfor hva var mulig,
og hun brukte femtilappen til å satse på nye rekker. Dark Horse endret
statusmeldinger og det føltes på samme måte som minsteloddet i Flax, en
midlertidig beskjed, signalet for å satse en siste gang. Alle visste
godt oddsene, milliongevinster kommer aldri, og likevel vant noen de
store lottopremiene hver uke.

Hun ønsket deits, sa ja til invitasjoner fra kolleger, venner, familie
og hvem som helst som inviterte. Valgte hun livet ville det krones med
seier. Svaret var derfor ja—alltid ja.

Sent den første kvelden var stillheten øredøvende. Insane sendte ingen
meldinger, Hilde dukket sjelden opp på hovedtavlen, aldri mer enn to
ganger om dagen, og likevel holdt enavnet fra Sørlandet og hånte med
grønne og oransje prikker. Hun fikk aldri meldinger. I sedvanlig stil
søkte hun på Dark Horse, Player, Snushøna, Britt75 og en sjelden gang
Fab hadde en inaktiv profil. Jevnlig dukket de opp, bare for å forsvinne
etter minutter. Postboksen forble tom, hun håpet på Insane, ville gi hva
som helst for å ta opp tonen. Det hadde vært den beste underholdning.
Hun delte gjerne med Hilde, i alle fall på Gaysir.

“Jeg kjeder meg. Kan vi ikke snakke sammen lenger når du satser på
Hilde, så trist, du som er det eneste morsomme her inne.”

Sa hun.

Stillheten skar sjelen, følelsen av savn etter at hun sendte Insane i
armene til Hilde. Hun ville ikke gjort ting annerledes. Kaoset gjorde
det umulig å innby til mer enn flørt på hovedtavlen med de åpenbare
begrensningene en skjerm og avstumpede meldinger mellom anonyme enavn
lovte. Hun erkjente meldinger fra Sandnes var det eneste hun gledet seg
over. Pulsen gikk i hvilemodus hver gang hun søkte på Dark Horse og
skuet profilbilde, og lyste en grønn prikk inntil gikk pulsen raskere.
Deretter følte hun ingenting. Hun rettet fokus en annen retning og følte
ordene lege sår for hvert smil de delte.

Svaret kom i dagboken, og i likhet med Dark Horse fikk hun et stykke
musikk å analysere, sangtekst hun ville lytte til om og om igjen for å
være trygg på at budskapet ble forstått. YouTube chillout i en Stella
Polaris Remikset med stemmeprakten til Tina Dico vekket skarp melankoli.

 | *Lying back to back at 10 to 1
 | You’re awake, like me
 | Trying to conceive that done is done
 | You made a mistake, I see
 | But I’m a little too tired, baby
 | It’s a little too late
 | To bring it up
 | In the weary dark of night
 | Between black and white
 | Is a thousand shades of gray
 | I’m not giving up but I need a little light
 | Oh – save it for the break of day*

Lett beat med tynn stemme akkommodert av en blondine i sorthvitbilde
spilte et halvt dusin ganger før hun logget ut og gikk til Grünerløkka
for å treffe Else. Venninnen leste utvalgte aviser på kafé Edvards, og
drakk en kaffelatte på vindusplass mot Thorvald Meyersgate. Nurket satt
ved siden av moren og dinglet med bena på den høye stolen. Hvis det
passet slik tok hun gjerne turen og snakket med dem på
lørdagsformiddager. Tiåringen var løkkas mest utelivstrente. Høsten beit
og skjerfet i halsen lå tynt på for å skjerme for iskald vind på
bryståpningen i jakkeslaget. Gadd hun å lukke jakken ville det hjulpet
sånn passe. Klærne i det ene laget vitnet om at hun ennå ikke tok den
framskredne vinteren alvorlig. Klokkeslaget i døren overdøvet barnets
glade tilrop når Nurket løp henne i møte på Edvards.

Hun kom sent.

Barnet var ferdig med å spise, drakk te og tegnet. Moren leste de siste
sidene i Dagens Næringsliv og gledet seg til Magasinet. Artikler i
Morgenbladet analyserte psykiatrisk helsevesen i Norge og
næringslivavisen var ikke dårligere. Kakao med krem rakk hun å bestille
før barnet var i gang med et skytespill på lesebrettet hun kjøpte straks
det ble lanserte et alternativ til ipad-tyranniet. Siden sist øvde hun
annenhver dag. Nurket lå bak på poengjakten og ble rastløs. Barnet ville
hjem. Hvis det ikke var for at hun kom ville Else avsluttet avisen og
tatt Nurket tjue meter lenger opp i gaten der de bodde i en liten
leilighet. Moren gjorde i stedet en avtale med barnet, som løp av sted
alene.

“Jeg har gjort noe dumt. Kanskje. Nei. Men jeg angrer.”

Sa hun.

Else lyttet.

Hun fortalte om Insane og flørten de siste ukene, toneleie, gleden og
smerten etter å finne en kontakt for deretter å miste alt. Hun sendte
enavnet fra Sørlandet i retning en annen. Hilde, Elses venninne.

“Har du snakket med Hilde?”

“Nei, ikke på en stund.”

Sa Else.

Hun var først, og fortalte Else hvordan sjansen glapp. Snart ville Hilde
ha ny kjærester. Flørten var for sterk til å stoppe, eskalert av en
ubetenksom kommentar ville den løpe til full forløsning. Else forsto
situasjonen, visste Hilde var singel og vanskelig å motstå. Forsøkene å
matche Hilde med henne lå i bakhode. Hun ble minnet om den andres
eksistens i en vennlig gest hver gang de begge ble single. Måneder
senere og det var nesten glemt. Psykologen sa noe hun valgte å ignorere.

“Hilde er søt.”

Sa Else.

“Jeg lot Insane slippe unna.”

De lo når hun fortalte hvordan Insane vitset med å kreve coladeiten
lovet. Straks hun rakk å komme i buksene på Hilde kunne de ta opp
tråden, fleipet sørlendingen. Hun planla ikke å fortelle den siste
kommentaren, morsom, skeiv humor i full åpenhet.

“Insane mente ingenting med det. Kun humor. Vi har en flott tone.”

Sa hun.

“Der gjorde du tabben. I stedet for å si hun burde konsentrere seg om
Hilde, kunne hun komme i buksene dine først, for deretter og deite
Hilde.”

Sa Else, leken.

God latter løsnet samtalene om det samme—alltid var det om henne og en
håpløs jakt på kjærlighet, eller Elses faglige ambisjoner og
frustrasjoner. Hvilken venninne ville få størst lojalitet; tanken slo
dem? Else kjente begge. Insane flørtet med to av venninnene, og Else
visste alt, kjente galskapen som ridde, fasetter i Hildes skilsmisse,
gjensynet med hovedtavlen, Dark Horse og slitasjen illusjonen hadde på
forholdet til Player. Kompromissløs angrep hun livets målsetning,
kjærligheten. Aldri ga hun opp det viktigste, det opphøyde ideal for
framtiden. Pensjonistliv sammen. Tosomhet. Insane var usannsynlig i
rollen, først og fremst fordi forholdet kom uventet på dem begge og
skremte. Potensiale for å grave mer og lære Sørlandskvinnen å kjenne
ville aldri fungere. Deres flørt fungerte som pauseinnslag i beste fall,
hun skjøv i et ærlig øyeblikk flørten videre og reddet dem. Hun hjalp
til å velge riktig.

Deretter angret hun. Else mente beslutningen fikk være som den ble. Hvis
hun svermet for Dark Horse kunne hun like godt la andre prelle og dytte
dem i en annen retning. Ingen protesterte; ikke Else, ikke Insane.

To dager uten morsomme meldinger, og hun var sjalu på Hilde. Player.
Alle.

Tilpass for henne.

Else stilte seg nøytral mellom henne og Hilde, og frigjorde skruplene.
Hun dro hjem og sendte en melding. Tina Dicos stemme til remiksrytmene i
sangen *Break of Day* surret i bakhode.

“Slår du opp?”

Hun sa det med et smil, ikonene sto i kø og demonstrerte anger for
valget de gjorde. Insane beit på kroken og svarte, de var på talefot.
Flere runder piplet til overflaten fra nød, et dyrisk ønske om og
utforske hverandre. Hun nekter å slippe tak.

Mye snakk om kvinner, hun fikk ustanselig små spørsmål om Hilde, og hun
terpet på fragmenter i bruddet med Player. De siste ukene søkte hun
mindre på ekskjæresten og de fiktive fantasiene om Dark Horse vokste i
betydning. Virkeligheten skjøv hun bort, fantasien holdt hun fast mer
innbitt enn noensinne. Enten det var kulturbegivenheter, allmennquiz
eller konsert var So stedet hun avsluttet kveldene. Lørdag gikk hun i
flokk med en sammensatt gjeng venninner. Omgangsvennene til panneluggen
var der, og senere kom Player i egen person. Situasjonen føltes klam for
flere. Player kom bort og de ga hverandre en klem til ære for vennene i
sirkel rundt. Skuespill for voksne. Panneluggen luktet godt, lyste opp i
lokalet, nyforelsket. Hun skimtet dem bak i det trange lokalet, måtte
bruke all energi på egen vennegruppe. Lenger inn sto blondinen som
deitet Player før henne, før Ingebjørg. Paradokset å stå midt i den
trange halsen, inneklemt mellom forrige og nåværende kjæreste unnslapp
ikke. Hun var relegert nedover gulvet til gruppen ekskjærester—søte
damer, for så vidt, og alle visste livet rullet brutalt videre.

Helgetur til Oslo kom opp i samtale med sørlendingen. Insane hadde
avtalt deit med Hilde og de unngikk tema, utforsket ikke om det fantes
tid for dem. Hemmelig ønsket hun det skulle gå dårlig, kjemien de følte
den første halvtimen og pleide i tekstmeldinger ville koke bort i intet
på null tid. Det var bare hun som sutret. Hun gjorde mye av det den
seneste tiden.

“Heldigvis var jeg kledd til fest på So lørdag.”

Sa hun.

Hvordan forklare bedre frustrasjonen ved å henge igjen ved bardisken på
So og se framtiden forvitre i andres lykke uten å vite hva og hvem ville
redde situasjonen. Det fantes en kur, et menneske for kjærlighet. Hun
var håpløs romantiker. Selvforsvaret fungerte, kledd i skjørt og sort
skjorte, og en sjelden gang sminket hun seg med dekkmaske, strek og
leppestift for kontraster i det solbrune fjeset. Hun trakk blikk,
selvtillit, styrke på tvers i motgang. Player smilte medlidenhet, hun
rettet ryggen og gikk tilbake til bordet trygg på øyene som fulgte hvert
skritt.

Sent hjem, inn i natten, fredag ble til lørdag. Hjemme spanet hun på
hovedtavlen for å slippe tankene fra kveldens prøvelser. Enavnet fra
Sørlandet var ute, og hun visste deiten med Hilde sprengte tidsrammer og
hemmelige forhåpninger. Insane var dømt til å klaffe hos Hilde. Hun
ville høre fra enavnet etter helgen, straks reisen hjemover gikk i boks.
Gaysir tok over søndagen, når begge lente seg tilbake i sofaen å gjorde
opp status. Kvinnen måtte legge bak seg timer på bussen sørover utstyrt
med to motemagasiner og snack til å døyve sulten før de igjen fant et
rom alene.

De slo opp, vennskap lå trygt og fortrolig i tonen. Nettvenner i
singelliv, jakten fortsatte og Insane lå foran i løpet, hodestup
forelsket i Hilde. Deit og sex, forelskelse fra start, og de lovet
hverandre troskap over en god middag. To mennesker forelsket.

Fredag sto hun i på So og spanet, spilte tøff og ignorerte Player. De to
elskede lå i sengen og lo av tanken på at de gikk raskt fram. De var i
en kjærlighetssone. Paret skilte lag neste morgen for å møte senere på
kino med datteren til Hilde. Akutt sykdom avgjorde da barnet fikk feber,
og Hilde avlyste, sendte Insane ned en spiral med alvorstunge tanker
egnet til å ødelegge det beste humør. Oppsamlet inntrykk fra helgen
leste hun samme kveld, vemod, tristesse mellom linjene og hodestup
forelskelse. Hun kunne ingenting beklage. De var et par og Hilde lovte
bedring. Neste helg besøkte Elses venninne Sørlandet.

Hun spilte *Cupido* og hadde tapt.

28
--

Uteliv sneik seg inn lik vannet som renner fra springen når du vrir
håndtaket. Først piplet noen dråper, deretter kommer hele fossekraften
straks beslutningen var tatt. Hun satt hjemme og ventet på et push, et
skurr i hypotenusen som indikerte hun var klar for å romstere i
kjønnslivet. Straks hun satset ville galskapen ta slutt, hun ville føle
den varme luften, pust, smaker der landskapet umulig lot seg planlegge,
knapt lot seg kartlegge. Smaken av overraskelse var lærdom, tanker, liv
og glede. Hun møtte verden optimistisk anlagt.

Uteliv og kjønnsliv hørte sammen i singlelivet. Søk på hovedtavlen ble
monotont. Gaysir var luktfri. Energi samlet opp viljen til å gå ut,
mikse blant folk og gjøre en innsats, hengi seg til dunkelyder i full
fart mot ekstase. Lengselen etter svette trakk kroppen mot døra.
Virkelighetens Gaysir tilhørte barer og dansegulv. De grønne prikkene
var gjestene i baren, køen i garderoben, på vei ut fra do og samling
foran speilet med en leppestift var de oransje prikkene. Klorstank fra
en toalettrenne gjør alt virkelig.

Offisielt var suksessmålet å skåre. Alltid. Ta en kvinne, smak og lukt,
helhjertet åpne for muligheter til mer. Hun søkte i lengsel, mente det
var like viktig å være, flagge tilstedeværelse og kjenne modning i
sjelen. Annonserte sjel fri og frank—her og nå. Hun mobiliserte. Vennene
måtte feste i hopetall, en dag i uken, pliktfølelse og klare instrukser
skulle sikre oppslutning. Flokken hang i baren, de spanet på damer.

Ambisjonene var langt mer ambisiøs enn da hun inviterte tre på Gaysir
til deit. Cola og et bord i trekk holdt ikke. Kontakt via nettet var nok
for deits og bevis var berøring gjør mulig. Uteliv var fylt med kontakt,
dult og skubb. Blant folk satset hun til siste minutt, alt eller
ingenting, investerte for sansene. Vennene krevde substans, de gikk i
samlet flokk, støttespillere til hverandre hang de i baren og heiet fram
smilene. Åpne lepper førte til nikk. Skuldrene sveivet borti kropper på
vei til toalettet, kollisjonskraften i de seksuelle undertonene vibrerte
og skapte resonans fra stram maske til rykk i mellomgulvet.
Fem-på-tre-deiten ventet i baren, dro med hjem og slapp svettekjertlene
fri.

Hun visste suksess krevde mye. Hun var aldri en måljeger, noen som
puttet ballen i nettet hver gang og gjorde det med presisjon. Passerte
hun keeperen ville uten unntak målet for sjekkingen ta initiativet;
smile til henne og si hei. Hun gikk raskt til verk når tryggheten satt
seg og det som gjensto var en avtale om hvor natten sluttet. Regelen
tilsa det var sent før hun visste sikkert hvor ting bar. Hun ventet.
Utfordringen hver gang var å drikke nok til å holde stand, sitte
oppreist og lydhør svare på henvendelser.

Sårene lå åpne, men hun hadde visdom nok til å forstå tiden ville skyve
minnene bort. Hjemme fra byen midt på natten logget hun en gang på
Gaysir for å få med et blunk fra Hundjevel. Profilbilde avslørte en
vakker blond kvinne, yngre på vennelisten til Insane. Hun var smått
beæret.

“Hva søker du her?”

Forespørsel var enkelt nok. Klokken var snart halvtre og hun var i ferd
med å forlate denne verden, forlate bits og sittekrampe for drømmene—de
faktisk fysiske manifestasjoner av hennes fantasi og evne til
innlevelse. Hun tenkte ikke mye over svaret som var enkelt:

“Jeg skal få meg litt søvn.”

Sa hun.

“Er du i nærheten av Frogner? Kanskje du vil gjøre noe annet enn å
sove?”

Besluttsom dame dukket opp fra intet. Hun satt et par minutter og skuet
på formuleringen, spekulerte om det var en invitasjon, tenkte det var en
feiltakelse. Nærgående. Hun stirret på bildet, leste den tilforlatelige
profilen. Hun var trøtt, ikke så lite oppgitt og skulle logge av for
natten. Hovedtavlen framsto mer som en lekeplass der det var lettere å
få sextreff enn gode gammeldagse deits, og Hundjevel bekreftet alle
fordommer. Hun var ingen forfører, snarere en sirene som lokket til lek,
fritt vilt, ute på vift sto hun i baren og gamene kom. Panneluggen sklei
over til å bli et minne hun ikke lenger søkte. Hun spanet på So, og ble
spanet på nettet. Hun sto i baren og hver gang registrerte hun, og lurte
på om ekskjæresten ville vise seg den kvelden. Rakrygget var hun ikke
redd. Tilværelsen i singellivet lovte mye, og hun trivdes for første
gang på lenge i rollen. So-kjelleren tok hun over med den største
selvfølge.

Dunkelydene på dansegulvet overdøvet alt bortsett fra smilene. Hun var
lav, håret langt og lyst, og smilet fylte ansiktet. Hun smilte tilbake.
Smilene gikk alle veier. Humøret dunket, i takt med musikken, i hurtig
og oppstemt tempo. Slik gikk det til når hun lot seg sjekke på So.

Venninnene satt i baren, de kom tre i flokk og kjøpte øl, lo og gapte.
Gjentok gamle historier som fikk dem til å le, minner knyttet et eget
vev og dekket over sårbarheten i situasjonen. De nøyde seg med to
barstoler og vekslet på å sitte og bane vei gjennom lokalet på vei til
doen. Hvis So var tettpakket observerte den sist på toalettet stemningen
ved dansepålen innerst i lokalet, og til de andre på veien tilbake.
Trioen satt bak lengst mulig. Hun likte ytterste post, stående mellom de
to. I bevegelse følte hun seg friere, og smilene var enklere å fordele
til alle som strøyk forbi. Av de tre var hun minst sjenert, tryggest,
godt vant med keitete tilnærminger og fornøyd med hvordan ting endte.
Hun var heldig stilt, ofte suksessfylt når det knep til og hun følte
klart det var i ferd med å skje noe. Ønsket hun deits, smake beruselse
og nippe borti følelsen å være forelsket skapte turer i byens uteliv
alltid en åpning. Spørsmålet var om hun ville ta sjansene, hvorvidt hun
følte seg tøff nok til enda en runde skuffelse. Hvor var potensialet,
enten til en kvikk retrett, eller lange netter i eufori, byggesteinen
for framtid. Optimisme. Glede. Attraksjon—alltid, smaken for kropp var
en del av det hele.

Hun la altfor mye lykke i tosomhet. Krevde for mye fra livet,
kompromissløs til et punkt, og pragmatisk når det passet. Kvelder som
denne eksponerte spriket. Hun smilte seg gjennom natten, takknemlig for
alt.

Friskmeldt, endelig!

Tiden det tok å komme over panneluggen overrasket. Noen måneder etter at
det lyse smilet slakket, mange uker etter de satt på plenen i
Frognerparken og nuppet i gresset, resonnerte fritt rundt samlivskrisen.

“Jeg vil ikke slutte her.”

Egentlig sa de ingenting nytt; de fant enighet om tingenes tilstand, og
var uenige i konklusjonen. Hun ville samtale om problemene, men tankene
hopet seg opp og det var umulig å slippe Player inn i hemmelige rom. Hun
skrev, sendte epost, tekstmeldte og forklarte alt hun følte. Hun brukte
postkassen på Gaysir framfor de på jobb. Player leste bønnen fire dager
etter den ble sendt og ristet oppgitt på hode. Deretter gikk
ekskjæresten på byen og plukket en drømmekvinne, et smil mindre
komplisert, lettvint tilgjengelig kollega fra fotballbanen. Panneluggen
forelsket seg.

Smerten stilnet. Hun tok aldri kontakt, lot avstanden vokse og blikket
hang på hovedtavlen der alle bortsett fra panneluggen viste seg hyppig.
Slik voksne gjør. Fab søkte hun forgjeves, men fant bare Dark Horse,
Insane, Fullklaff og Sexdyret lyse i grønt. Enavn der hun henholdsvis
var blokkert, forvist, avvist og sperret ute. Britt75 puslet i
bakgrunnen, og hun studerte enavnet hun mistenkte var Dark Horse, og
Snushøna hun mente å vite var Dark Horse. Viten hun aldri slapp løs, for
alt føltes utrygt. Spesielt etter søndagsettermiddagen der tilliten til
intuisjonene hun trengte for å overleve kjedsommelighet knakk. Hun og
Snushøna sendte tekstmeldinger til hverandre. Stiv dialog, kontrollert,
slik samtaler mellom vilt fremmede må være. Hun fikk et navn, og
Snushøna korrigerte både hvilken by hun bodde og alder. De kom ikke
lenger fra samme sted, eller fylke—Snushøna og Dark Horse het
forskjellige ting. Mistankene svekket, mer fordi Sexdyret ringte og
snakket med en dyp maskulin stemme, delvis fordi hun må tro folk. Hun
valgte tillit. Linjen til Snushøna var åpen. Hun bestemte seg for å
behandle enavnet som om det var en forlengelse av besettelsen hun kalte
Dogtooth. Høflig og bestemt. De paranoide vrangforestillingene tok henne
til et punkt, til slutt trengte hun fornuft.

“Du er innbitt fornuftig.”

Sa Else.

De fablet om smerten ved å være alene, utrygg, og styrken du finner når
det er nødvendig. Begge skjønt enig om at problemene ville hun vært
foruten hvis ikke for faktum hun var sterkt tynget av det romantiske
ideal. Forbannelsen hvilte på kulturen, klamt klistret på membran av
film og teve gjennom et langt liv.

Hun burde visst bedre, ristet på hode over siste påfunn og klaget til
Else. Sexdyret fjernet mye angst, Insane satte tilstedeværelse på Gaysir
i perspektiv når hun valgte å steppe til side for Hilde. Tid for farvel,
konfronter smerten og lukke dører!?

Fraværet av kamp for å vinne Insane, den morsomste og mest flørtende
profilen hun traff på Gaysir, gjorde kontakt med Dark Horse mindre
skummelt. Hun søkte på de gule sider og fant telefonnummeret, deretter
skrev hun en kort tekstmelding og ba om en samtale. Kort,
forretningsmessig. Suksessmålet var et svar, hun ville vite hva som
gjensto. Fantasier om et møte lå gruset, stemmen ville hjelpe, være nok
til at hun kunne vite hvorvidt alt var galskap.

“Får jeg ringe deg.”

Ivrig glemte hun å signere, forklare, og tenkte med seg selv at det var
et feilgrep for mobiltelefonen sto registrert på arbeidsgiveren og hun
lå gjemt bak et firmanavn.

“Hvem er du?”

Fullstendig unødvendig, hun visste arbeidsgiveren avslørte og hun var
identifisert. Dark Horse sikret seg, tok ingen sjanser og hun svarte med
både navn og enavn og ga en forklaring.

“Glemte å signere og du har blokkert meg.”

Sa hun.

Stillhet.

Bryte forbannelsen, å bringe tvangstankene inn i det virkelige rom, bort
fra Gaysir var den siste desperate handling på grense til et overgrep.
Alle samtaler på Gaysir det siste året var med Dark Horse, lange tråder
fri fantasi som skapte problemer i forholdet til Player. Debattene tok
henne fra himmelsk lykke til dyp frustrasjon, og hun kokte over med
aggresjon og fremmedgjøring. Profilene i rommet måtte tåle bivirkningene
av anstrengt paranoia utviklet i møte med en rødtopp.
Vrangforestillingene gikk så langt at hun ikke lenger visste hvem ståket
hvem. Bak hver uskyldig henvendelse ante hun konturene av Dogtooth.

Hun forsvarte for seg selv den siste akt med ståking. Etter
søndagsettermiddagen der hun veddet sex med en mann på Gaysir og tapte
gjorde neste skritt enkelt. Skam fantes ikke lenger. Insane avgjorde.
Den vakre kvinnen fra Sørlandet ville på coladeit og hun sendte kvinnen
strake veien, rett inn i armene på en annen. De ble et par, takket være
henne. Håpløst. Hvis ikke for romantikkens dans ville problemer på
Gaysir aldri oppstå. Nysgjerrighet, flørt, utlegg av følelser og håp
skapte en søt stank av desperasjon. Gaysir utartet og vridde
perversiteten til galimatias. Hun ville sette strek en gang for alle.
Trådde hun utenfor nettkanalen måtte i det minste dialogen være voksen,
presis, og hun bannet fordi det ble nødvendig å forklare hvem hun var.
Hun skulle signert. Ivrig knotet hun ned spørsmålet, svarte på impuls,
et ørlite signal hun knapt visste hvor oppsto trigget en spore.

Direkte kontakt med kvinnen hun kalte Dogtooth til nære venner trådde
over en grense. Vennene visste ikke hvor galt av sted det gikk. Else
mistenkte, utvilsomt. Knapt ville hun vedkjenne seg sykdommen. Innbitt
grep hun alle sanser foret med observasjoner fra virkeligheten og søkte
konstruktive drømmer. Hva gjorde siste triks når hver pålogging på
hovedtavlen dro blikket til den samme kvinnen. Menneske blokkerte henne.

“Jeg er ute med en venninne og det passer dårlig.”

Sa Dark Horse og kjøpte tid.

Singellivet stjal krefter i rykk og napp. Tilstanden var stabil, Player
lekte og hun fant trøst i det når hun sleit med grums fra fortiden.
Kyberlivet fløt over i virkelighet, og gapet mellom hva hun burde ha
følt og det hun slapp løs ble skeivt. Hun ville konfrontere følelsene og
kjenne de små ripsene fra likegyldighetens snert. Piske skinnet med
stemmeprakten til Dark Horse i ledende solistrolle, tvinge fram svar.
Fram til panneluggens nye kjæreste sluttet å være hemmelig ville hun
aldri erkjenne desperasjonen.

Innsikten sank inn over tid. Tredje mur kalte filmbransjen øvelsen der
skuespillere ser inn i kameraet og sier replikkene til publikum foran
lerretet. Hun brøyt den tredje muren i en desperat handling. Fornuften
tilsa en slutt måtte komme, og planen var å forsere, tvinge Dark Horse
inn i et hjørne.

Se meg! Smerteskriket lot seg ikke lenger stoppe.

“Jeg ønsker en samtale med deg. Kort. Ryddig. Saklig. Gjerne. Men en
samtale. Ikke Gaysir. Beklager at jeg bryter form. Vil det være mulig?”

Sa hun.

Timer ventet hun og tvang svaret fram.

“Nei, jeg har ikke lyst til å møte deg.”

Sa Dark Horse.

Hvem ville møte, hun visste det var utopisk. Skulle de avtale et møte,
finne sted og utveksle høfligheter ville det være suksessmålet hun ble
nektet. Sykt. Hun våget ikke drømme. Stemmen var tilstrekkelig, et snev
av medlidenhet, forakt, nok til at hun kunne slette alle drømmer og
fokusere realistisk. Kutte følelser.

“Jeg ville bare vite hva jeg skulle gjøre. Høre det når jeg ikke vil
høre. Takk.”

Hun bukket og neide seg ut av rommet, lik narren i gamle slottshaller
foran hoffet etter at kongen godlynnet sparte livet for den ubetenksomme
kommentar. Elskverdig og kort, ingen tvil, diffus beskjed. Budskapet
gikk mellom dem, to personer på hver linje, og tvilen kom henne ikke til
gode. Sagaen med Dark Horse var slutt. *Finito!* Brystkassen slukte all
luft i korte gisp, sort ubeskrivelig angst trakk sammen lungene og hun
knyttet neven i smerte. Kontakten var nødvendig, avslutningen, veien ut
av den mørke tunnelen. Gamle vitser unnslapp, det var ingen latter.
Lyset var i den andre tunnelen, virkelighetens møteplasser, barer og
dansegulv der kvinnene strutter i all prakt.

Hun slettet nummeret til Dark Horse og synkroniserte for å slette alle
spor, også de i den bærbare datamaskinen. Dark Horse var ikke lenger et
begrep i hennes verden. Nyeste kontakt på epostlistene ble siste
kontakt, slettet for alles beste.

Hun sleit med å sette fingeren på hva det var med kvinnen på dansegulvet
som trakk blikket, manet fram smilet og vekket nysgjerrighet.

Speilet hun? Hun hastet ikke, snudde seg etter mange, snakket med
vennene og lo høyt for hver slurk i pilsglasset.

“Jeg har sjanse på den lave med det lange lyse håret ved enden.”

Sa hun.

Kunsten å snu seg ubemerket, stirre gjennom fingrene og under armen
klarte Ingebjørg å perfeksjonere på en barstol en sen fredag i
So-kjelleren. Nikket bekreftet hun fikk tillatelse til å forsøke, og hun
koste seg med frihet til å drømme. Hun hastet ikke. Kvelden var ung og
det var usikkert om hun ville legge året bak seg—hvorvidt hun klarte å
gå videre. Måneden før gikk hun hjem alene, fornøyd med å lufte fjær
rundt dansepålen. Blunk fra Hundjevel og invitasjon til å treffes midt i
natten avviste hun bastant. Livet trenger lukter, og hun hadde bestemt
seg for at det er dører best å lukke. Livet var godt. Vennene fylte
månedene, og hun kunne stirre på hovedtavlen avstumpet det sterke
engasjementet. Tilbakeskuende søkte hun av og til ekskjæresten Player,
diverse fremmede enavn vekket nysgjerrighet, galskapen var en studie i
selvet—innadvendt psykoanalyse for spesielt interesserte, hvilket umulig
kan være mange. Hun skrev tanker. Og gikk på byen. Her sto de i baren og
lurte, snuste på mulighetene og lo mye hvis mulig. Livet var best når
smilet satt.

Hun sto i en fri posisjon. Venninnene lente seg mot bardisken og stirret
stadig dypere ned i pilsglasset, og hun tok hyppigere runden forbi
dansepålen, toalettet og tilbake. Midt i lokalet stoppet kvinnen med det
lange lyse håret henne med et tydelig ord.

“Hei.”

Hun så ned og smilte tilbake.

De kjente ikke hverandre og hun enset straks dialekten fra et sted på
Vestlandet.

“Bergenser, jeg er her på turistvisum.”

Smilet inviterte til alt. Hun likte kvinnen foran med et løfte i seg at
de kunne prøve en samtale og stoppe der. Med galskap skal galskap
fordrives, tenkte hun. Samtidig kjente hun ikke for de store
prosjektene. Brent nære solen. En ting sto igjen etter året og fraværet
av fornuft kostet for mye, hun burde taklet singellivet bedre. Hun
gjorde et forsøk og slappet av, lærte å se perspektiver i kortere
tidsløp. Aksen til pensjonistene sklei inn i diffus masse forbeholdt
andre enn henne.

De sto på gulvet og trakk bakover, inntil veggen sto de skjermet fra
strømmen av mennesker som gikk fram og tilbake fra bakrommet til
utgangen for å trekke luft. Smilene sa det meste, de trengte ikke si
mer. Hun fikk et navn, og ga sitt eget. Det lange håret kastet hun rundt
og skimmer fra minst fire ulike nyanser blond lekte i mørke. Samspillet
med diskoteklyset la et lag av skygge over øynene og det var umulig å se
hvorvidt fargen lå mer mot grønn enn blågrå. Flaskeslurpen lå lunken på
bunnen og hun måtte til baren for å kjøpe mer.

“Skal jeg kjøpe en øl til deg?”

“Ja.”

Sa kvinnen.

Hun kastet seg fram ved nærmeste åpning i baren og bestilte to flasker.
Tidsnok fikk hun oppdatere de andre, hun fortet seg tilbake. Kvinnen
foreslo de skulle danse. Hun nikket. Inntil dansegulvet satte hun
flasken på den tynne hyllen og kastet seg rundt til musikken. Der det
var naturlig grep de etter hender og snurret rundt i et ubehjelpelig
forsøk på pardans.

“Beklager, jeg sluttet på danseskolen etter bare tre uker.”

Sa hun.

De lo alltid når hun la ut om de keitete små skritt i ungdommen, for
sannheten var etter noen feilskjær svingte de godt sammen. Hun ledet.
Danseglede lærte cha-cha-cha trinnene i rekordfart, om enn haltende, og
enkel rock tok hun på strak arm. Hendene rykket under armen og klappet
øvre rumpesprekk. Gledestråler velkommen, framskredet språk fra
danseuniverset slår aldri feil. Hun trakk seg bort for å drikke mer.
Alkoholen traff, søvndrukken var neste fase hvis hun fortsatte å drikke.
Hun trakk kvinnen til side.

“Du drikker raskere enn meg.”

Hun kom knapt ned til etiketten, under en tredjedel av ølen lå tilbake i
flasken til kvinnen fra Bergen. Humoristisk sans hørte med, for
blondinen sverget det fantes ingenting bedre enn fyllesjuke. De fikk
forskjellig typer migrene.

“Jeg er dårlig på alkohol.”

Sa hun.

Hun ville fortelle at det var riktig å slutte, for hun var mildt
beruset, musikken gikk i hode og det var viktig at kroppen gikk
framover.

“Du er søt.”

Hun trengte ikke å si mer. Det lange lyse håret falt inn i sprekken
mellom pannene deres når de kysset. Turisten fra Bergen bodde hos en
venninne på Sagene, hun skulle samme vei. Det var best om de dro hjem
til henne. Nattbussene gikk hele natten og stoppet utenfor døren. Det
var raskere enn å finne en taxi, forklarte hun, og de gikk til
bussholdeplassen.

Begge rablet om ting som falt dem inn. Smilene avdekket at ingen skiftet
mening, de sto utenfor på gaten i Oslo og alt var i henhold til alles
planer. Kjølige vinder utløste impulsen å trekke sammen kroppen i
jakkeslaget og hun hørte dårligere. Dialekten irriterte. Hun forsto
sjelden mål med bred fremmed klang, og senere skulle de begynne en
vedvarende krangel om hva som er pent bergensk. Mye informasjon enset
hun knapt da de ventet på bussen, for hun stirret på taxilysene og søkte
åpning til å dytte dem begge ned i et varmt sete i sky. Taxibiler ville
korte reisetiden, men skiltene på taket nektet å lyse.

Skulle hun sverget utvekslet de anekdoter fra korpsleir, tanker om ufoer
og fortalte minner fra virkeligheten, sporadiske dykk inn i tanker om de
første lesbiske erfaringer. De kunne like godt ha utvekslet
kakeoppskrifter. Hun forsto ikke dialekten, bare smilet, en rekke med
hvite tenner lyste og fjernet redsler. Fantes det tvil kunne hun sagt
stopp-en-halv på Sagene bussholdeplass der de gikk av samtidig, men det
føltes naturlig å gå videre. De kysset, mildvær til tross for sur kuling
fra øst, enden var forutsigbar. Hun tenkte fint lite på ting, visste
godt det dårlige utgangspunktet for opplegg for natten som hun beviselig
feilet hver gang. Hun var ikke bygd for enkle løsninger, og likevel var
kvelden plankekjøring. Alle kjente rollene sine, hun våknet av den
friske vinden og kunne snakket til neste morgen. Latteren hang løs.
Hodene var kvikke i oppfatningen og ertesugne. Flørt på høygir.

“Jeg er kunstner.”

Hun stoppet aldri, nølte ikke etter det overraskende svaret.

Hun jobbet i et langt mer tradisjonelt firma, innenfor regulerte rammer
i en etablert bedriftskultur. Frie yrkesgrupper tilhørte sjeldenhetene
der hun hevet månedslønn. Folkene hun omgikk levde kjedelige liv, bar
uniform, eller fylte dagene med møter og epost. Virkeligheten var for
kjedelig. Skrev hun en roman om livene deres ville alt være løgner, slik
denne romanen bygger på fri fantasi. Ingen gjenlevende, eller døde
personer som mener å gjenkjenne handlinger vil ha rett, for fortellingen
du leser er fiksjon fra ende til annen.

“Jeg håper ikke natten vi skal ha sammen dukker opp i et kunstprosjekt?”

“Nja. Eg veit ikkje. Kan hende det?”

Latter ville bringe dem langt. Klingene lattertoner vitnet om en sans
for humor, og hun tenkte dette ville bli bra.
